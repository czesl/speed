﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Shark.Web.HttpProtocol.Creator
{
	class Control : IControl
	{
		#region protected properties

		/// <summary>
		/// Property http dotazu.
		/// Pokud má objekt rodiče, bere rodičovské hodnoty
		/// </summary>
		protected virtual HttpRequest Request
		{
			get { return RootPage.Request; }
			set { throw new NotImplementedException(); }
		}

		protected virtual HttpResponse Response
		{
			get { return RootPage.Response; }
			set { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Objekt stránky (rodič všech controlů)
		/// </summary>
		protected virtual Page RootPage { get; set; }

		#endregion

		#region private properties

		/// <summary>
		/// Objekt, kam se ukládá text odpovědi
		/// </summary>
		private TextWriter Output { get { return Response.Output; } }

		/// <summary>
		/// Obsahuje prefix používaný pro vytváření id html tagů
		/// </summary>
		private string Prefix { get; set; }

		/// <summary>
		/// Iterátor pro počet vytvořených potomků
		/// Přidává se k prefixu potomka
		/// </summary>
		private int _prefixNum = 1;

		/// <summary>
		/// Iterátor pro počet vytvořených potomků
		/// Přidává se k prefixu potomka
		/// Automaticky inkrementuje počet prefixů
		/// </summary>
		private int PrefixNum
		{
			get { return _prefixNum++; }
		}

		/// <summary>
		/// Rodičovský kontroler
		/// </summary>
		private Control ParentControl { get; set; }

		#endregion

		#region protected methods

		/// <summary>
		/// Smaže veškeré dočasné proměnné
		/// </summary>
		protected virtual void ClearTmpVariables()
		{
			RootPage.ClearTmpVariables();
		}

		/// <summary>
		/// Vrátí odsazení pro vkládání textu.
		/// Zohledňuje i odsazení rodičovského controlu
		/// </summary>
		/// <returns>AKtuální odsazení v počtech tabulátorů</returns>
		protected virtual int GetIndent()
		{
			return RootPage.GetIndent();
		}

		#endregion

		#region private methods

		/// <summary>
		/// Inkrementuje odsazení
		/// </summary>
		protected virtual void IncIndent()
		{
			RootPage.IncIndent();
		}

		/// <summary>
		/// Dekrementuje odsazení
		/// </summary>
		protected virtual void DecIndent()
		{
			RootPage.DecIndent();
		}

		/// <summary>
		/// Zapíše odsazený text a zvětší odsazení
		/// </summary>
		/// <param name="txt">Text k zapsání</param>
		private void WriteBeginTag(string txt)
		{
			WriteIndented(txt);
			IncIndent();
		}

		/// <summary>
		/// Změnší odsazení a zapíše odsazený tag
		/// </summary>
		/// <param name="txt">Text k zapsání</param>
		private void WriteEndTag(string txt)
		{
			DecIndent();
			WriteIndented(txt);
		}

		/// <summary>
		/// Zapíše odsazený text
		/// </summary>
		/// <param name="txt">Text k zapsání</param>
		private void WriteIndented(string txt)
		{
			write(GetStringIndent());
			writeNL(txt);
		}

		/// <summary>
		/// Vrátí odsazení v tabulátorech
		/// </summary>
		/// <returns>Odsazení v tabulátorech</returns>
		private string GetStringIndent()
		{
			string indent = "";
			return indent.PadLeft(GetIndent(), '\t');
		}

		/// <summary>
		/// Vrátí řetězec s nastavením atributu na danou hodnotu
		/// Pokud hodnota není nastavena, vrátí prázdný řetězec
		/// </summary>
		/// <param name="attribute">Název atributu</param>
		/// <param name="value">Hodnota atributu</param>
		/// <returns>Řetězec s nastavením atributu</returns>
		private string AttributeValue(string attribute, string value = null)
		{
			if (value == null)
			{
				return "";
			}
			//return attribute + "=\"" + encode(value) + "\" ";
			return attribute + "=\"" + value + "\" ";
		}

		#endregion

		#region Implementation of IHTMLWriter

		/// <summary>
		/// cesta k obrazku pro web browser. Hodnotou bude napr "/images/"
		/// </summary>
		public virtual string imgPath { get { return RootPage.imgPath; } set { RootPage.imgPath = value; } }

		private string _oldFormName;

		/// <summary>
		/// pokud je aktualne otevreny tag "<form>" (klidne nekde vysoko v rodici), pak je zde nazev otevreneho formulare. Jinak vraci null
		/// </summary>
		public virtual string formName
		{
			get
			{
				return RootPage.formName;
			}
			protected set
			{
				_oldFormName = RootPage.formName;
				RootPage.formName = value;
			}
		}



		/// <summary>
		/// Zakóduje text pro použití v html
		/// </summary>
		/// <param name="txt">Vstupní text</param>
		/// <returns>Zakódovaný text</returns>
		public string encode(string txt)
		{
			if (txt == null) return (null);

			txt = HttpUtility.HtmlEncode(txt);
			txt = txt.Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");
			return (txt);
		}

		/// <summary>
		/// Dekóduje text z html
		/// </summary>
		/// <param name="txt">Zakódovaný text</param>
		/// <returns>Čístý text</returns>
		public string decode(string txt)
		{
			if (txt == null) return (null);

			txt = txt.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&quot;", "\"");
			txt = HttpUtility.HtmlDecode(txt);
			return (txt);
		}

		/// <summary>
		/// vytvori pro browser plnou cestu ke grafickymu souboru
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public string createImgPath(string filename)
		{
			return (imgPath ?? "") + filename;
		}

		/// <summary>
		/// Zapíše text do odpovědi
		/// </summary>
		/// <param name="txt">Text k zapsání</param>
		public void write(string txt)
		{
			Output.Write(txt);
		}

		/// <summary>
		/// Zapíše text do odpovědi.
		/// Za text vloží konec řádku.
		/// </summary>
		/// <param name="txt">Text k zapsání</param>
		public void writeNL(string txt = null)
		{
			Output.WriteLine(txt);
		}

		public void doctype(string def)
		{
			writeNL("<!DOCTYPE " + def + ">");
		}

		public void html(string lang = null, string version = null, string xmlns = null)
		{

			WriteBeginTag("<html "
				+ AttributeValue("lang", lang)
				+ AttributeValue("version", version)
				+ AttributeValue("xmlns", xmlns)
				+ ">");
		}

		public void htmlEnd()
		{
			WriteEndTag("</html>");
		}

		public void head()
		{
			WriteBeginTag("<head>");
		}

		public void headEnd()
		{
			WriteEndTag("</head>");
		}

		public void body(string style = null, string clas = null)
		{
			WriteBeginTag("<body "
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas) + ">");
		}

		public void bodyEnd()
		{
			WriteEndTag("</body>");
		}

		public void form(string name, string action, string method = null, string target = null)
		{
			formName = name;
			WriteBeginTag("<form "
				+ AttributeValue("name", name)
				+ AttributeValue("action", action)
				+ AttributeValue("method", method)
				+ AttributeValue("target", target) + ">");
		}

		public void formEnd()
		{
			formName = _oldFormName;
			WriteEndTag("</form>");
		}

		public void div(string id = null, string align = null, string style = null, string clas = null)
		{
			WriteBeginTag("<div "
				+ ((id != null) ? AttributeValue("id", fullName(id)) : "")
				+ ((id != null) ? AttributeValue("name", fullName(id)) : "")
				+ AttributeValue("align", align)	// nevalidní atribut
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ ">");
		}

		public void divEnd()
		{
			WriteEndTag("</div>");
		}

		public void title(string title)
		{
			WriteIndented("<title>"
				+ encode(title)
				+ "</title>");
		}

		public void input(string id, string type, string value = null, bool isChecked = false, string fileAccept = null, string alt = null, string align = null, string maxlength = null, string size = null, string style = null, string clas = null, bool isDisabled = false)
		{
			WriteIndented("<input "
				+ AttributeValue("id", fullName(id))
				+ AttributeValue("name", fullName(id))
				+ AttributeValue("type", type)
				+ AttributeValue("value", value)
				+ ((isChecked) ? AttributeValue("checked", "checked") : "")
				+ AttributeValue("accept", fileAccept)
				+ AttributeValue("alt", alt)
				+ AttributeValue("align", align)	// nevalidní atribut
				+ AttributeValue("maxlength", maxlength)
				+ AttributeValue("size", size)
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ ((isDisabled) ? AttributeValue("disabled", "disabled") : "")
				+ "/>");
		}

		public void textArea(string id, string value = null, string cols = null, string rows = null, string style = null, string clas = null, bool isDisabled = false)
		{
			WriteIndented("<textarea "
				+ AttributeValue("id", fullName(id))
				+ AttributeValue("name", fullName(id))
				+ AttributeValue("cols", cols)
				+ AttributeValue("rows", rows)
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ ((isDisabled) ? AttributeValue("disabled", "disabled") : "")
				+ ">"
				+ ((value != null) ? encode(value) : "")
				+ "</textarea>");
		}

		public void sel(string id, bool isMultiple = false, string size = null, string style = null, string clas = null, bool isDisabled = false)
		{
			WriteBeginTag("<select "
				+ AttributeValue("id", fullName(id))
				+ AttributeValue("name", fullName(id))
				+ ((isMultiple) ? AttributeValue("multiple", "multiple") : "")
				+ AttributeValue("size", size)
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ ((isDisabled) ? AttributeValue("disabled", "disabled") : "")
				+ ">");
		}

		public void selEnd()
		{
			WriteEndTag("</select>");
		}

		public void option(string txt, string value = null, string label = null, bool isSelected = false, bool isDisabled = false)
		{
			WriteIndented("<option "
					+ AttributeValue("value", value)
					+ AttributeValue("label", label)
					+ ((isSelected) ? AttributeValue("selected", "selected") : "")
					+ ((isDisabled) ? AttributeValue("disabled", "disabled") : "")
					+ ">"
					+ ((txt != null) ? encode(txt) : "")
					+ "</option>");
		}

		public void table(string align = null, string background = null, string bgColor = null, string border = null, string cellPadding = null, string cellSpacing = null, string height = null, string width = null, string style = null, string clas = null)
		{
			WriteBeginTag("<table "
				+ AttributeValue("align", align)
				+ AttributeValue("background", background)	// nevalidní atribut
				+ AttributeValue("bgcolor", bgColor)	// nevalidní atribut
				+ AttributeValue("border", border)
				+ AttributeValue("cellpadding", cellPadding)
				+ AttributeValue("cellspacing", cellSpacing)
				+ AttributeValue("height", height)	// nevalidní atribut
				+ AttributeValue("width", width)
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ ">");
		}

		public void tableEnd()
		{
			WriteEndTag("</table>");
		}

		public void tr(string align = null, string vAlign = null, string bgColor = null, string style = null, string clas = null, string onMouseOver = null, string onMouseOut = null, string onClick = null)
		{
			WriteBeginTag("<tr "
					+ AttributeValue("align", align)
					+ AttributeValue("valign", vAlign)
					+ AttributeValue("bgcolor", bgColor)	// nevalidní atribut
					+ AttributeValue("style", style)
					+ AttributeValue("class", clas)
					+ AttributeValue("onmouseover", onMouseOver)
					+ AttributeValue("onmouseout", onMouseOut)
					+ AttributeValue("onclick", onClick)
					+ ">");
		}

		public void trEnd()
		{
			WriteEndTag("</tr>");
		}

		public void th(string colSpan = null, string rowSpan = null, string align = null, string vAlign = null, string background = null, string bgColor = null, string height = null, string width = null, string style = null, string clas = null, string title = null)
		{
			WriteBeginTag("<th "
					+ AttributeValue("colspan", colSpan)
					+ AttributeValue("rowspan", rowSpan)
					+ AttributeValue("align", align)
					+ AttributeValue("valign", vAlign)
					+ AttributeValue("background", background)	// nevalidní atribut
					+ AttributeValue("bgcolor", bgColor)
					+ AttributeValue("height", height)
					+ AttributeValue("width", width)
					+ AttributeValue("style", style)
					+ AttributeValue("class", clas)
					+ AttributeValue("title", title)
					+ ">");
		}

		public void thEnd()
		{
			WriteEndTag("</th>");
		}

		public void td(string colSpan = null, string rowSpan = null, string align = null, string vAlign = null, string background = null, string bgColor = null, string height = null, string width = null, string style = null, string clas = null, string title = null)
		{
			WriteBeginTag("<td "
					+ AttributeValue("colspan", colSpan)
					+ AttributeValue("rowspan", rowSpan)
					+ AttributeValue("align", align)
					+ AttributeValue("valign", vAlign)
					+ AttributeValue("background", background)	// nevalidní atribut
					+ AttributeValue("bgcolor", bgColor)	// nevalidní atribut
					+ AttributeValue("height", height)
					+ AttributeValue("width", width)
					+ AttributeValue("style", style)
					+ AttributeValue("class", clas)
					+ AttributeValue("title", title)
					+ ">");
		}

		public void tdEnd()
		{
			WriteEndTag("</td>");
		}

		public void a(string href, string id = null, string target = null, string title = null, string type = null, string style = null, string clas = null, string onClick = null)
		{
			WriteIndented("<a "
					+ AttributeValue("href", href)
					+ ((id != null) ? AttributeValue("id", fullName(id)) : "")
					+ ((id != null) ? AttributeValue("name", fullName(id)) : "")
					+ AttributeValue("target", target)
					+ AttributeValue("type", type)	// nevalidní atribut
					+ AttributeValue("style", style)
					+ AttributeValue("class", clas)
					+ AttributeValue("onclick", onClick)
					+ ">"
					+ ((title != null) ? encode(title) : "")
					+ "</a>");
		}

		public void img(string src, string id = null, string alt = null, string align = null, string border = null, string style = null, string clas = null)
		{
			WriteIndented("<img "
				+ AttributeValue("src", src)
				+ ((id != null) ? AttributeValue("id", fullName(id)) : "")
				+ ((id != null) ? AttributeValue("name", fullName(id)) : "")
				+ AttributeValue("alt", alt)
				+ AttributeValue("align", align)	// nevalidní atribut
				+ AttributeValue("border", border)
				+ AttributeValue("style", style)
				+ AttributeValue("class", clas)
				+ "/>");
		}

		public void script(string code, string type = "text/javascript", string src = null)
		{
			//type = (type ?? "text/javascript");
			WriteIndented("<script "
				+ AttributeValue("type", type)
				+ AttributeValue("lang", type)	// nevalidní atribut, stejný jako type
				+ AttributeValue("src", src)
				+ ">"
				+ code
				+ "</script>");
		}

		public void script(string type, string src = null)
		{
			WriteBeginTag("<script "
				+ AttributeValue("type", type)
				+ AttributeValue("lang", type)	// nevalidní atribut, stejný jako type
				+ AttributeValue("src", src)
				+ ">");
		}

		public void scriptEnd()
		{
			WriteEndTag("</script>");
		}

		public void br()
		{
			writeNL("<br/>");
		}

		public void bold()
		{
			write("<b>");
		}

		public void boldEnd()
		{
			write("</b>");
		}

		public void italic()
		{
			write("<i>");
		}

		public void italicEnd()
		{
			write("</i>");
		}

		public void underline()
		{
			write("<u>");
		}

		public void underlineEnd()
		{
			write("</u>");
		}

		public void meta(string httpEquiv = null, string content = null, string name = null)
		{
			WriteIndented("<meta "
			              + AttributeValue("http-equiv", httpEquiv)
						  + AttributeValue("name", name)
			              + AttributeValue("content", content)
						  +">");

		}

		public void link(string rel, string type, string href)
		{
			WriteIndented("<link "
						  + AttributeValue("rel", rel)
						  + AttributeValue("type", type)
						  + AttributeValue("href", href)
						  + ">");
		}

		public void span(string clas = null, string onClick = null)
		{
			WriteBeginTag("<span "
						  + AttributeValue("class", clas)
						  + AttributeValue("onclick", onClick)
						  + ">");
		}

		public void spanEnd()
		{
			WriteEndTag("</span>");
		}
		#endregion

		#region Implementation of IControl

		/// <summary>
		/// Vrátí proměnnou dotazu podle následujicích priorit:
		/// 1. Dočasná proměnná
		/// 2. Rodičova dočasná proměnná
		/// 2. Proměnná ve formuláři (POST)
		/// 3. Proměnná v http dotazu (GET)
		/// </summary>
		/// <param name="varName">Název proměnné</param>
		/// <returns></returns>
		public virtual string this[string varName]
		{
			get
			{
				return
					RootPage[fullName(varName)] ??
					ParentControl[varName];
			}
		}

		/// <summary>
		/// Nastaví dočasnou proměnnou
		/// </summary>
		/// <param name="varName">Název dočasné proměnné</param>
		/// <param name="val">Hodnota dočasné proměnné</param>
		public virtual void defineTmpParam(string varName, string val = null)
		{
			RootPage.defineTmpParam(fullName(varName), val);
		}

		/// <summary>
		/// Smaže dočasnou proměnnou
		/// </summary>
		/// <param name="varName">Název dočasné proměnné</param>
		public virtual void deleteTmpParam(string varName)
		{
			RootPage.deleteTmpParam(fullName(varName));
		}

		/// <summary>
		/// Vrátí seznam souborů, které klient nahrál
		/// </summary>
		/// <returns></returns>
		public IList<IPostedFile> downloadFiles()
		{
			List<IPostedFile> files = new List<IPostedFile>();

			foreach (HttpPostedFile httpFile in Request.Files)
			{
				byte[] buffer = new byte[httpFile.ContentLength];
				httpFile.InputStream.Read(buffer, 0, httpFile.ContentLength);
				PostedFile file = new PostedFile(httpFile.FileName, httpFile.ContentType, buffer, Path.GetFileName(httpFile.FileName));
				files.Add(file);
			}
			return files;
		}

		/// <summary>
		/// Vrátí název včetně prefixu
		/// </summary>
		/// <param name="varName">názevhtml tagu</param>
		/// <returns>Id html tagu obohacené o prefix</returns>
		public string fullName(string varName)
		{
			return Prefix + varName;
		}

		/// <summary>
		/// Vytvoří nový prefix pro potomka a vrátí jej
		/// </summary>
		/// <returns>Unikátní prefix</returns>
		public string createPrefix()
		{
			return Prefix + "i" + PrefixNum.ToString();
		}

		/// <summary>
		/// Vytvoří potomka třídy tak, že mu nastaví jako prefix svůj + nově vytvořený
		/// </summary>
		/// <returns>Nová page s nastaveným prefixem</returns>
		public IControl createChild()
		{
			return new Control { Prefix = createPrefix(), ParentControl = this, RootPage = RootPage };
		}

		public string getAbsoluteUri()
		{
			return Request.Url.AbsoluteUri;
		}

		#endregion
	}
}
