namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Interface describe item in menu
	/// </summary>
	internal interface IMenuItem
	{
		/// <summary>
		/// Is menu item displayable?
		/// </summary>
		/// <returns></returns>
		bool IsDisplayable();

		/// <summary>
		/// Generate html code of menu item
		/// </summary>
		void generate();
	}
}