﻿using System;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	internal class RowGenerate : IRowGenerate
	{
		/// <summary>
		/// Odd row class
		/// </summary>
		private const string CC_Oddrow = "rowodd";

		/// <summary>
		/// Even row class
		/// </summary>
		private const string CC_Evenrow = "roweven";

		/// <summary>
		/// Active row class
		/// </summary>
		private const string CC_Activerow = "registercellactive";

		/// <summary>
		/// Ordinary row class
		/// </summary>
		private const string CC_NonActiveRow = "registercell";

		#region private fields

		/// <summary>
		/// Control for creating html output
		/// </summary>
		private readonly IControl _control;

		/// <summary>
		/// Row id
		/// </summary>
		private readonly string _rowId;

		/// <summary>
		/// Odd row flag
		/// </summary>
		private readonly bool _isOdd;

		/// <summary>
		/// Main form name
		/// </summary>
		private string FormName { get { return _control.formName; } }

		/// <summary>
		/// Hidden input for setting currently selected row
		/// </summary>
		private readonly string _currRowInput;

		/// <summary>
		/// Active row flag
		/// </summary>
		private readonly bool _isActive = false;

		/// <summary>
		/// Clickable row flag
		/// </summary>
		private readonly bool _generateOnCLick = false;

		#endregion

		#region private methods

		/// <summary>
		/// Write new table line (tr)
		/// Sets all neccessary html attributes
		/// </summary>
		private void WriteNewLine()
		{
			string clas;

			// Non clickable row
			if (!_generateOnCLick)
			{
				clas = CC_NonActiveRow;
				clas += " " + ((_isOdd) ? CC_Oddrow : CC_Evenrow);
				_control.tr(
					clas: clas,
					onMouseOver: "this.className = '" + CC_Activerow + "';",
					onMouseOut: "this.className = '" + clas + "';"
					);
			}
			// clickable row
			else
			{
				// row is selected
				if (_isActive)
				{
					clas = CC_Activerow;
					_control.tr(
						clas: clas,
						onClick: "document." + FormName + "." + _currRowInput + ".value='" + _rowId + "'; document." + FormName + ".submit();\"",
						style: "cursor:hand;"
						);
				}
				// row isn't selected
				else
				{
					clas = CC_NonActiveRow;
					clas += " " + ((_isOdd) ? CC_Oddrow : CC_Evenrow);

					_control.tr(
						clas: clas,
						onMouseOver: "this.className = '" + CC_Activerow + "';",
						onMouseOut: "this.className = '" + clas + "';",
						onClick:
							"document." + FormName + "." + _currRowInput + ".value='" + _rowId + "'; document." + FormName + ".submit();\"",
						style: "cursor:hand;"
						);
				}
			}

		}
		#endregion

		#region constructors

		/// <summary>
		/// Constructor sets number of columns and Control for generating html
		/// </summary>
		/// <param name="rowId">Identification of row</param>
		/// <param name="control">Control which generates html</param>
		/// <param name="nCols">Number of columns</param>
		/// <param name="isOdd">Row is odd (or even if false)</param>
		internal RowGenerate(string rowId, IControl control, int nCols, bool isOdd)
		{
			_rowId = rowId;
			_control = control;
			cols = nCols;
			_isOdd = isOdd;
			_generateOnCLick = false;
		}

		/// <summary>
		/// Constructor sets number of columns and Control for generating html
		/// </summary>
		/// <param name="rowId">Identification of row</param>
		/// <param name="control">Control which generates html</param>
		/// <param name="nCols">Number of columns</param>
		/// <param name="isOdd">Row is odd (or even if false)</param>
		/// <param name="currRowInput">Id of input where to set selected row</param>
		/// <param name="isActive">Is this row active?</param>
		internal RowGenerate(string rowId, IControl control, int nCols, bool isOdd, string currRowInput, bool isActive = false)
		{
			_rowId = rowId;
			_control = control;
			cols = nCols;
			_isOdd = isOdd;
			_currRowInput = currRowInput;
			_isActive = isActive;
			_generateOnCLick = true;
		}

		#endregion

		#region Implementation of IRowGenerate
		/// <summary>
		/// Number of columns
		/// </summary>
		public int cols { get; private set; }

		/// <summary>
		/// Number of currently filled columns
		/// </summary>
		public int filledCols { get; private set; }

		public void addField(string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left, bool isMultiLines = false)
		{
			if (filledCols >= cols) throw new InvalidOperationException("Row is full");

			// handle alignment
			string align;
			string width;
			AlignmentConvertor.ToHtmlHorizontalString(alignment: horzAlign, align: out align, width: out width);

			if (isMultiLines) label = label.Replace(Environment.NewLine, "<br/>");

			#region generate html

			// first cell -> create new row
			if (filledCols == 0) WriteNewLine();

			_control.td(colSpan:nCols.ToString(), align: align, width: width, title: desc);
			_control.write(label);
			_control.tdEnd();

			#endregion
			filledCols++;
		}

		/// <summary>
		/// Finish row and reset filledCols 
		/// Prepare for adding new line
		/// </summary>
		public void startNewLine()
		{
			_control.trEnd();
			filledCols = 0;
		}

		/// <summary>
		/// Finish row
		/// </summary>
		public void finishLine()
		{
			_control.trEnd();
		}

		#endregion
	}
}