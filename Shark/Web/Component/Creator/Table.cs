﻿using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	internal class Table : Component, ITable
	{
		#region private constants

		/// <summary>
		/// id of hidden input for setting currently selected row
		/// </summary>
		private const string CI_CurrRowInput = "currRowId";

		#endregion

		#region private fields

		/// <summary>
		/// Defnition of table columns
		/// </summary>
		private readonly ColumnDefinition[] _columns;

		/// <summary>
		/// This method is called when table rows should be generated
		/// </summary>
		private readonly DTableAction _generate;

		/// <summary>
		/// This method is called when user clicked on some of tables rows
		/// </summary>
		private readonly DTableAction _actionOnRowClick;

		/// <summary>
		/// Is true where last generated row was odd
		/// </summary>
		private bool _oddLineGenerated = false;

		private int? _nCols;

		#endregion

		#region private properties

		private int NCols
		{
			get { return (int) (_nCols ?? (_nCols = GetColsNumber())); }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Create table and sets all necessary fields
		/// </summary>
		/// <param name="creator">Creator for generating subComponents</param>
		/// <param name="control">Control for generating html code</param>
		/// <param name="columns">Definition of columns</param>
		/// <param name="generate">Generate method</param>
		/// <param name="actionOnRowClick">Action on row click method</param>
		internal Table(ICreator creator, IControl control, ColumnDefinition[] columns, DTableAction generate = null, DTableAction actionOnRowClick = null)
			: base(creator, control)
		{
			_columns = columns;
			_generate = generate;
			_actionOnRowClick = actionOnRowClick;

			if (_actionOnRowClick != null)
			{
				onLoadWebData += OnOnLoadWebData;
			}
		}


		#endregion

		#region Implementation of IParent

		#endregion

		#region Implementation of IComponent

		/// <summary>
		/// Generate html code of this table
		/// </summary>
		public override void generate()
		{
			_control.input(id: CI_CurrRowInput, type: "hidden");
			_control.table();
			_control.tr();
			foreach (ColumnDefinition column in _columns)
			{
				string width;
				string align;
				AlignmentConvertor.ToHtmlHorizontalString(alignment: column.horzAlign, align: out align, width: out width);
				_control.th(colSpan: column.nCols.ToString(), width: width, align: align, title: column.desc);
				_control.write(column.label);
				_control.thEnd();
			}
			_control.trEnd();

			// generate table content
			_generate(this);

			_control.tableEnd();
		}

		#endregion

		#region Implementation of ITable

		/// <summary>
		/// Currently selected row
		/// </summary>
		public string currRowId { get; private set; }

		/// <summary>
		/// Create Row generator
		/// Methods handle neccesarity of generating onClick events
		/// Mehods also decides if generated row is odd or even
		/// Method should be called as reaction on generate event
		/// </summary>
		/// <param name="rowId">Row id</param>
		/// <returns>Row Generator</returns>
		public IRowGenerate fillLine(string rowId)
		{
			_oddLineGenerated ^= true;	// xor odd line operator. If last generated lina was odd, now it's even (not odd)

			if (_actionOnRowClick != null)
			{
				return new RowGenerate(rowId: rowId, control: _control, nCols: NCols, isOdd: _oddLineGenerated, currRowInput: _control.fullName(CI_CurrRowInput), isActive: (rowId == currRowId));
			}
			else
			{
				return new RowGenerate(rowId: rowId, control: _control, nCols: NCols, isOdd: _oddLineGenerated);
			}
		}

		#endregion

		#region private methods

		/// <summary>
		/// Initiate current row id and raise ActionOnClick
		/// </summary>
		/// <param name="cmp"></param>
		private void OnOnLoadWebData(IComponent cmp)
		{
			currRowId = _control[CI_CurrRowInput];
			if (_actionOnRowClick != null) _actionOnRowClick(this);
		}

		/// <summary>
		/// Returns number of columns
		/// Number is comuted from columns definition
		/// </summary>
		/// <returns>Number of columns</returns>
		private int GetColsNumber()
		{
			int number = 0;
			foreach (ColumnDefinition column in _columns)
			{
				number += column.nCols;
			}

			return number;
		}

		#endregion

	}
}