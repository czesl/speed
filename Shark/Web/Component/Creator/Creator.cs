﻿using Shark.Web.Component;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Class representing creator for components
	/// </summary>
	internal class Creator : ICreator
	{
		#region private fields

		/// <summary>
		/// Gets or sets Control of this creator
		/// </summary>
		private readonly IControl _control; 
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor, creates Control with its own Control
		/// </summary>
		/// <param name="control"></param>
		internal Creator(IControl control)
		{
			_control = control;
		}
		#endregion


		#region Implementation of ISelectValueCreator

		/// <summary>
		/// 
		/// </summary>
		/// <param name="val"></param>
		/// <param name="label"></param>
		/// <param name="desc"></param>
		/// <returns></returns>
		public ISelectValue selectValue(long val, string label, string desc = null)
		{
			return null;
		}
		#endregion


		#region Implementation of ICreator

		/// <summary>
		/// Creates component for rendering &nbsp HTML
		/// </summary>
		/// <returns>Nbsp component</returns>
		public IComponent nbsp()
		{
			IControl control = _control.createChild();
			ICreator creator = new Creator(control);
			return new Nbsp(creator, control);
		}

		/// <summary>
		/// Returns new page component
		/// </summary>
		/// <param name="comp">Inner component</param>
		/// <param name="title">Page title</param>
		/// <returns>New page component</returns>
		public IPage page(IComponent comp, string title = null)
		{
			return new Page(this, _control, comp, title);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="htmlCode"></param>
		/// <returns></returns>
		public IComponent htmlComp(string htmlCode)
		{
			IControl control = _control.createChild();
			ICreator creator = new Creator(control);
			return new HtmlComp(creator, control, htmlCode);
		}

		/// <summary>
		/// Creates component for rendering panel HTML
		/// </summary>
		/// <param name="comp">Component which will be rendered in panel</param>
		/// <param name="horzAlign">Horizontal alignment</param>
		/// <param name="vertAlign">Vertical alignment</param>
		/// <returns>Panel component</returns>
		public IComponent panel(IComponent comp, EControlAlignment horzAlign = EControlAlignment.CA_Both, EControlAlignment vertAlign = EControlAlignment.CA_Both)
		{
			IControl control = _control.createChild();
			ICreator creator = new Creator(control);
			return new Panel(creator:  creator, control: control, comp: comp, horzAlign: horzAlign, vertAlign: vertAlign);
		}

		/// <summary>
		/// Creates component for rendering layout HTML with specified dimensions
		/// </summary>
		/// <param name="xSize">Number of cells in row</param>
		/// <param name="ySize">Number of cells in column</param>
		/// <returns>Creator for creating layout component</returns>
		public ILayoutCreator layOut(int xSize = 1, int ySize = 1)
		{
			IControl control = _control.createChild();
			ICreator creator = new Creator(control);
			return new LayoutCreator(creator: creator, control: control, xSize: xSize, ySize: ySize);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IMenuCreator mainMenu()
		{
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IMenuComponentCreator menuFrmComponent()
		{
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cols"></param>
		/// <returns></returns>
		public IFormCreator form(int cols)
		{
			return null;
		}

		/// <summary>
		/// Returns new table creator
		/// </summary>
		/// <returns></returns>
		public ITableCreator table()
		{
			IControl control = _control.createChild();
			ICreator creator = new Creator(control);
			return new TableCreator(creator: creator, control: control);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IListDetailCreator listDetail()
		{
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IList3DetailCreator list3Detail()
		{
			return null;
		}
		#endregion
	}
}