﻿
namespace Shark.Speed.WebService.AnotaceCheck
 {
  public interface ICall
   {
    ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl);
	IAnswer					closeSession (string sessionId, string callUrl);

	IDownLoadAnswer	downLoad (string sessionId, string callUrl);
	IAnswer			upload (string sessionId, IUpLoadFile[] files, string callUrl);
   }

  public enum EAnswerStatus { AS_Ok = 0, AS_Error = 1, AS_Warning = 2 }
  
  public interface IAnswer
   {
    EAnswerStatus	isOk	{ get; }
	string			errMsg	{ get; }
   }

  public interface ICreateSessionAnswer : IAnswer
   { string	sessionId	{ get; } }

  public interface IDownLoadAnswer : IAnswer
   {
    IDownLoadFile[]	files	{ get; }
   }
  
  public interface IDownLoadFile
   {
    string	name		{ get; }
	string	cmdFile		{ get; }
	string	htmlFile	{ get; }
	string	xmlWFile	{ get; }
	string	xmlAFile	{ get; }
	string	xmlBFile	{ get; }
   }

  public interface IUpLoadFile : IDownLoadFile
   {
    string	warnings	{ get; }
	string	errors		{ get; }
   }

  public class Call : ICall
   {
    private Call (BI.IBi p_Bi)	{ m_Bi = p_Bi; m_Enc = new System.Text.UTF8Encoding (); }
	private BI.IBi						bi	{ get { return (m_Bi); } }	private BI.IBi						m_Bi;
	private System.Text.UTF8Encoding	enc	{ get { return (m_Enc); } } private System.Text.UTF8Encoding	m_Enc;

	public static ICall	create (BI.IBi p_Bi)	{ return (new Call (p_Bi)); }

	public ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl)
				{
				 Feat.ISession ses = ((userName != null)? Feat.Call.getSessionKeeper ().login (bi.user.loginUser (userName, passWd), callUrl): null);

 				 if (ses != null)
				  if (bi.user.hasUserRole (ses.userId, BI.User.EUserRole.UR_AutoChecker)) return (Answer.createSession (ses.id));
				  else { ses.logout (); System.Threading.Thread.Sleep (20000); return (Answer.createSessionError ("Not automat user!")); }
				 else { System.Threading.Thread.Sleep (20000); return (Answer.createSessionError ("Unknow user or invalid password!")); }
				}

	public IAnswer	closeSession (string sessionId, string callUrl)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createAnswerError ("Invalid session!"));

				 ses.logout (); return (Answer.createAnswer ());
				}

	public IDownLoadAnswer	downLoad (string sessionId, string callUrl)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createDownLoadError ("Invalid session!"));

				 System.Collections.Generic.List<IDownLoadFile> inbox = new System.Collections.Generic.List<IDownLoadFile> ();
				 System.Collections.Generic.Dictionary<string, long> usedFileNames = new System.Collections.Generic.Dictionary<string,long> ();
				 int count = 0;

				 Database.ICondBuilder cb = bi.database.condBuilder ();
				 {
				  Database.ICondBuilder stateCond = cb.addOrCond ();
				  string stateFldName = bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_StateId);
				  stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_KOvereni);
				  stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_KOdevzdani);
				  stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_KPrenosu);
				 }

				 string fileName;
				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  while (count < 100 && rs.moveNext ())
				   if (!(usedFileNames.ContainsKey (fileName = rs.currObj.getFileName ())))
				    {
					 inbox.Add (File.create (nm: fileName, cmd: createCmdFile (rs.currObj.id), htm: peekFile (rs.currObj.html),
										   xmlW: peekFile (rs.currObj.xmlW), xmlA: peekFile (rs.currObj.xmlA), xmlB: peekFile (rs.currObj.xmlB)));
				     usedFileNames.Add (fileName, rs.currObj.id);
					 count++;
					}

				 return (Answer.createDownLoad (inbox.ToArray ()));
				}
				
	private string	createCmdFile (long id)
				{
				 System.Text.StringBuilder sb = new System.Text.StringBuilder ();
				 sb = sb.AppendLine ("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				 sb = sb.AppendLine ("<AnotaceCheckCommandFile>");
				 sb = sb.Append ("  <Identification speedId=\"").Append (id).AppendLine ("\" />");
				 sb = sb.AppendLine ("</AnotaceCheckCommandFile>");

				 return (sb.ToString ());
				}

	private string	peekFile (BI.IReference<BI.ProcessData.IFile> flRef)
				{
				 try
				  {
				   BI.ProcessData.IFile fl = bi.processData.fileTable.getObject (flRef.id);
				 
				   if (fl == null || fl.data == null || fl.data.Length < 1) return (null);
				   return (enc.GetString (fl.data));
				  }
				 catch { return (null); }
				}

	public IAnswer	upload (string sessionId, IUpLoadFile[] files, string callUrl)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createDownLoadError ("Invalid session!"));

				 if (files != null)
				  using (Database.ITransaction trans = bi.database.openTransaction ())
				   for (int i = 0; i < files.Length; i++)
				    try
				     {
				      Xml.IDocument cmdDoc = bi.xml.parse (files [i].cmdFile);
					  BI.Process.ITask tsk = bi.process.taskTable.getObject (bi.language.tryScanLong (cmdDoc.findAttr ("AnotaceCheckCommandFile\\Identification", "speedId")));

					  if (tsk == null || (!(bi.language.isNull (tsk.finished)))) continue;

					  int newStateId;
					  switch ((BI.Process.EStateAnotace) (tsk.stateId))
					   {
					    case BI.Process.EStateAnotace.SA_KOvereni: newStateId = (int) BI.Process.EStateAnotace.SA_Prideleno; break;
						case BI.Process.EStateAnotace.SA_KOdevzdani: newStateId = (int) BI.Process.EStateAnotace.SA_Odevzdano; break;
						case BI.Process.EStateAnotace.SA_KPrenosu: newStateId = (int) ((isFileContentNull (files [i].errors))? BI.Process.EStateAnotace.SA_DoAmesu: BI.Process.EStateAnotace.SA_Odevzdano); break;
						default: continue;
					   }

					  if (tsk.isAble2Process (ses.userId, newStateId))
					   {
					    BI.ProcessData.IProcessData procDta = createNewProcessData (trans, tsk, files [i]);
						
						if (procDta != null)
						 {
						  string commentText = null;

						  if (isFileContentNull (files [i].errors)) { if (!(isFileContentNull (files [i].warnings))) commentText = files [i].warnings; }
						  else if (isFileContentNull (files [i].warnings)) commentText = files [i].errors;
						  else commentText = files [i].errors + "\n\n" + files [i].warnings;

						  if (commentText != null)
						   {
							BI.Process.IComment com = bi.process.commentTable.createNewObj ();
							com.process.id = tsk.process.id;
							com.user.id    = ses.userId;
							com.at         = System.DateTime.Now;
							com.text	   = commentText;
							bi.process.commentTable.insert (trans, com);
						   }

						  tsk.makeProcess (ses.userId, newStateId, trans: trans, newDta: procDta);
						
					      trans.finalyOk = true;
						 }
					   }
				     }
					catch { }
				
				 return (Answer.createAnswer ());
				}
	
	private bool isFileContentNull (string content)	{ return (content == null || content.Trim () == ""); }

	private BI.ProcessData.IProcessData	createNewProcessData (Database.ITransaction trans, BI.Process.ITask tsk, IUpLoadFile file)
				{
			     BI.ProcessData.IProcessData procDta = ((tsk == null)? null: tsk.data.parent);

			     if (procDta == null || procDta.type != BI.ProcessData.EProcessData.PD_Anotace) return (null);

				 BI.ProcessData.IProcessData newProcDta = bi.processData.processDataTable.createNewObj ();
				 newProcDta.type = BI.ProcessData.EProcessData.PD_Anotace;

				 BI.ProcessData.IProcessDataAnotace anoDta = newProcDta.anotace;

				 anoDta.anotator.id   = procDta.anotace.anotator.id;
				 anoDta.supervisor.id = procDta.anotace.supervisor.id;
				 
				 anoDta.html.id = saveFile (trans, name: file.name + ".html", dta: file.htmlFile, flRef: procDta.anotace.html);
				 anoDta.xmlW.id = saveFile (trans, name: file.name + ".w.xml", dta: file.xmlWFile, flRef: procDta.anotace.xmlW);
				 anoDta.xmlA.id = saveFile (trans, name: file.name + ".a.xml", dta: file.xmlAFile, flRef: procDta.anotace.xmlA);
				 anoDta.xmlB.id = saveFile (trans, name: file.name + ".b.xml", dta: file.xmlBFile, flRef: procDta.anotace.xmlB);
			     bi.processData.processDataTable.insert (trans, newProcDta);
				 return (newProcDta);
				}
	
	private long	saveFile (Database.ITransaction trans, string name, string dta, BI.IReference<BI.ProcessData.IFile> flRef)
				{
				 if (isFileContentNull (dta)) return (flRef.id);
				 
				 string content = peekFile (flRef);

				 if (!(isFileContentNull (content))) if (content == dta) return (flRef.id);
				 
				 BI.ProcessData.IFile fl = bi.processData.fileTable.createNewObj ();
				 fl.name = name;
				 if (dta != null) fl.data = enc.GetBytes (dta);
				 bi.processData.fileTable.insert (trans, fl);
				 return (fl.id);
				}

	private class Answer : IAnswer, ICreateSessionAnswer, IDownLoadAnswer
	 {
	  private Answer (string msg = null, EAnswerStatus ansStatus = EAnswerStatus.AS_Ok)	{ isOk = ansStatus; errMsg = msg; sessionId = null; files = null; }

	  public static IAnswer					createAnswerError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static ICreateSessionAnswer	createSessionError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static IDownLoadAnswer			createDownLoadError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }

	  public static IAnswer					createAnswer (string msg = null)						{ return (new Answer (msg)); }
	  public static ICreateSessionAnswer	createSession (string sesId, string msg = null)			{ Answer ans = new Answer (msg); ans.sessionId = sesId; return (ans); }
	  public static IDownLoadAnswer			createDownLoad (IDownLoadFile[] fls, string msg = null)	{ Answer ans = new Answer (msg); ans.files = fls; return (ans); }

      public EAnswerStatus	isOk	{ get; private set; }
	  public string			errMsg	{ get; private set; }

	  public string	sessionId	{ get; private set; }

	  public IDownLoadFile[]	files	{ get; private set; }
	 }
	
	private class File : IDownLoadFile
	 {
      private File (string nm, string cmd, string htm, string xmlW, string xmlA, string xmlB)	{ name = nm; cmdFile = cmd; htmlFile = htm; xmlWFile = xmlW; xmlAFile = xmlA; xmlBFile = xmlB;  }

	  public static IDownLoadFile	create (string nm, string cmd, string htm, string xmlW, string xmlA, string xmlB)	{ return (new File (nm: nm, cmd: cmd, htm: htm, xmlW: xmlW, xmlA: xmlA, xmlB: xmlB)); }
	  
	  public string	name		{ get; private set; }
	  public string	cmdFile		{ get; private set; }
	  public string	htmlFile	{ get; private set; }
	  public string	xmlWFile	{ get; private set; }
	  public string	xmlAFile	{ get; private set; }
	  public string	xmlBFile	{ get; private set; }
	 }
   }
 }
