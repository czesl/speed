﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="Spead.TestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Spead</title>
    <style type="text/css">
        #TextArea1
        {
            height: 200px;
            width: 800px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="btnShowComments" runat="server" onclick="btnShowComments_Click" 
            Text="Show Comments" />
    
        <asp:Button ID="btnDebugMakeProcess" runat="server" onclick="btnDebugMakeProcess_Click" 
            Text="Debug Make Process" Enabled="False" Visible="False" />
    
    </div>
    <p>
        &nbsp;</p>
    <asp:TextBox ID="txtOutput" runat="server" Rows="10" TextMode="MultiLine" 
        Width="800px"></asp:TextBox>
    </form>
</body>
</html>
