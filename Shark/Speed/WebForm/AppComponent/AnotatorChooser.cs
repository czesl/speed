﻿
namespace Shark.Speed.WebForm.AppComponent.AnotatorChooser
 {
  public class Creator
   {
    private const int	anotatorSelectId = 10;

	private Creator ()	{ }

	public static IChooserComponent	create (ICreator cre, Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId)
				{
				 Web.Component.IFormCreator frmCre = parent.form (2);

				 frmCre.addLabel (label: "Anotátor:");
				 frmCre.addSelect (id: anotatorSelectId, submitOnChange: true);
				 
				 return (new ChooserComponent (frmCre.create (), session, currSupervisorId));
				}

	private class ChooserComponent : IChooserComponent
	 {
	  public ChooserComponent (Web.Component.IForm frm, ISession ses, DCurrRowId currSupId)
				{ form = frm; session = ses; currSupervisorId = currSupId; prevSupervisorId = 0; currRowId = 0; form.onInitWebData += onInitWebData; form.onLoadWebData += onLoadWebData; }
	  
	  private Web.Component.IForm	form				{ get; set; }
	  private ISession				session				{ get; set; }
	  private DCurrRowId			currSupervisorId	{ get; set; }
	  private long					prevSupervisorId	{ get; set; }

	  public Web.Component.IComponent	comp	{ get { return (form); } }
	  
	  public long	currRowId	{ get; private set; }

	  private void	onInitWebData (Web.Component.IComponent cmp)
				{
				 long supId = currSupervisorId ();

				 if (supId == prevSupervisorId) return;

				 prevSupervisorId = supId; currRowId = 0;
				 System.Collections.Generic.List<Web.Component.ISelectValue> selValList = new System.Collections.Generic.List<Web.Component.ISelectValue> ();
				 BI.User.IModule userModule = session.requestBi.user;
				 Database.IFinder fnd = userModule.userTable.createFinder ();
				 fnd.sortBy (fnd.createSortByField (userModule.getFieldName (BI.User.EUserField.UF_UserName)));
				 Database.ICondBuilder andCond = fnd.createCondBuilder ();
				 Database.ICondBuilder existCond = andCond.addExists (tableName: userModule.userRoleTable.name, tableField: userModule.getFieldName (BI.User.EUserRoleField.URF_Id),
																    tableCondFld: userModule.getFieldName (BI.User.EUserRoleField.URF_UserId),
																	parentValueSide: andCond.fullFieldName (tableAcr: userModule.userTable.name, fieldName: userModule.getFieldName (BI.User.EUserField.UF_Id))); 
				 existCond.addValCond (leftSide: andCond.fullFieldName (tableAcr: userModule.userRoleTable.name, fieldName: userModule.getFieldName (BI.User.EUserRoleField.URF_Type)),
								     vl: (short) BI.User.EUserRole.UR_Anotator);

				 using (Database.IRecordSet<BI.User.IUser> rs = userModule.userTable.findObj (fnd))
				  while (rs.moveNext ()) selValList.Add (form.selectValue (val: rs.currObj.id, label: rs.currObj.userName, desc: rs.currObj.name));

				 if (selValList.Count < 1) return;
				 Web.Component.ISelectValue[] selArr = selValList.ToArray ();
				 currRowId = selArr [0].val;
				 form.setSelectValues (anotatorSelectId, selArr);
				}

	  private void	onLoadWebData (Web.Component.IComponent cmp)
				{ currRowId = session.requestBi.language.tryScanLong (form [anotatorSelectId]); }
	 }
   }
 }
