﻿
namespace Shark.Speed.BI
 {
  public interface IBiObject : Database.IObject
   { long	id	{ get; set; } }

  public interface IReference<Type> where Type : IBiObject
   {
    long	id			{ get; set; }
	string	name		{ get; }
	string	description	{ get; }

	Type	parent		{ get; set; }

	void	loadDta (Database.IQuery query, Database.IRecord rec, string refIdFldName);
   }

  public interface ICreator
   {
    IReference<Type>	reference<Type> (Database.IQuery<Type> masterQuery) where Type : IBiObject;
   }

  public interface IBi
   {
    Database.ILangTranslation	language	{ get; }
	Database.IDateTimeZone		timeZone	{ get; }

	Xml.ICreator		xml			{ get; }
	Database.IFactory	database	{ get; }

    User.IModule		user		{ get; }
	Process.IModule		process		{ get; }
	ProcessData.IModule	processData	{ get; }
   }

  public class Bi
   {
    private Bi ()	{ }

	public static IBi	createMsSQL (string connectionString, Database.ILangTranslation lang = null, Database.IDateTimeZone timeZone = null, Xml.ICreator x = null)
				{ return (create (createMsSQLDatabaseEngine (connectionString), lang: lang, timeZone: timeZone, x: x)); }

	public static IBi	create (Database.SQL.IEngine sqlEngine, Database.ILangTranslation lang = null, Database.IDateTimeZone timeZone = null, Xml.ICreator x = null)
				{
				 if (lang == null) lang = createDefaultLanguage ();
				 if (timeZone == null) timeZone = createDefaultTimeZone ();

				 return (create (createDefaultDatabaseFactory (sqlEngine, lang, timeZone), lang, timeZone, x));
				}
	
	public static IBi	create (Database.IFactory db, Database.ILangTranslation lang = null, Database.IDateTimeZone timeZone = null, Xml.ICreator x = null)
				{
				 if (lang == null) lang = createDefaultLanguage ();
				 if (timeZone == null) timeZone = createDefaultTimeZone ();
				 if (x == null) x = createDefaultXmlCreator ();

				 ICreator biCre = Creator.create (lang.longNullValue);

				 User.IModule userModule = User.Module.create (biCre, db, lang);
				 ProcessData.IModule procDtaModule = ProcessData.Module.create (biCre, db, userModule.userTable, userModule.getFieldName (User.EUserField.UF_Id));
				 Process.IModule procModule = Process.Module.create (biCre, db, lang, userModule, procDtaModule);

				 BiImp bi = new BiImp ();
				 bi.language    = lang;
				 bi.timeZone    = timeZone;
				 bi.xml         = x;
				 bi.database    = db;
				 bi.user        = userModule;
				 bi.processData = procDtaModule;
				 bi.process     = procModule;
				
				 return (bi);
				}

	private class Creator : ICreator
	 {
	  private Creator (long nllIdVl)	{ nullIdVal = nllIdVl; }

	  public static ICreator	create (long nullIdValue)	{ return (new Creator (nullIdValue)); }

	  private long	nullIdVal	{ get; set; }

	  public IReference<Type>	reference<Type> (Database.IQuery<Type> masterQuery) where Type : IBiObject	{ return (new Reference<Type> (nullIdVal, masterQuery)); }

	  private class Reference<Type> : IReference<Type> where Type : IBiObject
	   {
	    public Reference (long nllIdVl, Database.IQuery<Type> mstQ)	{ nullIdVal = nllIdVl; masterQuery = mstQ; }

        private long					nullIdVal	{ get; set; }
		private Database.IQuery<Type>	masterQuery	{ get; set; }

		public long		id			{ get; set; }
	    public string	name		{ get; private set; }
	    public string	description	{ get; private set; }

	    public Type	parent	{
		                     get { return (masterQuery.getObject (id)); }
		                     set { id = ((value == null)? nullIdVal: value.id); }
							}

	    public void	loadDta (Database.IQuery query, Database.IRecord rec, string refIdFldName)
					{
					 Database.IField fld = query [refIdFldName];

					 id          = fld.val (rec).bg.val;
					 name        = ((fld.referenceField.nameFieldName == null)? null: rec.val (query, fld.referenceField.nameFieldName).txt.val);
					 description = ((fld.referenceField.descFieldName == null)? null: rec.val (query, fld.referenceField.descFieldName).txt.val);
					}
	   }
	 }

	public static Database.ILangTranslation	createDefaultLanguage ()	{ return (DefaultLanguage.getLanguage ()); }

	private class DefaultLanguage : Database.ILangTranslation
	 {
	  private DefaultLanguage ()	{ }

	  public static Database.ILangTranslation	getLanguage ()
				{
				 if (ms_Language == null) ms_Language = new DefaultLanguage ();
				 return (ms_Language);
				}
	  private static Database.ILangTranslation	ms_Language = null;

      public int	language	{ get { return (getDefaultCultureInfo ().LCID); } }
    
      public string	translate (string txtCode, string defValue)	{ return ((defValue == null)? txtCode: defValue); }

	  public long				longNullValue		{ get { return (0); } }
	  public int				intNullValue		{ get { return (0); } }
	  public short				shortNullValue		{ get { return (0); } }
	  public bool				boolNullValue		{ get { return (false); } }
	  public System.DateTime	dtNullValue			{ get { return (System.DateTime.MinValue); } }
	  public string				stringNullValue		{ get { return (null); } }
	  public byte[]				byteArrNullValue	{ get { return (null); } }

	  public bool	isNull (long l)				{ return (l == longNullValue); }
	  public bool	isNull (int i)				{ return (i == intNullValue); }
	  public bool	isNull (short s)			{ return (s == shortNullValue); }
	  public bool	isNull (bool b)				{ return (b == boolNullValue); }
	  public bool	isNull (System.DateTime dt)	{ return (dt == dtNullValue); }
	  public bool	isNull (string txt)			{ return (txt == null || txt.Trim () == ""); }
	  public bool	isNull (byte[] arr)			{ return (arr == null || arr.Length < 1); }

	  public string	format (long l)	{ return (l.ToString ().Trim ()); }
	  public string	format (int i)	{ return (i.ToString ().Trim ()); }
	  public string	format (bool b)	{ return ((b)? "Ano": "No"); }
	  
	  public string	format (System.DateTime dt, Database.EDateTimeType type)
				{
				 switch (type)
				  {
				   case Database.EDateTimeType.DTT_Date: return (dt.ToString ("d.M.yyyy"));
				   case Database.EDateTimeType.DTT_Time: return (dt.ToString ("HH:mm"));
				   case Database.EDateTimeType.DTT_Full:
				   default: return (dt.ToString ("d.M.yyyy HH:mm"));
				  }
				}

	  public long				scanLong (string val)	{ return (System.Convert.ToInt64 (val)); }
	  public int				scanInt (string val)	{ return (System.Convert.ToInt32 (val)); }
	  public short				scanShort (string val)	{ return (System.Convert.ToInt16 (val)); }
	  public bool				scanBool (string val)	{ return (val != null && val.Trim ().ToLower () == "ano"); }
	  
	  public System.DateTime	scanDT (string val, Database.EDateTimeType type)
				{
				 System.DateTime dt = System.Convert.ToDateTime (val, getDefaultCultureInfo ());
				 
				 switch (type)
				  {
				   case Database.EDateTimeType.DTT_Date: dt = dt.Date; break;
				   case Database.EDateTimeType.DTT_Time: dt = new System.DateTime (1, 1, 1, dt.Hour, dt.Minute, dt.Second, dt.Millisecond); break;
				  }
				 return (dt);
				}
	  
	  private static System.Globalization.CultureInfo	getDefaultCultureInfo ()
				{
				 if (ms_DefaultCultureInfo == null) ms_DefaultCultureInfo = new System.Globalization.CultureInfo ("cs-CZ");
				 return (ms_DefaultCultureInfo);
				}
	  private static System.Globalization.CultureInfo	ms_DefaultCultureInfo = null;

	  public long	tryScanLong (string val, long defValue)	{ if (val == null) return (defValue); else try { return (scanLong (val)); } catch { return (defValue); } }
	  public int	tryScanInt (string val, int defValue)	{ if (val == null) return (defValue); else try { return (scanInt (val)); } catch { return (defValue); } }
	 }

	public static Database.IDateTimeZone	createDefaultTimeZone ()	{ return (DefaultTimeZone.getTimeZone ()); }

	private class DefaultTimeZone : Database.IDateTimeZone
	 {
	  private DefaultTimeZone ()	{ }

	  public static Database.IDateTimeZone	getTimeZone ()
				{
				 if (ms_TimeZone == null) ms_TimeZone = new DefaultTimeZone ();
				 return (ms_TimeZone);
				}
	  private static Database.IDateTimeZone	ms_TimeZone = null;

      public System.DateTime	zone2Server (System.DateTime dt)	{ return (dt); }
      public System.DateTime	server2Zone (System.DateTime dt)	{ return (dt); }
	 }
	
	public static Xml.ICreator	createDefaultXmlCreator ()	{ return (Xml.Creator.getCreator ()); }

	public static Database.IFactory	createDefaultDatabaseFactory (Database.SQL.IEngine sql, Database.ILangTranslation lang, Database.IDateTimeZone zone)
				{ return (Database.General.Factory.create (sql, lang, zone)); }

	public static Database.SQL.IEngine	createMsSQLDatabaseEngine (string connectionString)
				{ return (Database.SQL.MsSQL.Engine.create (connectionString)); }


	private class BiImp : IBi
	 {
      public Database.ILangTranslation	language	{ get; set; }
	  public Database.IDateTimeZone		timeZone	{ get; set; }

	  public Xml.ICreator		xml			{ get; set; }
	  public Database.IFactory	database	{ get; set; }

      public User.IModule			user		{ get; set; }
	  public Process.IModule		process		{ get; set; }
	  public ProcessData.IModule	processData	{ get; set; }
	 }
   }
 }
