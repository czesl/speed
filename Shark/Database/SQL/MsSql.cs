﻿
namespace Shark.Database.SQL.MsSQL
 {
  public class Engine
   {
    private Engine ()	{ }
	public static IEngine	create (string connectionString)	{ return (MsEngine.create (connectionString)); }

	private class ParamBuilder : IParamBuilder
	 {
	  private ParamBuilder (System.Data.SqlClient.SqlCommand sqlCmd)	{ sqlCommand = sqlCmd; }
	  public static IParamBuilder	create (System.Data.SqlClient.SqlCommand sqlCommand)	{ return (new ParamBuilder (sqlCommand)); }

	  private System.Data.SqlClient.SqlCommand	sqlCommand	{ get; set; }

	  private string	createParamName ()	{ lock (this.GetType ()) { return ("@parNo" + (++ms_ParamCnt).ToString ().Trim ()); } }
	  private static long	ms_ParamCnt = 0;
		
	  public string	addNull (EFieldType type)
				{
				 string parName = createParamName ();

				 switch (type)
				  {
				   case EFieldType.FT_BigInt:   sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.BigInt).Value = System.DBNull.Value; break;
				   case EFieldType.FT_Int:		sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = System.DBNull.Value; break;
				   case EFieldType.FT_Short:	sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = System.DBNull.Value; break;
				   case EFieldType.FT_Bool:		sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = System.DBNull.Value; break;
				   case EFieldType.FT_DateTime:	sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.DateTime).Value = System.DBNull.Value; break;
				   case EFieldType.FT_Text:		sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.NVarChar).Value = System.DBNull.Value; break;
				   case EFieldType.FT_BLOB:		sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Image).Value = System.DBNull.Value; break;
				   default: throw new System.ApplicationException ("InKnow data type: " + ((int) type).ToString ());
				  }

				 return (parName);
				}

	  public string	add (IValue val)
				{
				 switch (val.type)
				  {
				   case EFieldType.FT_BigInt:   return (add (val.bg.val));
				   case EFieldType.FT_Int:		return (add (val.it.val));
				   case EFieldType.FT_Short:	return (add (val.sh.val));
				   case EFieldType.FT_Bool:		return (add (val.bl.val));
				   case EFieldType.FT_DateTime:	return (add (val.dt.val));
				   case EFieldType.FT_Text:		return (add (val.txt.val));
				   case EFieldType.FT_BLOB:		return (add (val.blb.val));
				   default: throw new System.ApplicationException ("InKnow data type: " + ((int) (val.type)).ToString ());
				  }
				}
		
	  public string	add (long val)				{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.BigInt).Value = val; return (parName); }
	  public string	add (int val)				{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = val; return (parName); }
	  public string	add (short val)				{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = (int) val; return (parName); }
	  public string	add (bool val)				{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Int).Value = (int) ((val)? 1: 0); return (parName); }
	  public string	add (System.DateTime val)	{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.DateTime).Value = val; return (parName); }
	  public string	add (string val)			{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.NVarChar).Value = val; return (parName); }
	  public string	add (byte[] val)			{ string parName = createParamName (); sqlCommand.Parameters.Add (parName, System.Data.SqlDbType.Image).Value = val; return (parName); }
	 }

	private interface ICreateCond	{ string	create (); }

	private interface IImpCondBuilder : ICondBuilder, ICreateCond	{ }
	
	private class CondBuilder : IImpCondBuilder
	 {
	  private CondBuilder (IParamBuilder prBld, string opr = "AND")	{ parBld = prBld; condOper = " " + opr + " ";  m_Cond = new System.Collections.Generic.List<ICreateCond> (); }
	  public static IImpCondBuilder	create (IParamBuilder parBld)	{ return (new CondBuilder (parBld)); }

	  private System.Collections.Generic.List<ICreateCond>	m_Cond;

	  public IParamBuilder	parBld		{ get; private set; }
	  private string		condOper	{ get; set; }

	  public string	createTableAcr ()	{ lock (this.GetType ()) { return ("MsSQLTableAcrNo" + (++ms_TableCnt).ToString ().Trim ()); } }
	  private static long	ms_TableCnt = 0;

	  public string	fullFieldName (string tableAcr, string fieldName)	{ return (MsEngine.fullFldName (tableAcr: tableAcr, fieldName: fieldName)); }

	  public void	addIsNull (string leftSide)	{ m_Cond.Add (new StringCond ("(" + leftSide + " IS NULL)")); }
	  
	  public void	addCond (string leftSide, ECondOper oper, string rightSide)
				{
				 string opr;

				 switch (oper)
				  {
				   case ECondOper.CO_Eq:   opr = "="; break;
				   case ECondOper.CO_Like: opr = "LIKE"; break;
				   case ECondOper.CO_NEq:  opr = "<>"; break;
				   case ECondOper.CO_Le:   opr = "<="; break;
				   case ECondOper.CO_Lt:   opr = "<"; break;
				   case ECondOper.CO_Ge:   opr = ">="; break;
				   case ECondOper.CO_Gt:   opr = ">"; break;
				   default: throw new System.ApplicationException ("UnKnow operator type: " + ((int) oper).ToString ());
				  }

				 m_Cond.Add (new StringCond ("(" + leftSide + " " + opr + " " + rightSide + ")"));
				}

	  public ICondBuilder	addNotCond ()	{ IImpCondBuilder cndB = new CondBuilder (parBld); m_Cond.Add (new NotCond (cndB)); return (cndB); }
	  public ICondBuilder	addAndCond ()	{ IImpCondBuilder cndB = new CondBuilder (parBld); m_Cond.Add (cndB); return (cndB); }
	  public ICondBuilder	addOrCond ()	{ IImpCondBuilder cndB = new CondBuilder (parBld, "OR"); m_Cond.Add (cndB); return (cndB); }

	  public ICondBuilder	addExists (string tableName, string tableField, string tableAcr = null, string tableCondFld = null, string parentValueSide = null)
				{
				 System.Text.StringBuilder sb = new System.Text.StringBuilder ("SELECT ");
				 sb = sb.Append ((tableAcr == null)? tableName: tableAcr).Append (".").Append (tableField).Append (" FROM ").Append (tableName);
				 
				 if (tableAcr == null) tableAcr = tableName;
				 else sb = sb.Append (" AS ").Append (tableAcr);

				 IImpCondBuilder cndB = new CondBuilder (parBld);
				 if (tableCondFld != null && parentValueSide != null) cndB.addCond (fullFieldName (tableAcr: tableAcr, fieldName: tableCondFld), ECondOper.CO_Eq, parentValueSide);

				 m_Cond.Add (new ExistsCond (sb.ToString (), cndB));
				 return (cndB);
				}

	  public string	create ()
				{
				 System.Text.StringBuilder sb = null;

				 foreach (ICreateCond creCnd in m_Cond)
				  {
				   string cnd = creCnd.create ();
				   
				   if (cnd != null)
				    if (sb == null) sb = new System.Text.StringBuilder ("(" + cnd);
				    else sb = sb.Append (condOper).Append (cnd);
				  }

				 return ((sb == null)? null: sb.Append (")").ToString ());
				}

	  private class StringCond : ICreateCond
	   {
	    public			StringCond (string cnd)	{ m_Cond = cnd; }	private string	m_Cond;
		public string	create ()				{ return (m_Cond); }
	   }

	  private class NotCond : ICreateCond
	   {
	    public			NotCond (ICreateCond subCnd)	{ m_SubCond = subCnd; }	private ICreateCond	m_SubCond;
		public string	create ()						{ return ("(NOT " + m_SubCond.create () + ")"); }
	   }

	  private class ExistsCond : ICreateCond
	   {
	    public	ExistsCond (string sel, ICreateCond subCnd)	{ m_Select = sel; m_SubCond = subCnd; }
		private string		m_Select;
		private ICreateCond	m_SubCond;
		
		public string	create ()
					{
					 string subCnd = m_SubCond.create ();

					 return ("(EXISTS (" + m_Select + ((subCnd == null)? "": " WHERE " + subCnd) + "))");
					}
	   }
	 }

	private class Select : ISelect
	 {
	  private Select (System.Data.SqlClient.SqlCommand sqlCmd, System.Data.SqlClient.SqlConnection toCls, string tableName = null, bool useAllFields = false, string tableAcr = null)
				{
				 andImpCnd = CondBuilder.create (parBld = ParamBuilder.create (sqlCommand = sqlCmd));
				 toClose = toCls; m_FldBld = null; m_TblBld = null;

				 if (tableName != null) addTable (queryName: tableName, useAllFields: useAllFields, queryAcr: tableAcr);
				}
	  public static ISelect	create (System.Data.SqlClient.SqlCommand sqlCommand, System.Data.SqlClient.SqlConnection toClose, string tableName = null, bool useAllFields = false, string tableAcr = null)
				{ return (new Select (sqlCommand, toClose, tableName: tableName, useAllFields: useAllFields, tableAcr: tableAcr)); }

	  private System.Data.SqlClient.SqlCommand		sqlCommand	{ get; set; }
	  private System.Data.SqlClient.SqlConnection	toClose		{ get; set; }
	  private IImpCondBuilder						andImpCnd	{ get; set; }
	  private System.Text.StringBuilder	m_FldBld;
	  private System.Text.StringBuilder	m_TblBld;

	  public IParamBuilder	parBld		{ get; private set; }
	  public ICondBuilder	andCnd		{ get { return (andImpCnd); } }

	  public string	createTableAcr ()									{ return (andCnd.createTableAcr()); }
      public string	fullFieldName (string queryAcr, string fieldName)	{ return (andCnd.fullFieldName (tableAcr: queryAcr, fieldName: fieldName)); }
	  
	  public string	createFieldNameAcr ()	{ lock (this.GetType ()) { return ("FieldAcrNo" + (++ms_FieldCnt).ToString ().Trim ()); } }
	  private static long	ms_FieldCnt = 0;

	  public void	close ()
				{
				 try { if (sqlCommand != null) sqlCommand.Dispose (); }
				 finally
				  {
				   sqlCommand = null;
				   try { if (toClose != null) toClose.Dispose (); } finally { toClose = null; }
				  }
				}
	  public void	Dispose ()	{ close (); }
	  
	  public IJoinCond		createJoinCond (string leftSide, string rightSide)			{ return (new JoinCond (lfSd: leftSide, rgtSd: rightSide)); }
	  public ISortByField	createSortByField (string fullFieldName, bool asc = true)	{ return (new SortByField (fullFieldName, asc)); }

	  public void	addTable (string queryName, bool useAllFields = false, string queryAcr = null, ESelectJoin join = ESelectJoin.SJ_Join, IJoinCond oneCond = null, IJoinCond[] conds = null)
				{
				 if (m_TblBld == null)
				  {
				   m_TblBld = new System.Text.StringBuilder (queryName);
				   if (queryAcr != null) m_TblBld = m_TblBld.Append (" AS ").Append (queryAcr);
				   else queryAcr = queryName;
				  }
				 else
				  {
				   switch (join)
				    {
					 case ESelectJoin.SJ_Join:	   m_TblBld = m_TblBld.Append (" INNER JOIN "); break;
					 case ESelectJoin.SJ_LeftJoin: m_TblBld = m_TblBld.Append (" LEFT OUTER JOIN "); break;
					 default: throw new System.ApplicationException ("UnKnow join type: " + ((int) join).ToString ());
					}

				   m_TblBld = m_TblBld.Append (queryName);
				   if (queryAcr != null) m_TblBld = m_TblBld.Append (" AS ").Append (queryAcr);
				   else queryAcr = queryName;

				   if (oneCond != null || (conds != null && conds.Length > 0))
				    {
					 string oper = null;
					 m_TblBld = m_TblBld.Append (" ON (");
					 
					 m_TblBld = appendCond (m_TblBld, oneCond, oper);
					 if (oneCond != null) oper = " AND ";

					 if (conds != null)
					  for (int i = 0; i < conds.Length; i++)
					   if (conds [i] != null) { m_TblBld = appendCond (m_TblBld, conds [i], oper); oper = " AND "; }

					 m_TblBld = m_TblBld.Append (")");
					}
				  }

				 if (useAllFields) addShowField (fullFieldName (queryAcr: queryAcr, fieldName: "*"));
				}
	  
	  private System.Text.StringBuilder	appendCond (System.Text.StringBuilder sb, IJoinCond cnd, string oper)
				{
				 if (cnd == null) return (sb);

				 if (oper != null) sb = sb.Append (oper);
				 return (sb.Append (cnd.leftSide).Append (" = ").Append (cnd.rightSide));
				}

	  
	  public void	addShowField (string fullFieldName, string fieldAcr = null)
				{
				 if (m_FldBld == null) m_FldBld = new System.Text.StringBuilder (fullFieldName);
				 else m_FldBld = m_FldBld.Append (", ").Append (fullFieldName);

				 if (fieldAcr != null) m_FldBld = m_FldBld.Append (" AS ").Append (fieldAcr);
				}

	  public ISortByField[]	sortByFields	{ get; set; }

	  public long	maxRows		{ get; set; }	// -1 means every
	  public bool	forUpdate	{ get; set; }

	  public IFetch	select ()
				{
				 System.Text.StringBuilder sb = new System.Text.StringBuilder ("SELECT ");
				 
				 if (maxRows > 0) sb = sb.Append (" TOP ").Append (maxRows).Append (" ");
				 
				 sb = sb.Append (m_FldBld.ToString ()).Append (" FROM ").Append (m_TblBld.ToString ());
				 
				 string cnd = andImpCnd.create ();
				 if (cnd != null) sb = sb.Append (" WHERE ").Append (cnd);

				 if (sortByFields != null)
				  {
				   string comma = "";
				   sb = sb.Append (" ORDER BY ");

				   for (int i = 0; i < sortByFields.Length; i++)
				    {
					 sb = sb.Append (comma).Append (sortByFields [i].fldName).Append ((sortByFields [i].asc)? " asc": " desc");
					 comma = ", ";
					}
				  }
				 
				 sqlCommand.CommandText = sb.ToString ();
				 IFetch ftch = Fetch.create (sqlCommand, toClose);
				 sqlCommand = null; toClose = null;
				 return (ftch);
				}

	  private class JoinCond : IJoinCond
	   {
	    public JoinCond (string lfSd, string rgtSd)	{ leftSide = lfSd; rightSide = rgtSd; }
	    public string	leftSide	{ get; private set; }
		public string	rightSide	{ get; private set; }
	   }

	  private class SortByField : ISortByField
	   {
	    public SortByField (string fldNm, bool isAsc = true)	{ fldName = fldNm; asc = isAsc; }
	    public string	fldName	{ get; private set; }
		public bool		asc		{ get; private set; }
	   }

	  private class Fetch : IFetch
	   {
	    private Fetch (System.Data.SqlClient.SqlCommand cmd, System.Data.SqlClient.SqlConnection toCls)
					{
					 sqlCommand = cmd; toClose = toCls;
					 dataReader = sqlCommand.ExecuteReader ();
					}
		public static IFetch	create (System.Data.SqlClient.SqlCommand sqlCommand, System.Data.SqlClient.SqlConnection toClose)	{ return (new Fetch (sqlCommand, toClose)); }

	    private System.Data.SqlClient.SqlConnection	toClose		{ get; set; }
	    private System.Data.SqlClient.SqlCommand	sqlCommand	{ get; set; }
		private System.Data.SqlClient.SqlDataReader	dataReader	{ get; set; }

	    public void	close ()
					{
					 try { if (dataReader != null) dataReader.Close (); }
					 finally
					  {
					   dataReader = null;
					   try { if (sqlCommand != null) sqlCommand.Dispose (); }
					   finally
					    {
					     sqlCommand = null;
					     try { if (toClose != null) toClose.Dispose (); } finally { toClose = null; }
					    }
					  }
					}
	    public void	Dispose ()	{ close (); }

		public bool	moveNext ()	{ return (dataReader.Read ()); }

		private int	findFldIndex (string fieldName)	{ return (dataReader.GetOrdinal (fieldName)); }

		public bool	isDefined (string fieldName)	{ try { findFldIndex (fieldName); return (true); } catch { return (false); } }

		public bool				isNull (string fieldName)		{ return (dataReader.IsDBNull (findFldIndex (fieldName))); }
		public long				readLng (string fieldName)		{ return (dataReader.GetInt64 (findFldIndex (fieldName))); }
		public int				readInt (string fieldName)		{ return (dataReader.GetInt32 (findFldIndex (fieldName))); }
		public short			readShort (string fieldName)	{ return (dataReader.GetInt16 (findFldIndex (fieldName))); }
		public bool				readBool (string fieldName)		{ return (dataReader.GetInt16 (findFldIndex (fieldName)) != 0); }
		public System.DateTime	readDateTime (string fieldName)	{ return (dataReader.GetDateTime (findFldIndex (fieldName))); }
		public string			readString (string fieldName)	{ return (dataReader.GetString (findFldIndex (fieldName))); }
		public byte[]			readBLOB (string fieldName)		{ return ((byte[]) dataReader.GetValue (findFldIndex (fieldName))); }
	   }
	 }
	
	private class TransactionDef
	 {
	  private TransactionDef ()	{ }
	  public static ITransaction	create (string connectionString)	{ return (Transaction.create (connectionString)); }

	  private class Insert : IInsert
	   {
		private Insert (System.Data.SqlClient.SqlCommand sqlCmd, string tblNm = null)	{ parBld = ParamBuilder.create (sqlCommand = sqlCmd); tableName = tblNm; m_FldBld = null; m_ValBld = null; }
		public static IInsert	create (System.Data.SqlClient.SqlCommand sqlCommand, string tableName = null)	{ return (new Insert (sqlCommand, tableName)); }

		private System.Data.SqlClient.SqlCommand	sqlCommand	{ get; set; }
		private System.Text.StringBuilder	m_FldBld;
		private System.Text.StringBuilder	m_ValBld;

	    public IParamBuilder	parBld		{ get; private set; }
		public string			tableName	{ get; set; }

		public void	add (string fieldName, string parName)
					{
					 if (m_FldBld == null)
					  {
					   m_FldBld = new System.Text.StringBuilder ();
					   m_ValBld = new System.Text.StringBuilder ();
					  }
					 else { m_FldBld = m_FldBld.Append (", "); m_ValBld = m_ValBld.Append (", "); }

					 m_FldBld = m_FldBld.Append (fieldName);
					 m_ValBld = m_ValBld.Append (parName);
					}

		private void	insertAction ()
					{
					 sqlCommand.CommandText = "INSERT INTO " + tableName + " (" + m_FldBld.ToString () + ") VALUES (" + m_ValBld.ToString () + ")";
					 sqlCommand.ExecuteNonQuery ();
					}
		
		public void	insert ()	{ try { insertAction (); } finally { sqlCommand.Dispose (); } }
		
		public long	insert (string autoIncrementKeyName)
					{
					 try
					  {
					   insertAction ();

					   using (System.Data.SqlClient.SqlCommand selCmd = new System.Data.SqlClient.SqlCommand (cmdText: "select @@IDENTITY", connection: sqlCommand.Connection, transaction: sqlCommand.Transaction))
					    return (System.Convert.ToInt64 (selCmd.ExecuteScalar ()));
					  }
					 finally { sqlCommand.Dispose (); }
					}
	   }

	  private class Update : IUpdate
	   {
		private Update (System.Data.SqlClient.SqlCommand sqlCmd, string tblNm = null)
					{
					 andImpCnd = CondBuilder.create (parBld = ParamBuilder.create (sqlCommand = sqlCmd));
					 tableName = tblNm; m_UpdateBld = null;
					}
		public static IUpdate	create (System.Data.SqlClient.SqlCommand sqlCommand, string tableName = null)	{ return (new Update (sqlCommand, tableName)); }

		private System.Data.SqlClient.SqlCommand	sqlCommand	{ get; set; }
		private IImpCondBuilder						andImpCnd	{ get; set; }
		private System.Text.StringBuilder	m_UpdateBld;

	    public IParamBuilder	parBld		{ get; private set; }
		public ICondBuilder		andCnd		{ get { return (andImpCnd); } }
		public string			tableName	{ get; set; }

		public void	add (string fieldName, string leftSide)
					{
					 if (m_UpdateBld == null) m_UpdateBld = new System.Text.StringBuilder ();
					 else m_UpdateBld = m_UpdateBld.Append (", ");

					 m_UpdateBld = m_UpdateBld.Append (fieldName).Append (" = ").Append (leftSide);
					}

		public void	update ()
					{
					 try
					  {
					   System.Text.StringBuilder cmd = new System.Text.StringBuilder ("UPDATE ").Append (tableName).Append (" SET ").Append (m_UpdateBld.ToString ());

					   string cond = andImpCnd.create ();
					   if (cond != null) cmd = cmd.Append (" WHERE ").Append (cond); 

					   sqlCommand.CommandText = cmd.ToString ();
					   sqlCommand.ExecuteNonQuery ();
					  }
					 finally { sqlCommand.Dispose (); }
					}
	   }
	  
	  private class Delete : IDelete
	   {
		private Delete (System.Data.SqlClient.SqlCommand sqlCmd, string tblNm = null)
					{
					 andImpCnd = CondBuilder.create (parBld = ParamBuilder.create (sqlCommand = sqlCmd));
					 tableName = tblNm;
					}
		public static IDelete	create (System.Data.SqlClient.SqlCommand sqlCommand, string tableName = null)	{ return (new Delete (sqlCommand, tableName)); }

		private System.Data.SqlClient.SqlCommand	sqlCommand	{ get; set; }
		private IImpCondBuilder						andImpCnd	{ get; set; }

	    public IParamBuilder	parBld		{ get; private set; }
		public ICondBuilder		andCnd		{ get { return (andImpCnd); } }
		public string			tableName	{ get; set; }

		public void	delete ()
					{
					 try
					  {
					   System.Text.StringBuilder cmd = new System.Text.StringBuilder ("DELETE FROM ").Append (tableName);

					   string cond = andImpCnd.create ();
					   if (cond != null) cmd = cmd.Append (" WHERE ").Append (cond); 

					   sqlCommand.CommandText = cmd.ToString ();
					   sqlCommand.ExecuteNonQuery ();
					  }
					 finally { sqlCommand.Dispose (); }
					}
	   }
	  
	  private class Transaction : ITransaction
	   {
	    private Transaction (string connectionString)
					{
					 sqlConnection = new System.Data.SqlClient.SqlConnection (connectionString);
					 sqlConnection.Open ();

					 sqlTransaction = sqlConnection.BeginTransaction ();
					}
	    public static ITransaction	create (string connectionString)	{ return (new Transaction (connectionString)); }

		private System.Data.SqlClient.SqlConnection		sqlConnection	{ get; set; }
		private System.Data.SqlClient.SqlTransaction	sqlTransaction	{ get; set; }

	    public void	close ()
					{
					 lock (this)
					  {
					   if (sqlConnection == null) return;

					   try { if (finalyOk) sqlTransaction.Commit (); else sqlTransaction.Rollback (); } catch { } 
					   
					   System.Data.SqlClient.SqlConnection con = sqlConnection;
					   System.Data.SqlClient.SqlTransaction trans = sqlTransaction;
					   sqlConnection = null; sqlTransaction = null;

					   trans.Dispose (); con.Dispose ();
					  }
					}
	    public void	Dispose ()	{ close (); }

		public bool	finalyOk	{ get; set; }

		private System.Data.SqlClient.SqlCommand	createCommand ()
					{
					 System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand ();
					 cmd.Connection = sqlConnection;
					 cmd.Transaction = sqlTransaction;

					 return (cmd);
					}

		public IInsert	insert (string tableName = null)	{ return (Insert.create (createCommand (), tableName)); }
		public IUpdate	update (string tableName = null)	{ return (Update.create (createCommand (), tableName)); }
		public IDelete	delete (string tableName = null)	{ return (Delete.create (createCommand (), tableName)); }
		public ISelect	select (string tableName = null, bool useAllFields = false, string tableAcr = null)
					{ return (Select.create (createCommand (), null, tableName: tableName, useAllFields: useAllFields, tableAcr: tableAcr)); }

		public void	commit ()	{ close (); }
	   }
	 }

	private class MsEngine : IEngine
	 {
	  private MsEngine (string connStr)	{ connectionString = connStr; }
	  public static IEngine	create (string connectionString)	{ return (new MsEngine (connectionString)); }

	  private string	connectionString	{ get; set; }

	  public static string	fullFldName (string tableAcr, string fieldName)
				{ return ((fieldName == null)? null: (tableAcr == null)? fieldName: tableAcr + "." + fieldName); }
	  
	  public string	fullFieldName (string tableAcr, string fieldName)	{ return (fullFldName (tableAcr: tableAcr, fieldName: fieldName)); }

	  public void	close ()	{ }
	  public void	Dispose ()	{ close (); }

	  public ITransaction	openTransaction ()	{ return (TransactionDef.create (connectionString)); }

	  public IInsert	insert (ITransaction trans, string tableName = null)	{ return (trans.insert (tableName)); }
	  public IUpdate	update (ITransaction trans, string tableName = null)	{ return (trans.update (tableName)); }
	  public IDelete	delete (ITransaction trans, string tableName = null)	{ return (trans.delete (tableName)); }
	  public ISelect	select (ITransaction trans = null, string tableName = null, bool useAllFields = false, string tableAcr = null)
				{
				 if (trans != null) return (trans.select (tableName: tableName, useAllFields: useAllFields, tableAcr: tableAcr));

				 System.Data.SqlClient.SqlConnection con = null;
				 System.Data.SqlClient.SqlCommand cmd = null;
				 
				 try
				  {
				   con = new System.Data.SqlClient.SqlConnection (connectionString);
				   con.Open ();

				   cmd = new System.Data.SqlClient.SqlCommand ();
				   cmd.Connection = con;

				   ISelect sel = Select.create (cmd, con, tableName: tableName, useAllFields: useAllFields, tableAcr: tableAcr); 
				   cmd = null; con = null; return (sel);
				  }
				 finally
				  {
				   try { if (cmd != null) cmd.Dispose (); }
				   finally { if (con != null) con.Dispose (); }
				  }
				}
	 }
   }
 }
