﻿namespace Shark.Web.HttpProtocol.Creator
{
	/// <summary>
	/// Reprezentuje byte soubor pro download/upload
	/// </summary>
	class PostedFile : IPostedFile
	{
		#region Implementation of IPostedFile

		/// <summary>
		/// Typ souboru (mime)
		/// </summary>
		public string mimeType { get; private set; }

		/// <summary>
		/// Název souboru
		/// </summary>
		public string name { get; private set; }

		/// <summary>
		/// Zkrácený název
		/// </summary>
		public string shortName { get; private set; }

		/// <summary>
		/// Data souboru (byte)
		/// </summary>
		public byte[] data { get; private set; }

		#endregion

		#region constructors

		/// <summary>
		/// Interní konstruktor pro práci se soubory
		/// </summary>
		/// <param name="name">Název souboru</param>
		/// <param name="mimeType">Typ souboru (mime)</param>
		/// <param name="data">Obsah souboru (byty)</param>
		/// <param name="shortname">Zkrácený název souboru (pokud není nastaven, použije se plný název)</param>
		internal PostedFile(string name, string mimeType, byte[] data, string shortname = null)
		{
			this.name = name;
			this.mimeType = mimeType;
			this.data = data;
			this.shortName = (shortName ?? name);
		}
		#endregion
	}
}
