﻿using System.Collections.Generic;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Create new line of menu
	/// </summary>
	internal class MultiLevelMenuFiller : MenuFiller, IMultiLevelMenuFiller, IMenuItemCreator
	{
		#region protected fields

		/// <summary>
		/// Control for rendering html
		/// </summary>
		protected IControl Control;

		/// <summary>
		/// Nested level
		/// </summary>
		protected int Level;

		/// <summary>
		/// Container with menu item description
		/// </summary>
		protected MenuItemContainer Container;

		#endregion

		/// <summary>
		/// Create new line of menu
		/// </summary>
		/// <param name="subcreator">Creator for creating components</param>
		/// <param name="control">Control for rendering html</param>
		/// <param name="container">Container with menu item description</param>
		/// <param name="level">Nested level of submenu</param>
		internal MultiLevelMenuFiller(ICreator subcreator, IControl control, MenuItemContainer container, int level)
			: base(subcreator)
		{
			Control = control;
			Container = container;
			Level = level;
		}

		#region Implementation of IMultiLevelMenuFiller

		/// <summary>
		/// Add new submenu as new menuitem
		/// </summary>
		/// <param name="value"></param>
		/// <param name="label"></param>
		/// <param name="desc"></param>
		/// <param name="parm"></param>
		/// <returns></returns>
		public IMultiLevelMenuFiller addSubMenu(int value, string label, string desc = null, DMenuPermision parm = null)
		{
			MenuItemContainer container = new MenuItemContainer{Value = value, Label = label, Description = desc, Permission = parm};
			MultiLevelMenuFiller filler = new MultiLevelMenuFiller(subcreator: subCreator, control: Control, container: container, level: Level+1);
			children[value] = filler;

			return filler;
		}

		#endregion

		#region Implementation of IMenuItemCreator

		/// <summary>
		/// Create new Line menu
		/// </summary>
		/// <returns></returns>
		public IMenuItem create()
		{
			Dictionary<int, IMenuItem> items = new Dictionary<int, IMenuItem>();

			foreach (KeyValuePair<int, IMenuItemCreator> kvp in children)
			{
				items[kvp.Key] = kvp.Value.create();
			}

			return new LineMenu(creator: subCreator, control: Control, container: Container, level: Level, children: items);
		}

		#endregion
	}
}