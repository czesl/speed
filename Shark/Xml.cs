﻿
namespace Shark.Xml
 {
  public interface ICreator
   {
    string	encode (string txt);
	string	decode (string txt);

	string	formatDT (System.DateTime dt);

    IDocument	load (string fileName);
    IDocument	load (System.IO.Stream stream);
    IDocument	load (System.IO.TextReader reader);
    
    IDocument	parse (string xmlDocument);
    
    IValue				createValue (string val);
    IAttribute			createAttribute (string name, string val);
    IDeclarationCreator	createDeclaration (string name);
    IElementCreator		createElement (string name);
    IDocumentCreator	createDocument ();
   }

  public interface IWriter
   {
    void	write (string txt);
    void	writeLine (string txt);
   }

  public interface IItem			{ string	toStr (); }
  public interface IName : IItem	{ string	name	{ get; } }
  public interface IValue : IItem	{ string	val		{ get; } }
  public interface IAttribute : IName, IValue	{ }

  public interface IEnvelope
   {
    int			elementCount	{ get; }
    IElement	getElement (int order);
    
    IItem	findItem (string path);
    string	findValue (string path, string defValue = null);
	string	findAttr (string path, string attrName, string defValue = null);
    
    IElement[]	selectElements (string onPath, string elName);
   }

  public interface IDeclaration : IName
   {
    int			attributeCount	{ get; }
    IAttribute	getAttribute (int order);
   }

  public interface IElement : IEnvelope, IDeclaration
   {
    int		valueCount	{ get; }
    IValue	getValue (int order);
    string	values		{ get; }
    
    string	toStr (int level);
    void	write (IWriter w, int level);
   }

  public interface IDocument : IEnvelope, IItem
   {
    int				declarationCount	{ get; }
    IDeclaration	getDeclaration (int order);
    
    void	save2File (string fileName);
    void	write (IWriter w);
   }

  public interface IHasAttributeCreator	{ void	addAttribute (IAttribute att); }
  public interface IHasElementCreator	{ void	addElement (IElement el); }
  
  public interface IDeclarationCreator : IHasAttributeCreator
   { IDeclaration	create (); }

  public interface IElementCreator : IHasAttributeCreator, IHasElementCreator
   {
    void	addValue (IValue val);
    
    string		name	{ get; }
    IElement	create ();
   }

  public interface IDocumentCreator : IHasElementCreator
   {
    void	addDeclaration (IDeclaration decl);
    
    IDocument	create ();
   }

  public class Creator : ICreator
   {
    public static ICreator	getCreator ()
				{ return ((ms_Creator == null)? ms_Creator = new Creator (): ms_Creator); }
    private Creator ()	{ }
    private static ICreator	ms_Creator;

    private static string	enCode (string txt)	{ return (((txt == null)? null: System.Web.HttpUtility.HtmlEncode (txt))); }

    public string	encode (string txt)	{ return (enCode (txt)); }
	public string	decode (string txt)	{ return (((txt == null)? null: System.Web.HttpUtility.HtmlDecode (txt))); }

	public string	formatDT (System.DateTime dt)	{ return (dt.ToString ("yyyy-M-d\\THH:mm")); }
    
    public IDocument	load (string fileName)				{ return (load (new System.Xml.XmlTextReader (fileName))); }
    public IDocument	load (System.IO.Stream stream)		{ return (load (new System.Xml.XmlTextReader (stream))); }
    public IDocument	load (System.IO.TextReader reader)	{ return (load (new System.Xml.XmlTextReader (reader))); }
    
    public IDocument	parse (string xmlDocument)
				{
				 System.IO.StringReader sr = null;
				 
				 try		{ return (load (sr = new System.IO.StringReader (xmlDocument))); }
				 finally	{ if (sr != null) sr.Close (); }
				}
	
	private IDocument	load (System.Xml.XmlTextReader r)
				{
				 System.Collections.Stack st = new System.Collections.Stack ();
				 r.WhitespaceHandling = System.Xml.WhitespaceHandling.None;
				 IHasElementCreator currCreator = createDocument ();
				 
				 while (r.Read ())
				  switch (r.NodeType)
				   {
				    case System.Xml.XmlNodeType.Element:
				      {
				       IElementCreator cre = createElement (r.Name);
				       if (r.HasValue) cre.addValue (createValue (r.Value));
				       bool isEmpty = r.IsEmptyElement;
				       readAttributes (r, cre);
				       if (isEmpty) currCreator.addElement (cre.create ());
				       else { st.Push (currCreator); currCreator = cre; }
				       break;
				      }
				    case System.Xml.XmlNodeType.EndElement:
				      {
				       IElementCreator cre = (IElementCreator) currCreator;
				       if (r.Name != cre.name)
				        throw new System.ApplicationException ("Different tag (" + cre.name +
															 ") name and end tag (/" + r.Name + ") name!");
					   currCreator = (IHasElementCreator) (st.Pop ());
					   currCreator.addElement (cre.create ());
					   break;
				      }
				    case System.Xml.XmlNodeType.Text:
				      ((IElementCreator) currCreator).addValue (createValue (r.Value)); break;
				    case System.Xml.XmlNodeType.XmlDeclaration:
				      {
				       IDeclarationCreator cre = createDeclaration (r.Name); readAttributes (r, cre);
				       ((IDocumentCreator) currCreator).addDeclaration (cre.create ());
				       break;
				      }
				   }
				 r.Close (); return (((IDocumentCreator) currCreator).create ());
				}
	
	private void	readAttributes (System.Xml.XmlTextReader r, IHasAttributeCreator c)
				{
				 if (r.HasAttributes)
				  {
				   for (int i = 0; i < r.AttributeCount; i++)
				    {
				     r.MoveToAttribute (i);
				     c.addAttribute (createAttribute (r.Name, r.Value));
				    }
				   r.MoveToElement ();
				  }
				}
    
    public IValue				createValue (string val)					{ return (new Value (val)); }
    public IAttribute			createAttribute (string name, string val)	{ return (new Attribute (name, val)); }
    public IDeclarationCreator	createDeclaration (string name)				{ return (new DeclarationCreator (name)); }
    public IElementCreator		createElement (string name)					{ return (new ElementCreator (name)); }
    public IDocumentCreator		createDocument ()							{ return (new DocumentCreator ()); }

    private class TextWriter : IWriter
     {
      public TextWriter (System.IO.TextWriter writer)	{ m_Writer = writer; }
      System.IO.TextWriter	m_Writer;

      public void	write (string txt)		{ m_Writer.Write (txt); }
      public void	writeLine (string txt)	{ m_Writer.WriteLine (txt); }
     }
    
    private abstract class AValue
     {
      public AValue (string vl)	{ if ((m_Value = vl) != null) m_Value = vl.Trim (); }
	  public string	val	{ get { return (m_Value); } }	private string	m_Value;
     }
     
    private class Value : AValue, IValue
     {
      public Value (string vl)	: base (vl)	{ }
      public string	toStr ()	{ return (enCode (val)); }
     }
     
    private abstract class AAttribute : AValue
     {
      public AAttribute (string nm, string vl)	: base (vl)	{ m_Name = nm; }
	  public string	name	{ get { return (m_Name); } }	private string	m_Name;
     }
     
    private class Attribute : AAttribute, IAttribute
     {
      public Attribute (string nm, string vl)	: base (nm, vl)	{ }
      public string	toStr ()	{ return (enCode (name) + "=\"" + enCode (val) + "\""); }
     }
     
    private abstract class AEnvelope : IEnvelope
     {
      public AEnvelope (IElement[] elements)	{ m_Elements = elements; }
    
	  public int		elementCount	{ get { return ((m_Elements == null)? 0: m_Elements.Length); } }
      public IElement	getElement (int order)	{ return (m_Elements [order]); }
      private IElement[]	m_Elements;
    
      public IItem	findItem (string path)
				{
				 if (path == null || path == "") return ((this is IItem)? (IItem) this: null);
				 if (elementCount < 1) return (null); 
				 int pos = path.IndexOf ('\\');
				 
				 string name = ((pos < 0)? path: path.Substring (0, pos));
				 path = ((pos < 0 || pos >= path.Length - 1)? null: path.Substring (pos + 1));
				 
				 for (int i = 0; i < elementCount; i++)
				  if (getElement (i) != null)
				   if ((getElement (i)).name == name) return ((getElement (i)).findItem (path));
				 return (null);
				}
				
	  public string	findValue (string path, string defValue)
				{
				 IItem item = findItem (path);
				 if (item is IValue) return (((IValue) item).val);
				 else if (item is IElement) return (((IElement) item).values);
				 else return (defValue);
				}

	  public string	findAttr (string path, string attrName, string defValue)
				{
				 IItem item = findItem (path);

				 if (item is IElement)
				  {
				   IElement el = (IElement) item;
				   IAttribute atr;

				   for (int i = 0; i < el.attributeCount; i++)
				    if ((atr = el.getAttribute (i)) != null)
					 if (atr.name == attrName) return (atr.val);
				  }
				 
				 return (defValue);
				}

	  public IElement[]	selectElements (string onPath, string elName)
				{
				 IItem src = findItem (onPath); if (!(src is IEnvelope)) return (null);
				 IEnvelope env = (IEnvelope) src;
				 int i; int selCnt = 0; int cnt = env.elementCount;
				 for (i = 0; i < cnt; i++) if ((env.getElement (i)).name == elName) selCnt++;
				 
				 if (selCnt < 1) return (null);
				 IElement[] elArr = new IElement [selCnt]; selCnt = 0;
				 for (i = 0; i < cnt; i++)
				  if ((env.getElement (i)).name == elName) elArr [selCnt++] = env.getElement (i);
				 
				 return (elArr);
				}
     }
     
    private abstract class AElement : AEnvelope
     {
      public AElement (string nm, IElement[] elements, IAttribute[] attributes, IValue[] values)	: base (elements)
				{ m_Name = nm; m_Attributes = attributes; m_Values = values; }

      public string	name	{ get { return (m_Name); } }	private string	m_Name;

      public int		attributeCount	{ get { return ((m_Attributes == null)? 0: m_Attributes.Length); } }
      public IAttribute	getAttribute (int order)	{ return (m_Attributes [order]); }
      private IAttribute[]	m_Attributes;
    
      public int	valueCount	{ get { return ((m_Values == null)? 0: m_Values.Length); } }
      public IValue	getValue (int order)	{ return (m_Values [order]); }
      private IValue[]	m_Values;

      public string	values	{ get
							   {
							    int count = valueCount; string val = null;
								for (int i = 0; i < count; i++) val = ((i > 0)? val + "\n": "") + (getValue (i)).val;
				 
								return (val);
							   }
							}
				
	  protected string	attributes2Str ()
				{
				 string txt = "";
				 for (int i = 0; i < attributeCount; i++)
				  txt = txt + " " + (getAttribute (i)).toStr ();
				 return (txt);
				}
     }
    
    private class Element : AElement, IElement
     {
      public Element (string nm, IElement[] elements, IAttribute[] attributes, IValue[] values)
				: base (nm, elements, attributes, values)	{ }
    
      public string	toStr ()	{ return (toStr (0)); }
      public string	toStr (int level)
				{ 
				 System.IO.StringWriter sw = new System.IO.StringWriter ();
				 write (new TextWriter (sw), level);
				 return (sw.ToString ());
				}
      public void	write (IWriter w, int level)
				{
				 string me = "<" + enCode (name);
				 string levelPrefix = "";
				 int i, count;
				 for (i = 0; i < level; i++) levelPrefix = levelPrefix + "  ";
				 
				 level++; me = me + attributes2Str ();
				  
				 if (valueCount == 0 && elementCount == 0)
				  { w.writeLine (levelPrefix + me + "/>"); return; }
				 w.writeLine (levelPrefix + me + ">");
				 
				 count = valueCount; if (elementCount < count) count = elementCount;
				 for (i = 0; i < count; i++)
				  {
				   w.writeLine (levelPrefix + "  " + (getValue (i)).toStr ());
				   (getElement (i)).write (w, level);
				  }
				 if (count < valueCount)
				  for (i = count; i < valueCount; i++) w.writeLine (levelPrefix + "  " + (getValue (i)).toStr ());
				 else if (count < elementCount)
				  for (i = count; i < elementCount; i++) (getElement (i)).write (w, level);
				 w.writeLine (levelPrefix + "</" + enCode (name) + ">");
				}
     }
     
    private class Declaration : AElement, IDeclaration
     {
      public Declaration (string nm, IAttribute[] attributes)	: base (nm, null, attributes, null)	{ }
      public string	toStr ()	{ return ("<?" + name + attributes2Str () + "?>"); }
     }
     
    private class Document : AEnvelope, IDocument
     {
      public Document (IDeclaration[] declarations, IElement[] elements)	: base (elements)
				{ m_Declarations = declarations; }
     
      public int			declarationCount	{ get { return ((m_Declarations == null)? 0: m_Declarations.Length); } }
      public IDeclaration	getDeclaration (int order)	{ return (m_Declarations [order]); }
      private IDeclaration[]	m_Declarations;

      public void	save2File (string fileName)
				{ System.IO.TextWriter tw = new System.IO.StreamWriter (fileName); write (new TextWriter (tw)); tw.Close (); }
      public string	toStr ()
				{ System.IO.TextWriter sw = new System.IO.StringWriter (); write (new TextWriter (sw)); return (sw.ToString ()); }
      public void	write (IWriter w)
				{
				 int i;
				 for (i = 0; i < declarationCount; i++) w.writeLine ((getDeclaration (i)).toStr ());
				 for (i = 0; i < elementCount; i++) (getElement (i)).write (w, 0);
				}
     }
     
    private abstract class ACreator
     {
      public ACreator (string nm = null)	{ m_Name = nm; m_Attributes = null; m_Decls = null; m_Elems = null; m_Values = null; }
      
      public string	name	{ get { return (m_Name); } }	private string	m_Name;
      
      public void	addAttribute (IAttribute att)
				{
				 if (m_Attributes == null) m_Attributes = new System.Collections.SortedList ();
				 m_Attributes.Add (att.name, att);
				}
      protected IAttribute[]	getAttributes ()
				{
				 if (m_Attributes == null) return (null);
				 int cnt = m_Attributes.Count; if (cnt < 1) return (null);
				 
				 IAttribute[] att = new IAttribute [cnt]; int i = 0;
				 foreach (IAttribute a in m_Attributes.GetValueList ()) att [i++] = a;
				 return (att);
				}
      private System.Collections.SortedList	m_Attributes;
      
      public void	addDeclaration (IDeclaration decl)	{  addName (ref m_Decls, decl); }
      protected IDeclaration[]	getDeclarations ()
				{
				 int cnt = countNames (m_Decls); if (cnt < 1) return (null);
				 
				 IDeclaration[] decls = new IDeclaration [cnt]; int i = 0;
				 foreach (System.Collections.Queue itm in m_Decls.GetValueList ())
				  foreach (IDeclaration d in itm) decls [i++] = d;
				 return (decls);
				}
      private System.Collections.SortedList	m_Decls;
      
      public void	addElement (IElement el)	{  addName (ref m_Elems, el); }
      protected IElement[]	getElements ()
				{
				 int cnt = countNames (m_Elems); if (cnt < 1) return (null);
				 
				 IElement[] elems = new IElement [cnt]; int i = 0;
				 foreach (System.Collections.Queue itm in m_Elems.GetValueList ())
				  foreach (IElement e in itm) elems [i++] = e;
				 return (elems);
				}
      private System.Collections.SortedList	m_Elems;
	  
	  private static void	addName (ref System.Collections.SortedList list, IName el)
				{
				 if (list == null) list = new System.Collections.SortedList ();
				 System.Collections.Queue itm;
				 
				 if (list.ContainsKey (el.name)) itm = (System.Collections.Queue) (list [el.name]);
				 else { itm = new System.Collections.Queue (); list.Add (el.name, itm); }
				 
				 itm.Enqueue (el);
				}
	  private static int	countNames (System.Collections.SortedList list)
				{
				 if (list == null) return (0);
				 int cnt = 0; foreach (System.Collections.Queue itm in list.GetValueList ()) cnt += itm.Count;
				 
				 return (cnt);
				}
	  
	  public void	addValue (IValue val)
				{
				 if (m_Values == null) m_Values = new System.Collections.Generic.List<IValue> ();
				 m_Values.Add (val);
				}
	  protected IValue[]	getValues ()	{ return ((m_Values == null)? null: m_Values.ToArray ()); }
	  private System.Collections.Generic.List<IValue>	m_Values;
     }
     
    private class DeclarationCreator : ACreator, IDeclarationCreator
     {
      public DeclarationCreator (string nm)	: base (nm)	{ }
      
      public IDeclaration	create ()	{ return (new Declaration (name, getAttributes ())); }
     }
     
    private class ElementCreator : ACreator, IElementCreator
     {
      public ElementCreator (string nm)	: base (nm)	{ }

      public IElement	create ()	{ return (new Element (name, getElements (), getAttributes (), getValues ())); }
     }
     
    private class DocumentCreator : ACreator, IDocumentCreator
     {
      public DocumentCreator ()	: base ()	{ }
      
      public IDocument	create ()	{ return (new Document (getDeclarations (), getElements ())); }
     }
   }
 }
