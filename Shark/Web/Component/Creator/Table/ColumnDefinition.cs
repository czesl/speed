﻿namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Class defines table column
	/// </summary>
	internal class ColumnDefinition
	{
		/// <summary>
		/// Columns Label
		/// </summary>
		public string label { get; set; }

		/// <summary>
		/// Description of column
		/// </summary>
		public string desc { get; set; }

		/// <summary>
		/// Colspan value
		/// </summary>
		public int nCols { get; set; }

		/// <summary>
		/// Horizontal alignment
		/// </summary>
		public EControlAlignment horzAlign { get; set; }
	}
}