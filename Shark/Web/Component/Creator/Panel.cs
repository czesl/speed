﻿using System;
using Shark.Web.Component;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Component for rendering panel
	/// </summary>
	internal class Panel : Component
	{
		#region private fields

		/// <summary>
		/// Component which will be rendered inside of panel
		/// </summary>
		private readonly IComponent _component;

		/// <summary>
		/// Horizontal alignment of subcomponent in panel
		/// </summary>
		private readonly EControlAlignment _horizontalAlignment;

		/// <summary>
		/// Vertical alignment of subcomponent in panel
		/// </summary>
		private readonly EControlAlignment _verticalAlignment;
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		/// <param name="comp">Component which will be rendered in panel</param>
		/// <param name="horzAlign">Horizontal alignment</param>
		/// <param name="vertAlign">Vertical alignment</param>
		public Panel(ICreator creator, IControl control, IComponent comp, EControlAlignment horzAlign = EControlAlignment.CA_Both, EControlAlignment vertAlign = EControlAlignment.CA_Both) : base(creator, control)
		{
			if (comp != null)
			{
				_subcomponents.Add(comp);
			}

			_component = comp;
			_verticalAlignment = vertAlign;
			_horizontalAlignment = horzAlign;
		}
		#endregion


		#region Overrides of Component
		
		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		/// <exception cref="NullReferenceException">Exception thrown when there is invalid <see cref="Creator"/> object assigned to this component</exception>
		public override void generate()
		{
			string align, width, valign, height;

			AlignmentConvertor.ToHtmlHorizontalString(alignment: _horizontalAlignment, align: out align,width: out width);
			AlignmentConvertor.ToHtmlVerticalString(alignment: _verticalAlignment, valign: out valign, height: out height);

			IControl control = _control;

			control.table(border: "0");
			control.tr();
			control.td(align: align, width: width, vAlign: valign, height: height);
			
			if(_component != null)
			{
				_component.generate();
			}
			else
			{
				control.write("&nbsp;");
			}

			control.tdEnd();
			control.trEnd();
			control.tableEnd();
		}
		#endregion
	}
}

// Generated HTML:
//
// <table border="0">
//   <tr>
//     <td align="[horzAlign]" valign="[vetAlign]">
//       [SubComponent]
//     </td>
//   </tr>
// </table>
//
