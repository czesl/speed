﻿
using System.Collections.Generic;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Create simple menu object
	/// </summary>
	internal class MenuComponentCreator : MenuFiller, IMenuComponentCreator
	{
		#region private fields

		/// <summary>
		/// COntrol for rendering html
		/// </summary>
		private IControl _control;

		#endregion

		#region constructors

		/// <summary>
		/// Create new creator and set his parameter
		/// </summary>
		/// <param name="subcreator"></param>
		/// <param name="control"></param>
		internal MenuComponentCreator(ICreator subcreator, IControl control) : base(subcreator)
		{
			_control = control;
		}

		#endregion

		#region Implementation of IMenuComponentCreator

		/// <summary>
		/// Create simple menu object
		/// </summary>
		/// <returns></returns>
		public IMenu create()
		{
			Dictionary<int, IMenuItem> items = new Dictionary<int, IMenuItem>();

			foreach (KeyValuePair<int, IMenuItemCreator> kvp in children)
			{
				items[kvp.Key] = kvp.Value.create();
			}

			return new Menu(creator: subCreator, control: _control, children: items);
		}

		#endregion
	}
}