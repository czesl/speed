﻿using System.Web.Services;

namespace Shark.Speed.WebService.Feat.Feat2Speed
 {
	/// <summary>
	/// Summary description for Feat2Speed
	/// </summary>
  [WebService (Name = "Feat2SpeedWebService", Description = "Feat to Speed communication web service", Namespace = "http://speed.ufal.jager.cz/")]
  [WebServiceBinding (ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]

  public class Feat2Speed : System.Web.Services.WebService
   {
	private Database.SQL.IEngine	createSQLEngine ()
				{
				 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
				 return (BI.Bi.createMsSQLDatabaseEngine (conString));
				}
	
	[WebMethod (Description = "Opens Feat 2 Speed communication, creates session id")]
	public CreateSessionWebAnswer	createSession (string userName, string passWd)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new CreateSessionWebAnswer (call.createSession (userName: userName, passWd: passWd, callUrl: Context.Request.UserHostName)));
				  }
				}

	[WebMethod (Description = "Closes Feat 2 Speed communication, closes session id")]
	public WebAnswer	closeSession (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.closeSession (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				} 

	[WebMethod (Description = "Reads user inbox file names")]
	public CheckUserInboxAnswer	checkInbox (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new CheckUserInboxAnswer (call.checkUserInbox (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				}
	
	[WebMethod (Description = "Reads user inbox object identifications")]
	public UserInboxHeaderWebAnswer	getUserInboxHeader (string sessionId, bool is2ReadAnyFile)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new UserInboxHeaderWebAnswer (call.getUserInboxHeader (sessionId: sessionId, callUrl: Context.Request.UserHostName, is2ReadAnyFile: is2ReadAnyFile)));
				  }
				}
	
	[WebMethod (Description = "Reads one user inbox object with indetification taskId")]
	public PeekFileWebAnswer	peekFile (string sessionId, string taskId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new PeekFileWebAnswer (call.peekFile (sessionId: sessionId, callUrl: Context.Request.UserHostName, taskId: taskId)));
				  }
				}

	[WebMethod (Description = "Tells to speed that Feat has correctly read input file")]
	public WebAnswer	fileRead (string sessionId, string taskId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.fileRead (sessionId: sessionId, callUrl: Context.Request.UserHostName, taskId: taskId)));
				  }
				}

	[WebMethod (Description = "Feat saves outbox file")]
	public WebAnswer	saveFile (string sessionId, WebFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.saveFile (sessionId: sessionId, callUrl: Context.Request.UserHostName, file: file)));
				  }
				}
   }

  public interface IWebAnswer
   {
    int		status	{ get; set; }
	string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class WebAnswer : IWebAnswer
   {
    public WebAnswer ()	{ }
	public WebAnswer (IAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				}
	
    public int		status	{ get; set; }
	public string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class CreateSessionWebAnswer : IWebAnswer
   {
    public CreateSessionWebAnswer ()	{ }
    public CreateSessionWebAnswer (ICreateSessionAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				 sessionId = ans.sessionId;
				}
	
	public int		status		{ get; set; }
	public string	errMsg		{ get; set; }
    public string	sessionId	{ get; set; }
   }

  [System.Serializable]
  public class CheckUserInboxAnswer : IWebAnswer
   {
    public CheckUserInboxAnswer ()	{ }
    public CheckUserInboxAnswer (ICheckUserInboxAnswer ans)
				{
				 status            = (int) ans.isOk;
				 errMsg            = ans.errMsg;
				 inboxFileNameList = ans.inboxFileNameList;
				}
    
	public int		status				{ get; set; }
	public string	errMsg				{ get; set; }
    public string[]	inboxFileNameList	{ get; set; }
   }

  [System.Serializable]
  public class UserInboxHeaderWebAnswer : IWebAnswer
   {
    public UserInboxHeaderWebAnswer ()	{ }
    public UserInboxHeaderWebAnswer (IUserInboxHeaderAnswer ans)
				{
				 status          = (int) ans.isOk;
				 errMsg          = ans.errMsg;
				 inboxTaskIdList = ans.inboxTaskIdList;
				}
    
	public int		status			{ get; set; }
	public string	errMsg			{ get; set; }
    public string[]	inboxTaskIdList	{ get; set; }
   }

  [System.Serializable]
  public class PeekFileWebAnswer : IWebAnswer
   {
    public PeekFileWebAnswer ()	{ }
    public PeekFileWebAnswer (IPeekFileAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				 file      = new WebFile (ans.file);
				}
    
	public int		status	{ get; set; }
	public string	errMsg	{ get; set; }
    public WebFile	file	{ get; set; }
   }

  [System.Serializable]
  public class WebFile : IFile
   {
    public WebFile ()	{ }
    public WebFile (IFile fl)
				{
				 name       = fl.name;
				 isSpecFile = false;
				 cmdFile    = fl.cmdFile;
				 htmlFile   = fl.htmlFile;
				 xmlWFile   = fl.xmlWFile;
				 xmlAFile   = fl.xmlAFile;
				 xmlBFile   = fl.xmlBFile;
				}

    public string	name		{ get; set; }
	public bool		isSpecFile	{ get; set; }
	public string	cmdFile		{ get; set; }
	public string	htmlFile	{ get; set; }
	public string	xmlWFile	{ get; set; }
	public string	xmlAFile	{ get; set; }
	public string	xmlBFile	{ get; set; }
   }
 }
