﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Shark.Web.Component.Creator
{
	internal static class AlignmentConvertor
	{
		/// <summary>
		/// Vertical alignment
		/// Convert enum <see cref="EControlAlignment"/> to html attribute valign
		/// </summary>
		/// <param name="alignment">Vertical alignment</param>
		/// <param name="valign">HTML attribute valign</param>
		/// <param name="height">HTML attribute width</param>
		internal static void ToHtmlVerticalString(EControlAlignment alignment, out string valign, out string height)
		{
			valign = null;
			height = null;

			switch (alignment)
			{
				case EControlAlignment.CA_Top:
					valign = "top";
					break;
				case EControlAlignment.CA_Middle:
					valign = "middle";
					break;
				case EControlAlignment.CA_Bottom:
					valign = "bottom";
					break;
				case EControlAlignment.CA_Both:
					valign = "middle";
					height = "100%";
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>
		/// Horizontal alignment
		/// Convert enum <see cref="EControlAlignment"/> to html attribute align
		/// </summary>
		/// <param name="alignment">Horizontal alignment</param>
		/// <param name="align">HTML attribute align</param>
		/// <param name="width">HTML attribute width</param>
		internal static void ToHtmlHorizontalString(EControlAlignment alignment, out string align, out string width)
		{
			align = null;
			width = null;

			switch (alignment)
			{
				case EControlAlignment.CA_Left:
					align = "left";
					break;
				case EControlAlignment.CA_Middle:
					align = "center";
					break;
				case EControlAlignment.CA_Right:
					align = "right";
					break;
				case EControlAlignment.CA_Both:
					align = "center";
					width = "100%";
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}