﻿using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Component for rendering specified HTML code
	/// </summary>
	internal class HtmlComp : Component
	{
		#region private fields

		/// <summary>
		/// Html code to be rendered by this component
		/// </summary>
		private readonly string _htmlCode;
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		/// <param name="htmlCode">Html code to be rendered by this component</param>
		public HtmlComp(ICreator creator, IControl control, string htmlCode) : base(creator, control)
		{
			_htmlCode = htmlCode ?? "";
		}
		#endregion


		#region Overrides of Component

		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		public override void generate()
		{
			_control.write(_htmlCode);
		}
		#endregion
	}
}