﻿using System;
using System.Collections.Generic;
using System.Web;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Menu with submenu or others menuitems
	/// </summary>
	internal class Menu : LineMenu, IMenu
	{
		#region Implementation of IMenu

		/// <summary>
		/// Create new Menu and set his parameters
		/// </summary>
		/// <param name="creator">Component Creator</param>
		/// <param name="control">Control for creating html output</param>
		/// <param name="children">Submenu menu items</param>
		internal Menu(ICreator creator, IControl control, Dictionary<int, IMenuItem> children) : base(creator: creator, control: control, children: children, level: 0, container: null)
		{
		}

		public int subMenuListValue()
		{
			throw new NotImplementedException();
		}

		public IComponent subMenuListComp()
		{
			throw new NotImplementedException();
		}

		public void setMenuListValue(int value)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}