﻿namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Interface for creating menu items
	/// </summary>
	internal interface IMenuItemCreator
	{
		/// <summary>
		/// Create menu item
		/// </summary>
		/// <returns>New menu item</returns>
		IMenuItem create();
	}
}
