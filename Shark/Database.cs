﻿
namespace Shark.Database
 {
  public enum EDateTimeType { DTT_Full, DTT_Date, DTT_Time }
  
  public interface ILangTranslation
   {
    int		language	{ get; }
    
    string	translate (string txtCode, string defValue);

	long			longNullValue		{ get; }
	int				intNullValue		{ get; }
	short			shortNullValue		{ get; }
	bool			boolNullValue		{ get; }
	System.DateTime	dtNullValue			{ get; }
	string			stringNullValue		{ get; }
	byte[]			byteArrNullValue	{ get; }

	bool	isNull (long l);
	bool	isNull (int i);
	bool	isNull (short s);
	bool	isNull (bool b);
	bool	isNull (System.DateTime dt);
	bool	isNull (string txt);
	bool	isNull (byte[] arr);

	string	format (long l);
	string	format (int i);
	string	format (bool b);
	string	format (System.DateTime dt, EDateTimeType type = EDateTimeType.DTT_Full);

	//raises exeption
	long			scanLong (string val);
	int				scanInt (string val);
	short			scanShort (string val);
	bool			scanBool (string val);
	System.DateTime	scanDT (string val, EDateTimeType type = EDateTimeType.DTT_Full);

	long			tryScanLong (string val, long defValue = 0);
	int				tryScanInt (string val, int defValue = 0);
   }
   
  public interface IDateTimeZone
   {
    System.DateTime	zone2Server (System.DateTime dt);
    System.DateTime	server2Zone (System.DateTime dt);
   }
  
  public enum EFieldType { FT_BigInt, FT_Int, FT_Short, FT_Bool, FT_DateTime, FT_Text, FT_BLOB }

  public interface IFormatValue
   {
    bool	isNull	{ get; }
    string	format	{ get; set; }
    
    string	present (ILangTranslation trans);
   }
  
  public interface IBigIntValue	: IFormatValue	{ long	val { get; set; } }
  public interface IIntValue	: IFormatValue	{ int	val { get; set; } }
  public interface IShortValue	: IFormatValue	{ short	val { get; set; } }
  public interface IBoolValue	: IFormatValue	{ bool	val { get; set; } }
  
  public interface IDTValue : IFormatValue
   {
    EDateTimeType	dateType	{ get; }
    System.DateTime	val			{ get; set; }

	System.DateTime	this [IDateTimeZone zoneOuterOfValue]	{ get; set; }
    
    string	present (ILangTranslation trans = null, IDateTimeZone zone4Value = null);
	void	validate (string vl, ILangTranslation trans = null, IDateTimeZone zone4Value = null);
    
    void	setUp (string formated, ILangTranslation trans = null, IDateTimeZone zone = null);
    void	setUp (string formated, IDateTimeZone zone);
    void	setUp (System.DateTime dt, IDateTimeZone zone);
   }

  public interface ITxtValue	: IFormatValue	{ string	val { get; set; } }
  public interface IBLOBValue	: IFormatValue	{ byte[]	val { get; set; } }
  
  public interface IValue : IFormatValue
   {
    EFieldType		type	{ get; }
	
	IValue	createCopy ();
	string	createParam (SQL.IParamBuilder parBld);
	void	setNull ();
	void	readFetch (SQL.IFetch fetch, string fieldName);
    
    IBigIntValue	bg	{ get; }
    IIntValue		it	{ get; }
    IShortValue		sh	{ get; }
	IBoolValue		bl	{ get; }
    IDTValue		dt	{ get; }
    ITxtValue		txt	{ get; }
	IBLOBValue		blb	{ get; }
   }
  
  //OPTI public delegate void	DFieldChange (IField fld, IRecord rec, IValue prevVal);
  
  public interface IField
   {
    IQuery		query		{ get; }
    string		name		{ get; }
	int			fldIndex	{ get; }
    EFieldType	type		{ get; }
	EFieldRole	role		{ get; }
    bool		readOnly	{ get; }

	IReference	referenceField	{ get; }
	IOption		optionField		{ get; }
	IText		textField		{ get; }
    
    IValue	createNull ();
	IValue	val (IRecord rec);
    
    //OPTI event DFieldChange	onAfterSet;
    //OPTI event DFieldChange	onAfterValidate;
   }
   
  public enum ESelectJoin	{ SJ_Join, SJ_LeftJoin }

  public interface ISelectJoinCond
   {
    string	leftField	{ get; }
	string	rightField	{ get; }

	SQL.IJoinCond	create (SQL.ISelect sel);
   }

  public interface ISelectCreator
   {
    string	srcQueryAcr	{ get; }

	string			createQueryAcr (string queryName);
	ISelectJoinCond	createJoinCond (string leftFldName, string rightFldName, string leftQrAcr = null, string rightQrAcr = null);

	void	addSelectSource (string queryName, string queryAcr, ISelectJoinCond[] joinCond, ESelectJoin join = ESelectJoin.SJ_LeftJoin);
	void	addSelectSource (string queryName, string queryAcr, ISelectJoinCond joinCond, ESelectJoin join = ESelectJoin.SJ_LeftJoin);

	void	addSelectField (string queryAcr, string fieldName, string fieldAcr = null);
   }
  
  public interface IReferenceDefinition
   {
    void	defineReferenceFields (ITableCreator tc, IField refField);

	void	fillSelectInfo (ISelectCreator selCre);
	
	ITable	masterTable		{ get; }
	string	nameFieldName	{ get; }	// field name in detail record - could be null
	string	descFieldName	{ get; }	// field name in detail record - could be null

	long	getMasterRecIdVal (IRecord rec);
	string	getMasterRecordId (IRecord rec);
	IRecord	getMasterRecord (IRecord rec);

    IFinder	createMasterFinder (IRecord rec, string masterTableAcr = null);
   }
  
  public interface IReference : IField, IReferenceDefinition	{ }

  public interface IOptionValue
   {
    short	val { get; }
	string	txt	{ get; }

	string	present (ILangTranslation trans);
   }
  
  public interface IOption : IField	{ IOptionValue[]	options { get; } }
  public interface IText : IField	{ int	maxLen	{ get; } }
   
  public interface IRecord
   {
	long	recIdVal	{ get; }
    string	recordId	{ get; }
    
    IValue	val (IField fld);
    IValue	val (IQuery query, string name);
    IValue	this [int index]	{ get; }

	IRecord	createCopy ();

    void	insert (ITable table, ITransaction trans);
    void	update (ITable table, ITransaction trans);
    void	delete (ITable table, ITransaction trans);
   }

  public interface IClose : System.IDisposable	{ void	close (); }
  
  public interface IRecordSet : IClose
   {
    IRecord	currRec	{ get; }
    
    IRecord	createCopy (IRecord srcRec = null);	// on null copies curr record
    
    bool	moveNext ();
   }

  public interface IRecordSet<Type> : IRecordSet where Type: IObject
   {
    Type	currObj	{ get; }

    Type	createObjCopy (Type srcObj);	// on null copies curr object
	Type	createObjCopy ();
   }
   
  //OPTI public delegate void	DRecordChange (ITransaction trans, ITable table, IRecord rec, IRecord xRec);
 
  public interface IQuery
   {
    string	name	{ get; }

	IField	primaryKey	{ get; }
	IField	codeField	{ get; }	// could be null
	IField	descField	{ get; }	// could be null
    string	getCode (IRecord rec);
    string	getDesc (IRecord rec);
    
    IField	this [string name]	{ get; }
    IField	this [int index]	{ get; }

	int	fieldCount	{ get; }
	int	getFieldIndex (IField fld);
	int	getFieldIndex (string fldName);
    
    IRecord getRecord (long id, bool forChange = false);
    IRecord getRecord (string id, bool forChange = false);
    
    IFinder			createFinder ();
	ICondBuilder	createCondBuilder ();

	IRecordSet	find (IFinder finder);
    IRecordSet	find (ICondition cond, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (string fldName, long val, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (string fldName, int val, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (string fldName, short val, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (string fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (string fldName, string val, bool refCodeRead = true, bool forChange = false);
    IRecordSet	find (bool refCodeRead = true, bool forChange = false);
   }
   
  public interface IQuery<Type> : IQuery where Type: IObject
   {
    Type getObject (long id, bool forChange = false);
    Type getObject (string id, bool forChange = false);

    IRecordSet<Type>	findObj (IFinder finder);
    IRecordSet<Type>	findObj (ICondition cond, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (string fldName, long val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (string fldName, int val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (string fldName, short val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (string fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (string fldName, string val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (bool refCodeRead = true, bool forChange = false);
   }
  
  public interface ITable : IQuery
   {
	IRecord	createNew ();

    void	insert (ITransaction trans, IRecord rec);
    void	update (ITransaction trans, IRecord rec);
    void	delete (ITransaction trans, IRecord rec);
    
    //OPTI event	DRecordChange	onInitNew;
	//OPTI event	DRecordChange	onBeforeInsert;
    //OPTI event	DRecordChange	onAfterInsert;
    //OPTI event	DRecordChange	onBeforeUpdate;
    //OPTI event	DRecordChange	onAfterUpdate;
    //OPTI event	DRecordChange	onBeforeDelete;
    //OPTI event	DRecordChange	onAfterDelete;
   }

  //OPTI public delegate void	DRecordChange<Type> (ITransaction trans, ITable<Type> table, Type obj, Type xObj);

  public interface ITable<Type> : ITable, IQuery<Type> where Type: IObject
   {
	Type	createNewObj ();

    void	insert (ITransaction trans, Type obj);
    void	update (ITransaction trans, Type obj);
    void	delete (ITransaction trans, Type obj);

    //OPTI event	DRecordChange<Type>	onInitNewObj;
	//OPTI event	DRecordChange<Type>	onBeforeInsertObj;
    //OPTI event	DRecordChange<Type>	onAfterInsertObj;
    //OPTI event	DRecordChange<Type>	onBeforeUpdateObj;
    //OPTI event	DRecordChange<Type>	onAfterUpdateObj;
    //OPTI event	DRecordChange<Type>	onBeforeDeleteObj;
    //OPTI event	DRecordChange<Type>	onAfterDeleteObj;
   }

  public interface ITable<Type, EField> : ITable<Type> where Type: IObject
   {
    string	getFieldName (EField fld);
	string	getFullFieldName (EField fld);

    IRecordSet<Type>	findObj (EField fldName, long val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (EField fldName, int val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (EField fldName, short val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (EField fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false);
    IRecordSet<Type>	findObj (EField fldName, string val, bool refCodeRead = true, bool forChange = false);
   }
   
  public interface ITransaction : IClose
   {
    bool	finalyOk	{ get; set; }

	SQL.IInsert	insert (string tableName = null);
	SQL.IUpdate	update (string tableName = null);
	SQL.IDelete	delete (string tableName = null);
	SQL.ISelect	select (string tableName = null, bool useAllFields = false, string tableAcr = null);
    
    void	commit ();
   }
   
  public interface ICondition
   { void fillBuilder (SQL.ICondBuilder cndBuilder); }
  
  public interface ISortByField
   {
    string	fldName	{ get; }
    bool	asc		{ get; }
   }
  
  public interface IFinder
   {
	ICondBuilder	createCondBuilder ();
	ISortByField	createSortByField (string fldName, bool asc = true);

    void	add (ICondition cond);
    void	add (string fldName, long val);
    void	add (string fldName, int val);
    void	add (string fldName, short val);
    void	add (string fldName, bool val);
    void	add (string fldName, System.DateTime val);
    void	add (string fldName, string val);
    
    void	sortBy (ISortByField[] flds);
    void	sortBy (ISortByField fld);
    
    bool	is4Change		{ get; set; }
	bool	isRefCodeRead	{ get; set; }
	long	maxRows			{ get; set; }	// -1 means every

	void fillBuilder (SQL.ISelect selBuilder);
   }

  public enum ECondOper { CO_Eq, CO_NEq, CO_Lt, CO_Le, CO_Gt, CO_Ge, CO_Like }

  public interface ICondBuilder
   {
    string	createTableAcr ();
	string	fullFieldName (string tableAcr, string fieldName);

	void	addIsNull (string leftSide);
	void	addCond (string leftSide, string rightSide, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, long vl, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, int vl, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, short vl, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, bool vl, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, System.DateTime vl, ECondOper oper = ECondOper.CO_Eq);
	void	addValCond (string leftSide, string vl, ECondOper oper = ECondOper.CO_Eq);

	ICondBuilder	addNotCond ();
	ICondBuilder	addAndCond ();
	ICondBuilder	addOrCond ();

	ICondBuilder	addExists (string tableName, string tableField, string tableAcr = null, string tableCondFld = null, string parentValueSide = null);
	
	ICondition	create ();
   }

  public enum EFieldRole	{ FR_Normal, FR_Autoincrement, FR_Primary, FR_Code, FR_Desc, FR_ReferenceInfo }

  public enum EJoinCondType	{ JCT_Field, JCT_Value }

  public interface IJoinCond
   {
	string			masterFldName	{ get; }

    EJoinCondType	type	{ get; }
	IJoinFieldCond	fldCnd	{ get; }
	IJoinValueCond	vlCnd	{ get; }
   }

  public interface IJoinFieldCond : IJoinCond
   { string	fieldName	{ get; } }

  public interface IJoinValueCond : IJoinCond
   { IValue val	{ get; } }

  public interface IObject	{ IRecord	record	{ get; } }
  
  public interface IRecordLoader<Type>
   {
	Type	nullValue	{ get; }
    Type	create4Rec (IRecord rec);

	void	loadData (Type toObj, IRecord fromRec = null);	// on fromRec == null use inside define record
   }
  
  public interface ITypeConverter<Type> : IRecordLoader<Type>
   { IRecord	saveData (Type fromObj); }

  public delegate string	DFieldName<EField> (EField fldName);

  public interface ITableCreator
   {
    string	name	{ get; set; }

	void				addField (string name, EFieldType type, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	void				addPrimaryField (string name, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_Autoincrement);
	void				addReferenceField (string name, IReferenceDefinition refDel, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	IReferenceFiller	addReferenceField (string name, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null,
										 EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	IReferenceFiller<Type>	addReferenceField<Type> (string name, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null,
												   EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)  where Type: IObject;
	void				addReferenceField (string name, ITable masterTable, string masterFldName, IJoinCond[] lookUpCond = null, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	void				addOptionField (string name, IOptionValue[] values, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	void				addBoolField (string name, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	void				addTextField (string name, int maxSize, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);
	void				addBLOBField (string name, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal);

	IFieldFiller		addSpecField (string name, EFieldType type, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_ReferenceInfo);
	IFieldFiller		addSpecTextField (string name, int maxSize, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_ReferenceInfo);

	IOptionValue	createOptionValue (short sh, string txt);
	IJoinCond		createJoinFldCond (string fldName, string masterFldName);
	IJoinCond		createJoinValCond (string masterFldName, IValue val);
	IJoinCond		createJoinValCond (string masterFldName, long val);
	IJoinCond		createJoinValCond (string masterFldName, int val);
	IJoinCond		createJoinValCond (string masterFldName, short val);
	IJoinCond		createJoinValCond (string masterFldName, bool val);
	IJoinCond		createJoinValCond (string masterFldName, System.DateTime val);
	IJoinCond		createJoinValCond (string masterFldName, string val);

	IQuery			createQuery ();
	IQuery<Type>	createQuery<Type> (IRecordLoader<Type> recLoader) where Type: IObject;
	ITable			createTable ();
	ITable<Type>	createTable<Type> (ITypeConverter<Type> converter) where Type: IObject;
	ITable<Type, EField>	createTable<Type, EField> (ITypeConverter<Type> converter, DFieldName<EField> fldFce) where Type: IObject;
   }

  public interface IReferenceFiller
   {
    void	setJoinCond (IJoinCond[] cnd);
    void	setLookUpCond (IJoinCond[] cnd);
    void	setReferenceTable (ITable master);
   }

  public interface IReferenceFiller<Type> : IReferenceFiller where Type: IObject
   { void	setReferenceTable (ITable<Type> master); }

  public interface IFieldFiller : IField
   { void	setName (string name); }
  
  public interface IFactory
   {
    string	fullFieldName (string tableAcr, string fieldName);

    ICondBuilder	condBuilder ();
	ISortByField	sortByField (string fldName, bool asc = true);
	IFinder			finder (string tableAcr);

	ITableCreator	tableCreator (string name = null);

	ITransaction	openTransaction ();
   }
 }
