﻿using System.Web.Services;

namespace Shark.Speed.WebService.Inicialize.InicializeSpeedByJacobsonFileData
 {
	/// <summary>
	/// Summary description for InicializeSpeedByJacobsonFileData
	/// </summary>
  [WebService (Name = "InicializeSpeedByJacobsonFileData", Description = "Inicialize Speed by Jacobson File Data communication web service", Namespace = "http://inicialize.speed.ufal.jager.cz/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]

  public class InicializeSpeedByJacobsonFileData : System.Web.Services.WebService
   {
	private Database.SQL.IEngine	createSQLEngine ()
				{
				 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
				 return (BI.Bi.createMsSQLDatabaseEngine (conString));
				}

	[WebMethod (Description = "Opens inicialization communication, creates session id")]
	public CreateSessionWebAnswer	createSession (string userName, string passWd)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new CreateSessionWebAnswer (call.createSession (userName: userName, passWd: passWd, callUrl: Context.Request.UserHostName)));
				  }
				}

	[WebMethod (Description = "Closes inicialization communication, closes session id")]
	public WebAnswer	closeSession (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.closeSession (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				} 

	[WebMethod (Description = "Makes inicialization")]
	public WebAnswer	createProcessPrideleno (string sessionId, long supervisorId, long anotatorId, InizializeWebFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.createProcessPrideleno (sessionId: sessionId, callUrl: Context.Request.UserHostName, supervisorId: supervisorId, anotatorId: anotatorId, file: file)));
				  }
				} 

	[WebMethod (Description = "Makes inicialization")]
	public WebAnswer	createProcessOdevzdano (string sessionId, long supervisorId, long anotatorId, InizializeWebFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.createProcessOdevzdano (sessionId: sessionId, callUrl: Context.Request.UserHostName, supervisorId: supervisorId, anotatorId: anotatorId, file: file)));
				  }
				} 

	[WebMethod (Description = "Makes inicialization")]
	public WebAnswer	createProcessDoAmesu (string sessionId, long supervisorId, long anotatorId, InizializeWebFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.createProcessDoAmesu (sessionId: sessionId, callUrl: Context.Request.UserHostName, supervisorId: supervisorId, anotatorId: anotatorId, file: file)));
				  }
				} 

	[WebMethod (Description = "Overwrites data")]
	public WebAnswer	overWriteProcessState (string sessionId, string callUrl, int stateNo, long proDtaId, long taskId, long supervisorId, long anotatorId, long adjudikatorId, InizializeWebFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.overWriteProcessState (sessionId: sessionId, callUrl: Context.Request.UserHostName, stateNo: stateNo, proDtaId: proDtaId, taskId: taskId,
																    supervisorId: supervisorId, anotatorId: anotatorId, adjudikatorId: adjudikatorId, file: file)));
				  }
				}

	[WebMethod (Description = "Reads user anotation Process Data identifications")]
	public Feat.Feat2Speed.UserInboxHeaderWebAnswer	getAnotatitonProcessDataList (string sessionId, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new Feat.Feat2Speed.UserInboxHeaderWebAnswer (call.getAnotatitonProcessDataList (sessionId: sessionId, callUrl: Context.Request.UserHostName,
																										  forState: forState, onlyOpen: onlyOpen, forSupervisorId: forSupervisorId, forAnotatorId: forAnotatorId)));
				  }
				}

	[WebMethod (Description = "Reads one anotation object with indetification processDataId")]
	public Feat.Feat2Speed.PeekFileWebAnswer	peekAnotationFile (string sessionId, string procDtaId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new Feat.Feat2Speed.PeekFileWebAnswer (call.peekAnotationFile (sessionId: sessionId, callUrl: Context.Request.UserHostName, procDtaId: procDtaId)));
				  }
				}

	[WebMethod (Description = "Counts data")]
	public CountAnswer	calcWordCount (string sessionId, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new CountAnswer (call.calcWordCount (sessionId: sessionId, callUrl: Context.Request.UserHostName,
															  forState: forState, onlyOpen: onlyOpen, forSupervisorId: forSupervisorId, forAnotatorId: forAnotatorId)));
				  }
				} 
   }

  public interface IWebAnswer
   {
    int		status	{ get; set; }
	string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class WebAnswer : IWebAnswer
   {
    public WebAnswer ()	{ }
	public WebAnswer (Feat.IAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				}
	
    public int		status	{ get; set; }
	public string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class CreateSessionWebAnswer : IWebAnswer
   {
    public CreateSessionWebAnswer ()	{ }
    public CreateSessionWebAnswer (Feat.ICreateSessionAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				 sessionId = ans.sessionId;
				}
	
	public int		status		{ get; set; }
	public string	errMsg		{ get; set; }
    public string	sessionId	{ get; set; }
   }

  [System.Serializable]
  public class InizializeWebFile : IFile
   {
    public InizializeWebFile ()	{ }
    public InizializeWebFile (IFile fl)
				{
				 name       = fl.name;
				 htmlFile   = fl.htmlFile;
				 xmlWFile   = fl.xmlWFile;
				 xmlAFile   = fl.xmlAFile;
				 xmlBFile   = fl.xmlBFile;
				}

    public string	name		{ get; set; }
	public string	htmlFile	{ get; set; }
	public string	xmlWFile	{ get; set; }
	public string	xmlAFile	{ get; set; }
	public string	xmlBFile	{ get; set; }
   }

  [System.Serializable]
  public class CountAnswer : IWebAnswer
   {
    public CountAnswer ()	{ }
	public CountAnswer (ICountAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				 fileCount = ans.fileCount;
				 wordCount = ans.wordCount;
				}
	
    public int		status	{ get; set; }
	public string	errMsg	{ get; set; }

    public int	fileCount	{ get; set; }
	public int	wordCount	{ get; set; }
   }
 }
