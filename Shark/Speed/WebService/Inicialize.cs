﻿
namespace Shark.Speed.WebService.Inicialize
 {
  public interface ICall
   {
    Feat.ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl);
	Feat.IAnswer				closeSession (string sessionId, string callUrl);

	Feat.IAnswer	createProcessPrideleno (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file);
	Feat.IAnswer	createProcessOdevzdano (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file);
	Feat.IAnswer	createProcessDoAmesu (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file);

	Feat.IAnswer	overWriteProcessState (string sessionId, string callUrl, int stateNo, long proDtaId, long taskId, long supervisorId, long anotatorId, long adjudikatorId, IFile file);

	Feat.IUserInboxHeaderAnswer	getAnotatitonProcessDataList (string sessionId, string callUrl, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId);
	Feat.IPeekFileAnswer		peekAnotationFile (string sessionId, string callUrl, string procDtaId);

	ICountAnswer	calcWordCount (string sessionId, string callUrl, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId);
   }

  public interface IFile
   {
    string	name		{ get; }
	string	htmlFile	{ get; }
	string	xmlWFile	{ get; }
	string	xmlAFile	{ get; }
	string	xmlBFile	{ get; }
   }

  public interface ICountAnswer : Feat.IAnswer
   {
    int	fileCount	{ get; }
	int	wordCount	{ get; }
   }

  public class Call
   {
    private Call ()	{ }
	public static ICall	create (BI.IBi bi)	{ return (CallImp.create (bi)); }

	private class CallImp : ICall
	 {
	  private CallImp (BI.IBi p_Bi)				{ bi = p_Bi; enc = new System.Text.UTF8Encoding (); }
	  public static ICall	create (BI.IBi bi)	{ return (new CallImp (bi)); }
	  
	  private BI.IBi					bi	{ get; set; }
	  private System.Text.UTF8Encoding	enc	{ get; set; }

	  public Feat.ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl)
				{
				 Feat.ISession ses = ((userName != null && (userName == "Admin" || userName == "Administrator"))? Feat.Call.getSessionKeeper ().login (bi.user.loginUser (userName, passWd), callUrl): null);

				 if (ses != null) return (Feat.Call.createSession (ses.id));
				 
				 System.Threading.Thread.Sleep (20000);
				 return (Feat.Call.createSessionError ("Unknow user or invalid password!"));
				}

	  public Feat.IAnswer	closeSession (string sessionId, string callUrl)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Feat.Call.createAnswerError ("Invalid session!"));

				 ses.logout (); return (Feat.Call.createAnswer ());
				}

	  public Feat.IAnswer	createProcessPrideleno (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file)
				{ return (createProcess (sessionId: sessionId, callUrl: callUrl, supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId: BI.Process.EStateAnotace.SA_Prideleno)); }

	  public Feat.IAnswer	createProcessOdevzdano (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file)
				{ return (createProcess (sessionId: sessionId, callUrl: callUrl, supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId: BI.Process.EStateAnotace.SA_Odevzdano)); }

	  public Feat.IAnswer	createProcessDoAmesu (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file)
				{ return (createProcess (sessionId: sessionId, callUrl: callUrl, supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId: BI.Process.EStateAnotace.SA_DoAmesu)); }
	  
	  public Feat.IAnswer	overWriteProcessState (string sessionId, string callUrl, int stateNo, long proDtaId, long taskId, long supervisorId, long anotatorId, long adjudikatorId, IFile file)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Feat.Call.createAnswerError ("Invalid session!"));

				 try {
				   BI.ProcessData.IProcessData procDta = bi.processData.processDataTable.getObject (id: proDtaId);
				   BI.Process.ITask task = bi.process.taskTable.getObject (id: taskId);

				   if (procDta == null || task == null || task.data.id != procDta.id) return (Feat.Call.createAnswerError ("Invalid data!"));
				   
				   switch (procDta.type) {
				     case BI.ProcessData.EProcessData.PD_Anotace:
					   if (procDta.anotace.supervisor.id != supervisorId || procDta.anotace.anotator.id != anotatorId || adjudikatorId != 0)
					    return (Feat.Call.createAnswerError ("Invalid anotace data!"));
					   else break;
					 
					 case BI.ProcessData.EProcessData.PD_Text:
					   if (procDta.text.adjudikator.id != adjudikatorId || procDta.text.supervisor.id != supervisorId || anotatorId != 0)
					    return (Feat.Call.createAnswerError ("Invalid text data!"));
					   else break;
				   
				   	 default: return (Feat.Call.createAnswerError ("Invalid type of data!"));
				   }

				   if (bi.language.isNull (task.finished)) {
				     using (Database.ITransaction trans = bi.database.openTransaction ()) {
					   switch (procDta.type) {
					     case BI.ProcessData.EProcessData.PD_Anotace:
						   if (! (bi.language.isNull (file.htmlFile))) overWriteFile (trans, procDta.anotace.html.id, file.htmlFile);
						   if (! (bi.language.isNull (file.xmlWFile))) overWriteFile (trans, procDta.anotace.xmlW.id, file.xmlWFile);
						   if (! (bi.language.isNull (file.xmlAFile))) overWriteFile (trans, procDta.anotace.xmlA.id, file.xmlAFile);
						   if (! (bi.language.isNull (file.xmlBFile))) overWriteFile (trans, procDta.anotace.xmlB.id, file.xmlBFile);
						   trans.finalyOk = true; break;

					     case BI.ProcessData.EProcessData.PD_Text:
						   if (! (bi.language.isNull (file.htmlFile))) overWriteFile (trans, procDta.text.html.id, file.htmlFile);
						   if (! (bi.language.isNull (file.xmlWFile))) overWriteFile (trans, procDta.text.xmlW.id, file.xmlWFile);
						   if (! (bi.language.isNull (file.xmlAFile))) overWriteFile (trans, procDta.text.xmlA.id, file.xmlAFile);
						   if (! (bi.language.isNull (file.xmlBFile))) overWriteFile (trans, procDta.text.xmlB.id, file.xmlBFile);
						   trans.finalyOk = true; break;
						 
						 default: return (Feat.Call.createAnswerError ("Invalid type of data!"));
					   }
					 }
				     
				   } else return (Feat.Call.createAnswerError ("Task is finished!"));
				 } catch (System.Exception ex) { return (Feat.Call.createAnswerError (ex.Message + "\r\n" + ex.StackTrace)); }

				 return (Feat.Call.createAnswer ());
				}

	  private Feat.IAnswer	createProcess (string sessionId, string callUrl, long supervisorId, long anotatorId, IFile file, BI.Process.EStateAnotace stateId)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Feat.Call.createAnswerError ("Invalid session!"));

				 try
				  {
				   string errMsg = makeProcess (supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId: stateId);
				   if (errMsg != null) return (Feat.Call.createAnswerError (errMsg));
				  }
				 catch (System.Exception ex) { return (Feat.Call.createAnswerError (ex.Message + "\r\n" + ex.StackTrace)); }

				 return (Feat.Call.createAnswer ());
				}
	  
	  private string	makeProcess (long supervisorId, long anotatorId, IFile file, BI.Process.EStateAnotace stateId)
				{
				 if (supervisorId == 0) return ("Supervisor Id is null!");
				 if (anotatorId == 0) return ("Anotator Id is null!");
				 if (file == null) return ("File is null!");
				 if (file.name == null) return ("File name is null!");
				 if (file.htmlFile == null) return ("HTML File is null!");
				 if (file.xmlWFile == null) return ("XML W File is null!");
				 if (file.xmlAFile == null) return ("XML A File is null!");
				 if (file.xmlBFile == null) return ("XML B File is null!");

				 BI.ProcessData.IProcessData procDta = null;
				 using (Database.IRecordSet<BI.ProcessData.IProcessData> rs = bi.processData.processDataTable.findObj (bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_AmesId), file.name))
				  if (rs.moveNext ()) procDta = rs.currObj;
				 
				 if (procDta == null) return (createAnotace (supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId: stateId));
				 else if (procDta.type != BI.ProcessData.EProcessData.PD_Text) return ("Bad process data type found for: " + file.name);
				 else if (procDta.text.supervisor.id != supervisorId) return ("Another supervisor in process data type found for: " + file.name);

				 using (Database.IRecordSet<BI.Process.IProcess> rs = bi.process.processTable.findObj (bi.process.getFieldName (BI.Process.EProcessField.PF_DataId), procDta.id))
				  if (rs.moveNext ())
				   {
				    Database.ICondBuilder cndB = bi.database.condBuilder ();
				    cndB.addIsNull (bi.process.getFieldName (BI.Process.ETaskField.TF_Finished));
				    cndB.addValCond (bi.process.getFieldName (BI.Process.ETaskField.TF_ProcessId), rs.currObj.id);

					using (Database.IRecordSet<BI.Process.ITask> rsTsk = bi.process.taskTable.findObj (cndB.create ()))
					 if (rsTsk.moveNext ())
					  return (createAnotace (supervisorId: supervisorId, anotatorId: anotatorId, file: file, stateId:stateId, parentTaskId: rsTsk.currObj.id));
					 else return ("There is no open task for process: " + rs.currObj.id.ToString ());
				   }
				  else return ("There is no process for process data: " + procDta.id.ToString ());
				}

	  private string	createAnotace (long supervisorId, long anotatorId, IFile file, BI.Process.EStateAnotace stateId, long parentTaskId = 0)
				{
			     bool createClosed;
				 BI.Process.IProcess proc = null;

				 if (parentTaskId == 0) createClosed = false;
				 else
				  {
				   Database.ICondBuilder cndB = bi.database.condBuilder ();
				   cndB.addValCond (cndB.fullFieldName (tableAcr: bi.process.processTable.name, fieldName: bi.process.getFieldName (BI.Process.EProcessField.PF_ParentTaskId)), parentTaskId);
				   
				   string procDataAcr = cndB.createTableAcr ();
				   Database.ICondBuilder exist = cndB.addExists (tableName: bi.processData.processDataTable.name, tableField: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_Id),
															   tableAcr: procDataAcr, tableCondFld: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_Id),
															   parentValueSide: cndB.fullFieldName (tableAcr: bi.process.processTable.name, fieldName: bi.process.getFieldName (BI.Process.EProcessField.PF_DataId)));
				   exist.addValCond (cndB.fullFieldName (tableAcr: procDataAcr, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_SupervisorId)), supervisorId);
				   exist.addValCond (cndB.fullFieldName (tableAcr: procDataAcr, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_AnotatorId)), anotatorId);

				   BI.Process.EStateAnotace openState = BI.Process.EStateAnotace.SA_Start;
				   using (Database.IRecordSet<BI.Process.IProcess> procRs = bi.process.processTable.findObj (cndB.create ()))
				    if (createClosed = procRs.moveNext ())
					 {
					  proc = procRs.currObj;

					  Database.ICondBuilder taskQ = bi.database.condBuilder ();
					  taskQ.addValCond (taskQ.fullFieldName (tableAcr: bi.process.taskTable.name, fieldName: bi.process.getFieldName (BI.Process.ETaskField.TF_ProcessId)), procRs.currObj.id);
					  taskQ.addIsNull (taskQ.fullFieldName (tableAcr: bi.process.taskTable.name, fieldName: bi.process.getFieldName (BI.Process.ETaskField.TF_Finished)));
					  
					  using (Database.IRecordSet<BI.Process.ITask> tskRs = bi.process.taskTable.findObj (taskQ.create ()))
					   if (tskRs.moveNext ()) openState = (BI.Process.EStateAnotace) (tskRs.currObj.stateId);
					 }
					 

				   if (createClosed)
				    if (stateId == BI.Process.EStateAnotace.SA_Odevzdano)
					 { if (openState != BI.Process.EStateAnotace.SA_DoAmesu) return ("There is a another open anotation process for: " + file.name); }
					else if (stateId == BI.Process.EStateAnotace.SA_DoAmesu) return ("There is a another open anotation process for: " + file.name);
				    else if (stateId != BI.Process.EStateAnotace.SA_Prideleno) return ("Implementation error for this state!");
				  }

				 using (Database.ITransaction trans = bi.database.openTransaction ())
				  {
				   BI.ProcessData.IProcessData procDta = createAnotaceData (trans, supervisorId: supervisorId, anotatorId: anotatorId, file: file);
				   proc = createAnotaceProcess (trans, proc, supervisorId: supervisorId, anotatorId: anotatorId, procDtaId: procDta.id, parentTaskId: parentTaskId,
											  stateId: stateId, createClosed: createClosed);

				   if (parentTaskId == 0)
				    {
					 proc.parentTask.id = createTextProcess (trans, supervisorId, file.name, procDta);
					 bi.process.processTable.update (trans, proc);
					}
				   
				   trans.finalyOk = true;
				  }

				 return (null);
				}

	  private BI.ProcessData.IProcessData	createAnotaceData (Database.ITransaction trans, long supervisorId, long anotatorId, IFile file)
				{
				 BI.ProcessData.IProcessData procDta = bi.processData.processDataTable.createNewObj ();
				 procDta.type                  = BI.ProcessData.EProcessData.PD_Anotace;
				 procDta.anotace.html.id       = createFile (trans, file.name + ".html", file.htmlFile);
				 procDta.anotace.xmlW.id       = createFile (trans, file.name + ".w.xml", file.xmlWFile);
				 procDta.anotace.xmlA.id       = createFile (trans, file.name + ".a.xml", file.xmlAFile);
				 procDta.anotace.xmlB.id       = createFile (trans, file.name + ".b.xml", file.xmlBFile);
				 procDta.anotace.supervisor.id = supervisorId;
				 procDta.anotace.anotator.id   = anotatorId;
				 bi.processData.processDataTable.insert (trans, procDta);

				 return (procDta);
				}

	  private BI.Process.IProcess	createAnotaceProcess (Database.ITransaction trans, BI.Process.IProcess proc, long supervisorId, long anotatorId, long procDtaId, long parentTaskId,
														BI.Process.EStateAnotace stateId, bool createClosed)
				{
				 if (proc == null)
				  {
				   proc = bi.process.processTable.createNewObj ();
				   proc.startAt       = System.DateTime.Now;
				   proc.owner.id      = supervisorId;
				   proc.data.id       = procDtaId;
				   proc.parentTask.id = parentTaskId;
				   bi.process.processTable.insert (trans, proc);
				  }

				 BI.Process.ITask tsk = bi.process.taskTable.createNewObj ();
				 tsk.process.id  = proc.id;
				 tsk.created     = System.DateTime.Now;
				 tsk.data.id     = procDtaId;
				 tsk.stateId	 = (int) stateId;
				 tsk.stateDesc   = bi.process.getStateName (stateId);
				 tsk.priority    = 50;
				 switch (stateId)
				  {
				   case BI.Process.EStateAnotace.SA_Prideleno:
				     tsk.fromUser.id = supervisorId;
				     tsk.toUser.id   = anotatorId;
					 break;
				   case BI.Process.EStateAnotace.SA_Odevzdano:
				     tsk.fromUser.id = anotatorId;
				     tsk.toUser.id   = supervisorId;
					 break;
				   case BI.Process.EStateAnotace.SA_DoAmesu:
				     tsk.fromUser.id = supervisorId;
				     tsk.toUser.id   = supervisorId;

					 using (Database.IRecordSet<BI.User.IUserRole> rs = bi.user.userRoleTable.findObj (BI.User.EUserRoleField.URF_Type, (short) BI.User.EUserRole.UR_AutoChecker))
					  if (rs.moveNext ())
					   {
					    tsk.toUser.id = rs.currObj.user.id;
						tsk.stateId	  = (int) BI.Process.EStateAnotace.SA_KPrenosu;
						tsk.stateDesc = bi.process.getStateName (BI.Process.EStateAnotace.SA_KPrenosu);
					   }

					 break;
				   default: throw new System.Exception ("Internal error - not implemented state in createAnotaceProcess!");
				  }
				 if (createClosed)
				  {
				   tsk.started	= System.DateTime.Now;
				   tsk.finished = System.DateTime.Now;
				  }
				 bi.process.taskTable.insert (trans, tsk);

				 return (proc);
				}

	  private long	createTextProcess (Database.ITransaction trans, long supervisorId, string amesId, BI.ProcessData.IProcessData anotaceDta)
				{
				 BI.ProcessData.IProcessData procDta = bi.processData.processDataTable.createNewObj ();
				 procDta.type               = BI.ProcessData.EProcessData.PD_Text;
				 procDta.text.amesId		= amesId;
				 procDta.text.html.id       = anotaceDta.anotace.html.id;
				 procDta.text.xmlW.id       = anotaceDta.anotace.xmlW.id;
				 procDta.text.xmlA.id       = anotaceDta.anotace.xmlA.id;
				 procDta.text.xmlB.id       = anotaceDta.anotace.xmlB.id;
				 procDta.text.supervisor.id = supervisorId;
				 bi.processData.processDataTable.insert (trans, procDta);

				 BI.Process.IProcess proc = bi.process.processTable.createNewObj ();
				 proc.startAt       = System.DateTime.Now;
				 proc.owner.id      = supervisorId;
				 proc.data.id       = procDta.id;
				 bi.process.processTable.insert (trans, proc);

				 BI.Process.ITask tsk = bi.process.taskTable.createNewObj ();
				 tsk.process.id  = proc.id;
				 tsk.created     = System.DateTime.Now;
				 tsk.fromUser.id = supervisorId;
				 tsk.data.id     = procDta.id;
				 tsk.stateId	 = (int) BI.Process.EStateText.ST_KAnotaci;
				 tsk.stateDesc   = bi.process.getStateName (BI.Process.EStateText.ST_KAnotaci);
				 tsk.priority    = 40;
				 tsk.started     = System.DateTime.Now;
				 tsk.toUser.id   = supervisorId;
				 bi.process.taskTable.insert (trans, tsk);

				 return (tsk.id);
				}

	  private long	createFile (Database.ITransaction trans, string fileName, string fileDta)
				{
				 BI.ProcessData.IFile fl = bi.processData.fileTable.createNewObj ();
				 fl.name = fileName;
				 fl.data = enc.GetBytes (fileDta);
				 bi.processData.fileTable.insert (trans, fl);
				 return (fl.id);
				}

	  private void	overWriteFile (Database.ITransaction trans, long fileId, string fileDta)
				{
				 BI.ProcessData.IFile fl = bi.processData.fileTable.getObject (id: fileId);
				 fl.data = enc.GetBytes (fileDta);
				 bi.processData.fileTable.update (trans, fl);
				}

	  public Feat.IUserInboxHeaderAnswer	getAnotatitonProcessDataList (string sessionId, string callUrl, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Feat.Call.createInboxError ("Invalid session!"));

				 try
				  {
				   System.Collections.Generic.List<string> inbox = new System.Collections.Generic.List<string> ();
				   Database.ICondBuilder cndB = createAnotaceProcessDataCondition (procDtaTblName: bi.processData.processDataTable.name, forState: forState, onlyOpen: onlyOpen,
																			     forSupervisorId: forSupervisorId, forAnotatorId: forAnotatorId, onlyAnotation: false);
			     
				   using (Database.IRecordSet<BI.ProcessData.IProcessData> prcDta = bi.processData.processDataTable.findObj (cndB.create ()))
				    while (prcDta.moveNext ()) inbox.Add (prcDta.currObj.id.ToString ());
				  
				   return (Feat.Call.createInbox (inbox.ToArray ()));
				  }
				 catch (System.Exception ex) { return (Feat.Call.createInboxError (ex.Message + "\r\n" + ex.StackTrace)); }
				}
	  
	  public Feat.IPeekFileAnswer	peekAnotationFile (string sessionId, string callUrl, string procDtaId)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Feat.Call.createPeekError ("Invalid session!"));

				 try
				  {
				   BI.ProcessData.IProcessData procDta = bi.processData.processDataTable.getObject (procDtaId);
				   if (procDta != null && (procDta.type == BI.ProcessData.EProcessData.PD_Anotace || procDta.type == BI.ProcessData.EProcessData.PD_Text)) {
				     long tskId;
					 string fileName = getAnotationFileName (procDta, out tskId);
					 if (procDta.type == BI.ProcessData.EProcessData.PD_Anotace)
					  return (Feat.Call.createPeek (nm: fileName, cmd: getAnotationInfoFileContent (procDta, tskId), htm: peekFile (procDta.anotace.html),
						 				          xmlW: peekFile (procDta.anotace.xmlW), xmlA: peekFile (procDta.anotace.xmlA), xmlB: peekFile (procDta.anotace.xmlB)));
					 else return (Feat.Call.createPeek (nm: fileName, cmd: getAnotationInfoFileContent (procDta, tskId), htm: peekFile (procDta.text.html),
											          xmlW: peekFile (procDta.text.xmlW), xmlA: peekFile (procDta.text.xmlA), xmlB: peekFile (procDta.text.xmlB)));
				   } else return (Feat.Call.createPeekError ("Process Data does not exists!"));
				  }
				 catch (System.Exception ex) { return (Feat.Call.createPeekError (ex.Message + "\r\n" + ex.StackTrace)); }
				}

	  private string	getAnotationFileName (BI.ProcessData.IProcessData procDta, out long tskId)
				{
				 tskId = 0;
				 if (procDta.type == BI.ProcessData.EProcessData.PD_Text) return (procDta.text.amesId);

				 string amesId = null;

				 using (Database.IRecordSet<BI.Process.ITask> tskRc = bi.process.taskTable.findObj (bi.process.getFieldName (BI.Process.ETaskField.TF_DataId), procDta.id))
				  while (tskRc.moveNext ())
				   if (amesId == null || bi.language.isNull (tskRc.currObj.finished)) {
  				     BI.Process.IProcess anotProc = ((tskRc.currObj != null)? tskRc.currObj.process.parent: null);
				     BI.Process.ITask textTask = ((anotProc != null)? anotProc.parentTask.parent: null);
				     BI.ProcessData.IProcessData textDta = ((textTask != null)? textTask.data.parent: null);

				     if (textDta != null && textDta.type == BI.ProcessData.EProcessData.PD_Text && textDta.text.amesId != null && (!(textDta.text.amesId.Trim () == ""))) {
					   tskId = tskRc.currObj.id; amesId = textDta.text.amesId;
					   if (bi.language.isNull (tskRc.currObj.finished)) break;
					 }
				   }
				   
				 return (amesId);
				}
	  
	  private string	getAnotationInfoFileContent (BI.ProcessData.IProcessData procDta, long taskId = 0)
				{
				 System.Text.StringBuilder sb = new System.Text.StringBuilder ();
				 sb = sb.AppendLine ("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				 sb = sb.AppendLine ("<AnotaceInfoFile>");
				 
				 BI.ProcessData.IProcessDataAnotace proDtaAnot = procDta.anotace;
				 BI.ProcessData.IProcessDataText proDtaTxt = procDta.text;

				 BI.User.IUser usr = ((proDtaAnot != null)? proDtaAnot.supervisor.parent: (proDtaTxt != null)? proDtaTxt.supervisor.parent: null);

				 if (usr != null && (usr.id == 5 || usr.id == 7 || usr.id == 19))
				  {
				   sb = sb.AppendLine ("  <MetaData>");
				   sb = sb.Append ("    <LangGroup value=\"");
				   if (usr.id == 5) sb = sb.Append ("neslovani");
				   else if (usr.id == 7) sb = sb.Append ("slovani");
				   else if (usr.id == 19) sb = sb.Append ("romove");
				   sb = sb.AppendLine ("\" />");
				   sb = sb.AppendLine ("  </MetaData>");
				  }

				 sb = sb.Append ("  <SpeedInfo anotProcDtaId=\"").Append (procDta.id).AppendLine ("\" >");
				 
				 if (taskId != 0) sb = sb.Append ("    <task id=\"").Append (taskId).AppendLine ("\" />");
				 
				 if (usr != null)
				  sb = sb.Append ("    <supervisor id=\"").Append (usr.id).Append ("\" loginName=\"").Append (bi.xml.encode (usr.userName)).Append ("\" name=\"").Append (bi.xml.encode (usr.name)).AppendLine ("\" />");
				 
				 usr = ((proDtaAnot != null)? proDtaAnot.anotator.parent: null);
				 if (usr != null)
				  sb = sb.Append ("    <anotator id=\"").Append (usr.id).Append ("\" loginName=\"").Append (bi.xml.encode (usr.userName)).Append ("\" name=\"").Append (bi.xml.encode (usr.name)).AppendLine ("\" />");

				 usr = ((proDtaTxt != null)? proDtaTxt.adjudikator.parent: null);
				 if (usr != null)
				  sb = sb.Append ("    <adjudikator id=\"").Append (usr.id).Append ("\" loginName=\"").Append (bi.xml.encode (usr.userName)).Append ("\" name=\"").Append (bi.xml.encode (usr.name)).AppendLine ("\" />");

				 sb = sb.AppendLine ("  </SpeedInfo>");
				 
				 sb = sb.AppendLine ("</AnotaceInfoFile>");
				 
				 return (sb.ToString ());
				}

	  private string	peekFile (BI.IReference<BI.ProcessData.IFile> flRef)
				{
				 try
				  {
				   BI.ProcessData.IFile fl = ((flRef != null)? flRef.parent: null);
				 
				   if (fl == null || fl.data == null || fl.data.Length < 1) return (null);
				   return (enc.GetString (fl.data));
				  }
				 catch { return (null); }
				}
	  
	  public ICountAnswer	calcWordCount (string sessionId, string callUrl, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId)
				{
				 Feat.ISession ses = Feat.Call.getSessionKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createCountError ("Invalid session!"));

				 try
				  {
				   int flCnt; int wrdCnt;
				   string errMsg = calcWordCount (forState: forState, onlyOpen: onlyOpen, forSupervisorId: forSupervisorId, forAnotatorId: forAnotatorId, flCnt: out flCnt, wrdCnt: out wrdCnt);
				   if (errMsg != null) return (Answer.createCountError (errMsg));

				   return (Answer.createCount (flCount: flCnt, wrdCount: wrdCnt));
				  }
				 catch (System.Exception ex) { return (Answer.createCountError (ex.Message + "\r\n" + ex.StackTrace)); }
				}

	  private string	calcWordCount (int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId, out int flCnt, out int wrdCnt)
				{
				 flCnt = wrdCnt = 0;

				 Database.ICondBuilder cndB = createAnotaceProcessDataCondition (procDtaTblName: bi.processData.processDataTable.name, forState: forState, onlyOpen: onlyOpen,
																			   forSupervisorId: forSupervisorId, forAnotatorId: forAnotatorId);
			     
				 using (Database.IRecordSet<BI.ProcessData.IProcessData> prcDta = bi.processData.processDataTable.findObj (cndB.create ()))
				  while (prcDta.moveNext ()) { flCnt++; wrdCnt += prcDta.currObj.anotace.calcWordCount (); }

				 return (null);
				}
	 
	  private Database.ICondBuilder	createAnotaceProcessDataCondition (string procDtaTblName, int forState, bool onlyOpen, long forSupervisorId, long forAnotatorId, bool onlyAnotation = true)
				{
				 Database.ICondBuilder cndB = bi.database.condBuilder ();
				 if (onlyAnotation) cndB.addValCond (cndB.fullFieldName (tableAcr: procDtaTblName, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_Type)), (short) (BI.ProcessData.EProcessData.PD_Anotace));
				 if (forSupervisorId != 0) cndB.addValCond (cndB.fullFieldName (tableAcr: procDtaTblName, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_SupervisorId)), forSupervisorId);
				 if (forAnotatorId != 0) cndB.addValCond (cndB.fullFieldName (tableAcr: procDtaTblName, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_AnotatorId)), forAnotatorId);

				 if (onlyOpen || forState != 0)
				  {
				   string taskTblName = bi.process.taskTable.name;

				   Database.ICondBuilder exsTask = cndB.addExists (tableName: taskTblName, tableField: bi.process.getFieldName (BI.Process.ETaskField.TF_Id),
				                                                 tableCondFld: bi.process.getFieldName (BI.Process.ETaskField.TF_DataId),
																 parentValueSide: cndB.fullFieldName (tableAcr: procDtaTblName, fieldName: bi.processData.getFieldName (BI.ProcessData.EProcessDataField.PDF_Id)));
				   if (onlyOpen) exsTask.addIsNull (exsTask.fullFieldName (tableAcr: taskTblName, fieldName: bi.process.getFieldName (BI.Process.ETaskField.TF_Finished)));
				   if (forState != 0) exsTask.addValCond (exsTask.fullFieldName (tableAcr: taskTblName, fieldName: bi.process.getFieldName (BI.Process.ETaskField.TF_StateId)), (short) forState);
				  }

				 return (cndB);
				}
	 }

	private class Answer : ICountAnswer
	 {
	  private Answer (string msg = null, Feat.EAnswerStatus ansStatus = Feat.EAnswerStatus.AS_Ok)	{ isOk = ansStatus; errMsg = msg; fileCount = 0; wordCount = 0; }

	  public static ICountAnswer	createCountError (string p_ErrMsg, Feat.EAnswerStatus ansStatus = Feat.EAnswerStatus.AS_Error)		{ return (new Answer (p_ErrMsg, ansStatus)); }

	  public static ICountAnswer	createCount (int flCount, int wrdCount, string msg = null)		{ Answer ans = new Answer (msg); ans.fileCount = flCount; ans.wordCount = wrdCount; return (ans); }
      
	  public Feat.EAnswerStatus	isOk	{ get; private set; }
	  public string				errMsg	{ get; private set; }

      public int	fileCount	{ get; private set; }
	  public int	wordCount	{ get; private set; }
	 }
   }
 }