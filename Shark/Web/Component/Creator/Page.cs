﻿using System;
using System.Collections.Generic;
using System.Web;
using Shark.Web.Component;
using Shark.Web.HttpProtocol;
using IPage = Shark.Web.Component.IPage;

namespace Shark.Web.Component.Creator
{
	internal class Page : Component, IPage
	{
		#region private constants

		/// <summary>
		/// Constants which defines main page form name
		/// </summary>
		private const string CF_PageFormName = "frData";

		#endregion

		#region private fields

		/// <summary>
		/// inner component of page
		/// </summary>
		private readonly IComponent _subComponent;

		#endregion

		/// <summary>
		/// Page constructor sets page variables
		/// </summary>
		/// <param name="creator">Components creator</param>
		/// <param name="control">Control for generating html content</param>
		/// <param name="component">Inner page component</param>
		/// <param name="title">Page title</param>
		internal Page(ICreator creator, IControl control, IComponent component, string title) : base(creator, control)
		{
			this.title = title;
			_subcomponents.Add(component);
			_subComponent = component;
		}

		#region Overrides of Component

		/// <summary>
		/// Method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		public override void generate()
		{
			_control.html();
				_control.head();
					_control.title(title);
					_control.meta(httpEquiv: "content-type", content: "text/html; charset=UTF-16");
					_control.meta(name: "vs_defaultClientScript", content:"JavaScript");
					_control.link(rel: "StyleSheet", type: "text/css", href: "images/ver3_main.css");
					_control.writeNL("<!-- Copyright (c) petr@jager.cz; petr.jager@seznam.cz -->");
				_control.headEnd();
				_control.body();
					_control.form(name: CF_PageFormName, action: _control.getAbsoluteUri(), method: "post");
						_subComponent.generate();
					_control.formEnd();
				_control.bodyEnd();
			_control.htmlEnd();
		}

		#endregion

		#region Implementation of IPage

		/// <summary>
		/// Page title
		/// </summary>
		public string title { get; set; }

		#endregion
	}
}