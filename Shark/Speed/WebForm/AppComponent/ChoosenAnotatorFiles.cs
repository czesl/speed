﻿
namespace Shark.Speed.WebForm.AppComponent.ChoosenAnotatorFiles
 {
  public class Creator
   {
    private Creator ()	{ }

	public static Web.Component.IComponent	create (ICreator cre, Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId)
				{
				 Web.Component.ILayoutCreator layOut = parent.layOut (ySize: 2);
				 IChooserComponent chooser = cre.anotatorChooser (layOut.subCreator, session, currSupervisorId);
				 
				 layOut.addComponent (chooser.comp);
				 layOut.addComponent (cre.anotatorFiles (layOut.subCreator, session, delegate () { return (chooser.currRowId); }));
				 
				 return (layOut.create ());
				}
   }
 }
