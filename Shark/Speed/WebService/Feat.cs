﻿
namespace Shark.Speed.WebService.Feat
 {
  public interface ICall
   {
    ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl);
	IAnswer					closeSession (string sessionId, string callUrl);

	ICheckUserInboxAnswer		checkUserInbox (string sessionId, string callUrl, bool useOnlyAnotace = true);
	IUserInboxHeaderAnswer		getUserInboxHeader (string sessionId, string callUrl, bool is2ReadAnyFile, bool useOnlyAnotace = true);
	IPeekFileAnswer				peekFile (string sessionId, string callUrl, string taskId);
	IPeekSynchronizedFileAnswer	peekSyncFile (string sessionId, string callUrl, string taskId);
	IAnswer						fileRead (string sessionId, string callUrl, string taskId);

	IAnswer	saveFile (string sessionId, string callUrl, IFile file);
	IAnswer	saveFile (string sessionId, string callUrl, ISynchronizedFile file);
   }

  public enum EAnswerStatus { AS_Ok = 0, AS_Error = 1, AS_Warning = 2 }
  
  public interface IAnswer
   {
    EAnswerStatus	isOk	{ get; }
	string			errMsg	{ get; }
   }

  public interface ICreateSessionAnswer : IAnswer
   { string	sessionId	{ get; } }

  public interface ICheckUserInboxAnswer : IAnswer
   { string[]	inboxFileNameList	{ get; } }

  public interface IUserInboxHeaderAnswer : IAnswer
   { string[]	inboxTaskIdList	{ get; } }

  public interface IFile
   {
    string	name		{ get; }
	string	cmdFile		{ get; }
	string	htmlFile	{ get; }
	string	xmlWFile	{ get; }
	string	xmlAFile	{ get; }
	string	xmlBFile	{ get; }
   }
  
  public interface IPeekFileAnswer : IAnswer
   { IFile	file	{ get; } }


  public interface ISynchronizedFile
   {
    string						name	{ get; }
	ISynchronizedFileContent[]	head	{ get; }
	ISynchronizedFileFolder[]	folders	{ get; }
   }

  public interface ISynchronizedFileFolder
   {
    bool						isReadOnly		{ get; }
	bool						isWorkOnFile	{ get; }
	string						author			{ get; }
	ISynchronizedFileContent[]	content			{ get; }
   }

  public interface ISynchronizedFileContent
   {
    string	extension		{ get; }
	bool	isTextBased		{ get; }
	string	textContent		{ get; }
	bool	isBinary		{ get; }
	byte[]	binaryContent	{ get; }
   }

  public interface IPeekSynchronizedFileAnswer : IAnswer
   { ISynchronizedFile	syncFile	{ get; } }

  
  internal interface ISession
   {
	string			id			{ get; }
	long			userId		{ get; }
	string			userName	{ get; }
	System.DateTime	lastAction	{ get; set; }
	string			hostUrl		{ get; }

	void	logout ();
   }
	
  internal interface ISessionKeeper
   {
	ISession	login (BI.User.IUser usr, string callUrl);
	ISession	check (string sessionId, string callUrl);
   }

  public class Call : ICall
   {
    private Call (BI.IBi p_Bi)	{ m_Bi = p_Bi; m_Enc = new System.Text.UTF8Encoding (); }
	private BI.IBi						bi	{ get { return (m_Bi); } }	private BI.IBi						m_Bi;
	private System.Text.UTF8Encoding	enc	{ get { return (m_Enc); } } private System.Text.UTF8Encoding	m_Enc;

	public static ICall						createCall (BI.IBi p_Bi)																{ return (new Call (p_Bi)); }
	internal static ISessionKeeper			getSessionKeeper ()																		{ return (SessionKeeper.getKeeper ()); }
    
	internal static IAnswer					createAnswerError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (Answer.createAnswerError (p_ErrMsg, ansStatus)); }
	internal static ICreateSessionAnswer	createSessionError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (Answer.createSessionError (p_ErrMsg, ansStatus)); }
	internal static IUserInboxHeaderAnswer	createInboxError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (Answer.createInboxError (p_ErrMsg, ansStatus)); }
	internal static	IPeekFileAnswer			createPeekError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)		{ return (Answer.createPeekError (p_ErrMsg, ansStatus)); }
	
	internal static IAnswer					createAnswer (string msg = null)														{ return (Answer.createAnswer (msg)); }
	internal static ICreateSessionAnswer	createSession (string sesId, string msg = null)											{ return (Answer.createSession (sesId: sesId, msg: msg)); }
	internal static IUserInboxHeaderAnswer	createInbox (string[] namesLst, string msg = null)										{ return (Answer.createInbox (namesLst, msg)); }
	internal static IPeekFileAnswer			createPeek (string nm, string cmd, string htm, string xmlW, string xmlA, string xmlB, string msg = null)
				{ return (Answer.createPeek (File.create (nm:nm, cmd: cmd, htm: htm, xmlW: xmlW, xmlA: xmlA, xmlB: xmlB), msg: msg)); }

    public ICreateSessionAnswer	createSession (string userName, string passWd, string callUrl)
				{
				 ISession ses = SessionKeeper.getKeeper ().login (bi.user.loginUser (userName, passWd), callUrl);

				 if (ses != null) return (Answer.createSession (ses.id));
				 
				 System.Threading.Thread.Sleep (20000);
				 return (Answer.createSessionError ("Unknow user or invalid password!"));
				}

	public IAnswer	closeSession (string sessionId, string callUrl)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createAnswerError ("Invalid session!"));

				 ses.logout (); return (Answer.createAnswer ());
				}

	public ICheckUserInboxAnswer	checkUserInbox (string sessionId, string callUrl, bool useOnlyAnotace = true)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createCheckError ("Invalid session!"));

				 System.Collections.Generic.List<string> inbox = new System.Collections.Generic.List<string> ();
				 Database.ICondBuilder cb;

				 cb = createBasicInboxCond (ses.userId, useOnlyAnotace: useOnlyAnotace);

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  while (rs.moveNext ()) inbox.Add (rs.currObj.getFileName ());

				 return (Answer.createCheck (inbox.ToArray ()));
				}
	
	public IUserInboxHeaderAnswer	getUserInboxHeader (string sessionId, string callUrl, bool is2ReadAnyFile, bool useOnlyAnotace = true)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createInboxError ("Invalid session!"));

				 bool isSuperCall = isSupervisorCall (ses);

				 Database.ICondBuilder cb;
				 System.Collections.Generic.List<string> inbox = new System.Collections.Generic.List<string> ();
				 System.Collections.Generic.Dictionary<string, long> usedFileNames = new System.Collections.Generic.Dictionary<string,long> ();

				 cb = createBasicInboxCond (ses.userId, isSuperCall: isSuperCall, useOnlyAnotace: useOnlyAnotace);
				 cb.addNotCond ().addIsNull (bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_Started));

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  while (rs.moveNext ())
				   if (!(usedFileNames.ContainsKey (rs.currObj.getFileName ())))
				    {
					 usedFileNames.Add (rs.currObj.getFileName (), rs.currObj.id);
					 if (is2ReadAnyFile) inbox.Add (rs.currObj.id.ToString ());
					}

				 cb = createBasicInboxCond (ses.userId, isSuperCall: isSuperCall, useOnlyAnotace: useOnlyAnotace);
				 cb.addIsNull (bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_Started));

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  while (rs.moveNext ())
				   if (!(usedFileNames.ContainsKey (rs.currObj.getFileName ())))
				    { usedFileNames.Add (rs.currObj.getFileName (), rs.currObj.id); inbox.Add (rs.currObj.id.ToString ()); }

				 return (Answer.createInbox (inbox.ToArray ()));
				}
	
	public IPeekFileAnswer	peekFile (string sessionId, string callUrl, string taskId)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createPeekError ("Invalid session!"));

				 bool isSuperCall = isSupervisorCall (ses);

				 Database.ICondBuilder cb = createBasicInboxCond (ses.userId, isSuperCall);
				 cb.addValCond (cb.fullFieldName (tableAcr: bi.process.openTaskFileQuery.name, fieldName: bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_Id)), bi.language.tryScanLong (taskId));

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  if (rs.moveNext ()) return (peekFile (ses, rs.currObj.getFileName (), rs.currObj, isSuperCall));

				 return (Answer.createPeekError ("Task id=\"" + taskId + "\" not found!", EAnswerStatus.AS_Warning));
				}
	private IPeekFileAnswer	peekFile (ISession ses, string fileName, BI.Process.IOpenTaskFileQuery sendFile, bool isSuperCall)
				{
				 try
				  {
				   IFile fl = File.create (fileName, cmd: makeCmdFile (ses, fileName, sendFile, isSuperCall), htm: peekFile (sendFile.html),
										 xmlW: peekFile (sendFile.xmlW), xmlA: peekFile (sendFile.xmlA), xmlB: peekFile (sendFile.xmlB));
				   return (Answer.createPeek (fl));
				  }
				 catch { return (Answer.createPeekError ("File \"" + fileName + "\" could not be read!", EAnswerStatus.AS_Warning)); }
				}

	public IPeekSynchronizedFileAnswer	peekSyncFile (string sessionId, string callUrl, string taskId)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createSyncPeekError ("Invalid session!"));

				 bool isSuperCall = isSupervisorCall (ses);

				 Database.ICondBuilder cb = createBasicInboxCond (ses.userId, isSuperCall: isSuperCall, useOnlyAnotace: false);
				 cb.addValCond (cb.fullFieldName (tableAcr: bi.process.openTaskFileQuery.name, fieldName: bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_Id)), bi.language.tryScanLong (taskId));

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  if (rs.moveNext ()) return (peekSyncFile (ses, rs.currObj.getFileName (), rs.currObj, isSuperCall));

				 return (Answer.createSyncPeekError ("Task id=\"" + taskId + "\" not found!", EAnswerStatus.AS_Warning));
				}
	private IPeekSynchronizedFileAnswer	peekSyncFile (ISession ses, string fileName, BI.Process.IOpenTaskFileQuery sendFile, bool isSuperCall)
				{
				 try
				  {
				   ISynchronizedFile fl;
				   BI.Process.ITask task = bi.process.taskTable.getObject (sendFile.id);
				   BI.ProcessData.IProcessData procDta = ((task != null)? task.data.parent: null);

				   if (procDta != null && procDta.type == BI.ProcessData.EProcessData.PD_Text &&
				       (sendFile.stateId == (int) BI.Process.EStateText.ST_AdjPrideleno || sendFile.stateId == (int) BI.Process.EStateText.ST_AdjProblem ||
					    sendFile.stateId == (int) BI.Process.EStateText.ST_AdjVraceno || sendFile.stateId == (int) BI.Process.EStateText.ST_AdjKontrola))
				    {
					 System.Collections.Generic.List<ISynchronizedFileFolder> lst = new System.Collections.Generic.List<ISynchronizedFileFolder> (3);
					 BI.ProcessData.IProcessData anoProcDta = bi.processData.processDataTable.getObject (procDta.text.anotated1ProcessDataId);
					 if (anoProcDta != null && anoProcDta.type == BI.ProcessData.EProcessData.PD_Anotace)
					  lst.Add (SynchronizedFileFolder.create (htmlFile: peekFile (anoProcDta.anotace.html), xmlWFile: peekFile (anoProcDta.anotace.xmlW),
															xmlAFile: peekFile (anoProcDta.anotace.xmlA), xmlBFile: peekFile (anoProcDta.anotace.xmlB), isRO: true, isWrkOn: false,
															auth: anoProcDta.anotace.anotator.name));
					 anoProcDta = bi.processData.processDataTable.getObject (procDta.text.anotated2ProcessDataId);
					 if (anoProcDta != null && anoProcDta.type == BI.ProcessData.EProcessData.PD_Anotace)
					  lst.Add (SynchronizedFileFolder.create (htmlFile: peekFile (anoProcDta.anotace.html), xmlWFile: peekFile (anoProcDta.anotace.xmlW),
															xmlAFile: peekFile (anoProcDta.anotace.xmlA), xmlBFile: peekFile (anoProcDta.anotace.xmlB), isRO: true, isWrkOn: false,
															auth: anoProcDta.anotace.anotator.name));
					 if (sendFile.stateId != (int) BI.Process.EStateText.ST_AdjPrideleno) 
					  lst.Add (SynchronizedFileFolder.create (htmlFile: peekFile (procDta.text.html), xmlWFile: peekFile (procDta.text.xmlW), 
															xmlAFile: peekFile (procDta.text.xmlA), xmlBFile: peekFile (procDta.text.xmlB), auth: procDta.text.adjudikator.name));

					 fl = SynchronizedFile.create (nm: fileName, cmdFile: makeCmdFile (ses, fileName, sendFile, isSuperCall), fld: lst.ToArray ());
					}
				   else fl = SynchronizedFile.create (nm: fileName, cmdFile: makeCmdFile (ses, fileName, sendFile, isSuperCall), htmlFile: peekFile (sendFile.html),
					                                xmlWFile: peekFile (sendFile.xmlW), xmlAFile: peekFile (sendFile.xmlA), xmlBFile: peekFile (sendFile.xmlB),
													auth: ((procDta != null && procDta.type == BI.ProcessData.EProcessData.PD_Anotace)? procDta.anotace.anotator.name: null));

				   return (Answer.createSyncPeek (fl));
				  }
				 catch { return (Answer.createSyncPeekError ("File \"" + fileName + "\" could not be read!", EAnswerStatus.AS_Warning)); }
				}
	
	private bool	isSupervisorCall (ISession ses)
				{
				 if (ses == null) return (false);

				 return (ses.userId == 1 && (ses.userName == "Admin" || ses.userName == "Administrator"));
				}
	
	private string	makeCmdFile (ISession ses, string fileName, BI.Process.IOpenTaskFileQuery sendFile, bool isSuperCall)
				{
				 System.Text.StringBuilder sb = new System.Text.StringBuilder ();
				 sb = sb.AppendLine ("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				 sb = sb.AppendLine ("<FeatCommandFile>");
				 sb = sb.Append ("  <Identification speedId=\"").Append (sendFile.id).Append ("\" userId=\"").Append (ses.userId).
				           Append ("\" fileName=\"").Append (bi.xml.encode (fileName)).Append ("\" userName=\"").Append (bi.xml.encode (ses.userName)).
						   Append ("\" fromUserName=\"").Append (bi.xml.encode (sendFile.fromUser.description)).Append ("\" sendTime=\"").Append (bi.xml.formatDT (sendFile.created)).AppendLine ("\" />");
				 
				 sb = sb.Append ("  <Operation editable=\"").Append ((sendFile.stateId == (int) BI.Process.EStateText.ST_Konvertovano)? "False": "True").AppendLine ("\" mode=\"normal\">");
				 
				 if (isSuperCall) sb = appendCmdFileCommand (sb, sendFile.stateId, "CurrState", "CurrState");
				 else switch (sendFile.stateId)
				  {
				   case (int) BI.Process.EStateText.ST_Konvertovano:
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_KAnotaci, "Odsouhlasit", "V pořádku");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_ChybaKonverze, "Odlozit", "Odložit");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_Nacteno, "ZnovuKonvertovat", "Znovu konvertovat");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_KNacteni, "ZnovuNacist", "Znovu načíst");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_Vybran, "Vratit", "Vrátit nezpracované");
					 break;
				   
				   case (int) BI.Process.EStateAnotace.SA_Prideleno:
				   case (int) BI.Process.EStateAnotace.SA_VAnotaci:
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_KOdevzdani, "Odevzdavam", "Odevzdávám");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_KOvereni, "Testuji", "Předávám na automatický test");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_Prideleno, "Ukladam", "Ukládám rozpracované na server");
				     break;
				   
				   case (int) BI.Process.EStateAnotace.SA_Odevzdano:
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_KPrenosu, "Schvaluji", "Schvaluji");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_KOdevzdani, "Testuji", "Předávám na automatický test");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_Prideleno, "Vracim", "Vracím s připomínkami");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateAnotace.SA_Zruseno, "Odebiram", "Ruším anotaci a odebírám");
				     break;
				   
				   case (int) BI.Process.EStateText.ST_AdjPrideleno:
				   case (int) BI.Process.EStateText.ST_AdjProblem:
				   case (int) BI.Process.EStateText.ST_AdjVraceno:
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_Adjudikovano, "Odevzdavam", "Odevzdávám");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_AdjPrideleno, "Ukladam", "Ukládám rozpracované na server");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_AdjProblem, "Problem", "Mám problém, potřebuji poradit");
				     break;

				   case (int) BI.Process.EStateText.ST_Adjudikovano:
				   case (int) BI.Process.EStateText.ST_AdjKontrola:
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_DoAmesu, "Schvaluji", "Schvaluji");
				     sb = appendCmdFileCommand (sb, (int) BI.Process.EStateText.ST_AdjVraceno, "Vracim", "Vracím s připomínkami");
					 break;

				   default: return (null);
				  }
				 sb = sb.AppendLine ("  </Operation>");

				 sb = sb.AppendLine ("  <Data>");

				 Database.IFinder cmtFnd = bi.process.commentTable.createFinder ();
				 cmtFnd.add (bi.process.getFieldName (BI.Process.ECommentField.CF_ProcessId), sendFile.processId);
				 cmtFnd.sortBy (bi.database.sortByField (bi.process.getFieldName (BI.Process.ECommentField.CF_At)));

				 Database.IFinder tskFnd = bi.process.taskTable.createFinder ();
				 tskFnd.add (bi.process.getFieldName (BI.Process.ETaskField.TF_ProcessId), sendFile.processId);
				 tskFnd.sortBy (bi.database.sortByField (bi.process.getFieldName (BI.Process.ETaskField.TF_Created)));

				 using (Database.IRecordSet<BI.Process.IComment> cmtRs = bi.process.commentTable.findObj (cmtFnd))
				  using (Database.IRecordSet<BI.Process.ITask> tskRs = bi.process.taskTable.findObj (tskFnd))
				   {
				    bool isCmt = cmtRs.moveNext ();
					bool isTsk = tskRs.moveNext ();

					while (isCmt || isTsk)
					 {
					  bool callCmt;

					  if (isCmt) if (isTsk) callCmt = (cmtRs.currObj.at < tskRs.currObj.created);
					             else callCmt = true;
					  else callCmt = false;

					  if (callCmt)
					   {
					    sb = sb.Append ("    <Comment userName=\"").Append (bi.xml.encode (cmtRs.currObj.user.description)).Append ("\" at=\"").Append (bi.xml.formatDT (cmtRs.currObj.at)).AppendLine ("\">");
						sb = sb.Append ("      ").AppendLine (bi.xml.encode (cmtRs.currObj.text));
					    sb = sb.AppendLine ("    </Comment>");
						isCmt = cmtRs.moveNext ();
					   }
					  else
					   {
					    sb = sb.Append ("    <Comment userName=\"").Append (bi.xml.encode (tskRs.currObj.fromUser.description)).Append ("\" at=\"").Append (bi.xml.formatDT (tskRs.currObj.created)).AppendLine ("\">");
						sb = sb.Append ("      ").AppendLine (bi.xml.encode ("Předáno"));
					    sb = sb.AppendLine ("    </Comment>");
						isTsk = tskRs.moveNext ();
					   }
					 }
				   }

				 sb = sb.AppendLine ("  </Data>");
				 sb = sb.AppendLine ("</FeatCommandFile>");
				 return (sb.ToString ());
				}
	private System.Text.StringBuilder	appendCmdFileCommand (System.Text.StringBuilder sb, int cmdId, string name, string title)
				{ return (sb.Append ("    <Command id=\"").Append (cmdId).Append ("\" name=\"").Append (bi.xml.encode (name)).Append ("\" title=\"").Append (bi.xml.encode (title)).AppendLine ("\" />")); }
	
	private string	peekFile (BI.IReference<BI.ProcessData.IFile> flRef)
				{
				 try
				  {
				   BI.ProcessData.IFile fl = bi.processData.fileTable.getObject (flRef.id);
				 
				   if (fl == null || fl.data == null || fl.data.Length < 1) return (null);
				   return (enc.GetString (fl.data));
				   //using (System.IO.StreamReader sr = new System.IO.StreamReader (new System.IO.MemoryStream (fl.data)))
				   // return (sr.ReadToEnd ());
				  }
				 catch { return (null); }
				}

	public IAnswer	fileRead (string sessionId, string callUrl, string taskId)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createPeekError ("Invalid session!"));

				 Database.ICondBuilder cb = createBasicInboxCond (ses.userId, useOnlyAnotace: false);
				 cb.addValCond (cb.fullFieldName (tableAcr: bi.process.openTaskFileQuery.name, fieldName: bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_Id)), bi.language.tryScanLong (taskId));

				 using (Database.IRecordSet<BI.Process.IOpenTaskFileQuery> rs = bi.process.openTaskFileQuery.findObj (cb.create ()))
				  if (rs.moveNext ())
				   try
					{ 
					 if (bi.language.isNull (rs.currObj.started))
					  { BI.Process.ITask tsk = bi.process.taskTable.getObject (rs.currObj.id, true); tsk.start (); }
					 return (Answer.createAnswer ());
					}
				   catch { return (Answer.createAnswerError ("Task id=\"" + taskId + "\" is possibly used by other user!", EAnswerStatus.AS_Warning)); }

				 return (Answer.createPeekError ("Task id=\"" + taskId + "\" not found!", EAnswerStatus.AS_Warning));
				}

	private Database.ICondBuilder	createBasicInboxCond (long userId, bool isSuperCall = false, bool useOnlyAnotace = true)
				{
				 Database.ICondBuilder cb = bi.database.condBuilder ();

				 if (!isSuperCall) cb.addValCond (bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_ToUserId), userId);
				 Database.ICondBuilder stateCond = cb.addOrCond ();
				 string stateFldName = bi.process.getFieldName (BI.Process.EOpenTaskFileQueryField.OTFQ_StateId);
				 
				 if (isSuperCall)
				  {
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_Konvertovano);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_Prideleno);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_VAnotaci);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_Odevzdano);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_DoAmesu);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_Anotovano);

				   if (!useOnlyAnotace)
				    {
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_Anotovan);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjPrideleno);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjProblem);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjVraceno);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_Adjudikovano);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjKontrola);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_DoAmesu);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjUlozeno);
					}
				  }
				 else
				  {
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_Konvertovano);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_Prideleno);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_VAnotaci);
				   stateCond.addValCond (stateFldName, (short) BI.Process.EStateAnotace.SA_Odevzdano);

				   if (!useOnlyAnotace)
				    {
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjPrideleno);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjProblem);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjVraceno);
					 stateCond.addValCond (stateFldName, (short) BI.Process.EStateText.ST_AdjKontrola);
					}
				  }

				 return (cb);
				}

	public IAnswer	saveFile (string sessionId, string callUrl, IFile file)
				{
				 ISession ses = SessionKeeper.getKeeper ().check (sessionId, callUrl);

				 if (ses == null) return (Answer.createPeekError ("Invalid session!"));

				 try
				  {
				   Xml.IDocument cmdDoc = bi.xml.parse (file.cmdFile);
				   if (ses.userId != bi.language.tryScanLong (cmdDoc.findAttr ("FeatCommandFile\\Identification", "userId")))
				    return (saveUnTaskFile (ses, bi.language.tryScanLong (cmdDoc.findAttr ("FeatCommandFile\\Identification", "speedId")), file));

				   BI.Process.ITask tsk = bi.process.taskTable.getObject (bi.language.tryScanLong (cmdDoc.findAttr ("FeatCommandFile\\Identification", "speedId")));
				   if (tsk == null) return (saveUnTaskFile (ses, 0, file));

				   string stateComment = cmdDoc.findValue ("FeatCommandFile\\Result");
				   if (stateComment != null && stateComment.Trim ().Length > 0)
				    {
					 BI.Process.IComment com = bi.process.commentTable.createNewObj ();
					 com.process.id = tsk.process.id;
					 com.user.id    = ses.userId;
					 com.at         = System.DateTime.Now;
					 com.text		= stateComment;
					 using (Database.ITransaction trans = bi.database.openTransaction ())
					  { bi.process.commentTable.insert (trans, com); trans.finalyOk = true; }
					}

				   if (ses.userId != tsk.toUser.id) return (saveUnTaskFile (ses, tsk.id, file));

				   int cmdStateId = bi.language.tryScanInt (cmdDoc.findAttr ("FeatCommandFile\\Result", "commandId"));

				   if (tsk.isAble2Process (ses.userId, cmdStateId))
				    using (Database.ITransaction trans = bi.database.openTransaction ())
					 {
					  BI.ProcessData.IProcessData procDta = createNewProcessData (trans, tsk, file);
					  tsk.makeProcess (ses.userId, cmdStateId, trans: trans, newDta: procDta);
					  trans.finalyOk = true;

					  return (Answer.createAnswer ());
					 }
				   else return (saveUnTaskFile (ses, tsk.id, file));
				  }
				 catch { return (Answer.createAnswerError ("Unable to save file" + ((file == null || file.name == null)? "": " \"" + file.name + "\" ") + "!")); }
				}

	public IAnswer	saveFile (string sessionId, string callUrl, ISynchronizedFile file)
				{
				 string fileName, cmdFile, htmlFile, xmlWFile, xmlAFile, xmlBFile;

				 fileName = cmdFile = htmlFile = xmlWFile = xmlAFile = xmlBFile = null;

				 if (file != null)
				  {
				   fileName = file.name;
				   
				   if (file.head != null)
				    foreach (ISynchronizedFileContent cont in file.head)
					 if (cont != null && cont.isTextBased && cont.extension != null && (cont.extension.ToLower () == "cmd.xml" || cont.extension.ToLower () == ".cmd.xml")) cmdFile = cont.textContent;
				  
				   if (file.folders != null)
				    foreach (ISynchronizedFileFolder fld in file.folders)
					 if (fld != null && fld.isWorkOnFile && fld.content != null)
					  foreach (ISynchronizedFileContent cont in fld.content)
					   if (cont != null && cont.isTextBased && cont.extension != null)
					    {
						 string ext = cont.extension.ToLower ();

						 if (ext == "html" || ext == ".html" || ext == "htm" || ext == ".htm") htmlFile = cont.textContent;
						 else if (ext == "w.xml" || ext == ".w.xml") xmlWFile = cont.textContent;
						 else if (ext == "a.xml" || ext == ".a.xml") xmlAFile = cont.textContent;
						 else if (ext == "b.xml" || ext == ".b.xml") xmlBFile = cont.textContent;
						}
				  
				   return (saveFile (sessionId: sessionId, callUrl: callUrl, file: File.create (nm: fileName, cmd: cmdFile, htm: htmlFile, xmlW: xmlWFile, xmlA: xmlAFile, xmlB: xmlBFile)));
				  }
				 else return (Answer.createAnswerError ("Null file!"));
				}
	
	private IAnswer	saveUnTaskFile (ISession ses, long taskId, IFile file)
				{
				 using (Database.ITransaction trans = bi.database.openTransaction ())
				  { IAnswer ans = saveUnTaskFile (trans, ses, taskId, file); trans.finalyOk = true; return (ans); }
				}

	private IAnswer	saveUnTaskFile (Database.ITransaction trans, ISession ses, long taskId, IFile file)
				{
				 BI.Process.ITask tsk = bi.process.taskTable.getObject (taskId);
				 
				 if (file != null)
				  {
				   if (tsk == null) tsk  = bi.process.taskTable.createNewObj ();
				   
				   BI.ProcessData.IProcessData procDta = createNewProcessData (trans, tsk, file);
				   if (tsk != null) tsk.data.id = ((procDta == null)? 0: procDta.id);
			      }
				 else if (tsk == null) return (Answer.createAnswerError ("No task, no file!", EAnswerStatus.AS_Warning));

				 if (tsk == null) return (Answer.createAnswerError ("No task!", EAnswerStatus.AS_Warning));

				 tsk.created     = System.DateTime.Now;
				 tsk.fromUser.id = ses.userId;
				 tsk.stateId     = (int) BI.Process.EStateSpecial.SS_WrongCall;
				 tsk.stateDesc   = bi.process.getStateName (BI.Process.EStateSpecial.SS_WrongCall);
				 tsk.priority    = 0;
				 tsk.started     = tsk.created;
				 tsk.toUser.id   = ses.userId;
				 tsk.finished    = tsk.created;

				 bi.process.taskTable.insert (trans, tsk);

				 return (Answer.createAnswerError ("Not authorized user for file!", EAnswerStatus.AS_Warning));
				}

	private BI.ProcessData.IProcessData	createNewProcessData (Database.ITransaction trans, BI.Process.ITask tsk, IFile file)
				{
			     BI.ProcessData.IProcessData procDta = ((tsk == null)? null: tsk.data.parent);

				 BI.ProcessData.IProcessData newProcDta = ((procDta != null)? procDta.createNewCopy (bi.processData.processDataTable): bi.processData.processDataTable.createNewObj ());
				 
			     switch (newProcDta.type)
			      {
			       case BI.ProcessData.EProcessData.PD_Text:
			         {
				      BI.ProcessData.IProcessDataText txtDta = newProcDta.text;
				      txtDta.html.id = saveFile (trans, name: file.name + ".html", dta: file.htmlFile, flRef: ((procDta != null)? procDta.text.html: null));
					  txtDta.xmlW.id = saveFile (trans, name: file.name + ".w.xml", dta: file.xmlWFile, flRef: ((procDta != null)? procDta.text.xmlW: null));
					  txtDta.xmlA.id = saveFile (trans, name: file.name + ".a.xml", dta: file.xmlAFile, flRef: ((procDta != null)? procDta.text.xmlA: null));
					  txtDta.xmlB.id = saveFile (trans, name: file.name + ".b.xml", dta: file.xmlBFile, flRef: ((procDta != null)? procDta.text.xmlB: null));
				      break;
				     }
			       case BI.ProcessData.EProcessData.PD_Anotace:
			         {
				      BI.ProcessData.IProcessDataAnotace anoDta = newProcDta.anotace;
				      anoDta.html.id = saveFile (trans, name: file.name + ".html", dta: file.htmlFile, flRef: ((procDta != null)? procDta.anotace.html: null));
					  anoDta.xmlW.id = saveFile (trans, name: file.name + ".w.xml", dta: file.xmlWFile, flRef: ((procDta != null)? procDta.anotace.xmlW: null));
					  anoDta.xmlA.id = saveFile (trans, name: file.name + ".a.xml", dta: file.xmlAFile, flRef: ((procDta != null)? procDta.anotace.xmlA: null));
					  anoDta.xmlB.id = saveFile (trans, name: file.name + ".b.xml", dta: file.xmlBFile, flRef: ((procDta != null)? procDta.anotace.xmlB: null));
				      break;
				     }
			      }
			     bi.processData.processDataTable.insert (trans, newProcDta);
				 return (newProcDta);
				}
	
	private long	saveFile (Database.ITransaction trans, string name, string dta, BI.IReference<BI.ProcessData.IFile> flRef)
				{
				 string content = ((flRef != null)? peekFile (flRef): null);

				 if (!(isFileContentNull (content))) if (content == dta) return (flRef.id);

				 BI.ProcessData.IFile fl = bi.processData.fileTable.createNewObj ();
				 fl.name = name;
				 if (dta != null) fl.data = enc.GetBytes (dta);
				 bi.processData.fileTable.insert (trans, fl);
				 return (fl.id);
				}

	private bool isFileContentNull (string content)	{ return (content == null || content.Trim () == ""); }

	private class SessionKeeper : ISessionKeeper
	 {
	  private SessionKeeper ()	{ m_Sessions = new System.Collections.Generic.Dictionary<string, ISession> (); }
	  private System.Collections.Generic.IDictionary<string, ISession>	m_Sessions;

	  public static ISessionKeeper getKeeper ()
				{
				 if (ms_Keeper == null) ms_Keeper = new SessionKeeper ();
				 return (ms_Keeper);
				}
	  private static ISessionKeeper	ms_Keeper = null;

	  public ISession	login (BI.User.IUser usr, string callUrl)
				{
				 if (usr == null) return (null);

				 if (m_Sessions.Count > 10) cleanSessions ();

				 ISession ses = new Session (this, usr.id, usr.name, callUrl);
				 try { lock (m_Sessions) m_Sessions.Add (ses.id, ses); } catch { return (null); }

				 return (ses);
				}
	  
	  public ISession	check (string sessionId, string callUrl)
				{
				 try
				  {
				   ISession ses;  lock (m_Sessions) ses = m_Sessions [sessionId];
				   if (ses.hostUrl == callUrl && ses.lastAction.AddMinutes (10) > System.DateTime.Now)
				    { ses.lastAction = System.DateTime.Now; return (ses); }
				   else
				    {
					 lock (m_Sessions) m_Sessions.Remove (ses.id);
					 return (null);
					}
				  }
				 catch	{ return (null); }
				}

	  private void	cleanSessions ()
				{
				 System.Collections.Generic.IList<string> oldSesList = new System.Collections.Generic.List<string> ();

				 lock (m_Sessions)
				  foreach (ISession dictSes in m_Sessions.Values)
				   if (dictSes != null) if (dictSes.lastAction.AddMinutes (10) < System.DateTime.Now) oldSesList.Add (dictSes.id);

				 try	{ foreach (string sesId in oldSesList) lock (m_Sessions) m_Sessions.Remove (sesId); }
				 catch  { }
				}

	  private class Session : ISession
	   {
	    public Session (SessionKeeper par, long usr, string usrNm, string url)
					{
					 m_Parent = par; userId = usr; userName = usrNm; lastAction = System.DateTime.Now; hostUrl = url;

					 string tmp = (usr * 13 - 1).ToString ();
					 string tmpLen = tmp.Length.ToString ();
					 string tmpId = tmpLen.Length.ToString () + tmpLen + tmp;
					 tmp = System.DateTime.Now.Ticks.ToString ();
					 tmpLen = tmp.Length.ToString ();
					 id = tmpId + tmpLen.Length.ToString () + tmpLen + tmp;
					}
	    
		private SessionKeeper	m_Parent;
	    public string			id			{ get; private set; }
		public string			userName	{ get; private set; }
	    public long				userId		{ get; private set; }
	    public System.DateTime	lastAction	{ get; set; }
	    public string			hostUrl		{ get; private set; }

	    public void	logout () { lastAction = new System.DateTime (); m_Parent.m_Sessions.Remove (id); }
	   }
	 }

	private class Answer : IAnswer, ICreateSessionAnswer, ICheckUserInboxAnswer, IUserInboxHeaderAnswer, IPeekFileAnswer, IPeekSynchronizedFileAnswer
	 {
	  private Answer (string msg = null, EAnswerStatus ansStatus = EAnswerStatus.AS_Ok)	{ isOk = ansStatus; errMsg = msg; sessionId = null; inboxTaskIdList = null; file = null; syncFile = null; }

	  public static IAnswer						createAnswerError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static ICreateSessionAnswer		createSessionError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static ICheckUserInboxAnswer		createCheckError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static IUserInboxHeaderAnswer		createInboxError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static IPeekFileAnswer				createPeekError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)		{ return (new Answer (p_ErrMsg, ansStatus)); }
	  public static IPeekSynchronizedFileAnswer	createSyncPeekError (string p_ErrMsg, EAnswerStatus ansStatus = EAnswerStatus.AS_Error)	{ return (new Answer (p_ErrMsg, ansStatus)); }

	  public static IAnswer						createAnswer (string msg = null)							{ return (new Answer (msg)); }
	  public static ICreateSessionAnswer		createSession (string sesId, string msg = null)				{ Answer ans = new Answer (msg); ans.sessionId = sesId; return (ans); }
	  public static ICheckUserInboxAnswer		createCheck (string[] namesLst, string msg = null)			{ Answer ans = new Answer (msg); ans.inboxFileNameList = namesLst; return (ans); }
	  public static IUserInboxHeaderAnswer		createInbox (string[] namesLst, string msg = null)			{ Answer ans = new Answer (msg); ans.inboxTaskIdList = namesLst; return (ans); }
	  public static IPeekFileAnswer				createPeek (IFile fl, string msg = null)					{ Answer ans = new Answer (msg); ans.file = fl; return (ans); }
	  public static IPeekSynchronizedFileAnswer	createSyncPeek (ISynchronizedFile fl, string msg = null)	{ Answer ans = new Answer (msg); ans.syncFile = fl; return (ans); }

      public EAnswerStatus	isOk	{ get; private set; }
	  public string			errMsg	{ get; private set; }

	  public string	sessionId	{ get; private set; }

      public string[]	inboxFileNameList	{ get; private set; }
	  public string[]	inboxTaskIdList		{ get; private set; }

	  public IFile	file	{ get; private set; }

	  public ISynchronizedFile	syncFile	{ get; private set; }
	 }

	private class File : IFile
	 {
      private File (string nm, string cmd, string htm, string xmlW, string xmlA, string xmlB)	{ name = nm; cmdFile = cmd; htmlFile = htm; xmlWFile = xmlW; xmlAFile = xmlA; xmlBFile = xmlB;  }

	  public static IFile	create (string nm, string cmd, string htm, string xmlW, string xmlA, string xmlB)	{ return (new File (nm: nm, cmd: cmd, htm: htm, xmlW: xmlW, xmlA: xmlA, xmlB: xmlB)); }
	  
	  public string	name		{ get; private set; }
	  public string	cmdFile		{ get; private set; }
	  public string	htmlFile	{ get; private set; }
	  public string	xmlWFile	{ get; private set; }
	  public string	xmlAFile	{ get; private set; }
	  public string	xmlBFile	{ get; private set; }
	 }

    private class SynchronizedFile : ISynchronizedFile
     {
	  private SynchronizedFile (string nm, ISynchronizedFileContent[] hd, ISynchronizedFileFolder[] fld)	{ name = nm; head = hd; folders = fld; }
      
	  public static ISynchronizedFile	create (string nm, ISynchronizedFileContent[] hd, ISynchronizedFileFolder[] fld)	{ return (new SynchronizedFile (nm, hd, fld)); }
	  public static ISynchronizedFile	create (string nm, ISynchronizedFileContent cmdFile, ISynchronizedFileFolder[] fld)	{ return (new SynchronizedFile (nm, new ISynchronizedFileContent[1] { cmdFile }, fld)); }
	  public static ISynchronizedFile	create (string nm, string cmdFile, ISynchronizedFileFolder[] fld)					{ return (create (nm, SynchronizedFileContent.createTxt ("cmd.xml", val: cmdFile), fld)); }
	  public static ISynchronizedFile	create (string nm, ISynchronizedFileContent cmdFile, ISynchronizedFileFolder fld)	{ return (create (nm, cmdFile, new ISynchronizedFileFolder [1] { fld })); }
	  public static ISynchronizedFile	create (string nm, string cmdFile, ISynchronizedFileFolder fld)						{ return (create (nm, cmdFile, new ISynchronizedFileFolder [1] { fld })); }
	  public static ISynchronizedFile	create (string nm, string cmdFile, string htmlFile, string xmlWFile, string xmlAFile, string xmlBFile, bool isRO = false, bool isWrkOn = true, string auth = null)
				{ return (create (nm, cmdFile, SynchronizedFileFolder.create (htmlFile: htmlFile, xmlWFile: xmlWFile, xmlAFile: xmlAFile, xmlBFile: xmlBFile, isRO: isRO, isWrkOn: isWrkOn, auth: auth))); }

	  public string						name	{ get; private set; }
	  public ISynchronizedFileContent[]	head	{ get; private set; }
	  public ISynchronizedFileFolder[]	folders	{ get; private set; }
     }

    private class SynchronizedFileFolder : ISynchronizedFileFolder
     {
      private SynchronizedFileFolder (bool isRO, bool isWrkOn, string auth, ISynchronizedFileContent[] cont)	{ isReadOnly = isRO; isWorkOnFile = isWrkOn; author = auth; content = cont; }

	  public static ISynchronizedFileFolder	create (ISynchronizedFileContent[] cont, bool isRO = false, bool isWrkOn = true, string auth = null)
				{ return (new SynchronizedFileFolder (isRO: isRO, isWrkOn: isWrkOn, auth: auth, cont: cont)); }

	  public static ISynchronizedFileFolder	create (string htmlFile, string xmlWFile, string xmlAFile, string xmlBFile, bool isRO = false, bool isWrkOn = true, string auth = null)
				{
				 ISynchronizedFileContent[] cont = new ISynchronizedFileContent[4];
				 
				 cont [0] = SynchronizedFileContent.createTxt (ext: "html", val: htmlFile);
				 cont [1] = SynchronizedFileContent.createTxt (ext: "w.xml", val: xmlWFile);
				 cont [2] = SynchronizedFileContent.createTxt (ext: "a.xml", val: xmlAFile);
				 cont [3] = SynchronizedFileContent.createTxt (ext: "b.xml", val: xmlBFile);
				 return (new SynchronizedFileFolder (isRO: isRO, isWrkOn: isWrkOn, auth: auth, cont: cont));
				}

	  public bool						isReadOnly		{ get; private set; }
	  public bool						isWorkOnFile	{ get; private set; }
	  public string						author			{ get; private set; }
	  public ISynchronizedFileContent[]	content			{ get; private set; }
     }

    private class SynchronizedFileContent : ISynchronizedFileContent
     {
      private SynchronizedFileContent (string ext, bool isText, string txtVal = null, byte[] binVal = null) { extension = ext; isTextBased = isText; textContent = txtVal; isBinary = (!isText); binaryContent = binVal; }

	  public static ISynchronizedFileContent	createTxt (string ext, string val)	{ return (new SynchronizedFileContent (ext: ext, isText: true, txtVal: val)); }
	  public static ISynchronizedFileContent	createBin (string ext, byte[] val)	{ return (new SynchronizedFileContent (ext: ext, isText: false, binVal: val)); }

	  public string	extension		{ get; private set; }
	  public bool	isTextBased		{ get; private set; }
	  public string	textContent		{ get; private set; }
	  public bool	isBinary		{ get; private set; }
	  public byte[]	binaryContent	{ get; private set; }
     }
   }
 }
