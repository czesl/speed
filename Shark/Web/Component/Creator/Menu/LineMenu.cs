﻿using System;
using System.Collections.Generic;
using System.Web;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// One line of menu
	/// </summary>
	internal class LineMenu : Component, ILineMenu, IMenuItem // ILine menu describes operations of line menu and IMenuItem enable using LineMenu as MenuItem (submenu)
	{
		#region private fields

		/// <summary>
		/// Menu items
		/// </summary>
		private Dictionary<int, IMenuItem> _children;

		/// <summary>
		/// Describtion of LineMenu as menu item
		/// </summary>
		private MenuItemContainer _container;

		/// <summary>
		/// Nested level
		/// </summary>
		private int _level;

		#endregion

		#region constructors

		/// <summary>
		/// Create LineMenu and set his parameters
		/// </summary>
		/// <param name="creator">Component Creator</param>
		/// <param name="control">Control for creating html output</param>
		/// <param name="container">Container with menu item description</param>
		/// <param name="children">Submenu menu items</param>
		/// <param name="level">Nested level</param>
		internal LineMenu(ICreator creator, IControl control, MenuItemContainer container, Dictionary<int, IMenuItem> children, int level)
			: base(creator, control)
		{
			_children = children;
			_level = level;
		}

		#endregion


		#region Implementation of IMenuItem

		/// <summary>
		/// Is LineMenu displayable?
		/// </summary>
		/// <returns></returns>
		public bool IsDisplayable()
		{
			// if menu has no displayable child return false
			bool hasDisplayableChild = false;
			foreach ( IMenuItem item in _children.Values)
			{
				if (item.IsDisplayable() )
				{
					hasDisplayableChild = true;
					break;
				}
			}
			if (!hasDisplayableChild) return false;

			// handle permissions
			return _container.IsDisplayable();
		}

		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		public override void generate()
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Implementation of ILineMenu

		public int currValue
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		public IComponent currComp
		{
			get { throw new NotImplementedException(); }
		}

		#endregion
	}
}