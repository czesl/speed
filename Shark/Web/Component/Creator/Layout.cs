﻿using System;
using Shark.Web.Component;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Component for rendering Layout
	/// </summary>
	internal class Layout : Component
	{
		#region private fields

		/// <summary>
		/// Array of components to be rendered in layout
		/// </summary>
		private readonly IComponent[][] _layoutComponents;
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		/// <param name="layoutComponents">Array of components for layout</param>
		public Layout(ICreator creator, IControl control, IComponent[][] layoutComponents) : base(creator, control)
		{
			_layoutComponents = layoutComponents;

			foreach (IComponent[] components in layoutComponents)
			{
				foreach (IComponent component in components)
				{
					if(component == null)
					{
						continue;
					}

					_subcomponents.Add(component);
				}
			}
		}
		#endregion


		#region Overrides of Component

		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		/// <exception cref="NullReferenceException">Exception thrown when there is invalid <see cref="Creator"/> object assigned to this component</exception>
		public override void generate()
		{
			IControl control = _control;

			control.table(border: "0");

			foreach (IComponent[] components in _layoutComponents)
			{
				control.tr();

				foreach (IComponent component in components)
				{
					control.td();
			
					if(component != null)
					{
						component.generate();
					}
					else
					{
						control.write("&nbsp;");
					}

					control.tdEnd();
				}
				
				control.trEnd();
			}

			control.tableEnd();
		}
		#endregion
	}
}