﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Create simple menu, just with menu items (no submenu)
	/// </summary>
	internal class MenuFiller : IMenuFiller
	{

		#region private fields

		/// <summary>
		/// Menu item creators
		/// </summary>
		protected Dictionary<int, IMenuItemCreator> children; 

		#endregion

		internal MenuFiller (ICreator subcreator)
		{
			subCreator = subcreator;
		}

		#region Implementation of IParent

		public ICreator subCreator { get; private set; }

		#endregion

		#region Implementation of IMenuFiller

		/// <summary>
		/// Add new menu item with created component
		/// </summary>
		/// <param name="value"></param>
		/// <param name="comp"></param>
		/// <param name="label"></param>
		/// <param name="desc"></param>
		/// <param name="parm"></param>
		public void addMenuItem(int value, IComponent comp, string label, string desc = null, DMenuPermision parm = null)
		{
			MenuItemContainer container = new MenuItemContainer{Value = value, Label = label, Description = desc, Permission = parm};
			children[value] = new MenuItemCreator(container, comp);
		}

		/// <summary>
		/// Add new menu item with component creator (lazy creation of component)
		/// </summary>
		/// <param name="value"></param>
		/// <param name="compCre"></param>
		/// <param name="label"></param>
		/// <param name="desc"></param>
		/// <param name="parm"></param>
		public void addMenuItem(int value, DMenuCreator compCre, string label, string desc = null, DMenuPermision parm = null)
		{
			MenuItemContainer container = new MenuItemContainer { Value = value, Label = label, Description = desc, Permission = parm };
			children[value] = new MenuItemCreator(container, compCre);
		}

		#endregion
	}
}