﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Xsl;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Creator for creating new menu item
	/// </summary>
	internal class MenuItemCreator : IMenuItemCreator
	{
		#region private fields

		/// <summary>
		/// Container with information of menu item
		/// </summary>
		private MenuItemContainer _container;

		/// <summary>
		/// Menu item component
		/// </summary>
		private IComponent _component;

		/// <summary>
		/// Menu item component creator
		/// </summary>
		private DMenuCreator _componentCreator;

		#endregion

		#region constructors

		/// <summary>
		/// Creator with component
		/// </summary>
		/// <param name="container"></param>
		/// <param name="component"></param>
		internal MenuItemCreator(MenuItemContainer container, IComponent component)
		{
			_container = container;
			_component = component;
		}

		/// <summary>
		/// Creator with component creator
		/// </summary>
		/// <param name="container"></param>
		/// <param name="componentCreator"></param>
		internal MenuItemCreator(MenuItemContainer container, DMenuCreator componentCreator)
		{
			_container = container;
			_componentCreator = componentCreator;
		}

		#endregion

		#region Implementation of IMenuItemCreator

		/// <summary>
		/// Create new menu item
		/// </summary>
		/// <returns></returns>
		public IMenuItem create()
		{
			if (_component != null ) return new MenuItem(_container, _component);
			if (_componentCreator != null ) return new MenuItem(_container, _componentCreator);
			throw new NullReferenceException("Missing component and component creator.");
		}

		#endregion
	}
}