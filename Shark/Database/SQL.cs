﻿//server=192.168.1.5;uid=db4035;pwd=Speed;database=db4035
namespace Shark.Database.SQL
 {
  public interface IParamBuilder
   {
    // returns created param name
    string	addNull (EFieldType type);

	string	add (IValue val);
	string	add (long val);
	string	add (int val);
	string	add (short val);
	string	add (bool val);
	string	add (System.DateTime val);
	string	add (string val);
	string	add (byte[] val);
   }
  
  public interface ICondBuilder
   {
    IParamBuilder	parBld	{ get; }

	string	createTableAcr ();
    string	fullFieldName (string tableAcr, string fieldName);

	void	addIsNull (string leftSide);
	void	addCond (string leftSide, ECondOper oper, string rightSide);

	ICondBuilder	addNotCond ();
	ICondBuilder	addAndCond ();
	ICondBuilder	addOrCond ();

	ICondBuilder	addExists (string tableName, string tableField, string tableAcr = null, string tableCondFld = null, string parentValueSide = null);
   }
  
  public interface IInsert
   {
    IParamBuilder	parBld	{ get; }
	
    string	tableName	{ get; set; }

	void	add (string fieldName, string parName);

	void	insert ();
	long	insert (string autoIncrementKeyName);
   }

  public interface IUpdate
   {
    IParamBuilder	parBld	{ get; }
	ICondBuilder	andCnd	{ get; }
	
    string	tableName	{ get; set; }

	void	add (string fieldName, string leftSide);

	void	update ();
   }

  public interface IDelete
   {
    IParamBuilder	parBld	{ get; }
	ICondBuilder	andCnd	{ get; }
	
    string	tableName	{ get; set; }

	void	delete ();
   }

  public interface IJoinCond
   {
    string	leftSide	{ get; }
    string	rightSide	{ get; }
   }
  
  public enum ESelectGroupOperation	{ SGO_Max, SGO_Min, SGO_Avg, SGO_Count, SGO_First }

  public interface ISelect : IClose
   {
    IParamBuilder	parBld	{ get; }
	ICondBuilder	andCnd	{ get; }

	string			createTableAcr ();
    string			fullFieldName (string queryAcr, string fieldName);
	string			createFieldNameAcr ();
	IJoinCond		createJoinCond (string leftSide, string rightSide);
	ISortByField	createSortByField (string fullFieldName, bool asc = true);

	void	addTable (string queryName, bool useAllFields = false, string queryAcr = null, ESelectJoin join = ESelectJoin.SJ_Join, IJoinCond oneCond = null, IJoinCond[] conds = null);
	void	addShowField (string fullFieldName, string fieldAcr = null);
	//OPTI void	addGroupField (string fullFieldName, ESelectGroupOperation grpOper, string fieldAcr);

	//OPTI string[]		groupByFields	{ get; set; }
	ISortByField[]	sortByFields	{ get; set; }

	long	maxRows		{ get; set; }	// -1 means every
	bool	forUpdate	{ get; set; }

	IFetch	select ();
   }

  public interface IFetch : IClose
   {
    bool	moveNext ();

	bool	isDefined (string fieldName);
	bool	isNull (string fieldName);

	long			readLng (string fieldName);
	int				readInt (string fieldName);
	short			readShort (string fieldName);
	bool			readBool (string fieldName);
	System.DateTime	readDateTime (string fieldName);
	string			readString (string fieldName);
	byte[]			readBLOB (string fieldName);
   }

  public interface IEngine : IClose
   {
    string	fullFieldName (string tableAcr, string fieldName);
	
	ITransaction	openTransaction ();

	IInsert	insert (ITransaction trans, string tableName = null);
	IUpdate	update (ITransaction trans, string tableName = null);
	IDelete	delete (ITransaction trans, string tableName = null);
	ISelect	select (ITransaction trans = null, string tableName = null, bool useAllFields = false, string tableAcr = null);
   }
 }
