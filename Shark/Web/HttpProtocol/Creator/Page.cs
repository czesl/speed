﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Shark.Web.HttpProtocol.Creator
{
	class Page : Control, IPage
	{
		#region private variables

		/// <summary>
		/// Aktuálně používáné odsazení
		/// </summary>
		private int _currIndent = 0;

		#endregion

		#region protected properties

		protected override HttpRequest Request { get; set; }

		protected override HttpResponse Response { get; set; }

		
		/// <summary>
		/// Objekt stránky (rodič všech controlů)
		/// </summary>
		protected override Page RootPage
		{
			get { return this; }
		}

		#endregion

		#region protected methods

		/// <summary>
		/// Inkrementuje odsazení
		/// </summary>
		protected override void IncIndent()
		{
			_currIndent++;
		}

		/// <summary>
		/// Dekrementuje odsazení
		/// </summary>
		protected override void DecIndent()
		{
			 _currIndent--;

			if (_currIndent < 0)
				throw new InvalidOperationException("Odsazení je menší než 0. Zkontrolujte počet otevíracích a uzavíracích tagů");
		}

		/// <summary>
		/// Vrátí odsazení pro vkládání textu.
		/// Zohledňuje i odsazení rodičovského controlu
		/// </summary>
		/// <returns>AKtuální odsazení v počtech tabulátorů</returns>
		protected override int GetIndent()
		{
			return _currIndent;
		}

		#endregion

		#region Implementation of IHTMLWriter

		public override string imgPath { get; set; }

		public override string formName { get; protected set; }

		#endregion

		#region Implementation of IControl

		/// <summary>
		/// SLovník s dočasnými proměnnými
		/// </summary>
		private readonly Dictionary<string, string> _tmpParams = new Dictionary<string, string>();

		/// <summary>
		/// Smaže veškeré dočasné proměnné
		/// </summary>
		protected override void ClearTmpVariables()
		{
			_tmpParams.Clear();
		}

		/// <summary>
		/// Vrátí proměnnou dotazu podle následujicích priorit:
		/// 1. Dočasná proměnná
		/// 2. Rodičova dočasná proměnná
		/// 2. Proměnná ve formuláři (POST)
		/// 3. Proměnná v http dotazu (GET)
		/// </summary>
		/// <param name="varName">Název proměnné</param>
		/// <returns></returns>
		public override string this[string varName]
		{
			get
			{
				if (_tmpParams.ContainsKey(varName))
					return _tmpParams[varName];

				return 
					 Request.Form.Get(varName) ??
					 Request.QueryString.Get(varName);
			}
		}

		/// <summary>
		/// Nastaví dočasnou proměnnou
		/// </summary>
		/// <param name="varName">Název dočasné proměnné</param>
		/// <param name="val">Hodnota dočasné proměnné</param>
		public override void defineTmpParam(string varName, string val = null)
		{
			if (_tmpParams.ContainsKey(varName)) _tmpParams[varName] = val;
			else _tmpParams.Add(varName, val);
		}

		/// <summary>
		/// Smaže dočasnou proměnnou
		/// </summary>
		/// <param name="varName">Název dočasné proměnné</param>
		public override void deleteTmpParam(string varName)
		{
			if (_tmpParams.ContainsKey(varName)) _tmpParams.Remove(varName);
		}

		#endregion

		#region Implementation of IPage

		/// <summary>
		/// Inicializuje objekt
		/// </summary>
		/// <param name="request">Požadavek od klienta</param>
		/// <param name="response">Odpověď klientovi</param>
		public void startRequest(HttpRequest request, HttpResponse response)
		{
			ClearTmpVariables();
			Request = request;
			Response = response;
			RootPage = this;
		}

		/// <summary>
		/// Odešle odpověď klientovi
		/// </summary>
		public void finishRequest()
		{
			Response.End();
		}

		/// <summary>
		/// Nastaví název a typ souboru
		/// </summary>
		/// <param name="fileName">Název souboru</param>
		/// <param name="mimeType">Typ souboru (mime)</param>
		public void setMimeType(string fileName, string mimeType)
		{
			Response.ContentType = mimeType;
			Response.TransmitFile(fileName);
		}

		/// <summary>
		/// Pošle klientovi bytový soubor 
		/// </summary>
		/// <param name="file">Objekt reprezentující soubor</param>
		public void uploadFile(IPostedFile file)
		{
			uploadFile(file.name, file.mimeType, file.data);
		}

		/// <summary>
		/// Pošle klientovi bytový soubor
		/// </summary>
		/// <param name="fileName">Název souboru</param>
		/// <param name="mimeType">Typ souboru (mime)</param>
		/// <param name="data">Obsah souboru (byty)</param>
		public void uploadFile(string fileName, string mimeType, byte[] data)
		{
			Response.Clear();	// vyčistí buffer
			setMimeType(fileName, mimeType);
			Response.OutputStream.Write(data, 0, data.Length);
		}

		/// <summary>
		/// Pošle klientovi znakový soubor
		/// </summary>
		/// <param name="fileName">Název souboru</param>
		/// <param name="mimeType">Typ souboru (mime)</param>
		/// <param name="data">Obsah souboru (znaky)</param>
		public void uploadFile(string fileName, string mimeType, string data)
		{
			Response.Clear();	// vyčistí buffer
			setMimeType(fileName, mimeType);
			Response.Output.Write(data);
		}

		/// <summary>
		/// Přesměruje http dotaz na danou url
		/// </summary>
		/// <param name="url">Cíl přesměrování</param>
		public void redirectRequest(string url)
		{
			Response.Redirect(url);
		}

		#endregion

	}
}
