﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Spead
{
	public partial class TestPage : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnShowComments_Click (object sender, EventArgs e)
		{
		 System.Text.StringBuilder sb = new System.Text.StringBuilder ("PRC_Comment data:").AppendLine ();
		 
		 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
		 using (Shark.Database.SQL.IEngine sqlEngine = Shark.Speed.BI.Bi.createMsSQLDatabaseEngine (conString))
		  {
		   Shark.Database.SQL.ISelect sel = sqlEngine.select (tableName: "PRC_Comment", useAllFields: true);

		   using (Shark.Database.SQL.IFetch fech = sel.select ())
		    while (fech.moveNext ())
			 {
			  sb = sb.AppendLine ("-------------------------------------------------------");
			  sb = sb.AppendLine (fech.readString ("Text"));
			 }
		  }

		 txtOutput.Text = sb.ToString ();
		}

		protected void btnDebugMakeProcess_Click(object sender, EventArgs e)
		{
		 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
		 using (Shark.Database.SQL.IEngine sqlEngine = Shark.Speed.BI.Bi.createMsSQLDatabaseEngine (conString))
		  {
		   Shark.Speed.BI.IBi bi = Shark.Speed.BI.Bi.create (sqlEngine);
		   Shark.Speed.BI.Process.ITask tsk = bi.process.taskTable.getObject (13);

		   tsk.makeProcess (currUserId: 34, newStateId: 2040);
		  }
		}
	}
}