﻿namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Class holds neccessary information for rendering menu item
	/// </summary>
	internal class MenuItemContainer
	{
		public int Value { get; set; }
		public string Label { get; set; }
		public string Description { get; set; }
		public DMenuPermision Permission { get; set; }

		public bool IsDisplayable()
		{
			if (Permission == null) return true;

			return Permission(Value);
		}
	}
}