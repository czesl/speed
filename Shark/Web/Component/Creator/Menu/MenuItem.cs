﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Class represents one menu item with component or component creator
	/// </summary>
	internal class MenuItem : IMenuItem
	{
		#region private fields

		/// <summary>
		/// Component for rendering
		/// </summary>
		private IComponent _component;

		/// <summary>
		/// Component creator
		/// </summary>
		private DMenuCreator _componentCreator;

		/// <summary>
		/// Description of menuitem
		/// </summary>
		private MenuItemContainer _container;

		#endregion

		#region private properties

		/// <summary>
		/// Return component of menuitem
		/// If neccessary, create new component with component creator
		/// </summary>
		private IComponent Component
		{
			get
			{
				if ( _component == null )
				{
					_component = _componentCreator(_container.Value);
				}

				return _component;
			}
		}

		#endregion

		#region constructors

		/// <summary>
		/// Constructor with component
		/// </summary>
		/// <param name="container"></param>
		/// <param name="component"></param>
		public MenuItem (MenuItemContainer container, IComponent component)
		{
			_container = container;
			_component = component;
		}

		/// <summary>
		/// Constructor with component creator
		/// </summary>
		/// <param name="container"></param>
		/// <param name="componentCreator"></param>
		public MenuItem(MenuItemContainer container, DMenuCreator componentCreator)
		{
			_container = container;
			_componentCreator = componentCreator;
		}

		#endregion

		#region public methods

		/// <summary>
		/// Is menu item displayable?
		/// </summary>
		/// <returns></returns>
		public bool IsDisplayable()
		{
			return _container.IsDisplayable();
		}

		/// <summary>
		/// Generate menu item (component)
		/// </summary>
		public void generate()
		{
			Component.generate();
		}

		#endregion
	}
}