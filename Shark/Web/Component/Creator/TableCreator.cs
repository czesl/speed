﻿using System.Collections.Generic;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	internal class TableCreator : ITableCreator
	{
		#region private fields

		/// <summary>
		/// List with definition of columns
		/// </summary>
		private readonly List<ColumnDefinition> _columns = new List<ColumnDefinition>();

		/// <summary>
		/// Control for creating subcontrols for components
		/// </summary>
		private readonly IControl _control;

		#endregion

		#region constructors

		/// <summary>
		/// Constructor sets creator and Control for component
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		internal TableCreator(ICreator creator, IControl control)
		{
			subCreator = creator;
			_control = control;
		}

		#endregion

		#region Implementation of IParent

		/// <summary>
		/// Gets creater used for creating subcomponents
		/// </summary>
		public ICreator subCreator { get; private set; }

		#endregion

		#region Implementation of ITableFiller

		/// <summary>
		/// Delegate is called when someone clicked on table row
		/// </summary>
		public DTableAction actionOnRowClick { get; set; }

		/// <summary>
		/// Delegate is called when table rows are generated
		/// Delegate is responsible of generating table rows
		/// </summary>
		public DTableAction generate { get; set; }

		/// <summary>
		/// Add column to table
		/// </summary>
		/// <param name="label">Columns label</param>
		/// <param name="desc">Description of column</param>
		/// <param name="nCols">Colspan value</param>
		/// <param name="horzAlign">Horizontal alignment</param>
		public void addColumn(string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left)
		{
			ColumnDefinition column = new ColumnDefinition
										{
											label = label,
											desc = desc,
											nCols = nCols,
											horzAlign = horzAlign
										};
			_columns.Add(column);
		}

		/// <summary>
		/// Create new ITable component with defined columns and delegates
		/// </summary>
		/// <returns>New table component</returns>
		public ITable create()
		{
			return new Table(
				creator: subCreator,
				control: _control,
				columns: _columns.ToArray(),
				generate: generate,
				actionOnRowClick: actionOnRowClick
			);
		}

		#endregion

	}
}