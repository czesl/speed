﻿
namespace Shark.Speed.WebForm.AppComponent
 {
  public interface ICreator
   {
    Web.Component.IComponent	loginForm (Web.Component.ICreator parent, ISession session);
	
	IMenuComponent	startTask (Web.Component.ICreator parent, ISession session, DCurrTask currTask);
	IMenuComponent	processTask (Web.Component.ICreator parent, ISession session, DCurrTask currTask);
	IMenuComponent	takeTaskBack (Web.Component.ICreator parent, ISession session, DCurrTask currTask);

	IMenuComponent	startAnotace (Web.Component.ICreator parent, ISession session, DCurrTask currTask);

	Web.Component.IComponent	taskComment (Web.Component.ICreator parent, ISession session, DCurrRowId currProcessId);
	Web.Component.IComponent	taskHistory (Web.Component.ICreator parent, ISession session, DCurrRowId currProcessId);
	IMenuComponent				subTasks (Web.Component.ICreator parent, ISession session, DCurrRowId currTaskId);

	Web.Component.IComponent	anotatorStatistics (Web.Component.ICreator parent, ISession session, DCurrRowId currAnotatorId);
	Web.Component.IComponent	anotatorFiles (Web.Component.ICreator parent, ISession session, DCurrRowId currAnotatorId);

	IChooserComponent			anotatorChooser (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId);

	Web.Component.IComponent	choosenAnotatorStatistics (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId);
	Web.Component.IComponent	choosenAnotatorFiles (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId);
   }

  public interface IAppComponent
   {
    Web.Component.IComponent	comp	{ get; }
   }
  
  public interface IMenuComponent : IAppComponent
   {
    bool	isMenuPermission (int thisMenuItemValue);
   }

  public interface IChooserComponent : IAppComponent
   {
    long	currRowId	{ get; }
   }

  public delegate long				DCurrRowId ();
  public delegate BI.Process.ITask	DCurrTask ();

  public class Creator
   {
    private Creator ()	{ }

	public static ICreator	create ()	{ return (CreatorImp.create ()); }

	private class CreatorImp : ICreator
	 {
	  private CreatorImp ()	{ }

	  public static ICreator	create ()
				{
				 if (ms_Creator == null) ms_Creator = new CreatorImp ();
				 return (ms_Creator);
				}
	  private static ICreator	ms_Creator = null;

      public Web.Component.IComponent	loginForm (Web.Component.ICreator parent, ISession session)	{ return (null); } //TODO
	
	  public IMenuComponent	startTask (Web.Component.ICreator parent, ISession session, DCurrTask currTask)	{ return (null); } //TODO
	  public IMenuComponent	processTask (Web.Component.ICreator parent, ISession session, DCurrTask currTask)	{ return (null); } //TODO
	  public IMenuComponent	takeTaskBack (Web.Component.ICreator parent, ISession session, DCurrTask currTask)	{ return (null); } //TODO

	  public IMenuComponent	startAnotace (Web.Component.ICreator parent, ISession session, DCurrTask currTask)	{ return (null); } //TODO

	  public Web.Component.IComponent	taskComment (Web.Component.ICreator parent, ISession session, DCurrRowId currProcessId)	{ return (null); } //TODO
	  public Web.Component.IComponent	taskHistory (Web.Component.ICreator parent, ISession session, DCurrRowId currProcessId)	{ return (null); } //TODO
	  public IMenuComponent				subTasks (Web.Component.ICreator parent, ISession session, DCurrRowId currTaskId)	{ return (null); } //TODO

	  public Web.Component.IComponent	anotatorStatistics (Web.Component.ICreator parent, ISession session, DCurrRowId currAnotatorId)	{ return (null); } //TODO
	  public Web.Component.IComponent	anotatorFiles (Web.Component.ICreator parent, ISession session, DCurrRowId currAnotatorId)	{ return (null); } //TODO

	  public IChooserComponent			anotatorChooser (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId)
				{ return (AnotatorChooser.Creator.create (this, parent, session, currSupervisorId)); }

	  public Web.Component.IComponent	choosenAnotatorStatistics (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId)
				{ return (ChoosenAnotatorStatistics.Creator.create (this, parent, session, currSupervisorId)); }
	  
	  public Web.Component.IComponent	choosenAnotatorFiles (Web.Component.ICreator parent, ISession session, DCurrRowId currSupervisorId)
				{ return (ChoosenAnotatorFiles.Creator.create (this, parent, session, currSupervisorId)); }
	 }
   }
 }
