﻿using System.Web.Services;

namespace Shark.Speed.WebService.AnotaceCheck.AnotaceCheckService
 {
	/// <summary>
	/// Summary description for AnotaceAutomatChecker
	/// </summary>
  [WebService (Name = "AnotaceCheckService", Description = "Automaticaly check anotation web service", Namespace = "http://anotaceCheckService.speed.ufal.jager.cz/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]

  public class AnotaceAutomatChecker : System.Web.Services.WebService
   {
	private Database.SQL.IEngine	createSQLEngine ()
				{
				 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
				 return (BI.Bi.createMsSQLDatabaseEngine (conString));
				}

	[WebMethod (Description = "Opens Anotace Automat Checker communication, creates session id")]
    public CreateSessionAnswer	createSession (string userName, string passWd)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new CreateSessionAnswer (call.createSession (userName: userName, passWd: passWd, callUrl: Context.Request.UserHostName)));
				  }
				}
   
	[WebMethod (Description = "Closes Anotace Automat Checker communication, closes session id")]
   	public WebAnswer	closeSession (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.closeSession (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				}
   
	[WebMethod (Description = "Donwload anotation data for Anotace Automat Checker")]
   	public DownLoadAnswer	downLoad (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new DownLoadAnswer (call.downLoad (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				}
   
	[WebMethod (Description = "Upload checked anotation data from Anotace Automat Checker")]
   	public WebAnswer	upload (string sessionId, WebUpLoadFile[] files)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.create (BI.Bi.create (msEng));
				   return (new WebAnswer (call.upload (sessionId: sessionId, files: files, callUrl: Context.Request.UserHostName)));
				  }
				}
   }

  public interface IWebAnswer
   {
    int		status	{ get; set; }
	string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class WebAnswer : IWebAnswer
   {
    public WebAnswer ()	{ status = -1; errMsg = null; }
	public WebAnswer (IAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				}

    public int		status	{ get; set; }
	public string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class CreateSessionAnswer : WebAnswer
   {
    public CreateSessionAnswer () : base ()	{ }
	public CreateSessionAnswer (ICreateSessionAnswer ans) : base (ans)	{ sessionId = ans.sessionId; }

	public string	sessionId	{ get; set; }
   }

  [System.Serializable]
  public class DownLoadAnswer : WebAnswer
   {
    public DownLoadAnswer () : base ()	{ }
	public DownLoadAnswer (IDownLoadAnswer ans) : base (ans)
				{
				 if (ans.files == null) files = null;
				 else
				  {
				   files = new WebDownLoadFile [ans.files.Length];

				   for (int i = 0; i < files.Length; i++) files [i] = new WebDownLoadFile (ans.files [i]);
				  }
				}

    public WebDownLoadFile[]	files	{ get; set; }
   }

  public interface IWebDownLoadFile
   {
    string	name		{ get; set; }
	string	cmdFile		{ get; set; }
	string	htmlFile	{ get; set; }
	string	xmlWFile	{ get; set; }
	string	xmlAFile	{ get; set; }
	string	xmlBFile	{ get; set; }
   }

  [System.Serializable]
  public class WebDownLoadFile : IWebDownLoadFile, IDownLoadFile
   {
    public WebDownLoadFile ()	{ }
	public WebDownLoadFile (IDownLoadFile fl)
				{
				 name     = fl.name;
				 cmdFile  = fl.cmdFile;
				 htmlFile = fl.htmlFile;
				 xmlWFile = fl.xmlWFile;
				 xmlAFile = fl.xmlAFile;
				 xmlBFile = fl.xmlBFile;
				}

    public string	name		{ get; set; }
	public string	cmdFile		{ get; set; }
	public string	htmlFile	{ get; set; }
	public string	xmlWFile	{ get; set; }
	public string	xmlAFile	{ get; set; }
	public string	xmlBFile	{ get; set; }
   }

  public interface IWebUpLoadFile : IWebDownLoadFile
   {
    string	warnings	{ get; set; }
	string	errors		{ get; set; }
   }

  [System.Serializable]
  public class WebUpLoadFile : WebDownLoadFile, IWebUpLoadFile, IUpLoadFile
   {
    public WebUpLoadFile ()	{ }
	public WebUpLoadFile (IUpLoadFile fl) : base (fl)
				{
				 warnings = fl.warnings;
				 errors	  = fl.errors;
				}

    public string	warnings	{ get; set; }
	public string	errors		{ get; set; }
   }
 }
