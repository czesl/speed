﻿
namespace Shark.Web.HttpProtocol
 {
  // zapisuje HTML tagy do response
  public interface IHTMLWriter
   {
    string	imgPath		{ get; set; }	// cesta k obrazku pro web browser. Hodnotou bude napr "/images/"
	string	formName	{ get; }		// pokud je aktualne otevreny tag "<form>" (klidne nekde vysoko v rodici), pak je zde nazev otevreneho formulare. Jinak vraci null
	
	string	encode (string txt);	// koduje text, aby se dal zapsat do HTML textu a uzivatel ho videl opet jako text
	string	decode (string txt);	// dekoduje zakodovany text

	string	createImgPath (string fileName);	// vytvori pro browser plnou cestu ke grafickymu souboru
	
	void	write (string txt);				// zapisuje do response
	void	writeNL (string txt = null);	// zapisuje do response - vcetne noveho radku

	// prikazy pro jednotlive HTML tagy. Tento seznam se casem bude menit - doplnovat tagy a jejich parametry
	// obecne - tagy obsahujici id generuji jak parametr id, tak i parametr name
	//        - identifikace (tj. obsah id) se generuje vcetne prefixu daneho kontrolu
	//        - parametry s hodnotou null se do HTML vubec negeneruji
	//        - pro kazdou znacku je definova jeji konec. Kdyz je ale v prikazy znacky definovan i obsah, pak se konec generuje automaticky
  	void	doctype (string def);
	void	html (string lang = null, string version = null, string xmlns = null);
	void	htmlEnd ();
	void	head ();
	void	headEnd ();
	void	body (string style = null, string clas = null);
	void	bodyEnd ();
	void	form (string name, string action, string method = null, string target = null);
	void	formEnd ();
	void	div (string id = null, string align = null, string style = null, string clas = null);
	void	divEnd ();
	
	void	title (string title);

	void	input (string id, string type, string value = null, bool isChecked = false, string fileAccept = null, string alt = null, string align = null, string maxlength = null,
				 string size = null, string style = null, string clas = null, bool isDisabled = false);
	void	textArea(string id, string value = null, string cols = null, string rows = null, string style = null, string clas = null, bool isDisabled = false);

	void	sel (string id, bool isMultiple = false, string size = null, string style = null, string clas = null, bool isDisabled = false);
	void	selEnd ();
	void	option (string txt, string value = null, string label = null, bool isSelected = false, bool isDisabled = false);

	void	table (string align = null, string background = null, string bgColor = null, string border = null, string cellPadding = null, string cellSpacing = null,
				 string height = null, string width = null, string style = null, string clas = null);
	void	tableEnd ();
	void	tr (string align = null, string vAlign = null, string bgColor = null, string style = null, string clas = null, string onMouseOver = null, string onMouseOut = null, string onClick = null);
	void	trEnd ();
	void	th (string colSpan = null, string rowSpan = null, string align = null, string vAlign = null, string background = null, string bgColor = null,
		      string height = null, string width = null, string style = null, string clas = null, string title = null);
	void	thEnd ();
	void	td (string colSpan = null, string rowSpan = null, string align = null, string vAlign = null, string background = null, string bgColor = null,
			  string height = null, string width = null, string style = null, string clas = null, string title = null);
	void	tdEnd ();

  	void	a (string href, string id = null, string target = null, string title = null, string type = null, string style = null, string clas = null, string onClick = null);
	void	img (string src, string id = null, string alt = null, string align = null, string border = null, string style = null, string clas = null);

	void	script (string code, string type = null, string src = null);
	void	script (string type = null, string src = null);
	void	scriptEnd ();
	
	void	br ();
	void	bold ();
	void	boldEnd ();
	void	italic ();
	void	italicEnd ();
	void	underline ();
	void	underlineEnd ();
  	void    meta(string httpEquiv = null, string content = null, string name = null);
  	void    link(string rel, string type, string href);
  	void    span(string clas = null, string onClick = null);
   }
  
  // rozhranni, kterym se kazdy Control bavi s HTTP protokolem
  // Existuje jeden top level - a pak potomci. Kazdy potomek ma prefix, ktery automaticky strka pred nazvy svych promennych. Tim si zajistuje, ze budou jednoznacne od vsech ostatnich nazvu
  public interface IControl : IHTMLWriter
   {
	string	this [string varName]	{ get; }	// vraci hodnotu (nebo null pokud neni definovana) promenne. At je jedna o promennou z QueryString, nebo z Form, nebo o docasnou promennou
												// Docasna promenna ma vzdy prednost - pokud je definovana, vraci se jeji hodnota i kdyby ta byla null

	void	defineTmpParam (string varName, string val = null);	// definuje docasnou promennou. Na pocatku requestu jsou vsechny docasne promenne smazany.
	void	deleteTmpParam (string varName);					// maze docasnou promennou. Pokud takova neexistuje, nic se nestane

	System.Collections.Generic.IList<IPostedFile>	downloadFiles ();	// vraci seznam vsech prilozenych souboru

	string	fullName (string varName);		// vraci plne jmeno pro nejaky HTML prvek vcetne vsech pripadny predpon. Slouzi napr. pro odkazani se na prvek z JavaScript kodu

	string		createPrefix ();			// vytvari prefix pro novy prvek
	IControl	createChild ();				// vytvari novy dceriny IControl s novym prefixem. Kompletni prefix IControl muze obsahovat prefix tohoto, ale nemusi
  	string getAbsoluteUri();
   }

  // rozhranni, ktere samo nema zadny prefix - je vytvoreno pro cely request
  public interface IPage : IControl
   {
	void	startRequest (System.Web.HttpRequest request, System.Web.HttpResponse response);	// inicializace pro novy requst od klienta
	void	finishRequest ();																	// oznamuje, ze vse pro aktualni request bylo na klienta odeslano
	
	void	setMimeType (string fileName, string mimeType);
	void	uploadFile (IPostedFile file);
	void	uploadFile (string fileName, string mimeType, byte[] data);
    void	uploadFile (string fileName, string mimeType, string data);

	void	redirectRequest (string url);
   }

  // rozhranni stazeneho souboru od klienta
  public interface IPostedFile
   {
	string	mimeType	{ get; }
	string	name		{ get; }
	string	shortName	{ get; }	// name without path
	byte[]	data		{ get; }
   }
 }
