﻿
namespace Shark.Speed.BI.Process
 {
  public enum EStateSpecial { SS_WrongCall = 10 }

  public enum EStateText	{
							 ST_Start = 1000,
							 ST_Vybran = 1010, ST_KNacteni = 1020, ST_Nacteno = 1030, ST_Konvertovano = 1040, ST_ChybaKonverze = 1050, ST_Zruseno = 1060,
							 ST_KAnotaci = 1070, ST_Anotovan = 1080, 
							 ST_AdjPrideleno = 1200, ST_AdjProblem = 1210, ST_AdjVraceno = 1220, ST_Adjudikovano = 1250, ST_AdjKontrola = 1260,
							 ST_DoAmesu = 1300, ST_AdjUlozeno = 1310
							}

  public enum EStateAnotace {
							 SA_Start = 2000,
							 SA_Prideleno = 2010, SA_VAnotaci = 2020, SA_Odevzdano = 2030, SA_DoAmesu = 2040,
							 SA_KOvereni = 2120, SA_KOdevzdani = 2130, SA_KPrenosu = 2140,
							 SA_Zruseno = 2900, SA_Anotovano = 2910
							}

 
  public enum EProcessField { PF_Id, PF_StartAt, PF_OwnerId, PF_DataId, PF_ParentTaskId }

  public interface IProcess : IBiObject
   {
	System.DateTime							startAt		{ get; set; }
	IReference<User.IUser>					owner		{ get; }
	IReference<ProcessData.IProcessData>	data		{ get; }
	IReference<ITask>						parentTask	{ get; }
   }

  public enum ETaskField { TF_Id, TF_ProcessId, TF_Created, TF_FromUserId, TF_DataId, TF_StateId, TF_StateDesc, TF_Priority, TF_Started, TF_ToUserId, TF_Finished }

  public interface ITask : IBiObject
   {
	IReference<IProcess>					process		{ get; }
	System.DateTime							created		{ get; set; }
	IReference<User.IUser>					fromUser	{ get; }
	IReference<ProcessData.IProcessData>	data		{ get; }
	int										stateId		{ get; set; }
	string									stateDesc	{ get; set; }
	int										priority	{ get; set; }
	System.DateTime							started		{ get; set; }			// null date means not started by toUser
	IReference<User.IUser>					toUser		{ get; }
	System.DateTime							finished	{ get; set; }

	void	start (Database.ITransaction trans = null);
	bool	isAble2Process (long currUserId, int newStateId, ProcessData.IProcessData newDta = null);
	void	makeProcess (long currUserId, int newStateId, Database.ITransaction trans = null, ProcessData.IProcessData newDta = null);
   }

  public enum EOpenTaskFileQueryField { OTFQ_Id, OTFQ_ProcessId, OTFQ_StateId, OTFQ_Created, OTFQ_Started, OTFQ_FromUserId, OTFQ_ToUserId, OTFQ_HtmlId, OTFQ_XmlWId, OTFQ_XmlAId, OTFQ_XmlBId }

  public interface IOpenTaskFileQuery : IBiObject
   {
	long					processId	{ get; set; }
	int						stateId		{ get; set; }
	System.DateTime			created		{ get; set; }
	System.DateTime			started		{ get; set; }
	IReference<User.IUser>	fromUser	{ get; }
	IReference<User.IUser>	toUser		{ get; }

	IReference<ProcessData.IFile>	html	{ get; }
	IReference<ProcessData.IFile>	xmlW	{ get; }
	IReference<ProcessData.IFile>	xmlA	{ get; }
	IReference<ProcessData.IFile>	xmlB	{ get; }

	string	getFileName ();
   }

  public enum ECommentField { CF_Id, CF_ProcessId, CF_UserId, CF_At, CF_Text }

  public interface IComment : IBiObject
   {
	IReference<IProcess>	process	{ get; }
	IReference<User.IUser>	user	{ get; }
	System.DateTime			at		{ get; set; }
	string					text	{ get; set; }
   }

  public interface IModule
   {
	string	getStateName (EStateSpecial st);
	string	getStateName (EStateText st);
	string	getStateName (EStateAnotace st);

    string	getFieldName (EProcessField fld);
	string	getFieldName (ETaskField fld);
	string	getFieldName (EOpenTaskFileQueryField fld);
	string	getFieldName (ECommentField fld);

    Database.ITable<IProcess>	processTable	{ get; }
    Database.ITable<ITask>		taskTable		{ get; }
    Database.ITable<IComment>	commentTable	{ get; }

	Database.IQuery<IOpenTaskFileQuery>	openTaskFileQuery	{ get; }
   }

  public class Module
   {
    private Module ()	{ }

	public static IModule	create (ICreator biCreator, Database.IFactory db, Database.ILangTranslation lang, User.IModule userModule, ProcessData.IModule proDtaModule)
				{
				 System.Collections.Generic.IList<Database.IReferenceFiller<ITask>> taskFillLst = new System.Collections.Generic.List<Database.IReferenceFiller<ITask>> ();
				 Database.ITable<IProcess> procTable = createProcessTable (biCreator, db, userModule.userTable,
																		 proDtaModule.processDataTable, proDtaModule.getFieldName (ProcessData.EProcessDataField.PDF_Id), taskFillLst);
				 Database.ITable<ITask> tskTable = createTaskTable (biCreator, db, lang, userModule,
																  proDtaModule.processDataTable, proDtaModule.getFieldName (ProcessData.EProcessDataField.PDF_Id), procTable);
				 foreach (Database.IReferenceFiller<ITask> refFil in taskFillLst) refFil.setReferenceTable (tskTable);

				 Database.ITable<IComment> cmnTable = createCommentTable (biCreator, db, userModule.userTable, procTable);
				 Database.IQuery<IOpenTaskFileQuery> opnTskQuery = createOpenTaskQuery (biCreator, db, userModule.userTable,
																					  proDtaModule.fileTable, proDtaModule.getFieldName (ProcessData.EFileField.FF_Id));

				 return (new ModuleImp (procTable, tskTable, cmnTable, opnTskQuery));
				}

	private static string	getStName (EStateSpecial st)
				{
				 switch (st)
				  {
				   case EStateSpecial.SS_WrongCall: return ("WrongCall");
				   default: return ("unKnow");
				  }
				}
	
	private static string	getStName (EStateText st)
				{
				 switch (st)
				  {
				   case EStateText.ST_Start:		 return ("Start");
				   case EStateText.ST_Vybran:		 return ("Vybran");
				   case EStateText.ST_KNacteni:		 return ("KNacteni");
				   case EStateText.ST_Nacteno:		 return ("Nacteno");
				   case EStateText.ST_Konvertovano:	 return ("Konvertovano");
				   case EStateText.ST_ChybaKonverze: return ("ChybaKonverze");
				   case EStateText.ST_Zruseno:		 return ("Zruseno");
				   case EStateText.ST_KAnotaci:		 return ("KAnotaci");
				   case EStateText.ST_Anotovan:		 return ("Anotovan");
				   case EStateText.ST_AdjPrideleno:	 return ("AdjPrideleno");
				   case EStateText.ST_AdjProblem:	 return ("AdjProblem");
				   case EStateText.ST_AdjVraceno:	 return ("AdjVraceno");
				   case EStateText.ST_Adjudikovano:	 return ("Adjudikovano");
				   case EStateText.ST_AdjKontrola:	 return ("AdjKontrola");
				   case EStateText.ST_DoAmesu:		 return ("DoAmesu");
				   case EStateText.ST_AdjUlozeno:	 return ("AdjUlozeno");
				   default: return ("unKnow");
				  }
				}

	private static string	getStName (EStateAnotace st)
				{
				 switch (st)
				  {
				   case EStateAnotace.SA_Start:		 return ("Start");
				   case EStateAnotace.SA_Prideleno:  return ("Prideleno");
				   case EStateAnotace.SA_VAnotaci:	 return ("VAnotaci");
				   case EStateAnotace.SA_Odevzdano:	 return ("Odevzdano");
				   case EStateAnotace.SA_DoAmesu:	 return ("DoAmesu");
				   case EStateAnotace.SA_KOvereni:	 return ("KOvereni");
				   case EStateAnotace.SA_KOdevzdani: return ("KOdevzdani");
				   case EStateAnotace.SA_KPrenosu:	 return ("KPrenosu");
				   case EStateAnotace.SA_Zruseno:	 return ("Zruseno");
				   case EStateAnotace.SA_Anotovano:	 return ("Anotovano");
				   default: return ("unKnow");
				  }
				}

    private static string	getFldName (EProcessField fld)
				{
				 switch (fld)
				  {
				   case EProcessField.PF_Id:	       return ("Id");
				   case EProcessField.PF_StartAt:      return ("StartAt");
				   case EProcessField.PF_OwnerId:      return ("OwnerId");
				   case EProcessField.PF_DataId:       return ("DataId");
				   case EProcessField.PF_ParentTaskId: return ("ParentTaskId");
				   default: throw new System.SystemException ("Unknow field of Process.Process table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IProcess>	createProcessTable (ICreator biCre, Database.IFactory db, Database.ITable<User.IUser, User.EUserField> userTable,
																  Database.ITable<ProcessData.IProcessData> procDtaTable, string procDtaTableIdFldName,
																  System.Collections.Generic.IList<Database.IReferenceFiller<ITask>> taskFillLst)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRC_Process");
				 tc.addPrimaryField (getFldName (EProcessField.PF_Id));
				 tc.addField (getFldName (EProcessField.PF_StartAt), Database.EFieldType.FT_DateTime);
				 tc.addReferenceField (getFldName (EProcessField.PF_OwnerId), userTable, userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addReferenceField (getFldName (EProcessField.PF_DataId), procDtaTable, procDtaTableIdFldName);
				 taskFillLst.Add (tc.addReferenceField<ITask> (getFldName (EProcessField.PF_ParentTaskId),
	                                                         joinCond: new Database.IJoinCond [1] { tc.createJoinFldCond (getFldName (EProcessField.PF_ParentTaskId), getFldName (ETaskField.TF_Id)) }));

				 ProcessTypeConv conv = new ProcessTypeConv (biCre, userTable, procDtaTable);
				 taskFillLst.Add (conv.createTaskTableFiller ());
				 Database.ITable<IProcess> tb = tc.createTable<IProcess> (conv);
				 conv.table = tb;
				 return (tb);
				}
	
	private class ProcessTypeConv : Database.ITypeConverter<IProcess>
	 {
	  public ProcessTypeConv (ICreator biCre, Database.ITable<User.IUser> usrTb, Database.ITable<ProcessData.IProcessData> prcDtaTb)	{ biCreator = biCre; userTable = usrTb; procDataTable = prcDtaTb; }

	  public Database.ITable<IProcess>	table	{ private get; set; }

	  private ICreator									biCreator		{ get; set; }
	  private Database.ITable<User.IUser>				userTable		{ get; set; }
	  private Database.ITable<ProcessData.IProcessData>	procDataTable	{ get; set; }
	  private Database.ITable<ITask>					taskTable		{ get; set; }

	  public Database.IReferenceFiller<ITask>	createTaskTableFiller ()	{ return (new TaskTableFiller (this)); }

	  private class TaskTableFiller : Database.IReferenceFiller<ITask>
	   {
	    public TaskTableFiller (ProcessTypeConv parent)	{ m_Parent = parent; }
		private ProcessTypeConv	m_Parent;

		public void	setJoinCond (Database.IJoinCond[] cnd)		{ }
		public void	setLookUpCond (Database.IJoinCond[] cnd)	{ }

		public void	setReferenceTable (Database.ITable master)			{ m_Parent.taskTable = (Database.ITable<ITask>) master; }
		public void	setReferenceTable (Database.ITable<ITask> master)	{ m_Parent.taskTable = master; }
	   }

	  public IProcess	nullValue	{ get { return (null); } }

	  public IProcess	create4Rec (Database.IRecord rec)	{ IProcess prc = new Process (biCreator, userTable, procDataTable, taskTable, rec); loadData (prc); return (prc); }

	  public void	loadData (IProcess toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id      = fromRec.val (table, getFldName (EProcessField.PF_Id)).bg.val;
				 toObj.startAt = fromRec.val (table, getFldName (EProcessField.PF_StartAt)).dt.val;
				 toObj.owner.loadDta (table, fromRec, getFldName (EProcessField.PF_OwnerId));
				 toObj.data.loadDta (table, fromRec, getFldName (EProcessField.PF_DataId));
				 toObj.parentTask.loadDta (table, fromRec, getFldName (EProcessField.PF_ParentTaskId));
				}

	  public Database.IRecord	saveData (IProcess fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EProcessField.PF_Id)).bg.val            = fromObj.id;
				 toRec.val (table, getFldName (EProcessField.PF_StartAt)).dt.val       = fromObj.startAt;
				 toRec.val (table, getFldName (EProcessField.PF_OwnerId)).bg.val       = fromObj.owner.id;
				 toRec.val (table, getFldName (EProcessField.PF_DataId)).bg.val        = fromObj.data.id;
				 toRec.val (table, getFldName (EProcessField.PF_ParentTaskId)).bg.val  = fromObj.parentTask.id;

				 return (toRec);
				}

	  private class Process : IProcess
	   {
		public Process (ICreator biCreator, Database.ITable<User.IUser> userTable, Database.ITable<ProcessData.IProcessData> procDataTable, Database.ITable<ITask> taskTable, Database.IRecord rec)
					{
					 record = rec;
					 m_Owner      = biCreator.reference<User.IUser> (userTable);
					 m_Data       = biCreator.reference<ProcessData.IProcessData> (procDataTable);
					 m_ParentTask = biCreator.reference<ITask> (taskTable);
					}
		
		public Database.IRecord	record	{ get; private set; }

        public long									id			{ get; set; }
	    public System.DateTime						startAt		{ get; set; }
	    public IReference<User.IUser>				owner		{ get { return (m_Owner); } }		private IReference<User.IUser>					m_Owner;
	    public IReference<ProcessData.IProcessData>	data		{ get { return (m_Data); } }		private IReference<ProcessData.IProcessData>	m_Data;
		public IReference<ITask>					parentTask	{ get { return (m_ParentTask); } }	private IReference<ITask>						m_ParentTask;
	   }
	 }

	private static string	getFldName (ETaskField fld)
				{
				 switch (fld)
				  {
				   case ETaskField.TF_Id:		  return ("Id");
				   case ETaskField.TF_ProcessId:  return ("ProcessId");
				   case ETaskField.TF_Created:	  return ("Created");
				   case ETaskField.TF_FromUserId: return ("FromUserId");
				   case ETaskField.TF_DataId:	  return ("DataId");
				   case ETaskField.TF_StateId:	  return ("StateId");
				   case ETaskField.TF_StateDesc:  return ("StateDesc");
				   case ETaskField.TF_Priority:	  return ("Priority");
				   case ETaskField.TF_Started:	  return ("Started");
				   case ETaskField.TF_ToUserId:	  return ("ToUserId");
				   case ETaskField.TF_Finished:	  return ("Finished");
				   default: throw new System.SystemException ("Unknow field of Process.Task table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<ITask>	createTaskTable (ICreator biCre, Database.IFactory db, Database.ILangTranslation lang, User.IModule userModule,
														   Database.ITable<ProcessData.IProcessData> procDtaTable, string procDtaTableIdFldName, Database.ITable<IProcess> processTable)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRC_Task");
				 tc.addPrimaryField (getFldName (ETaskField.TF_Id));
				 tc.addReferenceField (getFldName (ETaskField.TF_ProcessId), processTable, getFldName (EProcessField.PF_Id));
				 tc.addField (getFldName (ETaskField.TF_Created), Database.EFieldType.FT_DateTime);
				 tc.addReferenceField (getFldName (ETaskField.TF_FromUserId), userModule.userTable, userModule.userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addReferenceField (getFldName (ETaskField.TF_DataId), procDtaTable, procDtaTableIdFldName);
				 tc.addField (getFldName (ETaskField.TF_StateId), Database.EFieldType.FT_Int);
				 tc.addTextField (getFldName (ETaskField.TF_StateDesc), 20);
				 tc.addField (getFldName (ETaskField.TF_Priority), Database.EFieldType.FT_Int);
				 tc.addField (getFldName (ETaskField.TF_Started), Database.EFieldType.FT_DateTime);
				 tc.addReferenceField (getFldName (ETaskField.TF_ToUserId), userModule.userTable, userModule.userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addField (getFldName (ETaskField.TF_Finished), Database.EFieldType.FT_DateTime);

				 TaskTypeConv conv = new TaskTypeConv (biCre, db, lang, userModule, procDtaTable, processTable);
				 Database.ITable<ITask> tb = tc.createTable<ITask> (conv);
				 conv.table = tb;
				 return (tb);
				}

	private class TaskTypeConv : Database.ITypeConverter<ITask>
	 {
	  public TaskTypeConv (ICreator biCre, Database.IFactory db, Database.ILangTranslation lang, User.IModule userMdl, Database.ITable<ProcessData.IProcessData> procDtaTb, Database.ITable<IProcess> procTb)
				{ database = db; language = lang; biCreator = biCre; userModule = userMdl; processDataTable = procDtaTb; processTable = procTb; }

	  private Database.IFactory										database			{ get; set; }
	  private Database.ILangTranslation								language			{ get; set; }
	  private ICreator												biCreator			{ get; set; }
	  private User.IModule											userModule			{ get; set; }
	  private Database.ITable<ProcessData.IProcessData>				processDataTable	{ get; set; }
	  private Database.ITable<IProcess>								processTable		{ get; set; }

	  public Database.ITable<ITask>	table	{ private get; set; }

	  public ITask	nullValue	{ get { return (null); } }

	  public ITask	create4Rec (Database.IRecord rec)	{ ITask tsk = new Task (database, language, table, biCreator, userModule, processDataTable, processTable, rec); loadData (tsk); return (tsk); }

	  public void	loadData (ITask toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id        = fromRec.val (table, getFldName (ETaskField.TF_Id)).bg.val;
				 toObj.process.loadDta (table, fromRec, getFldName (ETaskField.TF_ProcessId));
				 toObj.created   = fromRec.val (table, getFldName (ETaskField.TF_Created)).dt.val;
				 toObj.fromUser.loadDta (table, fromRec, getFldName (ETaskField.TF_FromUserId));
				 toObj.data.loadDta (table, fromRec, getFldName (ETaskField.TF_DataId));
				 toObj.stateId   = fromRec.val (table, getFldName (ETaskField.TF_StateId)).it.val;
				 toObj.stateDesc = fromRec.val (table, getFldName (ETaskField.TF_StateDesc)).txt.val;
				 toObj.priority  = fromRec.val (table, getFldName (ETaskField.TF_Priority)).it.val;
				 toObj.started   = fromRec.val (table, getFldName (ETaskField.TF_Started)).dt.val;
				 toObj.toUser.loadDta (table, fromRec, getFldName (ETaskField.TF_ToUserId));
				 toObj.finished  = fromRec.val (table, getFldName (ETaskField.TF_Finished)).dt.val;
				}

	  public Database.IRecord	saveData (ITask fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (ETaskField.TF_Id)).bg.val         = fromObj.id;
				 toRec.val (table, getFldName (ETaskField.TF_ProcessId)).bg.val  = fromObj.process.id;
				 toRec.val (table, getFldName (ETaskField.TF_Created)).dt.val    = fromObj.created;
				 toRec.val (table, getFldName (ETaskField.TF_FromUserId)).bg.val = fromObj.fromUser.id;
				 toRec.val (table, getFldName (ETaskField.TF_DataId)).bg.val     = fromObj.data.id;
				 toRec.val (table, getFldName (ETaskField.TF_StateId)).it.val    = fromObj.stateId;
				 toRec.val (table, getFldName (ETaskField.TF_StateDesc)).txt.val = fromObj.stateDesc;
				 toRec.val (table, getFldName (ETaskField.TF_Priority)).it.val   = fromObj.priority;
				 toRec.val (table, getFldName (ETaskField.TF_Started)).dt.val    = fromObj.started;
				 toRec.val (table, getFldName (ETaskField.TF_ToUserId)).bg.val   = fromObj.toUser.id;
				 toRec.val (table, getFldName (ETaskField.TF_Finished)).dt.val   = fromObj.finished;

				 return (toRec);
				}

	  private class Task : ITask
	   {
        public Task (Database.IFactory db, Database.ILangTranslation lang, Database.ITable<ITask> tskTb, ICreator biCreator,
				   User.IModule userMdl, Database.ITable<ProcessData.IProcessData> procDtTbl, Database.ITable<IProcess> processTable, Database.IRecord rec)
					{
					 record = rec; database = db; language = lang; table = tskTb; userModule = userMdl;

					 m_Process  = biCreator.reference<IProcess> (processTable);
					 m_FromUser = biCreator.reference<User.IUser> (userModule.userTable);
					 m_Data     = biCreator.reference<ProcessData.IProcessData> (procDtaTable = procDtTbl);
					 m_ToUser   = biCreator.reference<User.IUser> (userModule.userTable);
					}
		
		public Database.IRecord	record	{ get; private set; }

		private Database.IFactory							database		{ get; set; }
		private Database.ILangTranslation					language		{ get; set; }
		private Database.ITable<ITask>						table			{ get; set; }
		private Database.ITable<ProcessData.IProcessData>	procDtaTable	{ get; set; }

		private User.IModule	userModule	{ get; set; }

        public long									id			{ get; set; }
	    public IReference<IProcess>					process		{ get { return (m_Process); } }		private IReference<IProcess>					m_Process;
	    public System.DateTime						created		{ get; set; }
	    public IReference<User.IUser>				fromUser	{ get { return (m_FromUser); } }	private IReference<User.IUser>					m_FromUser;
	    public IReference<ProcessData.IProcessData>	data		{ get { return (m_Data); } }		private IReference<ProcessData.IProcessData>	m_Data;
	    public int									stateId		{ get; set; }
	    public string								stateDesc	{ get; set; }
	    public int									priority	{ get; set; }
	    public System.DateTime						started		{ get; set; }			// null date means not started by toUser
	    public IReference<User.IUser>				toUser		{ get { return (m_ToUser); } }		private IReference<User.IUser>					m_ToUser;
	    public System.DateTime						finished	{ get; set; }

	    public void	start (Database.ITransaction trans = null)
					{
					 if (language.isNull (started))
					  {
					   started = System.DateTime.Now;
					   
					   if (trans != null) table.update (trans, this);
					   else using (Database.ITransaction tr = database.openTransaction ()) { table.update (tr, this); tr.finalyOk = true; }
					  }
					}

	    public bool	isAble2Process (long currUserId, int newStateId, ProcessData.IProcessData newDta = null)
					{
					 if (newDta == null) newDta = data.parent;

					 if (newDta == null) { IProcess proc = process.parent; newDta = ((proc == null)? null: proc.data.parent); }
					 if (newDta == null) return (false);

					 INextProcess nextProc = NextProcess.createNextProcess (currUserId: currUserId, isHasUserRoleFce: userModule.hasUserRole, singleUserIdFce: findSingleUser,
																		  currStateId: stateId, newStateId: newStateId, procData: newDta);
					 return (nextProc != null && (!(language.isNull (nextProc.userId))));
					}

	    public void	makeProcess (long currUserId, int newStateId, Database.ITransaction trans = null, ProcessData.IProcessData newDta = null)
					{
					 if (newDta == null) newDta = data.parent;

					 if (newDta == null) { IProcess proc = process.parent; newDta = ((proc == null)? null: proc.data.parent); }
					 if (newDta == null) return;

					 INextProcess nextProc = NextProcess.createNextProcess (currUserId: currUserId, isHasUserRoleFce: userModule.hasUserRole, singleUserIdFce: findSingleUser,
																		  currStateId: stateId, newStateId: newStateId, procData: newDta);
					 if (nextProc == null || language.isNull (nextProc.userId)) return;
					 
					 if (trans != null) makeProcess (trans, currUserId, nextProc, newDta);
					 else using (trans = database.openTransaction ()) { makeProcess (trans, currUserId, nextProc, newDta); trans.finalyOk = true; }
					}

		private long	findSingleUser (User.EUserRole role)
					{
					 using (Database.IRecordSet<User.IUserRole> rs = userModule.userRoleTable.findObj (User.EUserRoleField.URF_Type, (short) role))
					  if (rs.moveNext ()) return (rs.currObj.user.id);
					 
					 return (0);
					}

	    private void	makeProcess (Database.ITransaction trans, long currUserId, INextProcess nextProcess, ProcessData.IProcessData newDta)
					{
					 if (language.isNull (started)) started = System.DateTime.Now;
					 if (language.isNull (finished)) finished = System.DateTime.Now;

					 table.update (trans, this);

					 ITask newTask = table.createNewObj ();
					 newTask.process.id  = process.id;
					 newTask.created     = System.DateTime.Now;
					 newTask.fromUser.id = currUserId; 
					 if (newDta != null) newTask.data.id = newDta.id;
					 newTask.stateId	 = nextProcess.stateId;
					 newTask.stateDesc   = nextProcess.stateDesc;
					 newTask.priority    = nextProcess.priority;
					 newTask.toUser.id   = nextProcess.userId;
					 if (nextProcess.isClosed) newTask.started = newTask.finished = System.DateTime.Now;

					 table.insert (trans, newTask);

					 switch (stateId)
					  {
					   case (int) EStateAnotace.SA_Zruseno:
					   case (int) EStateAnotace.SA_Anotovano: changeParentTaskPriority (trans, -3); break;
					  }
					 
					 switch (nextProcess.stateId)
					  {
					   case (int) EStateAnotace.SA_Prideleno:
					     if (stateId != (int) EStateAnotace.SA_Zruseno && stateId != (int) EStateAnotace.SA_Anotovano)
						  changeParentTaskPriority (trans, -3);
						 break;
					   case (int) EStateAnotace.SA_Zruseno:
					   case (int) EStateAnotace.SA_Anotovano: changeParentTaskPriority (trans, 3); break;
					   case (int) EStateAnotace.SA_DoAmesu: fillUpParentTaskAnotated (trans, newTask, newDta); break;
					  }
					}

		private void	changeParentTaskPriority (Database.ITransaction trans, int change)
					{
					 IProcess proc = process.parent;
					 ITask parTask = ((proc != null)? proc.parentTask.parent: null);
					 if (parTask == null) return;

					 parTask.priority += change;
					 table.update (trans, parTask);
					}

		private void	fillUpParentTaskAnotated (Database.ITransaction trans, ITask newTask, ProcessData.IProcessData newDta)
					{
					 IProcess thisProc = newTask.process.parent;
					 ITask parTask = ((thisProc != null)? thisProc.parentTask.parent: null);
					 ProcessData.IProcessData procData = ((parTask != null)? parTask.data.parent: null);
					 if (procData == null || procData.type != ProcessData.EProcessData.PD_Text || parTask.stateId != (int) EStateText.ST_KAnotaci) return;

					 if (procData.text.anotated1ProcessId == newTask.process.id) procData.text.anotated1ProcessDataId = newDta.id;
					 else if (procData.text.anotated2ProcessId == newTask.process.id) procData.text.anotated2ProcessDataId = newDta.id;
					 else if (procData.text.anotated1ProcessId == 0) { procData.text.anotated1ProcessId = newTask.process.id; procData.text.anotated1ProcessDataId = newDta.id; }
					 else if (procData.text.anotated2ProcessId == 0) { procData.text.anotated2ProcessId = newTask.process.id; procData.text.anotated2ProcessDataId = newDta.id; }
					 else return;

					 procDtaTable.update (trans, procData);

					 if (language.isNull (parTask.finished))
					  {
					   long anoSuperId = findSingleUser (User.EUserRole.UR_AdjSupervisor);
					   if (anoSuperId != 0) parTask.makeProcess (currUserId: anoSuperId, newStateId: (int) EStateText.ST_Anotovan, trans: trans, newDta: procData);
					  }
					}
		
		private interface INextProcess
		 {
		  int		stateId		{ get; }
		  string	stateDesc	{ get; }
		  int		priority	{ get; }
		  long		userId		{ get; }
		  bool		isClosed	{ get; }
		 }

		private class NextProcess : INextProcess
		 {
		  private NextProcess (int st, string desc, long usrId, bool isCls = false)
					{
					 stateId = st; stateDesc = desc; userId = usrId; isClosed = isCls;

					 switch (stateId)
					  {
					   case (int) EStateText.ST_KNacteni:      priority = 20; break;
					   case (int) EStateText.ST_Nacteno:       priority = 25; break;
					   case (int) EStateText.ST_ChybaKonverze: priority = 40; break;
					   case (int) EStateText.ST_Anotovan:	   priority = 50; break;
					   case (int) EStateAnotace.SA_DoAmesu:	   priority = 20; break;
					   default: priority = 50; break;
					  }
					}

		  public int	stateId		{ get; private set; }
		  public string	stateDesc	{ get; private set; }
		  public int	priority	{ get; private set; }
		  public long	userId		{ get; private set; }
		  public bool	isClosed	{ get; private set; }

		  public delegate bool DLazyHasUserRoleEvaluation (long currUserId, User.EUserRole role);
		  public delegate long DLazySingleUserIdEvaluation (User.EUserRole role);

	      public static INextProcess	createNextProcess (long currUserId, DLazyHasUserRoleEvaluation isHasUserRoleFce, DLazySingleUserIdEvaluation singleUserIdFce,
														 int currStateId, int newStateId, ProcessData.IProcessData procData)
					{
					 long automatId;

					 switch (currStateId)
					  {
					   case (int) EStateText.ST_Start:
					     if (currUserId == procData.text.koordinator.id && newStateId == (int) EStateText.ST_Vybran)
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 break;
					   
					   case (int) EStateText.ST_Vybran:
					     if (currUserId == procData.text.koordinator.id && newStateId == (int) EStateText.ST_KNacteni)
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.kontrolor.id));
						 break;
					   
					   case (int) EStateText.ST_KNacteni:
					     if (currUserId == procData.text.kontrolor.id)
						  if (newStateId == (int) EStateText.ST_Nacteno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Vybran)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.koordinator.id));
						 break;
					   
					   case (int) EStateText.ST_Nacteno:
					     if (currUserId == procData.text.kontrolor.id)
						  if (newStateId == (int) EStateText.ST_Konvertovano)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Vybran)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.koordinator.id));
						 break;
					   
					   case (int) EStateText.ST_Konvertovano:
					     if (currUserId == procData.text.kontrolor.id)
						  if (newStateId == (int) EStateText.ST_KAnotaci)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.supervisor.id));
						  else if (newStateId == (int) EStateText.ST_ChybaKonverze || newStateId == (int) EStateText.ST_KNacteni || newStateId == (int) EStateText.ST_Nacteno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Vybran)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.koordinator.id));
						  else if (newStateId == (int) EStateText.ST_Zruseno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId, isCls: true));
						 break;
					   
					   case (int) EStateText.ST_ChybaKonverze:
					     if (currUserId == procData.text.kontrolor.id)
						  if (newStateId == (int) EStateText.ST_KNacteni || newStateId == (int) EStateText.ST_Nacteno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Vybran)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.koordinator.id));
						  else if (newStateId == (int) EStateText.ST_Zruseno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId, isCls: true));
						 break;
					   
					   case (int) EStateText.ST_Zruseno:
					     if (currUserId == procData.text.kontrolor.id)
						  if (newStateId == (int) EStateText.ST_KNacteni || newStateId == (int) EStateText.ST_Nacteno || newStateId == (int) EStateText.ST_ChybaKonverze)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Vybran)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.koordinator.id));
						 break;
					   
					   case (int) EStateText.ST_KAnotaci:
					     if (currUserId == procData.text.supervisor.id)
						  if (newStateId == (int) EStateText.ST_Konvertovano && (automatId = singleUserIdFce (User.EUserRole.UR_AdjSupervisor)) != 0)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), automatId));
						  else if (newStateId == (int) EStateText.ST_Anotovan && procData.text.anotated1ProcessDataId != 0 && procData.text.anotated1ProcessId != 0 &&
								   procData.text.anotated2ProcessDataId != 0 && procData.text.anotated2ProcessId != 0)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 if (newStateId == (int) EStateText.ST_Anotovan && procData.text.anotated1ProcessDataId != 0 && procData.text.anotated1ProcessId != 0 &&
						     procData.text.anotated2ProcessDataId != 0 && procData.text.anotated2ProcessId != 0 && isHasUserRoleFce (currUserId, User.EUserRole.UR_AdjSupervisor))
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 break;
					   
					   case (int) EStateText.ST_Anotovan:
					     if (currUserId == procData.text.supervisor.id && newStateId == (int) EStateText.ST_KAnotaci)
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 if (procData.text.adjudikator.id != 0 && isHasUserRoleFce (currUserId, User.EUserRole.UR_AdjSupervisor) && newStateId == (int) EStateText.ST_AdjPrideleno)
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.adjudikator.id));
						 break;

					   case (int) EStateText.ST_AdjPrideleno:
					     if (currUserId == procData.text.adjudikator.id)
						  if (newStateId == (int) EStateText.ST_AdjPrideleno || newStateId == (int) EStateText.ST_AdjProblem || newStateId == (int) EStateText.ST_AdjVraceno)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if (newStateId == (int) EStateText.ST_Adjudikovano && (automatId = singleUserIdFce (User.EUserRole.UR_AdjSupervisor)) != 0)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), automatId));
						 if (newStateId == (int) EStateText.ST_Anotovan && isHasUserRoleFce (currUserId, User.EUserRole.UR_AdjSupervisor))
						  return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 break;

					   case (int) EStateText.ST_AdjProblem: goto case (int) EStateText.ST_AdjPrideleno;

					   case (int) EStateText.ST_AdjVraceno: goto case (int) EStateText.ST_AdjPrideleno;

					   case (int) EStateText.ST_Adjudikovano:
					     if (isHasUserRoleFce (currUserId, User.EUserRole.UR_AdjSupervisor))
						  if (newStateId == (int) EStateText.ST_Adjudikovano || newStateId == (int) EStateText.ST_AdjKontrola || newStateId == (int) EStateText.ST_DoAmesu)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						  else if ((newStateId == (int) EStateText.ST_AdjVraceno || newStateId == (int) EStateText.ST_AdjProblem) && procData.text.adjudikator.id != 0)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), procData.text.adjudikator.id));
						 break;

					   case (int) EStateText.ST_AdjKontrola: goto case (int) EStateText.ST_Adjudikovano;

					   case (int) EStateText.ST_DoAmesu:
					     if (newStateId == (int) EStateText.ST_AdjUlozeno && isHasUserRoleFce (currUserId, User.EUserRole.UR_AutoChecker))
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId, isCls: true));
					     goto case (int) EStateText.ST_Adjudikovano;

					   case (int) EStateText.ST_AdjUlozeno:
					     if (isHasUserRoleFce (currUserId, User.EUserRole.UR_AdjSupervisor))
						  if (newStateId == (int) EStateText.ST_Adjudikovano || newStateId == (int) EStateText.ST_AdjKontrola || newStateId == (int) EStateText.ST_DoAmesu)
						   return (new NextProcess (newStateId, getStName ((EStateText) newStateId), currUserId));
						 break;



					   case (int) EStateAnotace.SA_Start:
					     if (currUserId == procData.anotace.supervisor.id && newStateId == (int) EStateAnotace.SA_Prideleno)
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						 break;
					   
					   case (int) EStateAnotace.SA_Prideleno:
					     if (currUserId == procData.anotace.supervisor.id && newStateId == (int) EStateAnotace.SA_Zruseno)
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId, isCls: true));
					     if (currUserId == procData.anotace.anotator.id)
						  if (newStateId == (int) EStateAnotace.SA_VAnotaci || newStateId == (int) EStateAnotace.SA_Prideleno)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId));
						  else if (newStateId == (int) EStateAnotace.SA_Odevzdano || newStateId == (int) EStateAnotace.SA_KOdevzdani)
						   if ((automatId = singleUserIdFce (User.EUserRole.UR_AutoChecker)) != 0) return (new NextProcess ((int) EStateAnotace.SA_KOdevzdani, getStName (EStateAnotace.SA_KOdevzdani), automatId));
						   else return (new NextProcess ((int) EStateAnotace.SA_Odevzdano, getStName (EStateAnotace.SA_Odevzdano), procData.anotace.supervisor.id));
						  else if (newStateId == (int) EStateAnotace.SA_KOvereni)
						   if ((automatId = singleUserIdFce (User.EUserRole.UR_AutoChecker)) != 0) return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), automatId));
						   else return (new NextProcess ((int) EStateAnotace.SA_VAnotaci, getStName (EStateAnotace.SA_VAnotaci), currUserId));
						 break;
					   
					   case (int) EStateAnotace.SA_KOvereni:
					     if (newStateId == (int) EStateAnotace.SA_Prideleno &&
						     (currUserId == procData.anotace.anotator.id || currUserId == procData.anotace.supervisor.id || isHasUserRoleFce (currUserId, User.EUserRole.UR_AutoChecker)))
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						 break;
					   
					   case (int) EStateAnotace.SA_VAnotaci:
					     if (currUserId == procData.anotace.supervisor.id && newStateId == (int) EStateAnotace.SA_Zruseno)
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId, isCls: true));
					     if (currUserId == procData.anotace.anotator.id && newStateId == (int) EStateAnotace.SA_Odevzdano)
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.supervisor.id));
						 break;
					   
					   case (int) EStateAnotace.SA_KOdevzdani:
					     if (newStateId == (int) EStateAnotace.SA_Odevzdano && (currUserId == procData.anotace.supervisor.id || isHasUserRoleFce (currUserId, User.EUserRole.UR_AutoChecker)))
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.supervisor.id));
						 break;
					   
					   case (int) EStateAnotace.SA_Odevzdano:
					     if (currUserId == procData.anotace.supervisor.id)
						  if (newStateId == (int) EStateAnotace.SA_Prideleno)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						  else if (newStateId == (int) EStateAnotace.SA_VAnotaci)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						  else if (newStateId == (int) EStateAnotace.SA_DoAmesu || newStateId == (int) EStateAnotace.SA_KPrenosu)
						   if ((automatId = singleUserIdFce (User.EUserRole.UR_AutoChecker)) != 0) return (new NextProcess ((int) EStateAnotace.SA_KPrenosu, getStName (EStateAnotace.SA_KPrenosu), automatId));
						   else return (new NextProcess ((int) EStateAnotace.SA_DoAmesu, getStName (EStateAnotace.SA_DoAmesu), currUserId));
						  else if (newStateId == (int) EStateAnotace.SA_Zruseno)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId, isCls: true));
						  else if (newStateId == (int) EStateAnotace.SA_KOdevzdani || newStateId == (int) EStateAnotace.SA_Odevzdano)
						   if ((automatId = singleUserIdFce (User.EUserRole.UR_AutoChecker)) != 0) return (new NextProcess ((int) EStateAnotace.SA_KOdevzdani, getStName (EStateAnotace.SA_KOdevzdani), automatId));
						   else return (new NextProcess ((int) EStateAnotace.SA_Odevzdano, getStName (EStateAnotace.SA_Odevzdano), currUserId));
						 break;
					   
					   case (int) EStateAnotace.SA_KPrenosu:
					     if (newStateId == (int) EStateAnotace.SA_DoAmesu || newStateId == (int) EStateAnotace.SA_Odevzdano)
						  if (currUserId == procData.anotace.supervisor.id || isHasUserRoleFce (currUserId, User.EUserRole.UR_AutoChecker))
						   if (newStateId == (int) EStateAnotace.SA_DoAmesu) return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId));
						   else if (newStateId == (int) EStateAnotace.SA_Odevzdano) return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.supervisor.id));
						 break;
					   
					   case (int) EStateAnotace.SA_DoAmesu:
					     if (newStateId == (int) EStateAnotace.SA_Anotovano && (currUserId == procData.anotace.supervisor.id || isHasUserRoleFce (currUserId, User.EUserRole.UR_AutoChecker)))
						  return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId, isCls: true));
						 break;
					   
					   case (int) EStateAnotace.SA_Zruseno:
					     if (currUserId == procData.anotace.supervisor.id)
						  if (newStateId == (int) EStateAnotace.SA_Prideleno)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						  else if (newStateId == (int) EStateAnotace.SA_Odevzdano)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId));
						 break;
					   
					   case (int) EStateAnotace.SA_Anotovano:
					     if (currUserId == procData.anotace.supervisor.id)
						  if (newStateId == (int) EStateAnotace.SA_Prideleno)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), procData.anotace.anotator.id));
						  else if (newStateId == (int) EStateAnotace.SA_Odevzdano)
						   return (new NextProcess (newStateId, getStName ((EStateAnotace) newStateId), currUserId));
						 break;
					  }
					 
					 return (null);
					}
		 }
	   }
	 }
	
	private static string	getFldName (EOpenTaskFileQueryField fld)
				{
				 switch (fld)
				  {
				   case EOpenTaskFileQueryField.OTFQ_Id:		 return ("Id");
				   case EOpenTaskFileQueryField.OTFQ_ProcessId:	 return ("ProcessId");
				   case EOpenTaskFileQueryField.OTFQ_StateId:	 return ("StateId");
				   case EOpenTaskFileQueryField.OTFQ_Created:	 return ("Created");
				   case EOpenTaskFileQueryField.OTFQ_Started:	 return ("Started");
				   case EOpenTaskFileQueryField.OTFQ_FromUserId: return ("FromUserId");
				   case EOpenTaskFileQueryField.OTFQ_ToUserId:	 return ("ToUserId");
				   case EOpenTaskFileQueryField.OTFQ_HtmlId:	 return ("HtmlId");
				   case EOpenTaskFileQueryField.OTFQ_XmlWId:	 return ("XmlWId");
				   case EOpenTaskFileQueryField.OTFQ_XmlAId:	 return ("XmlAId");
				   case EOpenTaskFileQueryField.OTFQ_XmlBId:	 return ("XmlBId");
				   default: throw new System.SystemException ("Unknow field of Process.OpenTaskFile query: " + ((int) fld).ToString ());
				  }
				}

	private static Database.IQuery<IOpenTaskFileQuery>	createOpenTaskQuery (ICreator biCre, Database.IFactory db, Database.ITable<User.IUser, User.EUserField> userTable,
														                   Database.ITable<ProcessData.IFile> fileTable, string fileTableIdFldName)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRCQ_OpenTaskFile");
				 tc.addPrimaryField (getFldName (EOpenTaskFileQueryField.OTFQ_Id));
				 tc.addField (getFldName (EOpenTaskFileQueryField.OTFQ_ProcessId), Database.EFieldType.FT_BigInt);
				 tc.addField (getFldName (EOpenTaskFileQueryField.OTFQ_StateId), Database.EFieldType.FT_Int);
				 tc.addField (getFldName (EOpenTaskFileQueryField.OTFQ_Created), Database.EFieldType.FT_DateTime);
				 tc.addField (getFldName (EOpenTaskFileQueryField.OTFQ_Started), Database.EFieldType.FT_DateTime);
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_FromUserId), userTable, userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_ToUserId), userTable, userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_HtmlId), fileTable, fileTableIdFldName);
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_XmlWId), fileTable, fileTableIdFldName);
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_XmlAId), fileTable, fileTableIdFldName);
				 tc.addReferenceField (getFldName (EOpenTaskFileQueryField.OTFQ_XmlBId), fileTable, fileTableIdFldName);
				 
				 OpenTaskFileTypeConv conv = new OpenTaskFileTypeConv (biCre, userTable, fileTable);
				 Database.IQuery<IOpenTaskFileQuery> q = tc.createQuery<IOpenTaskFileQuery> (conv);
				 conv.query = q;
				 return (q);
				}

	private class OpenTaskFileTypeConv : Database.IRecordLoader<IOpenTaskFileQuery>
	 {
	  public OpenTaskFileTypeConv (ICreator biCre, Database.ITable<User.IUser> usrTb, Database.ITable<ProcessData.IFile> flTb)	{ biCreator = biCre; userTable = usrTb; fileTable = flTb; }

	  private ICreator								biCreator	{ get; set; }
	  private Database.ITable<User.IUser>			userTable	{ get; set; }
	  private Database.ITable<ProcessData.IFile>	fileTable	{ get; set; }

	  public Database.IQuery<IOpenTaskFileQuery>	query	{ private get; set; }

	  public IOpenTaskFileQuery	nullValue	{ get { return (null); } }

	  public IOpenTaskFileQuery	create4Rec (Database.IRecord rec)	{ IOpenTaskFileQuery opnTskFlQ = new OpenTaskFileQuery (biCreator, userTable, fileTable, rec); loadData (opnTskFlQ); return (opnTskFlQ); }

	  public void	loadData (IOpenTaskFileQuery toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id        = fromRec.val (query, getFldName (EOpenTaskFileQueryField.OTFQ_Id)).bg.val;
				 toObj.processId = fromRec.val (query, getFldName (EOpenTaskFileQueryField.OTFQ_ProcessId)).bg.val;
				 toObj.stateId   = fromRec.val (query, getFldName (EOpenTaskFileQueryField.OTFQ_StateId)).it.val;
				 toObj.created   = fromRec.val (query, getFldName (EOpenTaskFileQueryField.OTFQ_Created)).dt.val;
				 toObj.started   = fromRec.val (query, getFldName (EOpenTaskFileQueryField.OTFQ_Started)).dt.val;
				 toObj.fromUser.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_FromUserId));
				 toObj.toUser.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_ToUserId));
				 toObj.html.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_HtmlId));
				 toObj.xmlW.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_XmlWId));
				 toObj.xmlA.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_XmlAId));
				 toObj.xmlB.loadDta (query, fromRec, getFldName (EOpenTaskFileQueryField.OTFQ_XmlBId));
				}

	  private class OpenTaskFileQuery : IOpenTaskFileQuery
	   {
        public OpenTaskFileQuery (ICreator biCreator, Database.ITable<User.IUser> userTable, Database.ITable<ProcessData.IFile> fileTable, Database.IRecord rec)
					{
					 record = rec;
					 
					 m_FromUser = biCreator.reference<User.IUser> (userTable);
					 m_ToUser   = biCreator.reference<User.IUser> (userTable);
					 m_Html     = biCreator.reference<ProcessData.IFile> (fileTable);
					 m_XmlW     = biCreator.reference<ProcessData.IFile> (fileTable);
					 m_XmlA     = biCreator.reference<ProcessData.IFile> (fileTable);
					 m_XmlB     = biCreator.reference<ProcessData.IFile> (fileTable);
					}

		public Database.IRecord	record	{ get; private set; }

        public long						id			{ get; set; }
	    public long						processId	{ get; set; }
	    public int						stateId		{ get; set; }
		public System.DateTime			created		{ get; set; }
	    public System.DateTime			started		{ get; set; }
		public IReference<User.IUser>	fromUser	{ get { return (m_FromUser); } }	private IReference<User.IUser>	m_FromUser;
	    public IReference<User.IUser>	toUser		{ get { return (m_ToUser); } }		private IReference<User.IUser>	m_ToUser;

	    public IReference<ProcessData.IFile>	html	{ get { return (m_Html); } }	private IReference<ProcessData.IFile>	m_Html;
	    public IReference<ProcessData.IFile>	xmlW	{ get { return (m_XmlW); } }	private IReference<ProcessData.IFile>	m_XmlW;
	    public IReference<ProcessData.IFile>	xmlA	{ get { return (m_XmlA); } }	private IReference<ProcessData.IFile>	m_XmlA;
	    public IReference<ProcessData.IFile>	xmlB	{ get { return (m_XmlB); } }	private IReference<ProcessData.IFile>	m_XmlB;

	    public string	getFileName ()
					{
					 string fileName = getFileName (html.name, ".html"); if (fileName != null) return (fileName);
					 fileName = getFileName (html.name, ".htm"); if (fileName != null) return (fileName);
					 fileName = getFileName (xmlW.name, ".w.xml"); if (fileName != null) return (fileName);
					 fileName = getFileName (xmlA.name, ".a.xml"); if (fileName != null) return (fileName);
					 fileName = getFileName (xmlB.name, ".b.xml"); if (fileName != null) return (fileName);

					 return (null);
					}

		private static string	getFileName (string fileName, string endsByLowCase)
					{
					 if (fileName == null || fileName.Length <= endsByLowCase.Length) return (null);

					 if (fileName.ToLower ().EndsWith (endsByLowCase)) return (fileName.Substring (0, fileName.Length - endsByLowCase.Length));
					 else return (null);
					}

	   }
	 }
	
	private static string	getFldName (ECommentField fld)
				{
				 switch (fld)
				  {
				   case ECommentField.CF_Id:		return ("Id");
				   case ECommentField.CF_ProcessId: return ("ProcessId");
				   case ECommentField.CF_UserId:	return ("UserId");
				   case ECommentField.CF_At:		return ("CreatedAt");
				   case ECommentField.CF_Text:		return ("Text");
				   default: throw new System.SystemException ("Unknow field of Process.Comment table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IComment>	createCommentTable (ICreator biCre, Database.IFactory db, Database.ITable<User.IUser, User.EUserField> userTable,
														          Database.ITable<IProcess> processTable)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRC_Comment");
				 tc.addPrimaryField (getFldName (ECommentField.CF_Id));
				 tc.addReferenceField (getFldName (ECommentField.CF_ProcessId), processTable, getFldName (EProcessField.PF_Id));
				 tc.addReferenceField (getFldName (ECommentField.CF_UserId), userTable, userTable.getFieldName (User.EUserField.UF_Id));
				 tc.addField (getFldName (ECommentField.CF_At), Database.EFieldType.FT_DateTime);
				 tc.addTextField (getFldName (ECommentField.CF_Text), 2000, role: Database.EFieldRole.FR_Desc);

				 CommentTypeConv conv = new CommentTypeConv (biCre, userTable, processTable);
				 Database.ITable<IComment> tb = tc.createTable<IComment> (conv);
				 conv.table = tb;
				 return (tb);
				}

	private class CommentTypeConv : Database.ITypeConverter<IComment>
	 {
	  public CommentTypeConv (ICreator biCre, Database.ITable<User.IUser> usrTb, Database.ITable<IProcess> prcTb)	{ biCreator = biCre; userTable = usrTb; processTable = prcTb; }

	  private ICreator						biCreator		{ get; set; }
	  private Database.ITable<User.IUser>	userTable		{ get; set; }
	  private Database.ITable<IProcess>		processTable	{ get; set; }

	  public Database.ITable<IComment>	table	{ private get; set; }

	  public IComment	nullValue	{ get { return (null); } }

	  public IComment	create4Rec (Database.IRecord rec)	{ IComment cmn = new Comment (biCreator, userTable, processTable, rec); loadData (cmn); return (cmn); }

	  public void	loadData (IComment toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id   = fromRec.val (table, getFldName (ECommentField.CF_Id)).bg.val;
				 toObj.process.loadDta (table, fromRec, getFldName (ECommentField.CF_ProcessId));
				 toObj.user.loadDta (table, fromRec, getFldName (ECommentField.CF_UserId));
				 toObj.at   = fromRec.val (table, getFldName (ECommentField.CF_At)).dt.val;
				 toObj.text = fromRec.val (table, getFldName (ECommentField.CF_Text)).txt.val;
				}

	  public Database.IRecord	saveData (IComment fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (ECommentField.CF_Id)).bg.val        = fromObj.id;
				 toRec.val (table, getFldName (ECommentField.CF_ProcessId)).bg.val = fromObj.process.id;
				 toRec.val (table, getFldName (ECommentField.CF_UserId)).bg.val    = fromObj.user.id;
				 toRec.val (table, getFldName (ECommentField.CF_At)).dt.val        = fromObj.at;
				 toRec.val (table, getFldName (ECommentField.CF_Text)).txt.val     = fromObj.text;

				 return (toRec);
				}

	  private class Comment : IComment
	   {
        public Comment (ICreator biCreator, Database.ITable<User.IUser> userTable, Database.ITable<IProcess> processTable, Database.IRecord rec)
					{ record = rec; m_User = biCreator.reference<User.IUser> (userTable); m_Process = biCreator.reference<IProcess> (processTable); }

		public Database.IRecord	record	{ get; private set; }

        public long						id		{ get; set; }
	    public IReference<IProcess>		process	{ get { return (m_Process); } }	private IReference<IProcess>	m_Process;
	    public IReference<User.IUser>	user	{ get { return (m_User); } }	private IReference<User.IUser>	m_User;
	    public System.DateTime			at		{ get; set; }
	    public string					text	{ get; set; }
	   }
	 }

	private class ModuleImp : IModule
	 {
	  public ModuleImp (Database.ITable<IProcess> procTb, Database.ITable<ITask> tskTb, Database.ITable<IComment> cmnTb, Database.IQuery<IOpenTaskFileQuery> opnTskFlQ)
				{ m_ProcessTable = procTb; m_TaskTable = tskTb; m_CommentTable = cmnTb; m_OpenTaskFileQuery = opnTskFlQ; }

	  public string	getStateName (EStateSpecial st)	{ return (getStName (st)); }
	  public string	getStateName (EStateText st)	{ return (getStName (st)); }
	  public string	getStateName (EStateAnotace st)	{ return (getStName (st)); }

      public string	getFieldName (EProcessField fld)			{ return (getFldName (fld)); }
	  public string	getFieldName (ETaskField fld)				{ return (getFldName (fld)); }
	  public string	getFieldName (EOpenTaskFileQueryField fld)	{ return (getFldName (fld)); }
	  public string	getFieldName (ECommentField fld)			{ return (getFldName (fld)); }

      public Database.ITable<IProcess>	processTable	{ get { return (m_ProcessTable); } }	private Database.ITable<IProcess>	m_ProcessTable;
      public Database.ITable<ITask>		taskTable		{ get { return (m_TaskTable); } }		private Database.ITable<ITask>		m_TaskTable;
      public Database.ITable<IComment>	commentTable	{ get { return (m_CommentTable); } }	private Database.ITable<IComment>	m_CommentTable;

	  public Database.IQuery<IOpenTaskFileQuery>	openTaskFileQuery	{ get { return (m_OpenTaskFileQuery); } }	private Database.IQuery<IOpenTaskFileQuery>	m_OpenTaskFileQuery;
	 }
   }
 }
