﻿
namespace Shark.Speed.BI.ProcessData
 {
  public enum EProcessData	{ PD_Text = 10, PD_Anotace = 20 }

  public enum EProcessDataField
   {
    PDF_Id, PDF_Type, PDF_AmesId, PDF_DataSource,
	PDF_HtmlId, PDF_XmlWId, PDF_XmlAId, PDF_XmlBId,
	PDF_KoordinatorId, PDF_KontrolorId, PDF_SupervisorId, PDF_AnotatorId, PDF_AdjudikatorId,
	PDF_Anotated1ProcessDataId, PDF_Anotated1ProcessId, PDF_Anotated2ProcessDataId, PDF_Anotated2ProcessId
   }

  public enum EProcessDataSource { PDS_AmesNeslovani = 10, PDS_AmesSlovani = 20, PDS_AmesRomove = 30 }

  public interface IProcessData : IBiObject
   {
	EProcessData	type	{ get; set; }

	IProcessDataText	text	{ get; }
	IProcessDataAnotace	anotace	{ get; }

	IProcessData	createNewCopy (Database.ITable<IProcessData> tb);
   }

  public interface IProcessDataText : IProcessData
   {
    string				amesId		{ get; set; }
	EProcessDataSource	dataSource	{ get; set; }
	
	IReference<IFile>	html	{ get; }
	IReference<IFile>	xmlW	{ get; }
	IReference<IFile>	xmlA	{ get; }
	IReference<IFile>	xmlB	{ get; }

	IReference<User.IUser>	koordinator	{ get; }
	IReference<User.IUser>	kontrolor	{ get; }
	IReference<User.IUser>	supervisor	{ get; }
	IReference<User.IUser>	adjudikator	{ get; }

	long	anotated1ProcessDataId	{ get; set; }
	long	anotated1ProcessId		{ get; set; }

	long	anotated2ProcessDataId	{ get; set; }
	long	anotated2ProcessId		{ get; set; }

	int	calcWordCount ();
   }

  public interface IProcessDataAnotace : IProcessData
   {
	IReference<IFile>	html	{ get; }
	IReference<IFile>	xmlW	{ get; }
	IReference<IFile>	xmlA	{ get; }
	IReference<IFile>	xmlB	{ get; }

	IReference<User.IUser>	supervisor	{ get; }
	IReference<User.IUser>	anotator	{ get; }

	int	calcWordCount ();
   }

  public enum EFileField { FF_Id, FF_Name, FF_Data }

  public interface IFile : IBiObject
   {
	string	name	{ get; set; }
	byte[]	data	{ get; set; }
   }

  public interface IModule
   {
    string	getFieldName (EProcessDataField fld);
    string	getFieldName (EFileField fld);

    Database.ITable<IProcessData>	processDataTable	{ get; }
	Database.ITable<IFile>			fileTable			{ get; }
   }

  public class Module
   {
    private Module ()	{ }

	public static IModule	create (ICreator biCreator, Database.IFactory db, Database.ITable<User.IUser> userTable, string userTableIdFldName)
				{
				 Database.ITable<IFile> flTb = createFileTable (db);
				 return (new ModuleImp (createProcessDataTable (biCreator, db, userTable, userTableIdFldName, flTb), flTb));
				}

	private static string	getFldName (EProcessDataField fld)
				{
				 switch (fld)
				  {
				   case EProcessDataField.PDF_Id:					  return ("Id");
				   case EProcessDataField.PDF_Type:					  return ("DataType");
				   case EProcessDataField.PDF_AmesId:				  return ("AmesId");
				   case EProcessDataField.PDF_DataSource:			  return ("DataSource");
				   case EProcessDataField.PDF_HtmlId:				  return ("HtmlId");
				   case EProcessDataField.PDF_XmlWId:				  return ("XmlWId");
				   case EProcessDataField.PDF_XmlAId:				  return ("XmlAId");
				   case EProcessDataField.PDF_XmlBId:				  return ("XmlBId");
				   case EProcessDataField.PDF_KoordinatorId:		  return ("KoordinatorId");
				   case EProcessDataField.PDF_KontrolorId:			  return ("KontrolorId");
				   case EProcessDataField.PDF_SupervisorId:			  return ("SupervisorId");
				   case EProcessDataField.PDF_AnotatorId:			  return ("AnotatorId");
				   case EProcessDataField.PDF_AdjudikatorId:		  return ("AdjudikatorId");
				   case EProcessDataField.PDF_Anotated1ProcessDataId: return ("Anotated1ProcessDataId");
				   case EProcessDataField.PDF_Anotated1ProcessId:	  return ("Anotated1ProcessId");
				   case EProcessDataField.PDF_Anotated2ProcessDataId: return ("Anotated2ProcessDataId");
				   case EProcessDataField.PDF_Anotated2ProcessId:	  return ("Anotated2ProcessId");
				   default: throw new System.SystemException ("Unknow field of ProcessData.ProcessData table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IProcessData>	createProcessDataTable (ICreator biCre, Database.IFactory db, Database.ITable<User.IUser> userTable, string userTableIdFldName, Database.ITable<IFile> fileTable)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRD_ProcessData");
				 tc.addPrimaryField (getFldName (EProcessDataField.PDF_Id));
				 tc.addOptionField (getFldName (EProcessDataField.PDF_Type),
				                  new Database.IOptionValue [2] { tc.createOptionValue ((short) EProcessData.PD_Text, "text"), tc.createOptionValue ((short) EProcessData.PD_Anotace, "anotace") });
				 tc.addTextField (getFldName (EProcessDataField.PDF_AmesId), 100);
				 tc.addOptionField (getFldName (EProcessDataField.PDF_DataSource),
				                  new Database.IOptionValue [3] { tc.createOptionValue ((short) EProcessDataSource.PDS_AmesNeslovani, "Ames neslovani"),
								                                 tc.createOptionValue ((short) EProcessDataSource.PDS_AmesSlovani, "Ames slovani"),
								                                 tc.createOptionValue ((short) EProcessDataSource.PDS_AmesRomove, "Ames romove") });
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_HtmlId), fileTable, getFldName (EFileField.FF_Id));
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_XmlWId), fileTable, getFldName (EFileField.FF_Id));
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_XmlAId), fileTable, getFldName (EFileField.FF_Id));
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_XmlBId), fileTable, getFldName (EFileField.FF_Id));
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_KoordinatorId), userTable, userTableIdFldName);
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_KontrolorId), userTable, userTableIdFldName);
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_SupervisorId), userTable, userTableIdFldName);
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_AnotatorId), userTable, userTableIdFldName);
				 tc.addReferenceField (getFldName (EProcessDataField.PDF_AdjudikatorId), userTable, userTableIdFldName);

				 tc.addField (getFldName (EProcessDataField.PDF_Anotated1ProcessDataId), Database.EFieldType.FT_BigInt);
				 tc.addField (getFldName (EProcessDataField.PDF_Anotated1ProcessId), Database.EFieldType.FT_BigInt);
				 tc.addField (getFldName (EProcessDataField.PDF_Anotated2ProcessDataId), Database.EFieldType.FT_BigInt);
				 tc.addField (getFldName (EProcessDataField.PDF_Anotated2ProcessId), Database.EFieldType.FT_BigInt);

				 ProcessDataTypeConv conv = new ProcessDataTypeConv (biCre, userTable, fileTable);
				 Database.ITable<IProcessData> tb = tc.createTable<IProcessData> (conv);
				 conv.table = tb;
				 return (tb);
				}

	private class ProcessDataTypeConv : Database.ITypeConverter<IProcessData>
	 {
	  public ProcessDataTypeConv (ICreator biCre, Database.ITable<User.IUser> usrTb, Database.ITable<IFile> flTb)	{ biCreator = biCre; userTable = usrTb; fileTable = flTb; }

	  private ICreator						biCreator	{ get; set; }
	  private Database.ITable<User.IUser>	userTable	{ get; set; }
	  private Database.ITable<IFile>		fileTable	{ get; set; }

	  public Database.ITable<IProcessData>	table	{ private get; set; }

	  public IProcessData	nullValue	{ get { return (null); } }

	  public IProcessData	create4Rec (Database.IRecord rec)	{ IProcessData procDta = new ProcessData (biCreator, userTable, fileTable, rec); loadData (procDta); return (procDta); }

	  public void	loadData (IProcessData toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id   = fromRec.val (table, getFldName (EProcessDataField.PDF_Id)).bg.val;
				 toObj.type = (EProcessData) (fromRec.val (table, getFldName (EProcessDataField.PDF_Type)).sh.val);

				 switch (toObj.type)
				  {
				   case EProcessData.PD_Text:
				     toObj.text.amesId     = fromRec.val (table, getFldName (EProcessDataField.PDF_AmesId)).txt.val;
					 toObj.text.dataSource = (EProcessDataSource) (fromRec.val (table, getFldName (EProcessDataField.PDF_DataSource)).sh.val);
					 toObj.text.html.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_HtmlId));
					 toObj.text.xmlW.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlWId));
					 toObj.text.xmlA.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlAId));
					 toObj.text.xmlB.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlBId));
					 toObj.text.koordinator.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_KoordinatorId));
					 toObj.text.kontrolor.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_KontrolorId));
					 toObj.text.supervisor.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_SupervisorId));
					 toObj.text.adjudikator.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_AdjudikatorId));
					 toObj.text.anotated1ProcessDataId = fromRec.val (table, getFldName (EProcessDataField.PDF_Anotated1ProcessDataId)).bg.val;
					 toObj.text.anotated1ProcessId     = fromRec.val (table, getFldName (EProcessDataField.PDF_Anotated1ProcessId)).bg.val;
					 toObj.text.anotated2ProcessDataId = fromRec.val (table, getFldName (EProcessDataField.PDF_Anotated2ProcessDataId)).bg.val;
					 toObj.text.anotated2ProcessId     = fromRec.val (table, getFldName (EProcessDataField.PDF_Anotated2ProcessId)).bg.val;
					 break;
				   case EProcessData.PD_Anotace:
					 toObj.anotace.html.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_HtmlId));
					 toObj.anotace.xmlW.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlWId));
					 toObj.anotace.xmlA.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlAId));
					 toObj.anotace.xmlB.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_XmlBId));
					 toObj.anotace.supervisor.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_SupervisorId));
					 toObj.anotace.anotator.loadDta (table, fromRec, getFldName (EProcessDataField.PDF_AnotatorId));
				     break;
				  }
				}

	  public Database.IRecord	saveData (IProcessData fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EProcessDataField.PDF_Id)).bg.val   = fromObj.id;
				 toRec.val (table, getFldName (EProcessDataField.PDF_Type)).sh.val = (short) (fromObj.type);

				 switch (fromObj.type)
				  {
				   case EProcessData.PD_Text:
				     toRec.val (table, getFldName (EProcessDataField.PDF_AmesId)).txt.val				 = fromObj.text.amesId;
					 toRec.val (table, getFldName (EProcessDataField.PDF_DataSource)).sh.val			 = (short) (fromObj.text.dataSource);
					 toRec.val (table, getFldName (EProcessDataField.PDF_HtmlId)).bg.val				 = fromObj.text.html.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlWId)).bg.val				 = fromObj.text.xmlW.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlAId)).bg.val				 = fromObj.text.xmlA.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlBId)).bg.val				 = fromObj.text.xmlB.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_KoordinatorId)).bg.val			 = fromObj.text.koordinator.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_KontrolorId)).bg.val			 = fromObj.text.kontrolor.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_SupervisorId)).bg.val			 = fromObj.text.supervisor.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_AdjudikatorId)).bg.val			 = fromObj.text.adjudikator.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_Anotated1ProcessDataId)).bg.val = fromObj.text.anotated1ProcessDataId;
					 toRec.val (table, getFldName (EProcessDataField.PDF_Anotated1ProcessId)).bg.val	 = fromObj.text.anotated1ProcessId;
					 toRec.val (table, getFldName (EProcessDataField.PDF_Anotated2ProcessDataId)).bg.val = fromObj.text.anotated2ProcessDataId;
					 toRec.val (table, getFldName (EProcessDataField.PDF_Anotated2ProcessId)).bg.val	 = fromObj.text.anotated2ProcessId;
					 break;
				   case EProcessData.PD_Anotace:
					 toRec.val (table, getFldName (EProcessDataField.PDF_HtmlId)).bg.val       = fromObj.anotace.html.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlWId)).bg.val       = fromObj.anotace.xmlW.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlAId)).bg.val       = fromObj.anotace.xmlA.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_XmlBId)).bg.val       = fromObj.anotace.xmlB.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_SupervisorId)).bg.val = fromObj.anotace.supervisor.id;
					 toRec.val (table, getFldName (EProcessDataField.PDF_AnotatorId)).bg.val   = fromObj.anotace.anotator.id;
					 break;
				  }

				 return (toRec);
				}

	  private class ProcessData : IProcessData, IProcessDataText, IProcessDataAnotace
	   {
        public ProcessData (ICreator biCreator, Database.ITable<User.IUser> userTable, Database.ITable<IFile> fileTable, Database.IRecord rec)
					{
					 record = rec;

					 m_Koordinator = biCreator.reference<User.IUser> (userTable);
					 m_Kontrolor   = biCreator.reference<User.IUser> (userTable);
					 m_Supervisor  = biCreator.reference<User.IUser> (userTable);
					 m_Anotator    = biCreator.reference<User.IUser> (userTable);
					 m_Adjudikator = biCreator.reference<User.IUser> (userTable);

					 m_Html = biCreator.reference<IFile> (fileTable);
					 m_XmlW = biCreator.reference<IFile> (fileTable);
					 m_XmlA = biCreator.reference<IFile> (fileTable);
					 m_XmlB = biCreator.reference<IFile> (fileTable);
					}

		public Database.IRecord	record	{ get; private set; }

        public long			id		{ get; set; }
	    public EProcessData	type	{ get; set; }

	    public IProcessDataText		text	{ get { return (this); } }
	    public IProcessDataAnotace	anotace	{ get { return (this); } }

        public string				amesId		{ get; set; }
		public EProcessDataSource	dataSource	{ get; set; }
	
	    public IReference<IFile>	html	{ get { return (m_Html); } }	private IReference<IFile>	m_Html;
	    public IReference<IFile>	xmlW	{ get { return (m_XmlW); } }	private IReference<IFile>	m_XmlW;
	    public IReference<IFile>	xmlA	{ get { return (m_XmlA); } }	private IReference<IFile>	m_XmlA;
	    public IReference<IFile>	xmlB	{ get { return (m_XmlB); } }	private IReference<IFile>	m_XmlB;

	    public IReference<User.IUser>	koordinator	{ get { return (m_Koordinator); } }	private IReference<User.IUser>	m_Koordinator;
	    public IReference<User.IUser>	kontrolor	{ get { return (m_Kontrolor); } }	private IReference<User.IUser>	m_Kontrolor;
	    public IReference<User.IUser>	supervisor	{ get { return (m_Supervisor); } }	private IReference<User.IUser>	m_Supervisor;
		public IReference<User.IUser>	anotator	{ get { return (m_Anotator); } }	private IReference<User.IUser>	m_Anotator;
		public IReference<User.IUser>	adjudikator	{ get { return (m_Adjudikator); } }	private IReference<User.IUser>	m_Adjudikator;

		public long	anotated1ProcessDataId	{ get; set; }
		public long	anotated1ProcessId		{ get; set; }

		public long	anotated2ProcessDataId	{ get; set; }
		public long	anotated2ProcessId		{ get; set; }

	    public IProcessData	createNewCopy (Database.ITable<IProcessData> tb)
					{
					 IProcessData cp = tb.createNewObj ();

					 switch (cp.type = type)
					  {
					   case EProcessData.PD_Text:
					     cp.text.amesId					= amesId;
						 cp.text.dataSource				= dataSource;
						 cp.text.html.id				= html.id;
						 cp.text.xmlW.id				= xmlW.id;
						 cp.text.xmlA.id				= xmlA.id;
						 cp.text.xmlB.id				= xmlB.id;
						 cp.text.koordinator.id			= koordinator.id;
						 cp.text.kontrolor.id			= kontrolor.id;
						 cp.text.supervisor.id			= supervisor.id;
						 cp.text.adjudikator.id			= adjudikator.id;
						 cp.text.anotated1ProcessDataId = anotated1ProcessDataId;
						 cp.text.anotated1ProcessId		= anotated1ProcessId;
						 cp.text.anotated2ProcessDataId = anotated2ProcessDataId;
						 cp.text.anotated2ProcessId		= anotated2ProcessId;
						 break;
					  
					   case EProcessData.PD_Anotace:
					     cp.anotace.html.id		  = html.id;
						 cp.anotace.xmlW.id		  = xmlW.id;
						 cp.anotace.xmlA.id		  = xmlA.id;
						 cp.anotace.xmlB.id		  = xmlB.id;
						 cp.anotace.supervisor.id = supervisor.id;
						 cp.anotace.anotator.id	  = anotator.id;
						 break;
					  }

					 return (cp);
					}

		public int	calcWordCount ()
					{
					 IFile xmlFile = xmlW.parent;

					 if (xmlFile == null || xmlFile.data == null || xmlFile.data.Length < 1) return (0);

					 System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding ();
					 string content = enc.GetString (xmlFile.data); if (content == null) return (0);
					 System.IO.StringReader sr = new System.IO.StringReader (content);

					 string line; int count = 0;
					 while ((line = sr.ReadLine ()) != null) if (line.TrimStart ().StartsWith ("<w")) count++;
					 
					 return (count);
					}
	   }
	 }

	private static string	getFldName (EFileField fld)
				{
				 switch (fld)
				  {
				   case EFileField.FF_Id:	return ("Id");
				   case EFileField.FF_Name: return ("Name");
				   case EFileField.FF_Data: return ("Data");
				   default: throw new System.SystemException ("Unknow field of ProcessData.File table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IFile>	createFileTable (Database.IFactory db)
				{
				 Database.ITableCreator tc = db.tableCreator ("PRD_File");
				 tc.addPrimaryField (getFldName (EFileField.FF_Id));
				 tc.addTextField (getFldName (EFileField.FF_Name), 100, role: Database.EFieldRole.FR_Code);
				 tc.addBLOBField (getFldName (EFileField.FF_Data));

				 FileTypeConv conv = new FileTypeConv ();
				 Database.ITable<IFile> tb = tc.createTable<IFile> (conv);
				 conv.table = tb;
				 return (tb);
				}

	private class FileTypeConv : Database.ITypeConverter<IFile>
	 {
	  public FileTypeConv ()	{ }

	  public Database.ITable<IFile>	table	{ private get; set; }

	  public IFile	nullValue	{ get { return (null); } }

	  public IFile	create4Rec (Database.IRecord rec)	{ IFile fl = new File (rec); loadData (fl); return (fl); }

	  public void	loadData (IFile toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id   = fromRec.val (table, getFldName (EFileField.FF_Id)).bg.val;
				 toObj.name = fromRec.val (table, getFldName (EFileField.FF_Name)).txt.val;
				 toObj.data = fromRec.val (table, getFldName (EFileField.FF_Data)).blb.val;
				}

	  public Database.IRecord	saveData (IFile fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EFileField.FF_Id)).bg.val    = fromObj.id;
				 toRec.val (table, getFldName (EFileField.FF_Name)).txt.val = fromObj.name;
				 toRec.val (table, getFldName (EFileField.FF_Data)).blb.val = fromObj.data;
				 
				 return (toRec);
				}

	  private class File : IFile
	   {
        public File (Database.IRecord rec)	{ record = rec; }

		public Database.IRecord	record	{ get; private set; }

    	public long		id		{ get; set; }
		public string	name	{ get; set; }
		public byte[]	data	{ get; set; }
	   }
	 }

    private class ModuleImp : IModule
	 {
      public ModuleImp (Database.ITable<IProcessData> procDtaTb, Database.ITable<IFile> flTb)	{ m_ProcessDataTable = procDtaTb; m_FileTable = flTb; }
	  
	  public string	getFieldName (EProcessDataField fld)	{ return (getFldName (fld)); }
      public string	getFieldName (EFileField fld)			{ return (getFldName (fld)); }

      public Database.ITable<IProcessData>	processDataTable	{ get { return (m_ProcessDataTable); } }	private Database.ITable<IProcessData>	m_ProcessDataTable;
	  public Database.ITable<IFile>			fileTable			{ get { return (m_FileTable); } }			private Database.ITable<IFile>			m_FileTable;
	 }
   }
 }
