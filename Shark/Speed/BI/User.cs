﻿
namespace Shark.Speed.BI.User
 {
  public enum EUserField { UF_Id, UF_UserName, UF_Name, UF_PassWord, UF_Valid }

  public interface IUser : IBiObject
   {
	string	userName	{ get; set; }
	string	name		{ get; set; }
	string	passWord	{ get; set; }
	bool	valid		{ get; set; }

	//IUserRole[]	findRoles ();
   }

  public enum EUserRole	{
						  UR_Koordinator = 10, UR_Kontrolor = 20, UR_Supervisor = 30, UR_Anotator = 40, UR_AdjSupervisor = 50, UR_Adjudicator = 60,
						  UR_AutoChecker = 110
						}

  public enum EUserRoleField { URF_Id, URF_UserId, URF_Type, URF_MySupervisorId }

  public interface IUserRole : IBiObject
   {
	IReference<IUser>	user	{ get; }
	EUserRole			type	{ get; set; }

//	IUserRoleKoordinator	koordinator	{ get; }
//	IUserRoleKontrolor		kontrolor	{ get; }
//	IUserRoleSupervisor		supervisor	{ get; }
	IUserRoleAnotator		anotator	{ get; }
   }

//  public interface IUserRoleKoordinator : IUserRole
//   { } //IReference<IUser>[]	findKontrolors (); }

//  public interface IUserRoleKontrolor : IUserRole
//   { } //IReference<IUser>[]	findKoordinators (); }
  
//  public interface IUserRoleSupervisor : IUserRole
//   { } //IReference<IUser>[]	findAnotators (); }
  
  public interface IUserRoleAnotator : IUserRole
   { IReference<IUserRole>	mySupervisor	{ get; } }

  public enum EKood2KontrField { K2KF_Id, K2KF_KoordId, K2KF_KontrId }

  public interface IKoordinator2Kontrolor : IBiObject
   {
    IReference<IUserRole>	koordinator	{ get; }
    IReference<IUserRole>	kontrolor	{ get; }
   }

  public interface IModule
   {
    string	getFieldName (EUserField fld);
    string	getFieldName (EUserRoleField fld);
    string	getFieldName (EKood2KontrField fld);

    Database.ITable<IUser, EUserField>							userTable			{ get; }
	Database.ITable<IUserRole, EUserRoleField>					userRoleTable		{ get; }
	Database.ITable<IKoordinator2Kontrolor, EKood2KontrField>	koord2kontrTable	{ get; }

	IUser	loginUser (string name, string pass);
	bool	hasUserRole (long userId, EUserRole role);
   }

  public class Module
   {
    private Module ()	{ }

	public static IModule	create (ICreator biCreator, Database.IFactory db, Database.ILangTranslation lng)
				{
				 Database.ITable<IUser, EUserField> userTable = createUserTable (db);
				 Database.ITable<IUserRole, EUserRoleField> userRoleTable = createUserRoleTable (biCreator, db, userTable);
				 return (new ModuleImp (lng, userTable, userRoleTable, createKood2KontrTable (biCreator, db, userRoleTable)));
				}

    private static string	getFldName (EUserField fld)
				{
				 switch (fld)
				  {
				   case EUserField.UF_Id:		return ("Id");
				   case EUserField.UF_UserName:	return ("UserName");
				   case EUserField.UF_Name:		return ("FullName");
				   case EUserField.UF_PassWord:	return ("PassWord");
				   case EUserField.UF_Valid:	return ("UserValid");
				   default: throw new System.SystemException ("Unknow field of User.User table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IUser, EUserField>	createUserTable (Database.IFactory db)
				{
				 Database.ITableCreator tc = db.tableCreator ("USR_User");
				 tc.addPrimaryField (getFldName (EUserField.UF_Id));
				 tc.addTextField (getFldName (EUserField.UF_UserName), 20, role: Database.EFieldRole.FR_Code);
				 tc.addTextField (getFldName (EUserField.UF_Name), 50, role: Database.EFieldRole.FR_Desc);
				 tc.addTextField (getFldName (EUserField.UF_PassWord), 20);
				 tc.addBoolField (getFldName (EUserField.UF_Valid));

				 UserTypeConv conv = new UserTypeConv ();
				 Database.ITable<IUser, EUserField> tb = tc.createTable<IUser, EUserField> (conv, getFldName);
				 conv.table = tb;
				 return (tb);
				}

	private class UserTypeConv : Database.ITypeConverter<IUser>
	 {
	  public UserTypeConv ()	{ }

	  public Database.ITable<IUser>	table	{ private get; set; }

	  public IUser	nullValue	{ get { return (null); } }

	  public IUser	create4Rec (Database.IRecord rec)	{ IUser usr = new User (rec); loadData (usr); return (usr); }
	  
	  public void	loadData (IUser toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id       = fromRec.val (table, getFldName (EUserField.UF_Id)).bg.val;
				 toObj.userName = fromRec.val (table, getFldName (EUserField.UF_UserName)).txt.val;
				 toObj.name     = fromRec.val (table, getFldName (EUserField.UF_Name)).txt.val;
				 toObj.passWord = fromRec.val (table, getFldName (EUserField.UF_PassWord)).txt.val;
				 toObj.valid    = fromRec.val (table, getFldName (EUserField.UF_Valid)).bl.val;
				}
	  
	  public Database.IRecord	saveData (IUser fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EUserField.UF_Id)).bg.val        = fromObj.id;
				 toRec.val (table, getFldName (EUserField.UF_UserName)).txt.val = fromObj.userName;
				 toRec.val (table, getFldName (EUserField.UF_Name)).txt.val     = fromObj.name;
				 toRec.val (table, getFldName (EUserField.UF_PassWord)).txt.val = fromObj.passWord;
				 toRec.val (table, getFldName (EUserField.UF_Valid)).bl.val     = fromObj.valid;
				 
				 return (toRec);
				}

	  private class User : IUser
	   {
        public User (Database.IRecord rec)	{ record = rec; }

		public Database.IRecord	record	{ get; private set; }

		public long		id			{ get; set; }
	    public string	userName	{ get; set; }
	    public string	name		{ get; set; }
	    public string	passWord	{ get; set; }
	    public bool		valid		{ get; set; }
	   }
	 }

    private static string	getFldName (EUserRoleField fld)
				{
				 switch (fld)
				  {
				   case EUserRoleField.URF_Id:			   return ("Id");
				   case EUserRoleField.URF_UserId:		   return ("UserId");
				   case EUserRoleField.URF_Type:		   return ("RoleType");
				   case EUserRoleField.URF_MySupervisorId: return ("SupervisorId");
				   default: throw new System.SystemException ("Unknow field of User.UserRole table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IUserRole, EUserRoleField>	createUserRoleTable (ICreator biCreator, Database.IFactory db, Database.ITable<IUser> userTable)
				{
				 Database.ITableCreator tc = db.tableCreator ("USR_UserRole");
				 tc.addPrimaryField (getFldName (EUserRoleField.URF_Id));
				 tc.addReferenceField (getFldName (EUserRoleField.URF_UserId), userTable, getFldName (EUserField.UF_Id));
				 tc.addOptionField (getFldName (EUserRoleField.URF_Type),
				                  new Database.IOptionValue [7] { tc.createOptionValue ((short) EUserRole.UR_Koordinator, "koordinator"), tc.createOptionValue ((short) EUserRole.UR_Kontrolor, "kontrolor"),
								                                 tc.createOptionValue ((short) EUserRole.UR_Supervisor, "supervisor"), tc.createOptionValue ((short) EUserRole.UR_Anotator, "anotatot"),
																 tc.createOptionValue ((short) EUserRole.UR_AdjSupervisor, "adjSupervisor"), tc.createOptionValue ((short) EUserRole.UR_Adjudicator, "adjudikator"),
																 tc.createOptionValue ((short) EUserRole.UR_AutoChecker, "autoChecker") });
				 Database.IReferenceFiller refFil = tc.addReferenceField (getFldName (EUserRoleField.URF_MySupervisorId),
				                                                        joinCond: new Database.IJoinCond [1] { tc.createJoinFldCond (getFldName (EUserRoleField.URF_MySupervisorId), getFldName (EUserRoleField.URF_Id)) },
																		lookUpCond: new Database.IJoinCond [1] { tc.createJoinValCond (getFldName (EUserRoleField.URF_Type), (short) EUserRole.UR_Supervisor) });
				 
				 UserRoleTypeConv conv = new UserRoleTypeConv (biCreator, userTable);
				 Database.ITable<IUserRole, EUserRoleField> tb = tc.createTable<IUserRole, EUserRoleField> (conv, getFldName);
				 conv.table = tb;
				 refFil.setReferenceTable (tb);
				 return (tb);
				}

	private class UserRoleTypeConv : Database.ITypeConverter<IUserRole>
	 {
	  public UserRoleTypeConv (ICreator biCre, Database.ITable<IUser> usrTb)	{ biCreator = biCre; userTable = usrTb; }

	  private ICreator					biCreator	{ get; set; }
	  private Database.ITable<IUser>	userTable	{ get; set; }

	  public Database.ITable<IUserRole>	table	{ private get; set; }

	  public IUserRole	nullValue	{ get { return (null); } }

	  public IUserRole	create4Rec (Database.IRecord rec)	{ IUserRole usRole = new UserRole (biCreator, userTable, table, rec); loadData (usRole); return (usRole); }

	  public void	loadData (IUserRole toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id   = fromRec.val (table, getFldName (EUserRoleField.URF_Id)).bg.val;
				 toObj.user.loadDta (table, fromRec, getFldName (EUserRoleField.URF_UserId));
				 toObj.type = (EUserRole) (fromRec.val (table, getFldName (EUserRoleField.URF_Type)).sh.val);

				 switch (toObj.type)
				  {
				   case EUserRole.UR_Anotator: toObj.anotator.mySupervisor.loadDta (table, fromRec, getFldName (EUserRoleField.URF_MySupervisorId)); break;
				  }
				}

	  public Database.IRecord	saveData (IUserRole fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EUserRoleField.URF_Id)).bg.val     = fromObj.id;
				 toRec.val (table, getFldName (EUserRoleField.URF_UserId)).bg.val = fromObj.user.id;
				 toRec.val (table, getFldName (EUserRoleField.URF_Type)).sh.val   = (short) (fromObj.type);

				 switch (fromObj.type)
				  {
				   case EUserRole.UR_Anotator: toRec.val (table, getFldName (EUserRoleField.URF_MySupervisorId)).bg.val = fromObj.anotator.mySupervisor.id; break;
				  }

				 return (toRec);
				}

	  private class UserRole : IUserRole, IUserRoleAnotator		// IUserRoleKoordinator, IUserRoleKontrolor, IUserRoleSupervisor
	   {
        public UserRole (ICreator biCreator, Database.ITable<IUser> userTable, Database.ITable<IUserRole> userRoleTable, Database.IRecord rec)
					{ record = rec; m_UserRef = biCreator.reference<IUser> (userTable); m_MySupervisorRef = biCreator.reference<IUserRole> (userRoleTable); }

		public Database.IRecord	record	{ get; private set; }

        public long					id		{ get; set; }
	    public IReference<IUser>	user	{ get { return (m_UserRef); } }	private IReference<IUser>	m_UserRef;
	    public EUserRole			type	{ get; set; }

	    //public IUserRoleKoordinator	koordinator	{ get { return ((type == EUserRole.UR_Koordinator)? this: null); } }
	    //public IUserRoleKontrolor	kontrolor	{ get { return ((type == EUserRole.UR_Kontrolor)? this: null); } }
	    //public IUserRoleSupervisor	supervisor	{ get { return ((type == EUserRole.UR_Supervisor)? this: null); } }
	    public IUserRoleAnotator	anotator	{ get { return ((type == EUserRole.UR_Anotator)? this: null); } }

		public IReference<IUserRole>	mySupervisor	{ get { return (m_MySupervisorRef); } }	private IReference<IUserRole>	m_MySupervisorRef;
	   }
	 }
    
	private static string	getFldName (EKood2KontrField fld)
				{
				 switch (fld)
				  {
				   case EKood2KontrField.K2KF_Id:      return ("Id");
				   case EKood2KontrField.K2KF_KoordId: return ("KoordinatorId");
				   case EKood2KontrField.K2KF_KontrId: return ("KontrolorId");
				   default: throw new System.SystemException ("Unknow field of User.Koordinator2Kontrolor table: " + ((int) fld).ToString ());
				  }
				}

	private static Database.ITable<IKoordinator2Kontrolor, EKood2KontrField>	createKood2KontrTable (ICreator biCreator, Database.IFactory db, Database.ITable<IUserRole> userRoleTable)
				{
				 Database.ITableCreator tc = db.tableCreator ("USR_Koord2Kontr");
				 tc.addPrimaryField (getFldName (EKood2KontrField.K2KF_Id));
				 tc.addReferenceField (getFldName (EKood2KontrField.K2KF_KoordId), userRoleTable, getFldName (EUserRoleField.URF_Id),
												 lookUpCond: new Database.IJoinCond [1] { tc.createJoinValCond (getFldName (EUserRoleField.URF_Type), (short) EUserRole.UR_Koordinator) });
				 tc.addReferenceField (getFldName (EKood2KontrField.K2KF_KontrId), userRoleTable, getFldName (EUserRoleField.URF_Id),
												 lookUpCond: new Database.IJoinCond [1] { tc.createJoinValCond (getFldName (EUserRoleField.URF_Type), (short) EUserRole.UR_Kontrolor) });
				 
				 Koordinator2KontrolorTypeConv conv = new Koordinator2KontrolorTypeConv (biCreator, userRoleTable);
				 Database.ITable<IKoordinator2Kontrolor, EKood2KontrField> tb = tc.createTable<IKoordinator2Kontrolor, EKood2KontrField> (conv, getFldName);
				 conv.table = tb;
				 return (tb);
				}

	private class Koordinator2KontrolorTypeConv : Database.ITypeConverter<IKoordinator2Kontrolor>
	 {
	  public Koordinator2KontrolorTypeConv (ICreator biCre, Database.ITable<IUserRole> usrRlTb)	{ biCreator = biCre; userRoleTable = usrRlTb; }

	  private ICreator						biCreator		{ get; set; }
	  private Database.ITable<IUserRole>	userRoleTable	{ get; set; }

	  public Database.ITable<IKoordinator2Kontrolor>	table	{ private get; set; }

	  public IKoordinator2Kontrolor	nullValue	{ get { return (null); } }

	  public IKoordinator2Kontrolor	create4Rec (Database.IRecord rec)	{ IKoordinator2Kontrolor k2k = new Koordinator2Kontrolor (biCreator, userRoleTable, rec); loadData (k2k); return (k2k); }

	  public void	loadData (IKoordinator2Kontrolor toObj, Database.IRecord fromRec = null)
				{
				 if (fromRec == null) fromRec = toObj.record;

				 toObj.id   = fromRec.val (table, getFldName (EKood2KontrField.K2KF_Id)).bg.val;
				 toObj.koordinator.loadDta (table, fromRec, getFldName (EKood2KontrField.K2KF_KoordId));
				 toObj.kontrolor.loadDta (table, fromRec, getFldName (EKood2KontrField.K2KF_KontrId));
				}

	  public Database.IRecord	saveData (IKoordinator2Kontrolor fromObj)
				{
				 Database.IRecord toRec = fromObj.record;

				 toRec.val (table, getFldName (EKood2KontrField.K2KF_Id)).bg.val      = fromObj.id;
				 toRec.val (table, getFldName (EKood2KontrField.K2KF_KoordId)).bg.val = fromObj.koordinator.id;
				 toRec.val (table, getFldName (EKood2KontrField.K2KF_KontrId)).bg.val = fromObj.kontrolor.id;

				 return (toRec);
				}

	  private class Koordinator2Kontrolor : IKoordinator2Kontrolor
	   {
        public Koordinator2Kontrolor (ICreator biCreator, Database.ITable<IUserRole> userRoleTable, Database.IRecord rec)
					{ record = rec; m_Koordinator = biCreator.reference<IUserRole> (userRoleTable); m_Kontrolor = biCreator.reference<IUserRole> (userRoleTable); }

		public Database.IRecord	record	{ get; private set; }

        public long						id			{ get; set; }
        public IReference<IUserRole>	koordinator	{ get { return (m_Koordinator); } }	private IReference<IUserRole>	m_Koordinator;
        public IReference<IUserRole>	kontrolor	{ get { return (m_Kontrolor); } }	private IReference<IUserRole>	m_Kontrolor;
	   }
	 }

	private class ModuleImp : IModule
	 {
	  public ModuleImp (Database.ILangTranslation lng, Database.ITable<IUser, EUserField> usrTb, Database.ITable<IUserRole, EUserRoleField> usrRlTb, Database.ITable<IKoordinator2Kontrolor, EKood2KontrField> k2kTb)
				{ m_Lang = lng; userTable = usrTb; userRoleTable = usrRlTb; koord2kontrTable = k2kTb; }

	  private Database.ILangTranslation	language	{ get { return (m_Lang); } }	private Database.ILangTranslation	m_Lang;

	  public string	getFieldName (EUserField fld)		{ return (getFldName (fld)); }
      public string	getFieldName (EUserRoleField fld)	{ return (getFldName (fld)); }
      public string	getFieldName (EKood2KontrField fld)	{ return (getFldName (fld)); }

      public Database.ITable<IUser, EUserField>							userTable			{ get; private set; }
	  public Database.ITable<IUserRole, EUserRoleField>					userRoleTable		{ get; private set; }
	  public Database.ITable<IKoordinator2Kontrolor, EKood2KontrField>	koord2kontrTable	{ get; private set; }

	  public IUser	loginUser (string name, string pass)
				{
				 if (name == null) return (null);

				 using (Database.IRecordSet<IUser> rs = userTable.findObj (getFieldName (EUserField.UF_UserName), name))
				  while (rs.moveNext ())
				   if (rs.currObj.valid)
				    if (language.isNull (rs.currObj.passWord)) return ((pass == null)? rs.currObj: null);
					else if (pass != null && pass == rs.currObj.passWord) return (rs.currObj);
					else return (null);

				 return (null);
				}
	 
	  public bool	hasUserRole (long userId, EUserRole role)
				{
				 Database.ICondBuilder cb = userRoleTable.createCondBuilder ();

				 cb.addValCond (userRoleTable.getFullFieldName (EUserRoleField.URF_UserId), userId);
				 cb.addValCond (userRoleTable.getFullFieldName (EUserRoleField.URF_Type), (short) role);

				 using (Database.IRecordSet<IUserRole> rs = userRoleTable.findObj (cb.create ())) return (rs.moveNext ());
				}
	 }
   }
 }
