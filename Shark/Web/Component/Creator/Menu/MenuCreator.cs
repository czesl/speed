﻿using System.Collections.Generic;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator.Menu
{
	/// <summary>
	/// Multiline menu creator
	/// </summary>
	internal class MenuCreator : MultiLevelMenuFiller, IMenuCreator
	{
		/// <summary>
		/// Create new creator and set his parameter
		/// </summary>
		/// <param name="subcreator"></param>
		/// <param name="control"></param>
		internal MenuCreator(ICreator subcreator, IControl control) : base(subcreator: subcreator, control: control, container: null, level: 0)
		{
		}

		#region Implementation of IMenuCreator

		/// <summary>
		/// Create new multiline menu
		/// </summary>
		/// <returns></returns>
		public new IMenu create()
		{
			Dictionary<int, IMenuItem> items = new Dictionary<int, IMenuItem>();

			foreach (KeyValuePair<int, IMenuItemCreator> kvp in children)
			{
				items[kvp.Key] = kvp.Value.create();
			}

			return new Menu(creator: subCreator, control: Control, children: items);
		}

		#endregion
	}
}