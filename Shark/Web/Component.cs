﻿
namespace Shark.Web.Component
 {
  public enum EControlAlignment { CA_Left = 1, CA_Top = 1, CA_Middle = 2, CA_Right = 3, CA_Bottom = 3, CA_Both = 4 }

  public interface ICreator : ISelectValueCreator
   {
    IComponent				nbsp ();
	IPage					page (IComponent comp, string title = null);
	IComponent				htmlComp (string htmlCode);
	IComponent				panel (IComponent comp, EControlAlignment horzAlign = EControlAlignment.CA_Both, EControlAlignment vertAlign = EControlAlignment.CA_Both);
    ILayoutCreator			layOut (int xSize = 1, int ySize = 1);
	IMenuCreator			mainMenu ();
	IMenuComponentCreator	menuFrmComponent ();
	IFormCreator			form (int cols);
	ITableCreator			table ();
	IListDetailCreator		listDetail ();
	IList3DetailCreator		list3Detail ();
   }
  
  public interface IParent
   {
    ICreator	subCreator	{ get; }
   }
  
  public delegate void	DComponentInit (IComponent cmp);

  public interface IComponent : IParent
   {
	void	initWebData ();
	void	loadWebData ();

	event DComponentInit	onInitWebData;
	event DComponentInit	onLoadWebData;

	void	generate ();
   }

  public interface IPage : IComponent
   {
    string	title	{ get; set; }
   }

  public interface ILayoutCreator : IParent
   {
    int	xSize	{ get; }
    int	ySize	{ get; }

	void	addComponent (IComponent cmp, int xPos = -1, int yPos = -1);
	
	IComponent	create ();
   }

  
  public interface ILineMenu : IComponent
   {
    int			currValue	{ get; set; }		// value od aktualne vybrane polozky menu - na teto urovni (zadne podurovne se zde neuvazuji)
	IComponent	currComp	{ get; }			// componenta od aktualne vybrane polozky menu. Pokud je pod aktualni polozkou menu dalsi menu, pak muze vracet hodnotu componenty subMenu, nebo i null
   }
  
  public interface IMenu : ILineMenu
   {
	int			subMenuListValue ();			// value od aktualne vybrane listove polozky menu - tj. na nejnizsi urovni menu, kde je hodnotou jiz componenta
	IComponent	subMenuListComp ();				// componenta od aktualne vybrane listove polozky menu - tj. na nejnizsi urovni menu

	void	setMenuListValue (int value);		// nastaveni aktualni listove polozky menu. Tj. value se vztahuje k necemu, co muze byt skryto v hloubce struktury menu
   }
  
  public delegate bool			DMenuPermision (int value);
  public delegate IComponent	DMenuCreator (int value);

  public interface IMenuFiller : IParent
   {
	void	addMenuItem (int value, IComponent comp, string label, string desc = null, DMenuPermision parm = null);
	void	addMenuItem (int value, DMenuCreator compCre, string label, string desc = null, DMenuPermision parm = null);
   }

  public interface IMultiLevelMenuFiller : IMenuFiller
   { IMultiLevelMenuFiller	addSubMenu (int value, string label, string desc = null, DMenuPermision parm = null); }
  
  public interface IMenuCreator : IMultiLevelMenuFiller
   { IMenu	create (); }

  public interface IMenuComponentCreator : IMenuFiller
   {
	IMenu	create ();
   }


  public interface ISelectValue
   {
    long	val		{ get; }
	string	label	{ get; }
	string	desc	{ get; }
   }

  public interface ISelectValueCreator
   {
    ISelectValue	selectValue (long val, string label, string desc = null);
   }

  
  public interface IForm : IComponent, ISelectValueCreator
   {
    string	this	[int ctrlId]	{ get; set; }
	
	void	setSelectValues (int ctrlId, ISelectValue[] vals);
   }

  public delegate void	DFormEvent (IForm frm);

  public interface IFormFiller : IParent
   {
    int	cols		{ get; }
    int	filledCols	{ get; }

	bool	showCaption	{ get; set; }
	string	caption		{ get; set; }
	
	void	addNbsp (int nCols = 1);
	void	finishLine ();

	void	addLabel (string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left, bool showAsValue = false);
	void	addTextField (int id, int nCols = 1, bool readOnly = false, EControlAlignment horzAlign = EControlAlignment.CA_Left, EControlAlignment txtAlign = EControlAlignment.CA_Left);
	void	addSelect (int id, ISelectValue[] vals = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left, bool submitOnChange = false);
	void	addTextArray (int id, int width = 80, int nRows = 5, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Middle);
	void	addButton (int id, DFormEvent action, string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Middle);
   }

  public interface IFormCreator : IFormFiller
   {
    IForm	create ();
   }

  
  public interface IRowGenerate
   {
    int	cols		{ get; }		// definovany pocet sloupcu
    int	filledCols	{ get; }		// aktualni pocet vyplnenych sloupcu na HTML radku

	void	addField (string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left, bool isMultiLines = false);
									// generuje jedno pole tabulky. Pokud by pole presahlo pocet definovany sloupcu, je vyvolana vyjimka
									// Pokud je isMultiLines == true, pak se label patricne naformatuje aby CR zalamovalo skutecne na dalsi radek

	void	startNewLine ();		// je volano v prubehu generovani, pokud Database.record se ma generovat na vice HTML radku. Po volani se opet nastavi aktualni HTML sloupec na pocatek

	void	finishLine ();			// vola se po zkonceni generovani Database.record
   }

  public interface ITable : IComponent
   {
    string	currRowId	{ get; }				// vraci aktualni id radku. Na pocatku neni aktualni zadny radek

	IRowGenerate	fillLine (string rowId);	// generuje jeden datovy radek (tj. vse pro jeden databasovy record)
   }

  public delegate void	DTableAction (ITable tbl);
  
  public interface ITableFiller : IParent
   {
	DTableAction	actionOnRowClick	{ get; set; }		// udalost, ktera bude vyvolana, kdyz uzivatel v prohlizeci klepne (onclick) na radek. Pokud je null, pak onclick v HTML neni definovane
	DTableAction	generate			{ get; set; }		// metoda, kterou tabulka vygeneruje, kdyz chce byt generovana
	
	void	addColumn (string label, string desc = null, int nCols = 1, EControlAlignment horzAlign = EControlAlignment.CA_Left);	// definice sloupce - vcetne zahlavi. Jedno zahlavi muze prekryvat vice datovych sloupcu
   }

  public interface ITableCreator : ITableFiller
   {
    ITable	create ();
   }

  public interface IListDetail : IComponent
   {
    string	currRowId		{ get; }
	bool	isListActive	{ get; }

    ITable		listTable	{ get; }
	ILineMenu	menu		{ get; }

	void	showDetail (string rowId);
   }

  public interface IListDetailFiller : IParent
   {
    ITableFiller	listCreator	{ get; }
	IMenuFiller		menuCreator	{ get; }

	IComponent	detail	{ get; set; }
   }

  public interface IListDetailCreator : IListDetailFiller
   {
	IListDetail	create ();
   }


  public interface IList3Detail : IListDetail
   {
    ILineMenu	actionMenu	{ get; }
	ILineMenu	infoMenu	{ get; }
   }

  public interface IList3DetailFiller : IListDetailFiller
   {
	IMenuFiller	actionMenuCreator	{ get; }
	IMenuFiller	infoMenuCreator		{ get; }
   }

  public interface IList3DetailCreator : IList3DetailFiller
   {
	IList3Detail	create ();
   }
 }
