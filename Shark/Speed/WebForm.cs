﻿
namespace Shark.Speed.WebForm
 {
  public interface ISession
   {
    IUserInfo	currUser	{ get; }
	BI.IBi		requestBi	{ get; }
   }
  
  public interface IUserInfo
   {
    long	id			{ get; }
	string	userName	{ get; }
	string	fullName	{ get; }

	bool	isSupervisor	{ get; }
	bool	isAnotator		{ get; }
   }
 }
