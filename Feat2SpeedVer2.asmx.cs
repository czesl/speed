﻿using System.Web.Services;

namespace Shark.Speed.WebService.Feat.Feat2SpeedVer2
 {
	/// <summary>
	/// Summary description for Feat2SpeedVer2
	/// </summary>
  [WebService (Name = "Feat2SpeedWebServiceVer2", Description = "Feat to Speed communication web service version 2.0", Namespace = "http://speed2featVer2.ufal.jager.cz/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]

  public class Feat2SpeedVer2 : System.Web.Services.WebService
   {
	private Database.SQL.IEngine	createSQLEngine ()
				{
				 string conString = System.Configuration.ConfigurationManager.AppSettings ["ConnectionString"];
				 return (BI.Bi.createMsSQLDatabaseEngine (conString));
				}
	
	[WebMethod (Description = "Opens Feat 2 Speed communication, creates session id")]
	public CreateSessionWebAnswer	createSession (string userName, string passWd)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new CreateSessionWebAnswer (call.createSession (userName: userName, passWd: passWd, callUrl: Context.Request.UserHostName)));
				  }
				}

	[WebMethod (Description = "Closes Feat 2 Speed communication, closes session id")]
	public WebAnswer	closeSession (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.closeSession (sessionId: sessionId, callUrl: Context.Request.UserHostName)));
				  }
				} 

	[WebMethod (Description = "Reads user inbox file names")]
	public CheckUserInboxAnswer	checkInbox (string sessionId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new CheckUserInboxAnswer (call.checkUserInbox (sessionId: sessionId, callUrl: Context.Request.UserHostName, useOnlyAnotace: false)));
				  }
				}
	
	[WebMethod (Description = "Reads user inbox object identifications")]
	public UserInboxHeaderWebAnswer	getUserInboxHeader (string sessionId, bool is2ReadAnyFile)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new UserInboxHeaderWebAnswer (call.getUserInboxHeader (sessionId: sessionId, callUrl: Context.Request.UserHostName, is2ReadAnyFile: is2ReadAnyFile, useOnlyAnotace: false)));
				  }
				}
	
	[WebMethod (Description = "Reads one user inbox object with indetification taskId")]
	public WebPeekSynchronizedFileAnswer	peekFile (string sessionId, string taskId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebPeekSynchronizedFileAnswer (call.peekSyncFile (sessionId: sessionId, callUrl: Context.Request.UserHostName, taskId: taskId)));
				  }
				}

	[WebMethod (Description = "Tells to speed that Feat has correctly read input file")]
	public WebAnswer	fileRead (string sessionId, string taskId)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.fileRead (sessionId: sessionId, callUrl: Context.Request.UserHostName, taskId: taskId)));
				  }
				}

	[WebMethod (Description = "Feat saves outbox file")]
	public WebAnswer	saveFile (string sessionId, WebSynchronizedFile file)
				{
				 using (Database.SQL.IEngine msEng = createSQLEngine ())
				  {
				   ICall call = Call.createCall (BI.Bi.create (msEng));
				   return (new WebAnswer (call.saveFile (sessionId: sessionId, callUrl: Context.Request.UserHostName, file: ((file != null)? file.create (): null))));
				  }
				}
   }

  public interface IWebAnswer
   {
    int		status	{ get; set; }
	string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class WebAnswer : IWebAnswer
   {
    public WebAnswer ()	{ }
	public WebAnswer (IAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				}
	
    public int		status	{ get; set; }
	public string	errMsg	{ get; set; }
   }

  [System.Serializable]
  public class CreateSessionWebAnswer : IWebAnswer
   {
    public CreateSessionWebAnswer ()	{ }
    public CreateSessionWebAnswer (ICreateSessionAnswer ans)
				{
				 status    = (int) ans.isOk;
				 errMsg    = ans.errMsg;
				 sessionId = ans.sessionId;
				}
	
	public int		status		{ get; set; }
	public string	errMsg		{ get; set; }
    public string	sessionId	{ get; set; }
   }

  [System.Serializable]
  public class CheckUserInboxAnswer : IWebAnswer
   {
    public CheckUserInboxAnswer ()	{ }
    public CheckUserInboxAnswer (ICheckUserInboxAnswer ans)
				{
				 status            = (int) ans.isOk;
				 errMsg            = ans.errMsg;
				 inboxFileNameList = ans.inboxFileNameList;
				}
    
	public int		status				{ get; set; }
	public string	errMsg				{ get; set; }
    public string[]	inboxFileNameList	{ get; set; }
   }

  [System.Serializable]
  public class UserInboxHeaderWebAnswer : IWebAnswer
   {
    public UserInboxHeaderWebAnswer ()	{ }
    public UserInboxHeaderWebAnswer (IUserInboxHeaderAnswer ans)
				{
				 status          = (int) ans.isOk;
				 errMsg          = ans.errMsg;
				 inboxTaskIdList = ans.inboxTaskIdList;
				}
    
	public int		status			{ get; set; }
	public string	errMsg			{ get; set; }
    public string[]	inboxTaskIdList	{ get; set; }
   }

  [System.Serializable]
  public class WebPeekSynchronizedFileAnswer : IWebAnswer
   {
	public WebPeekSynchronizedFileAnswer ()	{ }
	public WebPeekSynchronizedFileAnswer (IPeekSynchronizedFileAnswer answ)
				{
				 status	  = (int) answ.isOk;
				 errMsg   = answ.errMsg;
				 syncFile = ((answ.syncFile != null)? new WebSynchronizedFile (answ.syncFile): null);
				}
	
	public int					status		{ get; set; }
	public string				errMsg		{ get; set; }
	public WebSynchronizedFile	syncFile	{ get; set; }
   }
  
  [System.Serializable]
  public class WebSynchronizedFile
   {
    public WebSynchronizedFile ()	{ }
    public WebSynchronizedFile (ISynchronizedFile fl)
				{
				 name	 = fl.name;
				 head	 = WebSynchronizedFileContent.create (fl.head);
				 folders = WebSynchronizedFileFolder.create (fl.folders);
				}
	public ISynchronizedFile	create ()	{ return (new SynchronizedFile (this)); }

	public string						name	{ get; set; }
	public WebSynchronizedFileContent[]	head	{ get; set; }
	public WebSynchronizedFileFolder[]	folders	{ get; set; }

	public class SynchronizedFile : ISynchronizedFile
	 {
      public SynchronizedFile (WebSynchronizedFile fl)
				{
				 name	 = fl.name;
				 head	 = WebSynchronizedFileContent.create (fl.head);
				 folders = WebSynchronizedFileFolder.create (fl.folders);
				}
	  
	  public string						name	{ get; set; }
	  public ISynchronizedFileContent[]	head	{ get; set; }
	  public ISynchronizedFileFolder[]	folders	{ get; set; }
	 }
   }
  
  [System.Serializable]
  public class WebSynchronizedFileFolder
   {
    public WebSynchronizedFileFolder ()	{ }
	public WebSynchronizedFileFolder (ISynchronizedFileFolder cont)
				{
				 isReadOnly	  = cont.isReadOnly;
				 isWorkOnFile = cont.isWorkOnFile;
				 author		  = cont.author;
				 content	  = WebSynchronizedFileContent.create (cont.content);
				}
	public ISynchronizedFileFolder	create ()	{ return (new SynchronizedFileFolder (this)); }

	public static WebSynchronizedFileFolder[]	create (ISynchronizedFileFolder[] arr)
				{
				 if (arr == null) return (null);

				 WebSynchronizedFileFolder[] webArr = new WebSynchronizedFileFolder [arr.Length];
				 for (int i = 0; i < arr.Length; i++) webArr [i] = ((arr [i] != null)? new WebSynchronizedFileFolder (arr [i]): null);
				 
				 return (webArr); 
				}

	public static ISynchronizedFileFolder[]	create (WebSynchronizedFileFolder[] arr)
				{
				 if (arr == null) return (null);

				 ISynchronizedFileFolder[] webArr = new ISynchronizedFileFolder [arr.Length];
				 for (int i = 0; i < arr.Length; i++) webArr [i] = ((arr [i] != null)? arr [i].create (): null);
				 
				 return (webArr); 
				}
    
	public bool							isReadOnly		{ get; set; }
	public bool							isWorkOnFile	{ get; set; }
	public string						author			{ get; set; }
	public WebSynchronizedFileContent[]	content			{ get; set; }

	public class SynchronizedFileFolder : ISynchronizedFileFolder
	 {
      public SynchronizedFileFolder (WebSynchronizedFileFolder cont)
				{
				 isReadOnly	  = cont.isReadOnly;
				 isWorkOnFile = cont.isWorkOnFile;
				 author		  = cont.author;
				 content	  = WebSynchronizedFileContent.create (cont.content);
				}
	  
	  public bool						isReadOnly		{ get; set; }
	  public bool						isWorkOnFile	{ get; set; }
	  public string						author			{ get; set; }
	  public ISynchronizedFileContent[]	content			{ get; set; }
	 }
   }
  
  [System.Serializable]
  public class WebSynchronizedFileContent : ISynchronizedFileContent
   {
    public WebSynchronizedFileContent ()	{ }
	public WebSynchronizedFileContent (ISynchronizedFileContent cont)
				{
				 extension	   = cont.extension;
				 isTextBased   = cont.isTextBased;
				 textContent   = cont.textContent;
				 isBinary	   = cont.isBinary;
				 binaryContent = cont.binaryContent;
				}
	
	public static WebSynchronizedFileContent[]	create (ISynchronizedFileContent[] arr)
				{
				 if (arr == null) return (null);

				 WebSynchronizedFileContent[] webArr = new WebSynchronizedFileContent [arr.Length];
				 for (int i = 0; i < arr.Length; i++) webArr [i] = ((arr [i] != null)? new WebSynchronizedFileContent (arr [i]): null);
				 
				 return (webArr); 
				}
    
	public static ISynchronizedFileContent[]	create (WebSynchronizedFileContent[] arr)	{ return (arr); }

	public string	extension		{ get; set; }
	public bool		isTextBased		{ get; set; }
	public string	textContent		{ get; set; }
	public bool		isBinary		{ get; set; }
	public byte[]	binaryContent	{ get; set; }
   }
 }
