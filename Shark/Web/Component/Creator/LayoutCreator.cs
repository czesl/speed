﻿using System;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Creator for creating layout component
	/// </summary>
	internal class LayoutCreator : ILayoutCreator
	{
		#region private fields

		/// <summary>
		/// Index of last used row
		/// </summary>
		private int? _lastUsedRow = null;

		/// <summary>
		/// Index of last used column
		/// </summary>
		private int? _lastUsedColumn = null;

		/// <summary>
		/// Gets components added into layout
		/// </summary>
		private readonly IComponent[][] _layoutComponents;

		/// <summary>
		/// Control for creating subcontrols for components
		/// </summary>
		private readonly IControl _control;
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		/// <param name="xSize">Number of cells in row</param>
		/// <param name="ySize">Number of cells in column</param>
		/// <exception cref="ArgumentException">This exception is thrown if there are specified invalid dimensions for layout</exception>
		public LayoutCreator(ICreator creator, IControl control, int xSize = 1, int ySize = 1)
		{
			if(xSize < 1 || ySize < 1)
			{
				throw new ArgumentException("One of dimensions arguments for Layout had invalid values (value < 1)");
			}

			int x;
			
			subCreator = creator;
			_control = control;
			_layoutComponents = new IComponent[ySize][];

			for(x = 0; x < ySize; x++)
			{
				_layoutComponents[x] = new IComponent[xSize];
			}
		}
		#endregion


		#region Implementation of IParent

		/// <summary>
		/// Gets creater used for creating subcomponents
		/// </summary>
		public ICreator subCreator
		{
			get;
			private set;
		}
		#endregion


		#region Implementation of ILayoutCreator

		/// <summary>
		/// Gets number of columns in layout
		/// </summary>
		public int xSize
		{
			get
			{
				return _layoutComponents[0].Length;
			}
		}

		/// <summary>
		/// Gets number of rows in layout
		/// </summary>
		public int ySize
		{
			get
			{
				return _layoutComponents.Length;
			}
		}

		/// <summary>
		/// Adds component to layout, if position is occupied or outside of layout borders exception is thrown
		/// </summary>
		/// <param name="cmp">Object of component to be added at specific position in layout</param>
		/// <param name="xPos">Horizontal position in layout (index of column)</param>
		/// <param name="yPos">Vertical position in layout (index of row)</param>
		/// <exception cref="ArgumentException">Exception is thrown when someone is trying to put component on existing component or outside of layout</exception>
		public void addComponent(IComponent cmp, int xPos = -1, int yPos = -1)
		{

			if(xPos < -1 || yPos < -1)
			{
				throw new ArgumentException("Invalid position of added component into layout. X:" + xPos + " Y:" + yPos);
			}
	
			// check new x, y position
			int x, y;

			if (xPos == -1 && yPos == -1)	// 0 column of next row
			{
				x = (_lastUsedColumn ?? -1) +1;
				y = (_lastUsedRow ?? 0);

				if (x == xSize)
				{
					x = 0;
					y++;
				}

			}
			else
			{
				if (xPos == -1)	// next column
				{
					x = (_lastUsedColumn+1 ?? 0);
				}
				else
				{
					x = xPos;
				}

				if (yPos == -1)	// next row
				{
					y = (_lastUsedRow+1 ?? 0);
				}
				else
				{
					y = yPos;
				}
			}

			// check avaliability of position
			if (x< 0 || x >= xSize || y < 0 || y >= ySize)
			{
				throw new ArgumentException("Invalid position of added component into layout, outside of layout borders. X:" + x + " Y:" + y);
			}

			// check occupation of position
			if ( _layoutComponents[y][x] != null)
			{
				throw new ArgumentException("Invalid position of added component into layout, component already exists on this position. X:" + x + " Y:" + y);
			}

			_layoutComponents[y][x] = cmp;

			_lastUsedColumn = x;
			_lastUsedRow = y;
		}

		/// <summary>
		/// Creates component using provided dimensions and subcomponents
		/// </summary>
		/// <returns>Layout component</returns>
		public IComponent create()
		{
			return new Layout(subCreator, _control, _layoutComponents);
		}
		#endregion
	}
}