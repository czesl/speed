﻿using System;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Class representing Nbsp component
	/// </summary>
	internal class Nbsp : Component
	{
		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		public Nbsp(ICreator creator, IControl control) : base(creator, control)
		{
		}
		#endregion


		#region Overrides of Component

		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		/// <exception cref="NullReferenceException">Exception thrown when there is invalid <see cref="Creator"/> object assigned to this component</exception>
		public override void generate()
		{
			_control.write("&nbsp;");
		}
		#endregion
	}
}