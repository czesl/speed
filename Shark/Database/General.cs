﻿
namespace Shark.Database.General
 {
  public class Factory
   {
    private Factory ()	{ }

	public static IFactory	create (SQL.IEngine sql, ILangTranslation lang, IDateTimeZone zone)
				{ return (FactoryImp.create (sql, lang, zone)); }

	private interface IImpFactory : IFactory
	 {
	  SQL.IEngine	engine	{ get; }
	  ICulture		culture	{ get; }
	 }
	
	private interface ICulture
	 {
	  ILangTranslation	language	{ get; }
	  IDateTimeZone		timeZone	{ get; }
	 }

	private interface ICondBuilderPlus : ICondBuilder
	 { void	addCond (ICondition cond); }

	private class CondBuilder
	 {
	  private CondBuilder ()	{ }

	  public static ICondBuilderPlus	create (SQL.IEngine sql, ICulture cult)	{ return (CondBuilderImp.create (sql, cult)); }

	  private interface IBuildElement	{ ICondition	build (); }

	  private class CondBuilderImp : ICondBuilderPlus
	   {
	    private CondBuilderImp (SQL.IEngine sql, ICulture cult)	{ engine = sql; culture = cult; m_Elements = new System.Collections.Generic.List<IBuildElement> (); }
		private System.Collections.Generic.IList<IBuildElement>	m_Elements;

		public static ICondBuilderPlus	create (SQL.IEngine sql, ICulture cult)	{ return (new CondBuilderImp (sql, cult)); }

	    private SQL.IEngine	engine	{ get; set; }
	    private ICulture	culture	{ get; set; }

        public string	createTableAcr ()	{ lock (this.GetType ()) return ("Table" + culture.language.format (++ms_LastTableAcr)); }
	    private static long	ms_LastTableAcr = 0;

	    public string	fullFieldName (string tableAcr, string fieldName)	{ return (engine.fullFieldName (tableAcr, fieldName)); }

	    public void	addIsNull (string leftSide)	{ lock (m_Elements) m_Elements.Add (new IsNullElement (leftSide)); }
	    public void	addCond (ICondition cond)	{ lock (m_Elements) m_Elements.Add (new InnerCondElement (cond)); }
		public void	addCond (string leftSide, string rightSide, ECondOper oper = ECondOper.CO_Eq)		{ lock (m_Elements) m_Elements.Add (new OperElement (leftSide, oper, rightSide)); }
	    public void	addValCond (string leftSide, long vl, ECondOper oper = ECondOper.CO_Eq)				{ lock (m_Elements) m_Elements.Add (new LongCondElement (leftSide, oper, vl)); }
	    public void	addValCond (string leftSide, int vl, ECondOper oper = ECondOper.CO_Eq)				{ lock (m_Elements) m_Elements.Add (new IntCondElement (leftSide, oper, vl)); }
	    public void	addValCond (string leftSide, short vl, ECondOper oper = ECondOper.CO_Eq)			{ lock (m_Elements) m_Elements.Add (new ShortCondElement (leftSide, oper, vl)); }
	    public void	addValCond (string leftSide, bool vl, ECondOper oper = ECondOper.CO_Eq)				{ lock (m_Elements) m_Elements.Add (new BoolCondElement (leftSide, oper, vl)); }
	    public void	addValCond (string leftSide, System.DateTime vl, ECondOper oper = ECondOper.CO_Eq)	{ lock (m_Elements) m_Elements.Add (new DTCondElement (leftSide, oper, vl)); }
	    public void	addValCond (string leftSide, string vl, ECondOper oper = ECondOper.CO_Eq)			{ lock (m_Elements) m_Elements.Add (new TxtCondElement (leftSide, oper, vl)); }

	    public ICondBuilder	addNotCond ()	{ return (addGroupCond (new NotCond ())); }
	    public ICondBuilder	addAndCond ()	{ return (addGroupCond (new AndCond ())); }
	    public ICondBuilder	addOrCond ()	{ return (addGroupCond (new OrCond ())); }

	    public ICondBuilder	addExists (string tableName, string tableField, string tableAcr = null, string tableCondFld = null, string parentValueSide = null)
					{ return (addGroupCond (new ExistsCond (tableName: tableName, tableField: tableField, tableAcr: tableAcr, tableCondFld: tableCondFld, parentValueSide: parentValueSide))); }

		private ICondBuilder	addGroupCond (IGroupCond grpCnd)	{ SubCondElement sce = new SubCondElement (grpCnd, create (engine, culture)); lock (m_Elements) m_Elements.Add (sce); return (sce.builder); }
	
	    public ICondition	create ()
					{
					 lock (m_Elements)
					  {
					   int cnt = m_Elements.Count;
					 
					   if (cnt < 1) return (new Condition (null));
					   if (cnt == 1) return (m_Elements [0].build ());

					   System.Collections.Generic.List<ICondition> cndLst = new System.Collections.Generic.List<ICondition> (cnt);
					   foreach (IBuildElement el in m_Elements) cndLst.Add (el.build ());

					   return (new Condition (cndLst.ToArray ()));
					  }
					}

		private class Condition : ICondition
		 {
		  public Condition (ICondition[] conds)	{ m_CondArr = conds; }
		  private ICondition[]	m_CondArr;

		  public void fillBuilder (SQL.ICondBuilder cndBuilder)
					{ if (m_CondArr != null) for (int i = 0; i < m_CondArr.Length; i++) m_CondArr [i].fillBuilder (cndBuilder); }
		 }
	   }

	  private class IsNullElement : IBuildElement, ICondition
	   {
	    public IsNullElement (string leftSide)	{ m_LeftSide = leftSide; }
		private string	m_LeftSide;

		public ICondition	build ()									{ return (this); }
		public void			fillBuilder (SQL.ICondBuilder cndBuilder)	{ cndBuilder.addIsNull (m_LeftSide); }
	   }

	  private class InnerCondElement : IBuildElement, ICondition
	   {
	    public InnerCondElement (ICondition cnd)	{ m_Cond = cnd; }
		private ICondition	m_Cond;

		public ICondition	build ()									{ return (this); }
		public void			fillBuilder (SQL.ICondBuilder cndBuilder)	{ m_Cond.fillBuilder (cndBuilder); }
	   }

	  private abstract class AOperElement : IBuildElement, ICondition
	   {
	    protected AOperElement (string lftSd, ECondOper opr)	{ leftSide = lftSd;  oper = opr; }
		private string		leftSide	{ get; set; }
		private ECondOper	oper		{ get; set; }

		public ICondition	build ()									{ return (this); }
		public void			fillBuilder (SQL.ICondBuilder cndBuilder)	{ cndBuilder.addCond (leftSide, oper, getRightSide (cndBuilder.parBld)); }
		protected abstract string	getRightSide (SQL.IParamBuilder parBld);
	   }
	  
	  private class OperElement : AOperElement
	   {
	    public OperElement (string lftSd, ECondOper opr, string rightSide) : base (lftSd, opr)	{ m_RightSide = rightSide; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (m_RightSide); }	private string	m_RightSide;
	   }

	  private class LongCondElement : AOperElement
	   {
	    public LongCondElement (string lftSd, ECondOper opr, long val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private long	m_Val;
	   }

	  private class IntCondElement : AOperElement
	   {
	    public IntCondElement (string lftSd, ECondOper opr, int val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private int	m_Val;
	   }

	  private class ShortCondElement : AOperElement
	   {
	    public ShortCondElement (string lftSd, ECondOper opr, short val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private short	m_Val;
	   }

	  private class BoolCondElement : AOperElement
	   {
	    public BoolCondElement (string lftSd, ECondOper opr, bool val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private bool	m_Val;
	   }

	  private class DTCondElement : AOperElement
	   {
	    public DTCondElement (string lftSd, ECondOper opr, System.DateTime val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private System.DateTime	m_Val;
	   }

	  private class TxtCondElement : AOperElement
	   {
	    public TxtCondElement (string lftSd, ECondOper opr, string val) : base (lftSd, opr)	{ m_Val = val; }
		protected override string	getRightSide (SQL.IParamBuilder parBld)	{ return (parBld.add (m_Val)); }	private string	m_Val;
	   }

	  private interface IGroupCond	{ SQL.ICondBuilder	createGrp (SQL.ICondBuilder parent); }
	  
	  private class SubCondElement	: IBuildElement
	   {
	    public SubCondElement (IGroupCond grpCnd, ICondBuilder bld)	{ groupCond = grpCnd; builder = bld; }
		private IGroupCond	groupCond	{ get; set; }
		public ICondBuilder	builder		{ get; private set; }

		public ICondition	build ()	{ return (new Condition (groupCond, builder.create ())); }

		private class Condition : ICondition
		 {
		  public Condition (IGroupCond grpCond, ICondition cnd)	{ m_GroupCond = grpCond; m_Condition = cnd; }
		  private IGroupCond	m_GroupCond;
		  private ICondition	m_Condition;

		  public void	fillBuilder (SQL.ICondBuilder cndBuilder)	{ m_Condition.fillBuilder (m_GroupCond.createGrp (cndBuilder)); }
		 }
	   }

	  private class NotCond : IGroupCond { public SQL.ICondBuilder	createGrp (SQL.ICondBuilder parent)	{ return (parent.addNotCond ()); } }
	  private class AndCond : IGroupCond { public SQL.ICondBuilder	createGrp (SQL.ICondBuilder parent)	{ return (parent.addAndCond ()); } }
	  private class OrCond : IGroupCond { public SQL.ICondBuilder	createGrp (SQL.ICondBuilder parent)	{ return (parent.addOrCond ()); } }

	  private class ExistsCond : IGroupCond
	   {
	    public ExistsCond (string tableName, string tableField, string tableAcr = null, string tableCondFld = null, string parentValueSide = null)
					{ m_TableName = tableName; m_TableField = tableField; m_TableAcr = tableAcr; m_TableCondFld = tableCondFld; m_ParentValueSide = parentValueSide; }
	    private string	m_TableName;
		private string	m_TableField;
		private string	m_TableAcr;
		private string	m_TableCondFld;
		private string	m_ParentValueSide;

		public SQL.ICondBuilder	createGrp (SQL.ICondBuilder parent)
					{ return (parent.addExists (tableName: m_TableName, tableField: m_TableField, tableAcr: m_TableAcr, tableCondFld: m_TableCondFld, parentValueSide: m_ParentValueSide)); }
	   }
	 }

	private class Finder : IFinder
	 {
	  private Finder (IFactory db, string tbAcr, ICondBuilderPlus cndBld)	{ database = db; tableAcr = tbAcr; condBuilder = cndBld; is4Change = false; isRefCodeRead = true; maxRows = -1; }
	  private IFactory			database	{ get; set; }
	  private string			tableAcr	{ get; set; }
	  private ICondBuilderPlus	condBuilder	{ get; set; }

	  public static IFinder	create (IFactory db, string tableAcr, ICondBuilderPlus condBld)	{ return (new Finder (db, tableAcr, condBld)); }

	  public ICondBuilder	createCondBuilder ()								{ return (condBuilder.addAndCond ()); }
	  public ISortByField	createSortByField (string fldName, bool asc = true)	{ return (database.sortByField (condBuilder.fullFieldName (tableAcr, fldName), asc)); }

      public void	add (ICondition cond)						{ condBuilder.addCond (cond); }
      public void	add (string fldName, long val)				{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }
      public void	add (string fldName, int val)				{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }
      public void	add (string fldName, short val)				{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }
      public void	add (string fldName, bool val)				{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }
      public void	add (string fldName, System.DateTime val)	{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }
      public void	add (string fldName, string val)			{ condBuilder.addValCond (condBuilder.fullFieldName (tableAcr, fldName), val); }

      public void	sortBy (ISortByField[] flds)	{ m_SortByFields = flds; }
      public void	sortBy (ISortByField fld)		{ if (fld == null) m_SortByFields = null; else sortBy (new ISortByField [1] { fld }); }
	  private ISortByField[]	m_SortByFields;

      public bool	is4Change		{ get; set; }
	  public bool	isRefCodeRead	{ get; set; }
	  public long	maxRows			{ get; set; }

	  public void fillBuilder (SQL.ISelect selBuilder)
				{
				 condBuilder.create ().fillBuilder (selBuilder.andCnd);
				 selBuilder.forUpdate    = is4Change;
				 selBuilder.maxRows      = maxRows;
				 selBuilder.sortByFields = m_SortByFields;
				}
	 }

	private class TableDef
	 {
	  private TableDef ()	{ }

	  public static ITableCreator	create (IImpFactory db, string name = null)	{ return (TableCreator.create (db, name)); }

	  private class Value
	   {
	    private Value ()	{ }

		public static IValue	create (ILangTranslation trans, EFieldType type)
					{
					 switch (type)
					  {
					   case EFieldType.FT_BigInt:   return (new LongValue (trans, trans.longNullValue));
					   case EFieldType.FT_Int:	    return (new IntValue (trans, trans.intNullValue));
					   case EFieldType.FT_Short:	return (new ShortValue (trans, trans.shortNullValue));
					   case EFieldType.FT_Bool:		return (new BoolValue (trans, trans.boolNullValue));
					   case EFieldType.FT_DateTime:	return (new DTValue (trans, trans.dtNullValue));
					   case EFieldType.FT_Text:     return (new TxtValue (trans, trans.stringNullValue));
					   case EFieldType.FT_BLOB:     return (new BLOBValue (trans, trans.byteArrNullValue));
					   default: return (null);
					  }
					}

		public static IValue	create (ILangTranslation trans, long l)																{ return (new LongValue (trans, l)); }
		public static IValue	create (ILangTranslation trans, int i)																{ return (new IntValue (trans, i)); }
		public static IValue	create (ILangTranslation trans, short s)															{ return (new ShortValue (trans, s)); }
		public static IValue	create (ILangTranslation trans, IOptionValue[] valArr, short s)										{ return (new OptionValue (trans, valArr, s)); }
		public static IValue	create (ILangTranslation trans, bool b)																{ return (new BoolValue (trans, b)); }
		public static IValue	create (ILangTranslation trans, System.DateTime dt, EDateTimeType dtType = EDateTimeType.DTT_Full)	{ return (new DTValue (trans, dt, dtType)); }
		public static IValue	create (ILangTranslation trans, string txt)															{ return (new TxtValue (trans, txt)); }
		public static IValue	create (ILangTranslation trans, byte[] blb)															{ return (new BLOBValue (trans, blb)); }

		public static IValue	createOption (ILangTranslation trans, IOptionValue[] valArr)										{ return (new OptionValue (trans, valArr, trans.shortNullValue)); }
		public static IValue	createDt (ILangTranslation trans, EDateTimeType dtType = EDateTimeType.DTT_Full)					{ return (new DTValue (trans, trans.dtNullValue, dtType)); }
		
		private class LongValue : IValue, IBigIntValue
		 {
		  public LongValue (ILangTranslation trans, long vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (language.format (val)); } set { val = language.scanLong (value); } }

		  public string	present (ILangTranslation trans)
					{
					 if (trans == null) trans = language;
					 return (trans.format (val));
					}
		  
		  public IValue	createCopy ()									{ return (new LongValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.longNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readLng (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_BigInt); } }
		  public long			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (this); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class IntValue : IValue, IIntValue
		 {
		  public IntValue (ILangTranslation trans, int vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (language.format (val)); } set { val = language.scanInt (value); } }

		  public string	present (ILangTranslation trans)
					{
					 if (trans == null) trans = language;
					 return (trans.format (val));
					}
		  
		  public IValue	createCopy ()									{ return (new IntValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.intNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readInt (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_Int); } }
		  public int			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (this); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class ShortValue : IValue, IShortValue
		 {
		  public ShortValue (ILangTranslation trans, short vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (language.format (val)); } set { val = language.scanShort (value); } }

		  public string	present (ILangTranslation trans)
					{
					 if (trans == null) trans = language;
					 return (trans.format (val));
					}
		  
		  public IValue	createCopy ()									{ return (new ShortValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.shortNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readShort (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_Short); } }
		  public short			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (this); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class OptionValue : IValue, IShortValue
		 {
		  public OptionValue (ILangTranslation trans, IOptionValue[] valArr, short vl)	{ language = trans; valueArr = valArr; val = vl; }

		  private ILangTranslation	language	{ get; set; }
		  private IOptionValue[]	valueArr	{ get; set; }

		  private IOptionValue	find ()
					{
					 if (valueArr != null) for (int i = 0; i < valueArr.Length; i++) if (valueArr [i].val == val) return (valueArr [i]);
					 return (null);
					}

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ 
								 get { IOptionValue optVal = find (); return ((optVal == null)? language.format (val): optVal.present (language)); }
								 set
								  {
								   if (valueArr != null) for (int i = 0; i < valueArr.Length; i++) if (valueArr [i].txt == value) { val = valueArr [i].val; return; }
								   val = language.scanShort (value);
								  }
								}

		  public string	present (ILangTranslation trans)
					{
					 if (trans == null) trans = language;
					 IOptionValue optVal = find ();
					 return ((optVal == null)? trans.format (val): optVal.present (trans));
					}
		  
		  public IValue	createCopy ()									{ return (new OptionValue (language, valueArr, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.shortNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readShort (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_Short); } }
		  public short			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (this); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class BoolValue : IValue, IBoolValue
		 {
		  public BoolValue (ILangTranslation trans, bool vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (language.format (val)); } set { val = language.scanBool (value); } }

		  public string	present (ILangTranslation trans)
					{
					 if (trans == null) trans = language;
					 return (trans.format (val));
					}
		  
		  public IValue	createCopy ()									{ return (new BoolValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.boolNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readBool (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_Bool); } }
		  public bool			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (this); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class DTValue : IValue, IDTValue
		 {
		  public DTValue (ILangTranslation trans, System.DateTime vl, EDateTimeType dtType = EDateTimeType.DTT_Full)	{ language = trans; val = vl; dateType = dtType; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (language.format (val, dateType)); } set { val = language.scanDT (value, dateType); } }

		  public string	present (ILangTranslation trans)	{ return (present (trans, null)); }

		  public string	present (ILangTranslation trans = null, IDateTimeZone zone4Value = null)
					{
					 System.DateTime dt = this [zone4Value];
					 if (trans == null) trans = language;
					 return (trans.format (dt, dateType));
					}

		  public IValue	createCopy ()									{ return (new DTValue (language, val, dateType)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.dtNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readDateTime (fieldName); }

		  public void	validate (string vl, ILangTranslation trans = null, IDateTimeZone zone4Value = null)	{ setUp (vl, trans, zone4Value); }

    	  public void	setUp (string formated, ILangTranslation trans = null, IDateTimeZone zone = null)
					{
					 if (trans == null) trans = language;
					 this [zone] = trans.scanDT (formated, dateType);
					}
    	  public void	setUp (string formated, IDateTimeZone zone)		{ setUp (formated, null, zone); }
    	  public void	setUp (System.DateTime dt, IDateTimeZone zone)	{ this [zone] = dt; }
		  
		  public EFieldType			type		{ get { return (EFieldType.FT_DateTime); } }
		  public System.DateTime	val			{ get; set; }
		  public EDateTimeType		dateType	{ get; private set; }

		  public System.DateTime	this [IDateTimeZone zoneOuterOfValue]	{
																			 get { return ((zoneOuterOfValue == null)? val: zoneOuterOfValue.server2Zone (val)); }
																			 set { val = ((zoneOuterOfValue == null)? value: zoneOuterOfValue.zone2Server (value)); }
																			}
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (this); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class TxtValue : IValue, ITxtValue
		 {
		  public TxtValue (ILangTranslation trans, string vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{ get { return (val); } set { val = value; } }

		  public string	present (ILangTranslation trans)	{ return (format); }
		  
		  public IValue	createCopy ()									{ return (new TxtValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.stringNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readString (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_Text); } }
		  public string			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (this); } }
	      public IBLOBValue		blb	{ get { return (null); } }
		 }

		private class BLOBValue : IValue, IBLOBValue
		 {
		  public BLOBValue (ILangTranslation trans, byte[] vl)	{ language = trans; val = vl; }

		  private ILangTranslation	language	{ get; set; }

          public bool	isNull	{ get { return (language.isNull (val)); } }
		  public string	format	{
		                         get { if (isNull) return (null); else { System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding (); return (enc.GetString (val)); } }
		                         set { if (value == null) val = null; else { System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding (); val = enc.GetBytes (value); } }
								}

		  public string	present (ILangTranslation trans)	{ return (format); }
		  
		  public IValue	createCopy ()									{ return (new BLOBValue (language, val)); }
		  public string	createParam (SQL.IParamBuilder parBld)			{ return (parBld.add (val)); }
		  public void	setNull ()										{ val = language.byteArrNullValue; }
		  public void	readFetch (SQL.IFetch fetch, string fieldName)	{ val = fetch.readBLOB (fieldName); }

		  public EFieldType		type	{ get { return (EFieldType.FT_BLOB); } }
		  public byte[]			val		{ get; set; }
    
          public IBigIntValue	bg	{ get { return (null); } }
          public IIntValue		it	{ get { return (null); } }
          public IShortValue	sh	{ get { return (null); } }
	      public IBoolValue		bl	{ get { return (null); } }
          public IDTValue		dt	{ get { return (null); } }
          public ITxtValue		txt	{ get { return (null); } }
	      public IBLOBValue		blb	{ get { return (this); } }
		 }
	   }
	  
	  private class Record : IRecord
	   {
	    private Record (SQL.IEngine eng, IQuery query)
					{
					 engine = eng;

					 m_PrimaryKeyIndex = query.getFieldIndex (query.primaryKey);
					 m_Values = new IValue [query.fieldCount];

					 for (int i = 0; i < m_Values.Length; i++) m_Values [i] = query [i].createNull ();
					}
	    
		private Record (SQL.IEngine eng, int primKeyIndx, IValue[] vals)
					{
					 engine = eng; m_PrimaryKeyIndex = primKeyIndx;

					 m_Values = new IValue [vals.Length];
					 for (int i = 0; i < m_Values.Length; i++) m_Values [i] = vals [i].createCopy ();
					}
		
		public static IRecord	create (SQL.IEngine engine, IQuery query)	{ return (new Record (engine, query)); }

		private SQL.IEngine	engine	{ get; set; }
		private int			m_PrimaryKeyIndex;
		private IValue[]	m_Values;

		public long		recIdVal	{ get { return (m_Values [m_PrimaryKeyIndex].bg.val); } }
		public string	recordId	{ get { return (m_Values [m_PrimaryKeyIndex].bg.format); } }

    	public IValue	val (IField fld)				{ return (this [fld.fldIndex]); }
    	public IValue	val (IQuery query, string name)	{ return (this [query.getFieldIndex (name)]); }
    	public IValue	this [int index]	{ get { return (m_Values [index]); } }

		public IRecord	createCopy ()	{ return (new Record (engine, m_PrimaryKeyIndex, m_Values)); }

    	public void	insert (ITable table, ITransaction trans)
					{
					 SQL.IInsert ins = engine.insert (trans, table.name);
					 string autoIncFldName = null;
					 int autoIncFldIndex = -1;

					 for (int i = 0; i < m_Values.Length; i++)
					  {
					   IField fld = table [i];

					   if (fld.role == EFieldRole.FR_Autoincrement) { autoIncFldName = fld.name; autoIncFldIndex = i; }
					   else if ((!(fld.readOnly)) && fld.role != EFieldRole.FR_ReferenceInfo)
					    ins.add (fld.name, addFieldParam (fld, m_Values [i], ins.parBld));
					  }

					 if (autoIncFldName == null) ins.insert ();
					 else m_Values [autoIncFldIndex].bg.val = ins.insert (autoIncFldName);
					}
    	
		public void	update (ITable table, ITransaction trans)
					{
					 SQL.IUpdate upd = engine.update (trans, table.name);

					 fillSQLCond (table [m_PrimaryKeyIndex], m_Values [m_PrimaryKeyIndex], upd.andCnd, upd.parBld);

					 for (int i = 0; i < m_Values.Length; i++)
					  {
					   IField fld = table [i];

					   if (fld.role != EFieldRole.FR_Autoincrement && fld.role != EFieldRole.FR_Primary &&
					     (!(fld.readOnly)) && fld.role != EFieldRole.FR_ReferenceInfo)
						upd.add (fld.name, addFieldParam (fld, m_Values [i], upd.parBld));
					  }

					 upd.update ();
					}
    	
		public void	delete (ITable table, ITransaction trans)
					{
					 SQL.IDelete del = engine.delete (trans, table.name);

					 fillSQLCond (table [m_PrimaryKeyIndex], m_Values [m_PrimaryKeyIndex], del.andCnd, del.parBld);
					 del.delete ();
					}

		private static string	addFieldParam (IField fld, IValue val, SQL.IParamBuilder parBld)
					{ return ((val.isNull)? parBld.addNull (val.type): val.createParam (parBld)); }

		private static void	fillSQLCond (IField fld, IValue val, SQL.ICondBuilder cndBld, SQL.IParamBuilder parBld)
					{ cndBld.addCond (fld.name, ECondOper.CO_Eq, addFieldParam (fld, val, parBld)); }
	   }

	  private class TableImp
	   {
	    private TableImp ()	{ }

		public static IQuery		createQuery (IImpFactory factory, string name, IField[] fieldArr)	{ return (Query.create (factory, name, fieldArr)); }
	    
		public static IQuery<Type>	createQuery<Type> (IImpFactory factory, string name, IField[] fieldArr, IRecordLoader<Type> recLoader) where Type: IObject
					{ return (Query<Type>.create (factory, name, fieldArr, recLoader)); }
	    
		public static ITable		createTable (IImpFactory factory, string name, IField[] fieldArr)	{ return (Table.create (factory, name, fieldArr)); }
	    
		public static ITable<Type>	createTable<Type> (IImpFactory factory, string name, IField[] fieldArr, ITypeConverter<Type> converter) where Type: IObject
					{ return (Table<Type>.create (factory, name, fieldArr, converter)); }

		public static ITable<Type, EField>	createTable<Type, EField> (IImpFactory factory, string name, IField[] fieldArr, ITypeConverter<Type> converter, DFieldName<EField> fldFce) where Type: IObject
					{ return (Table<Type, EField>.create (factory, name, fieldArr, converter, fldFce)); }

		private class RecordSet : IRecordSet
		 {
		  private RecordSet (SQL.IEngine engine, IQuery q, SQL.IFetch f)	{ query = q; fetch = f; currRec = Record.create (engine, query); }
		  public static IRecordSet	create (SQL.IEngine engine, IQuery query, SQL.IFetch fetch)	{ return (new RecordSet (engine, query, fetch)); }

		  private IQuery		query	{ get; set; }
		  private SQL.IFetch	fetch	{ get; set; }

		  public void	close ()	{ fetch.close (); }
		  public void	Dispose ()	{ close (); }

		  public IRecord	currRec	{ get; private set; }

		  public IRecord	createCopy (IRecord srcRec = null)
					{ return ((srcRec == null)? currRec.createCopy (): srcRec.createCopy ()); }

		  public bool	moveNext ()
					{
					 if (!(fetch.moveNext ())) return (false);

					 for (int i = 0; i < query.fieldCount; i++)
					  {
					   IField fld = query [i];

					   if (fld.role == EFieldRole.FR_ReferenceInfo)
					    if (fld.name == null) continue;
						else if (!(fetch.isDefined (fld.name))) { currRec [i].setNull (); continue; }

					   if (fetch.isNull (fld.name)) currRec [i].setNull ();
					   else currRec [i].readFetch (fetch, fld.name);
					  }

					 return (true);
					}
		 }

		private class RecordSet<Type> : IRecordSet<Type> where Type: IObject
		 {
		  private RecordSet (IRecordSet rs, IRecordLoader<Type> rcLd)	{ currObj = (recLoader = rcLd).create4Rec ((recordSet = rs).currRec); }

		  public static IRecordSet<Type>	create (IRecordSet recordSet, IRecordLoader<Type> recLoader)								{ return (new RecordSet<Type> (recordSet, recLoader)); }
		  public static IRecordSet<Type>	create (SQL.IEngine engine, IQuery query, SQL.IFetch fetch, IRecordLoader<Type> recLoader)	{ return (create (RecordSet.create (engine, query, fetch), recLoader)); }

		  private IRecordSet			recordSet	{ get; set; }
		  private IRecordLoader<Type>	recLoader	{ get; set; }

		  public void	close ()	{ recordSet.close (); }
		  public void	Dispose ()	{ close (); }

		  public IRecord	currRec	{ get { return (recordSet.currRec); } }
		  public Type		currObj	{ get; private set; }

		  public IRecord	createCopy (IRecord srcRec = null)	{ return (recordSet.createCopy (srcRec)); }
		  public Type		createObjCopy (Type srcObj)			{ return (recLoader.create4Rec (createCopy (srcObj.record))); }
		  public Type		createObjCopy ()					{ return (recLoader.create4Rec (createCopy ())); }

		  public bool	moveNext ()
					{
					 if (!(recordSet.moveNext ())) return (false);

					 recLoader.loadData (currObj, currRec);
					 return (true);
					}
		 }
		
		private abstract class AQuery : IQuery
		 {
		  protected AQuery (IImpFactory fct, string nm, IField[] fldArr)
					{
					 factory = fct; name = nm; fieldArr = fldArr;

					 for (int i = 0; i < fieldArr.Length; i++)
					  switch (fieldArr [i].role)
					   {
					    case EFieldRole.FR_Autoincrement:
						case EFieldRole.FR_Primary: primaryKey = fieldArr [i]; break;
						case EFieldRole.FR_Code: codeField = fieldArr [i]; break;
						case EFieldRole.FR_Desc: descField = fieldArr [i]; break;
					   }
					}
		  
		  protected IImpFactory	factory	{ get; private set; }

		  public string	name			{ get; private set; }
		  private IField[]	fieldArr	{ get; set; }

		  public IField	primaryKey	{ get; private set; }
		  public IField	codeField	{ get; private set; }
		  public IField	descField	{ get; private set; }
		  public string	getCode (IRecord rec)	{ return ((codeField == null)? rec.recordId: codeField.val (rec).format); }
		  public string	getDesc (IRecord rec)	{ return ((descField == null)? getCode (rec): descField.val (rec).format); }

		  public IField	this [string name]	{ get { return (this [getFieldIndex (name)]); } }
		  public IField	this [int index]	{ get { return (fieldArr [index]); } }

		  public int	fieldCount	{ get { return (fieldArr.Length); } }
		  public int	getFieldIndex (IField fld)		{ return (getFieldIndex (fld.name)); }
		  public int	getFieldIndex (string fldName)
					{
					 for (int i = 0; i < fieldArr.Length; i++) if (fieldArr [i].name == fldName) return (i);
					 return (-1);
					}

		  public IRecord getRecord (long id, bool forChange = false)
					{
					 if (factory.culture.language.isNull (id)) return (null);
					 
					 IFinder fnd = createFinder (forChange: forChange);
					 fnd.add (primaryKey.name, id);
					 fnd.maxRows = 1;

					 using (IRecordSet rs = find (fnd)) if (rs.moveNext ()) return (rs.currRec);
					 return (null);
					}
		  
		  public IRecord getRecord (string id, bool forChange = false)
					{
					 if (factory.culture.language.isNull (id)) return (null);

					 IValue idVal = primaryKey.createNull (); idVal.format = id;
					 return (getRecord (idVal.bg.val, forChange));
					}

    	  public IFinder		createFinder ()			{ return (factory.finder (name)); }
		  public ICondBuilder	createCondBuilder ()	{ return (factory.condBuilder ()); }

    	  private IFinder		createFinder (bool refCodeRead = true, bool forChange = false)			{ IFinder fnd = createFinder (); fnd.isRefCodeRead = refCodeRead; fnd.is4Change = forChange; return (fnd); }

		  public IRecordSet	find (IFinder finder)
					{
					 using (SQL.ISelect sel = factory.engine.select (tableName: name))
					  {
					   finder.fillBuilder (sel);

					   for (int i = 0; i < fieldArr.Length; i++)
					    {
					     IField fld = fieldArr [i];

					     if (fld.role != EFieldRole.FR_ReferenceInfo)
					      {
						   sel.addShowField (sel.fullFieldName (name, fld.name));

						   if (finder.isRefCodeRead && fld.referenceField != null) fld.referenceField.fillSelectInfo (SelectCreator.create (sel, name));
						  }
					    }
					 
					   return (RecordSet.create (factory.engine, this, sel.select ()));
					  }
					}
    	  
		  public IRecordSet	find (ICondition cond, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (cond); return (find (fnd)); }
		  public IRecordSet	find (string fldName, long val, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (fldName, val); return (find (fnd)); }
    	  public IRecordSet	find (string fldName, int val, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (fldName, val); return (find (fnd)); }
    	  public IRecordSet	find (string fldName, short val, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (fldName, val); return (find (fnd)); }
    	  public IRecordSet	find (string fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (fldName, val); return (find (fnd)); }
    	  public IRecordSet	find (string fldName, string val, bool refCodeRead = true, bool forChange = false)
					{ IFinder fnd = createFinder (refCodeRead: refCodeRead, forChange: forChange); fnd.add (fldName, val); return (find (fnd)); }
    	  
		  public IRecordSet	find (bool refCodeRead = true, bool forChange = false)	{ return (find (createFinder (refCodeRead: refCodeRead, forChange: forChange))); }

		  private class SelectCreator : ISelectCreator
		   {
		    private SelectCreator (SQL.ISelect sel, string srcQAcr)	{ select = sel; srcQueryAcr = srcQAcr; }
			public static ISelectCreator	create (SQL.ISelect select, string srcQueryAcr)	{ return (new SelectCreator (select, srcQueryAcr)); }

            private SQL.ISelect	select		{ get; set; }
			public string		srcQueryAcr	{ get; private set; }

	        public string			createQueryAcr (string queryName)	{ return (select.createTableAcr ()); }
	        
			public ISelectJoinCond	createJoinCond (string leftFldName, string rightFldName, string leftQrAcr = null, string rightQrAcr = null)
						{
						 if (leftQrAcr != null) leftFldName = select.fullFieldName (queryAcr: leftQrAcr, fieldName: leftFldName);
						 if (rightQrAcr != null) rightFldName = select.fullFieldName (queryAcr: rightQrAcr, fieldName: rightFldName);
						 return (SelectJoinCond.create (leftField: leftFldName, rightField: rightFldName));
						}

	        public void	addSelectSource (string queryName, string queryAcr, ISelectJoinCond[] joinCond, ESelectJoin join = ESelectJoin.SJ_LeftJoin)
						{
						 SQL.IJoinCond[] sqlCnd = null;

						 if (joinCond != null && joinCond.Length > 0)
						  {
						   sqlCnd = new SQL.IJoinCond [joinCond.Length];
						   for (int i = 0; i < joinCond.Length; i++) sqlCnd [i] = joinCond [i].create (select);
						  }

						 select.addTable (queryName: queryName, queryAcr: queryAcr, conds: sqlCnd, join: join);
						}
	        
			public void	addSelectSource (string queryName, string queryAcr, ISelectJoinCond joinCond, ESelectJoin join = ESelectJoin.SJ_LeftJoin)
						{
						 SQL.IJoinCond sqlCnd = ((joinCond == null)? null: joinCond.create (select));
						 select.addTable (queryName: queryName, queryAcr: queryAcr, oneCond: sqlCnd, join: join);
						}

			public void	addSelectField (string queryAcr, string fieldName, string fieldAcr = null)
						{ select.addShowField (fullFieldName: select.fullFieldName (queryAcr: queryAcr, fieldName: fieldName), fieldAcr: fieldAcr); }
		   
		    private class SelectJoinCond : ISelectJoinCond
			 {
			  private SelectJoinCond (string lfFld, string rghFld)	{ leftField = lfFld; rightField = rghFld; }
			  public static ISelectJoinCond	create (string leftField, string rightField)	{ return (new SelectJoinCond (leftField, rightField)); }

			  public string	leftField	{ get; private set; }
			  public string	rightField	{ get; private set; }

			  public SQL.IJoinCond	create (SQL.ISelect sel)	{ return (sel.createJoinCond (leftSide: leftField, rightSide: rightField)); }
			 }
		   }
		 }
		
		private class Query : AQuery
		 {
		  private Query (IImpFactory fct, string nm, IField[] fldArr)	: base (fct, nm, fldArr)	{ }
		  public static IQuery	create (IImpFactory factory, string name, IField[] fieldArr)	{ return (new Query (factory, name, fieldArr)); }
		 }

		private abstract class AQuery<Type> : AQuery, IQuery<Type> where Type: IObject
		 {
		  protected AQuery (IImpFactory fct, string nm, IField[] fldArr, IRecordLoader<Type> rcLdr)	: base (fct, nm, fldArr)	{ recLoader = rcLdr; }

		  private IRecordLoader<Type>	recLoader	{ get; set; }

		  public Type getObject (long id, bool forChange = false)
					{ IRecord rec = getRecord (id, forChange); return ((rec == null)? recLoader.nullValue: recLoader.create4Rec (rec)); }

		  public Type getObject (string id, bool forChange = false)
					{ IRecord rec = getRecord (id, forChange); return ((rec == null)? recLoader.nullValue: recLoader.create4Rec (rec)); }

		  public IRecordSet<Type>	findObj (IFinder finder)	{ return (RecordSet<Type>.create (find (finder), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (ICondition cond, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (cond, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (string fldName, long val, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (fldName: fldName, val: val, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (string fldName, int val, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (fldName: fldName, val: val, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (string fldName, short val, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (fldName: fldName, val: val, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (string fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (fldName: fldName, val: val, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		  
		  public IRecordSet<Type>	findObj (string fldName, string val, bool refCodeRead = true, bool forChange = false)
					{ return (RecordSet<Type>.create (find (fldName: fldName, val: val, refCodeRead: refCodeRead, forChange: forChange), recLoader)); }

		  public IRecordSet<Type>	findObj (bool refCodeRead = true, bool forChange = false)	{ return (RecordSet<Type>.create (find (refCodeRead: refCodeRead, forChange: forChange), recLoader)); }
		 }

		private class Query<Type> : AQuery<Type> where Type: IObject
		 {
		  private Query (IImpFactory fct, string nm, IField[] fldArr, IRecordLoader<Type> rcLdr)	: base (fct, nm, fldArr, rcLdr)	{ }
		  public static IQuery<Type>	create (IImpFactory factory, string name, IField[] fieldArr, IRecordLoader<Type> recordLoader)
					{ return (new Query<Type> (factory, name, fieldArr, recordLoader)); }
		 }

		private class Table : AQuery, ITable
		 {
		  private Table (IImpFactory fct, string nm, IField[] fldArr)	: base (fct, nm, fldArr)	{ }
		  public static ITable	create (IImpFactory factory, string name, IField[] fieldArr)	{ return (new Table (factory, name, fieldArr)); }

		  public IRecord	createNew ()	{ return (Record.create (factory.engine, this)); }

		  public void	insert (ITransaction trans, IRecord rec)	{ rec.insert (this, trans); }
		  public void	update (ITransaction trans, IRecord rec)	{ rec.update (this, trans); }
		  public void	delete (ITransaction trans, IRecord rec)	{ rec.delete (this, trans); }
		 }

		private abstract class ATypeTable<Type> : AQuery<Type>, ITable<Type> where Type: IObject
		 {
		  protected ATypeTable (IImpFactory fct, string nm, IField[] fldArr, ITypeConverter<Type> cnvr)	: base (fct, nm, fldArr, cnvr)	{ converter = cnvr; }

		  private ITypeConverter<Type>	converter	{ get; set; }

		  public IRecord	createNew ()	{ return (Record.create (factory.engine, this)); }
		  public Type		createNewObj ()	{ return (converter.create4Rec (createNew ())); }

		  public void	insert (ITransaction trans, IRecord rec)	{ rec.insert (this, trans); }
		  public void	insert (ITransaction trans, Type obj)		{ insert (trans, converter.saveData (obj)); converter.loadData (obj); }

		  public void	update (ITransaction trans, IRecord rec)	{ rec.update (this, trans); }
		  public void	update (ITransaction trans, Type obj)		{ update (trans, converter.saveData (obj)); }

		  public void	delete (ITransaction trans, IRecord rec)	{ rec.delete (this, trans); }
		  public void	delete (ITransaction trans, Type obj)		{ delete (trans, converter.saveData (obj)); }
		 }

		private class Table<Type> : ATypeTable<Type> where Type: IObject
		 {
		  protected Table (IImpFactory fct, string nm, IField[] fldArr, ITypeConverter<Type> cnvr)	: base (fct, nm, fldArr, cnvr)	{ }
		  public static ITable<Type>	create (IImpFactory factory, string name, IField[] fieldArr, ITypeConverter<Type> converter)	{ return (new Table<Type> (factory, name, fieldArr, converter)); }
		 }

		private class Table<Type, EField> : ATypeTable<Type>, ITable<Type, EField> where Type: IObject
		 {
		  private Table (IImpFactory fct, string nm, IField[] fldArr, ITypeConverter<Type> cnvr, DFieldName<EField> fldFce)	: base (fct, nm, fldArr, cnvr)	{ fieldNameFce = fldFce; }
		  public static ITable<Type, EField>	create (IImpFactory factory, string name, IField[] fieldArr, ITypeConverter<Type> converter, DFieldName<EField> fldFce)
					{ return (new Table<Type, EField> (factory, name, fieldArr, converter, fldFce)); }
		 
		  private DFieldName<EField>	fieldNameFce	{ get; set; }

		  public string	getFieldName (EField fld)		{ return (fieldNameFce (fld)); }
		  public string	getFullFieldName (EField fld)	{ return (factory.engine.fullFieldName (name, getFieldName (fld))); }

    	  public IRecordSet<Type>	findObj (EField fldName, long val, bool refCodeRead = true, bool forChange = false)				{ return (findObj (getFieldName (fldName), val: val, refCodeRead: refCodeRead, forChange: forChange)); }
    	  public IRecordSet<Type>	findObj (EField fldName, int val, bool refCodeRead = true, bool forChange = false)				{ return (findObj (getFieldName (fldName), val: val, refCodeRead: refCodeRead, forChange: forChange)); }
    	  public IRecordSet<Type>	findObj (EField fldName, short val, bool refCodeRead = true, bool forChange = false)			{ return (findObj (getFieldName (fldName), val: val, refCodeRead: refCodeRead, forChange: forChange)); }
    	  public IRecordSet<Type>	findObj (EField fldName, System.DateTime val, bool refCodeRead = true, bool forChange = false)	{ return (findObj (getFieldName (fldName), val: val, refCodeRead: refCodeRead, forChange: forChange)); }
    	  public IRecordSet<Type>	findObj (EField fldName, string val, bool refCodeRead = true, bool forChange = false)			{ return (findObj (getFieldName (fldName), val: val, refCodeRead: refCodeRead, forChange: forChange)); }
		 }
	   }
	  
	  private interface IImpField : IFieldFiller
	   { void	setQuery (IQuery query, int fldIndex); }

	  private interface IImpRefDelegate : IReferenceDefinition
	   {
	    IReferenceFiller		createFiller ();
		IReferenceFiller<Type>	createFiller<Type> () where Type: IObject;
	   }
	  
	  private class FieldCreator
	   {
	    private FieldCreator ()	{ }

		public static IImpField	field (ILangTranslation language, string name, EFieldType type, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (Field.create (language, name, type, isReadOnly, role)); }
		
		public static IImpField	option (ILangTranslation language, string name, IOptionValue[] values, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (Option.create (language, name, values, isReadOnly, role)); }

		public static IImpField	text (ILangTranslation language, string name, int maxSize, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (Text.create (language, name, maxSize, isReadOnly, role)); }

		public static IImpField	reference (ILangTranslation language, string name, IReferenceDefinition refDel, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (Reference.create (language, name, refDel, type, isReadOnly, role)); }

		public static IImpRefDelegate	refDelegate (IImpFactory factory, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null)
					{ return (RefDelegate.create (factory, masterTable, joinCond: joinCond, lookUpCond: lookUpCond)); }

		private abstract class AField
		 {
		  protected AField (ILangTranslation lng, string nm, EFieldType tp, bool isRdOnl = false, EFieldRole rl = EFieldRole.FR_Normal)
					{ language = lng; query = null; name = nm; fldIndex = -1; type = tp; role = rl; readOnly = isRdOnl; }

		  private ILangTranslation	language	{ get; set; }

		  public IQuery		query		{ get; private set; }
		  public string		name		{ get; private set; }
		  public int		fldIndex	{ get; private set; }
		  public EFieldType	type		{ get; private set; }
		  public EFieldRole	role		{ get; private set; }
		  public bool		readOnly	{ get; private set; }

		  public IValue	createNull ()		{ return (Value.create (language, type)); }
		  public IValue	val (IRecord rec)	{ return (rec [fldIndex]); }

	      public void	setQuery (IQuery q, int fldIdx)	{ query = q; fldIndex = fldIdx; }
		  public void	setName (string nm)				{ name = nm; }
		 }

		private class Field : AField, IImpField
		 {
		  private Field (ILangTranslation lng, string nm, EFieldType tp, bool isRdOnl = false, EFieldRole rl = EFieldRole.FR_Normal) : base (lng, nm, tp, isRdOnl, rl)	{ }
		  public static IImpField	create (ILangTranslation language, string name, EFieldType type, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (new Field (language, name, type, isReadOnly, role )); }

		  public IReference	referenceField	{ get { return (null); } }
		  public IOption	optionField		{ get { return (null); } }
		  public IText		textField		{ get { return (null); } }
		 }

		private class Option : AField, IImpField, IOption
		 {
		  private Option (ILangTranslation lng, string nm, IOptionValue[] vls, bool isRdOnl = false, EFieldRole rl = EFieldRole.FR_Normal) : base (lng, nm, EFieldType.FT_Short, isRdOnl, rl)	{ options = vls; }
		  public static IImpField	create (ILangTranslation language, string name, IOptionValue[] values, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (new Option (language, name, values, isReadOnly, role )); }

		  public IReference	referenceField	{ get { return (null); } }
		  public IOption	optionField		{ get { return (this); } }
		  public IText		textField		{ get { return (null); } }

		  public IOptionValue[]	options { get; private set; }
		 }

		private class Text : AField, IImpField, IText
		 {
		  private Text (ILangTranslation lng, string nm, int mxSz, bool isRdOnl = false, EFieldRole rl = EFieldRole.FR_Normal) : base (lng, nm, EFieldType.FT_Text, isRdOnl, rl)	{ maxLen = mxSz; }
		  public static IImpField	create (ILangTranslation language, string name, int maxSize, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (new Text (language, name, maxSize, isReadOnly, role )); }

		  public IReference	referenceField	{ get { return (null); } }
		  public IOption	optionField		{ get { return (null); } }
		  public IText		textField		{ get { return (this); } }

		  public int	maxLen	{ get; private set; }
		 }

		private class Reference : AField, IImpField, IReference
		 {
		  private Reference (ILangTranslation lng, string nm, IReferenceDefinition rfDef, EFieldType tp = EFieldType.FT_BigInt, bool isRdOnl = false, EFieldRole rl = EFieldRole.FR_Normal)
					: base (lng, nm, tp, isRdOnl, rl)	{ refDefinition = rfDef; }
		  public static IImpField	create (ILangTranslation language, string name, IReferenceDefinition refDefinition, EFieldType type = EFieldType.FT_BigInt,
										  bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ return (new Reference (language, name, refDefinition, type, isReadOnly, role )); }

		  public IReference	referenceField	{ get { return (this); } }
		  public IOption	optionField		{ get { return (null); } }
		  public IText		textField		{ get { return (null); } }

		  private IReferenceDefinition	refDefinition	{ get; set; }

    	  public void	defineReferenceFields (ITableCreator tc, IField refField)	{ refDefinition.defineReferenceFields (tc, refField); }

		  public void	fillSelectInfo (ISelectCreator selCre)		{ refDefinition.fillSelectInfo (selCre); }
	
		  public ITable	masterTable		{ get { return (refDefinition.masterTable); } }
		  public string	nameFieldName	{ get { return (refDefinition.nameFieldName); } }
		  public string	descFieldName	{ get { return (refDefinition.descFieldName); } }

		  public long		getMasterRecIdVal (IRecord rec)	{ return (refDefinition.getMasterRecIdVal (rec)); }
		  public string		getMasterRecordId (IRecord rec)	{ return (refDefinition.getMasterRecordId (rec)); }
		  public IRecord	getMasterRecord (IRecord rec)	{ return (refDefinition.getMasterRecord (rec)); }

    	  public IFinder	createMasterFinder (IRecord rec, string masterTableAcr = null)	{ return (refDefinition.createMasterFinder (rec, masterTableAcr)); }
		 }

		private class RefDelegate : IImpRefDelegate, IReferenceFiller
		 {
		  private RefDelegate (IImpFactory fct, ITable mstrTb = null, IJoinCond[] jnCnd = null, IJoinCond[] lkUpCnd = null)
					{
					 factory = fct; masterTable = null; myField = null; joinCond = jnCnd; lookUpCond = lkUpCnd;
					 specNameField = null; masterNameFldName = null; specDescField = null; masterDescFldName = null;

					 if (mstrTb != null) setReferenceTable (mstrTb);
					}
		  public static IImpRefDelegate	create (IImpFactory factory, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null)
					{ return (new RefDelegate (factory, masterTable, jnCnd: joinCond, lkUpCnd: lookUpCond)); }

		  private IImpFactory	factory	{ get; set; }

          public void	defineReferenceFields (ITableCreator tc, IField refField)
					{
					 myField = refField;

					 specNameField = tc.addSpecTextField (name: null, maxSize: 0x10000);
					 specDescField = tc.addSpecTextField (name: null, maxSize: 0x10000);
					 setUpSpecFieldsNames ();
					}

		  private void	setUpSpecFieldsNames ()
					{
					 if (specNameField == null && specDescField == null) return;
					 
					 string specNmName = null;
					 string specDscName = null;
					 
					 if (masterTable != null)
					  {
					   masterNameFldName = null; masterDescFldName = null;

					   for (int i = 0; i < masterTable.fieldCount; i++)
					    if (masterNameFldName == null && masterTable [i].role == EFieldRole.FR_Code) masterNameFldName = masterTable [i].name;
						else if (masterDescFldName == null && masterTable [i].role == EFieldRole.FR_Desc) masterDescFldName = masterTable [i].name;

					   if (masterNameFldName != null) specNmName = myField.name + "_" + masterTable.name + "_RefName";
					   if (masterDescFldName != null) specDscName = myField.name + "_" + masterTable.name + "_RefDesc";
					  }

					 if (specNameField != null) specNameField.setName (specNmName);
					 if (specDescField != null) specDescField.setName (specDscName);
					}

	      public void	fillSelectInfo (ISelectCreator selCre)
					{
					 if (masterTable == null || myField == null) return;
					 if (specNameField == null && specDescField == null) return;
					 if (masterNameFldName == null && masterDescFldName == null) return;

					 string masterAcr = selCre.createQueryAcr (masterTable.name);

					 ISelectJoinCond[] selJnCnd = ((joinCond == null || joinCond.Length < 1)? null: new ISelectJoinCond [joinCond.Length]);
					 if (selJnCnd != null)
					  for (int i = 0; i < selJnCnd.Length; i++)
					   if (joinCond [i].type == EJoinCondType.JCT_Field)
					    selJnCnd [i] = selCre.createJoinCond (leftQrAcr: selCre.srcQueryAcr, leftFldName: joinCond [i].fldCnd.fieldName, rightQrAcr: masterAcr, rightFldName: joinCond [i].masterFldName);
					   else throw new System.ApplicationException ("Join condition of reference field could be only by field!");
					 
					 selCre.addSelectSource (queryName: masterTable.name, queryAcr: masterAcr, joinCond: selJnCnd);

					 if (specNameField != null && masterNameFldName != null && specNameField.name != null)
					  selCre.addSelectField (queryAcr: masterAcr, fieldName: masterNameFldName, fieldAcr: specNameField.name);
					 
					 if (specDescField != null && masterDescFldName != null && specDescField.name != null)
					  selCre.addSelectField (queryAcr: masterAcr, fieldName: masterDescFldName, fieldAcr: specDescField.name);
					}
	
	      public ITable	masterTable		{ get; private set; }
		  
		  private IField		myField				{ get; set; }
		  private IJoinCond[]	joinCond			{ get; set; }
		  private IJoinCond[]	lookUpCond			{ get; set; }
		  private IFieldFiller	specNameField		{ get; set; }
		  private string		masterNameFldName	{ get; set; }
		  private IFieldFiller	specDescField		{ get; set; }
		  private string		masterDescFldName	{ get; set; }

	      public string	nameFieldName	{ get { return ((specNameField == null)? null: specNameField.name); } }
	      public string	descFieldName	{ get { return ((specDescField == null)? null: specDescField.name); } }

	      public long		getMasterRecIdVal (IRecord rec)	{ return (myField.val (rec).bg.val); }
	      public string		getMasterRecordId (IRecord rec)	{ return (myField.val (rec).bg.format); }
	      public IRecord	getMasterRecord (IRecord rec)	{ return (masterTable.getRecord (getMasterRecIdVal (rec))); }

          public IFinder	createMasterFinder (IRecord rec, string masterTableAcr = null)
					{
					 if (masterTable == null) return (null);

					 if (masterTableAcr == null) masterTableAcr = masterTable.name;
					 IFinder fnd = factory.finder (masterTableAcr);

					 if (lookUpCond != null)
					  for (int i = 0; i < lookUpCond.Length; i++)
					   {
					    string fullFldName = factory.fullFieldName (tableAcr: masterTableAcr, fieldName: lookUpCond [i].masterFldName);
						IValue cndVal = null;
						
						switch (lookUpCond [i].type)
					     {
						  case EJoinCondType.JCT_Field: cndVal = rec [myField.query.getFieldIndex (lookUpCond [i].fldCnd.fieldName)]; break;
						  case EJoinCondType.JCT_Value: cndVal = lookUpCond [i].vlCnd.val; break;
						 }

						if (cndVal != null)
						 if (cndVal.isNull) fnd.createCondBuilder ().addIsNull (fullFldName);
						 else switch (cndVal.type)
						  {
						   case EFieldType.FT_BigInt:   fnd.add (fullFldName, cndVal.bg.val); break;
						   case EFieldType.FT_Int:      fnd.add (fullFldName, cndVal.it.val); break;
						   case EFieldType.FT_Short:    fnd.add (fullFldName, cndVal.sh.val); break;
						   case EFieldType.FT_Bool:     fnd.add (fullFldName, cndVal.bl.val); break;
						   case EFieldType.FT_DateTime: fnd.add (fullFldName, cndVal.dt.val); break;
						   case EFieldType.FT_Text:     fnd.add (fullFldName, cndVal.txt.val); break;
						   default: throw new System.ApplicationException ("UnKnow value type: " + ((int) (cndVal.type)).ToString ());
						  }
					   }
					 
					 if (masterNameFldName != null) fnd.sortBy (factory.sortByField (factory.fullFieldName (tableAcr: masterTableAcr, fieldName: masterNameFldName)));
					 
					 return (fnd);
					}

		  public IReferenceFiller		createFiller ()								{ return (this); }
		  public IReferenceFiller<Type>	createFiller<Type> () where Type: IObject	{ return (ReferenceFiller<Type>.create (this)); }

    	  public void	setJoinCond (IJoinCond[] cnd)		{ joinCond = cnd; }
    	  public void	setLookUpCond (IJoinCond[] cnd)		{ lookUpCond = cnd; }
    	  public void	setReferenceTable (ITable master)	{ masterTable = master; setUpSpecFieldsNames (); }

		  private class ReferenceFiller<Type> : IReferenceFiller<Type> where Type: IObject
		   {
		    private ReferenceFiller (IReferenceFiller prn)	{ parent = prn; }
			public static IReferenceFiller<Type>	create (IReferenceFiller parent)	{ return (new ReferenceFiller<Type> (parent)); }
			
			private IReferenceFiller	parent	{ get; set; } 

    	    public void	setJoinCond (IJoinCond[] cnd)		{ parent.setJoinCond (cnd); }
    	    public void	setLookUpCond (IJoinCond[] cnd)		{ parent.setLookUpCond (cnd); }
    	    public void	setReferenceTable (ITable master)	{ parent.setReferenceTable (master); }

			public void	setReferenceTable (ITable<Type> master)	{ parent.setReferenceTable (master); }
		   }
		 }
	   }
	  
	  private class TableCreator : ITableCreator
	   {
        private TableCreator (IImpFactory fact, string nm = null)	{ factory = fact; name = nm; m_Fields = new System.Collections.Generic.List<IImpField> (); }
		public static ITableCreator	create (IImpFactory factory, string name = null)	{ return (new TableCreator (factory, name)); }

		private IImpFactory	factory	{ get; set; }

		public string		name	{ get; set; }

		private System.Collections.Generic.List<IImpField>	m_Fields;

	    public void	addField (string nm, EFieldType type, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)	{ m_Fields.Add (FieldCreator.field (factory.culture.language, nm, type, isReadOnly, role)); }
		public void	addTextField (string nm, int maxSize, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)	{ m_Fields.Add (FieldCreator.text (factory.culture.language, nm, maxSize, isReadOnly, role)); }
		public void	addOptionField (string nm, IOptionValue[] values, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{ m_Fields.Add (FieldCreator.option (factory.culture.language, nm, values, isReadOnly, role)); }
	    
		public void	addPrimaryField (string nm, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_Autoincrement)	{ addField (nm, type, isReadOnly, role); }
	    public void	addBoolField (string nm, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)													{ addField (nm, EFieldType.FT_Bool, isReadOnly, role); }
	    public void	addBLOBField (string nm, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)													{ addField (nm, EFieldType.FT_BLOB, isReadOnly, role); }
	    
	    
		public void	addReferenceField (string nm, IReferenceDefinition refDel, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{
					 IImpField refField = FieldCreator.reference (factory.culture.language, nm, refDel, type, isReadOnly, role);
					 m_Fields.Add (refField);
					 refField.referenceField.defineReferenceFields (this, refField);
					}
	    
		public void	addReferenceField (string nm, ITable masterTable, string masterFldName, IJoinCond[] lookUpCond = null, EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false,
									 EFieldRole role = EFieldRole.FR_Normal)
					{
					 IJoinCond jn = ((masterFldName == null)? null: createJoinFldCond (fldName: nm, masterFldName: masterFldName));
					 addReferenceField (nm, FieldCreator.refDelegate (factory, masterTable, joinCond: new IJoinCond [1] { jn }, lookUpCond: lookUpCond), type, isReadOnly, role);
					}
	    
		public IReferenceFiller	addReferenceField (string nm, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null,
										         EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal)
					{
					 IImpRefDelegate del = FieldCreator.refDelegate (factory, masterTable, joinCond: joinCond, lookUpCond: lookUpCond);
					 addReferenceField (nm, del, type, isReadOnly, role);
					 return (del.createFiller ());
					}

	    public IReferenceFiller<Type>	addReferenceField<Type> (string nm, ITable masterTable = null, IJoinCond[] joinCond = null, IJoinCond[] lookUpCond = null,
												               EFieldType type = EFieldType.FT_BigInt, bool isReadOnly = false, EFieldRole role = EFieldRole.FR_Normal) where Type: IObject
					{
					 IImpRefDelegate del = FieldCreator.refDelegate (factory, masterTable, joinCond: joinCond, lookUpCond: lookUpCond);
					 addReferenceField (nm, del, type, isReadOnly, role);
					 return (del.createFiller<Type> ());
					}

		public IFieldFiller		addSpecField (string nm, EFieldType type, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_ReferenceInfo)
					{ IImpField fld = FieldCreator.field (factory.culture.language, nm, type, isReadOnly, role); m_Fields.Add (fld); return (fld); }

		public IFieldFiller		addSpecTextField (string nm, int maxSize, bool isReadOnly = true, EFieldRole role = EFieldRole.FR_ReferenceInfo)
					{ IImpField fld = FieldCreator.text (factory.culture.language, nm, maxSize, isReadOnly, role); m_Fields.Add (fld); return (fld); }

	    public IOptionValue	createOptionValue (short sh, string txt)	{ return (new OptionValue (sh, txt)); }
	    public IJoinCond	createJoinFldCond (string fldName, string masterFldName)		{ return (JoinCond.create (childFieldName: fldName, masterFldName: masterFldName)); }
	    public IJoinCond	createJoinValCond (string masterFldName, IValue val)			{ return (JoinCond.create (masterFldName, val)); }
	    public IJoinCond	createJoinValCond (string masterFldName, long val)				{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }
	    public IJoinCond	createJoinValCond (string masterFldName, int val)				{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }
	    public IJoinCond	createJoinValCond (string masterFldName, short val)				{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }
	    public IJoinCond	createJoinValCond (string masterFldName, bool val)				{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }
	    public IJoinCond	createJoinValCond (string masterFldName, System.DateTime val)	{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }
	    public IJoinCond	createJoinValCond (string masterFldName, string val)			{ return (JoinCond.create (masterFldName, Value.create (factory.culture.language, val))); }

	    public IQuery		createQuery ()
					{
					 IImpField[] fldArr = m_Fields.ToArray ();
					 IQuery q = TableImp.createQuery (factory, name, fldArr);
					 for (int i = 0; i < fldArr.Length; i++) fldArr [i].setQuery (q, i);
					 return (q);
					}
	    
		public IQuery<Type>	createQuery<Type> (IRecordLoader<Type> recLoader) where Type: IObject
					{
					 IImpField[] fldArr = m_Fields.ToArray ();
					 IQuery<Type> q = TableImp.createQuery<Type> (factory, name, fldArr, recLoader);
					 for (int i = 0; i < fldArr.Length; i++) fldArr [i].setQuery (q, i);
					 return (q);
					}
	    
		public ITable		createTable ()
					{
					 IImpField[] fldArr = m_Fields.ToArray ();
					 ITable t = TableImp.createTable (factory, name, fldArr);
					 for (int i = 0; i < fldArr.Length; i++) fldArr [i].setQuery (t, i);
					 return (t);
					}
	    
	    public ITable<Type>	createTable<Type> (ITypeConverter<Type> converter) where Type: IObject
					{
					 IImpField[] fldArr = m_Fields.ToArray ();
					 ITable<Type> t = TableImp.createTable<Type> (factory, name, fldArr, converter);
					 for (int i = 0; i < fldArr.Length; i++) fldArr [i].setQuery (t, i);
					 return (t);
					}

	    public ITable<Type, EField>	createTable<Type, EField> (ITypeConverter<Type> converter, DFieldName<EField> fldFce) where Type: IObject
					{
					 IImpField[] fldArr = m_Fields.ToArray ();
					 ITable<Type, EField> t = TableImp.createTable<Type, EField> (factory, name, fldArr, converter, fldFce);
					 for (int i = 0; i < fldArr.Length; i++) fldArr [i].setQuery (t, i);
					 return (t);
					}

		private class OptionValue : IOptionValue
		 {
		  public OptionValue (short vl, string tx)	{ val = vl; txt = tx; }
		  public short	val { get; private set; }
		  public string	txt	{ get; private set; }

		  public string	present (ILangTranslation trans)	{ return (txt); }
		 }

		private class JoinCond : IJoinCond, IJoinFieldCond, IJoinValueCond
		 {
		  private JoinCond (string chFldNm, string mstFldNm)	{ masterFldName = mstFldNm; type = EJoinCondType.JCT_Field; fieldName = chFldNm; }
		  private JoinCond (string mstFldNm, IValue vl)			{ masterFldName = mstFldNm; type = EJoinCondType.JCT_Value; val = vl; }

		  public static IJoinCond	create (string childFieldName, string masterFldName)	{ return (new JoinCond (chFldNm: childFieldName, mstFldNm: masterFldName)); }
		  public static IJoinCond	create (string masterFldName, IValue val)				{ return (new JoinCond (masterFldName, val)); }

		  public string			masterFldName	{ get; private set; }
		  public EJoinCondType	type			{ get; private set; }

		  public IJoinFieldCond	fldCnd	{ get { return ((type == EJoinCondType.JCT_Field)? this: null); } }
		  public IJoinValueCond	vlCnd	{ get { return ((type == EJoinCondType.JCT_Value)? this: null); } }

		  public string	fieldName	{ get; private set; }
		  public IValue val			{ get; private set; }
		 }
	   }
	 }

	private class FactoryImp : IImpFactory
	 {
	  private FactoryImp (SQL.IEngine sql, ILangTranslation lang, IDateTimeZone zone)	{ engine = sql; culture = new Culture (lang, zone); }

	  public static IFactory	create (SQL.IEngine sql, ILangTranslation lang, IDateTimeZone zone)	{ return (new FactoryImp (sql, lang, zone)); }

	  public SQL.IEngine	engine	{ get; private set; }
	  public ICulture		culture	{ get; private set; }

      public string	fullFieldName (string tableAcr, string fieldName)	{ return (engine.fullFieldName (tableAcr: tableAcr, fieldName: fieldName)); }

	  public ICondBuilder	condBuilder ()									{ return (CondBuilder.create (engine, culture)); }
	  public ISortByField	sortByField (string fldName, bool asc = true)	{ return (new SortByField (fldName, asc)); }
	  public IFinder		finder (string tableAcr)						{ return (Finder.create (this, tableAcr, CondBuilder.create (engine, culture))); }

	  public ITableCreator	tableCreator (string name = null)	{ return (TableDef.create (this, name)); }

	  public ITransaction	openTransaction ()	{ return (engine.openTransaction ()); }

	  private class Culture : ICulture
	   {
	    public Culture (ILangTranslation lang, IDateTimeZone zone)	{ language = lang; timeZone = zone; }

	    public ILangTranslation	language	{ get; private set; }
	    public IDateTimeZone	timeZone	{ get; private set; }
	   }

	  private class SortByField : ISortByField
	   {
	    public SortByField (string fldNm, bool ac = true)	{ fldName = fldNm; asc = ac; }

        public string	fldName	{ get; private set; }
        public bool		asc		{ get; private set; }
	   }
	 }
   }
 }
