﻿using System;
using System.Collections.Generic;
using Shark.Web.HttpProtocol;

namespace Shark.Web.Component.Creator
{
	/// <summary>
	/// Base class for all components
	/// </summary>
	internal abstract class Component : IComponent
	{
		#region protected fields

		/// <summary>
		/// Array of all subcomponents
		/// </summary>
		protected readonly List<IComponent> _subcomponents;

		/// <summary>
		/// Components own Control
		/// </summary>
		protected readonly IControl _control;
		#endregion


		#region constructors

		/// <summary>
		/// Default constructor which requires its own creator
		/// </summary>
		/// <param name="creator">Subcreator of component</param>
		/// <param name="control">Object of Control for rendering this component</param>
		protected Component(ICreator creator, IControl control)
		{
			_control = control;
			_subcomponents = new List<IComponent>();
			subCreator = creator;
		}
		#endregion


		#region Implementation of IParent

		/// <summary>
		/// Gets creater used for creating subcomponents
		/// </summary>
		public ICreator subCreator
		{
			get;
			private set;
		}
		#endregion


		#region Implementation of IComponent

		/// <summary>
		/// Called when there is need to init web data for this component and all its subcomponents, method fires event <see cref="onInitWebData"/>.
		/// </summary>
		public void initWebData()
		{
			OnInitWebDataInvoke(this);

			foreach (IComponent component in _subcomponents)
			{
				component.initWebData();
			}
		}

		/// <summary>
		/// Called when there is need to load web data for this component and all its subcomponents, method fires event <see cref="onLoadWebData"/>.
		/// </summary>
		public void loadWebData()
		{
			OnLoadWebDataInvoke(this);

			foreach (IComponent component in _subcomponents)
			{
				component.loadWebData();				
			}
		}

		/// <summary>
		/// Occurs when <see cref="initWebData"/> method is called
		/// </summary>
		public event DComponentInit onInitWebData;

		/// <summary>
		/// Occurs when <see cref="loadWebData"/> method is called on this component
		/// </summary>
		public event DComponentInit onLoadWebData;

		/// <summary>
		/// Abstract method for rendering HTML of this component to output (response), in overriden method generates HTML for component
		/// </summary>
		public abstract void generate();
		#endregion


		#region protected methods

		/// <summary>
		/// Method fires <see cref="onInitWebData"/> event
		/// </summary>
		/// <param name="component">Components which fires this event</param>
		protected void OnInitWebDataInvoke(IComponent component)
		{
			if(onInitWebData != null)
			{
				onInitWebData(component);
			}
		}

		/// <summary>
		/// Method fires <see cref="onLoadWebData"/> event
		/// </summary>
		/// <param name="component">Components which fires this event</param>
		protected void OnLoadWebDataInvoke(IComponent component)
		{
			if(onLoadWebData != null)
			{
				onLoadWebData(component);
			}
		}

		#endregion
	}
}